<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                        = 'auth_panel/Login';
$route['404_override']                              = '';
$route['translate_uri_dashes']                      = FALSE;

#Chalklit API Mapping
$route['my_testing']                                = "data_model/testing/Testing/my_testing";
$route['cl_search']                                 = "data_model/search/Trainings/cl_search";
$route['get_training_agencies']                     = "data_model/trainings/Trainings/get_training_agencies";
$route['training_list']                             = "data_model/trainings/Trainings/training_list";
$route['add_training']                              = "data_model/trainings/Trainings/add_training";
$route['edit_training']                             = "data_model/trainings/Trainings/edit_training";
$route['get_training_instances']                    = "data_model/trainings/Trainings/get_training_instances";
$route['get_training_modules']                      = "data_model/trainings/Trainings/get_training_modules";

$route['get_training_toggle_visibility']            = "data_model/trainings/Training_visibility/get_training_toggle_visibility";
$route['update_training_toggle_visibility']         = "data_model/trainings/Training_visibility/update_training_toggle_visibility";

$route['map_certificates_to_trainings']             = "data_model/certificates/Training_certificates/map_certificates_to_trainings";
$route['list_of_mapping_certificates_to_trainings'] = "data_model/certificates/Training_certificates/list_of_mapping_certificates_to_trainings";
$route['delete_certificates_mapping']               = "data_model/certificates/Training_certificates/delete_certificates_mapping";
$route['certificates_list']                         = "data_model/certificates/Training_certificates/certificates_list";
$route['certificate_approves_list']                 = "data_model/certificates/Training_certificates/certificate_approves_list";
$route['delete_certificate_approve']                = "data_model/certificates/Training_certificates/delete_certificate_approve";

$route['get_training_courses']                      = "data_model/courses/Training_courses/get_training_courses";

$route['training_n_courses_mapping']                = "data_model/courses/Course_mapping/training_n_courses_mapping";
$route['training_n_courses_mapping_list']           = "data_model/courses/Course_mapping/training_n_courses_mapping_list";
$route['delete_courses_training_mapping']           = "data_model/courses/Course_mapping/delete_courses_training_mapping";

$route['sender_id_list']                            = "data_model/sender_id/Sender_id/sender_id_list";
$route['add_sender_id']                             = "data_model/sender_id/Sender_id/add_sender_id";
$route['delete_sender_id']                          = "data_model/sender_id/Sender_id/delete_sender_id";
$route['edit_sender_id']                            = "data_model/sender_id/Sender_id/edit_sender_id";


$route['sender_id_mapping_list']                    = "data_model/sender_id/Sender_id_mapping/sender_id_mapping_list";
$route['map_sender_id']                             = "data_model/sender_id/Sender_id_mapping/map_sender_id";
$route['delete_sender_id_mapping']                  = "data_model/sender_id/Sender_id_mapping/delete_sender_id_mapping";

$route['users_list_by_role']                        = "data_model/users/Users/users_list_by_role";

$route['map_users_to_trainings']                    = "data_model/users/User_mapping/map_users_to_trainings";
$route['list_of_mapping_users_to_trainings']        = "data_model/users/User_mapping/list_of_mapping_users_to_trainings";
$route['delete_users_mapping']                      = "data_model/users/User_mapping/delete_users_mapping";

$route['add_rights']                                = "data_model/users/Manage_rights/add_rights";
$route['list_of_mapping_rights']                    = "data_model/users/Manage_rights/list_of_mapping_rights";
$route['delete_instance_rights_mapping']            = "data_model/users/Manage_rights/delete_instance_rights_mapping";

$route['get_channel_list']                          = "data_model/wall/Wall_channels/get_channel_list";
$route['master_channel_list']                       = "data_model/wall/Wall_channels/master_channel_list";
$route['edit_channel']                              = "data_model/wall/Wall_channels/edit_channel";
$route['add_channel']                               = "data_model/wall/Wall_channels/add_channel";
$route['delete_channels']                           = "data_model/wall/Wall_channels/delete_channels";

$route['get_wall_posting_users']                    = "data_model/wall/Wall_posting_users/get_wall_posting_users";
$route['add_wall_poster']                           = "data_model/wall/Wall_posting_users/add_wall_poster";
$route['delete_wall_posters']                       = "data_model/wall/Wall_posting_users/delete_wall_posters";

$route['get_report']                                = "data_model/report/Report/get_report";
