<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
//local server
//leave it blank for live server
define('FOR_INTERNAL',"Test_Folder/");

define("BANNER_IMAGES_S3", FOR_INTERNAL."banner_images");
define("DOC_FOLDER_S3", FOR_INTERNAL."doc_folder");
define("FANWALL_IMAGES_S3", FOR_INTERNAL."fanwall_images");
define("PROFILE_IMAGES_S3", FOR_INTERNAL."profile_images");
define("VIDEO_THUMB_IMAGES_S3", FOR_INTERNAL."video_thumbnails");

//define("AMS_BUCKET_NAME","dams-apps-production");
//define("AMS_S3_KEY","AKIAJOX5DIUSM6P6AIYA");
//define("AMS_SECRET","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");
*/
//payUMoney 
//defined('PAYUMONEY__MERCHANT_KEY')      OR define('PAYUMONEY__MERCHANT_KEY', 'R5kh9BiH'); // highest automatically-assigned error code
//defined('PAYUMONEY__SALT')      OR define('PAYUMONEY__SALT', 'xI2rtFaz00'); // highest automatically-assigned error code

$baseurl = "http://localhost/project_folder/chalklit_local/";

defined('PROJECT_NAME')      OR define('PROJECT_NAME', 'CHALKLIT'); 
defined('API_RESPONSE_STATUS_TRUE')      OR define('API_RESPONSE_STATUS_TRUE', 'success'); 
defined('API_RESPONSE_STATUS_FALSE')     OR define('API_RESPONSE_STATUS_FALSE', 'fail');  

defined('TARGET_PATH_TO_UPLOAD_CHANNEL_LOGO')   OR define('TARGET_PATH_TO_UPLOAD_CHANNEL_LOGO', getcwd()."/resources/wall/logo/");
defined('TARGET_PATH_TO_UPLOAD_TRA_LOGO')   OR define('TARGET_PATH_TO_UPLOAD_TRA_LOGO', getcwd()."/resources/tra/logo/");
defined('TARGET_PATH_TO_UPLOAD_TRA_SPLASH')   OR define('TARGET_PATH_TO_UPLOAD_TRA_SPLASH', getcwd()."/resources/tra/splash/");
 
defined('TARGET_PATH_TO_DISPLAY_CHANNEL_LOGO')	OR define('TARGET_PATH_TO_DISPLAY_CHANNEL_LOGO', $baseurl."resources/wall/logo/");
defined('TARGET_PATH_TO_DISPLAY_TRA_LOGO')	OR define('TARGET_PATH_TO_DISPLAY_TRA_LOGO', $baseurl.'resources/tra/logo/');
defined('TARGET_PATH_TO_DISPLAY_TRA_SPLASH')	OR define('TARGET_PATH_TO_DISPLAY_TRA_SPLASH', $baseurl.'resources/tra/splash/');

defined('GENERIC_INTERFACE_JSON_QUERY_PATH')	OR define('GENERIC_INTERFACE_JSON_QUERY_PATH', $baseurl.'resources/generic_interface/generic_interface_query_builder');
defined('ELASTICSEARCH_ERROR_MESSAGE') OR define('ELASTICSEARCH_ERROR_MESSAGE', 'Search feature is currently unavailable, please try after sometime.');

defined('PORTAL_VERSION')	OR define('PORTAL_VERSION', 'version 2.2');

defined('API_BASE_URL')	OR define('API_BASE_URL', $baseurl.'index.php/data_model/Api/index');
defined('API_URL_JAVA')	OR define('API_URL_JAVA', $baseurl.'http://www.chalklit.in/chalklitone/MobileServlet');

 

 

