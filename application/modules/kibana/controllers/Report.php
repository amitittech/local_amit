<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('aul');
        modules::run('kibana/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->helper('custom');
    }

    

    public function activity_report() {
        $view_data['page'] = "activity_report";
        $data['page_title'] = "Activity Report";
        $data['page_data'] = $this->load->view('kibana/reports/activity_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    
    public function training_report() {
        $view_data['page'] = "training_report";
        $data['page_title'] = "Training Report";
        $data['page_data'] = $this->load->view('kibana/reports/training_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    

}
