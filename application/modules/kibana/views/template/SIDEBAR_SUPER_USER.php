<?php


if(isset($page) && !empty($page)){
    $dashboard=$kibana_reports=$kibana_reports=$activity_report=$training_report="";
    $active_pages = ['dashboard','activity_report','training_report'];
    foreach($active_pages as $each){
        if ($page == $each) {
            ${$each} = 'active';
            if(in_array($page,['activity_report,training_report'])){
                $kibana_reports = 'active';
            }
        }else{
            ${$each} = '';
        }
    }
}


$sidebar_url    = array('web_user/all_user_list');
$sidebar_url[]  = 'admin/index';
//$sidebar_url[] = 'admin/create_backend_user';
$sidebar_url[]  = 'Training/traing_agencies';

if (is_array($sidebar_url)) {
    $sidebar_url = implode("','", $sidebar_url);
    $sidebar_url = "'" . $sidebar_url . "'";
}
$session_userdata = $this->session->userdata();

?>
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a class="<?php echo $dashboard; ?>" href="<?php echo AUTH_PANEL_URL . 'user/index'; ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:;" class="dcjq-parent <?php echo $kibana_reports; ?>">
                    <i class="fa fa-calculator"></i>
                    <span>Kibana Reports</span>
                    <span class="dcjq-icon"></span>
                </a>
                <ul class="sub" style="display: block;">
                    <li class="<?php echo $activity_report; ?>">
                        <a href="<?php echo AUTH_PANEL_URL . 'Report/activity_report'; ?>">Activity Report </a>
                    </li>
                    <li class="<?php echo $training_report; ?>">
                        <a href="<?php echo AUTH_PANEL_URL . 'Report/training_report'; ?>">Training Report</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</aside>