<?php
	$starttime 					= time();
	
	include('elasticsearch_helper.php');
	include('php_custom_helper.php');	
	include('database_conn.php');
	$config 		= array(
						'hostname' => '49.50.125.152',
						'username' => 'esuser',
						'password' => 'EsUser@342#msf',
						'database' => 'chalklit_analysis',
					);	
	$conn 			= database_connection($config);
	
	$index_name		= "user_activity_profile";
	$where 			= "where 1=1";
	$date_from 		= date('Y-m-d')." 00:00:00";
	$date_to 		= date('Y-m-d')." 23:59:59";
	$where 			= $where." and generated_on between '".$date_from."' and '".$date_to."'";
	#echo $where. PHP_EOL . date('Y-m-d H:i:s'); die;
	$activity_query 			= "select activityprofile.*,DATE(activitydate) AS activity_date,DATE(generated_on) AS generated_on, userprofile.languagepref as user_language_pref,userprofile.gender as user_gender,userprofile.district as user_district,userprofile.state as user_state,userprofile.trninvitedfor as user_trn_invited_for,userprofile.trncompleted as user_trn_completed,userprofile.trncompletionpercentage as user_trn_completion_percentage,userprofile.createddate as user_created_date,userprofile.trn_list_invited_for as user_trn_list_invited_for,userprofile.trn_list_completed as user_trn_list_completed,userprofile.ti_list as user_ti_list,userprofile.school as user_school,userprofile.list_class_subject as user_list_class_subject,userprofile.list_field_of_study as user_list_field_of_study,userprofile.list_degree as user_list_degree,userprofile.list_tra as user_list_tra,userprofile.tra_count as user_tra_count,userprofile.audience_set as user_audience_set,contentprofile.contenttype as content_type,contentprofile.channelname as content_channel_name,contentprofile.source as content_source,contentprofile.contentcategory as content_category,contentprofile.classname as content_class,contentprofile.subject as content_subject,contentprofile.audienceset as content_audience_set,contentprofile.containerid as content_container_id FROM activityprofile left outer join userprofile on userprofile.userid=activityprofile.userid left outer join contentprofile on contentprofile.contentid=activityprofile.contentid and contentprofile.appsection=activityprofile.appsection ".$where." and activityprofile.userid=";	
	
	$limit 			= 100;
	$offset 		= 0;
	$records 		= 0;
	$error 			= "";
	$active_users_query 				= "SELECT distinct userid FROM activityprofile ".$where.""; 
	$active_users_result 				= mysqli_query($conn,$active_users_query);
	while($active_user 					= mysqli_fetch_assoc($active_users_result)){ 
		$sql_user_activities 			= "SELECT COUNT(*) AS total FROM activityprofile ".$where." and userid=".$active_user['userid']; 
		$result_user_activities 		= mysqli_query($conn,$sql_user_activities); 
		$user_activities 				= mysqli_fetch_assoc($result_user_activities);	
		$loop_end 						= ceil($user_activities['total']/$limit);  
		$finalquery 					= $activity_query."'".$active_user['userid']."' order by activityid asc "; #die('working');
		$logdata 						= populate_user_activities($finalquery,$limit,$offset,$index_name,$loop_end);
		$records 						= $records+$user_activities['total'];
	}
	$logdata = json_encode(array("Time"=>date('d-m-Y H:i:s',$starttime),"Records"=>$records,"time_taken_in_seconds"=>date('s',time()-$starttime),"error"=>$error),true);
	file_put_contents('/home/centos/php-script/logs/cronjob_user_activities.txt', $logdata . PHP_EOL, FILE_APPEND);
?>
