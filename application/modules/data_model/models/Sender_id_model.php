<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sender_id_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
    public function sender_id_list($data) { 
        if(isset($data['length']) && isset($data['start'])){
            $this->db->limit($data['length'],$data['start']);
        }
        $this->db->where('isactive','1');
        $this->db->order_by('senderrefid','desc');
        $result = $this->db->get('clt_mat_sms_sender')->result_array();

        foreach ($result as $key => $value) {

            $this->db->where('senderid', $value['senderrefid']);
            $checkmapping = $this->db->get('clt_tra_sms_sender')->result_array();
            if(!empty($checkmapping)){
               $result[$key]['ismapped'] = true;
            }else{
               $result[$key]['ismapped'] = false;
            }
        }
        return $result;
    }
	
    public function get_sender_id($data){
        if(isset($data['senderrefid']) && !empty($data['senderrefid'])){			
                $this->db->where('senderrefid', $data['senderrefid']);
        }
        if(isset($data['sendercode']) && !empty($data['sendercode'])){			
                $this->db->where('sendercode', $data['sendercode']);
        } 
        $result = $this->db->get('clt_mat_sms_sender')->row_array();
        return $result;
    }
	
    public function add_sender_id($data){
        $input['sendercode'] = $data['sendercode'];
        $input['createdby'] = $data['login_id'];
        $input['isactive'] = TRUE;
        $input['createddate'] 	= date('Y-m-d H:i:s');

        $this->db->insert('clt_mat_sms_sender',$input);
        return   array("sender_id" => $this->db->insert_id(),"message"=>"true");
    }
        
    public function get_sender_id_by_id($data){
        if(isset($data['senderrefid']) && !empty($data['senderrefid'])){            
            $this->db->where('senderrefid', $data['senderrefid']);
        }
        
        $result = $this->db->get('clt_mat_sms_sender')->row_array();
        return $result;
    }


    public function edit_sender_id($data){
		$this->db->where('senderrefid', $data['senderrefid']);
		$result = $this->db->update('clt_mat_sms_sender',array("sendercode"=>$data['sendercode']));
		return true;
	}
	
	
    public function delete_sender_ids($data) {
        if(is_array($data['senderrefid'])) {
            foreach ($data['senderrefid'] as $key => $value) {
                $this->db->where('senderrefid', $value);
                $this->db->update('clt_mat_sms_sender',array('isdeleted'=>TRUE,'isactive'=>FALSE));
            }
            return true;
        }else{
            $this->db->where('senderrefid', $data['senderrefid']);
            return $result = $this->db->update('clt_mat_sms_sender',array('isdeleted'=>TRUE,'isactive'=>FALSE));
        }
    }

}
