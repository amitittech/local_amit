<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role_mgmt_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
/*
    public function user_roles($data) { //pre($data);
        $this->db->select("tmrefid,clt_tra_tm.rlmrefid,rlmrole,curtra.trarefid,curtra.trarefid,tralogo,traname,IF(tralogo is null, '', concat('logo-path/',tralogo)) as tralogo,clt_trn.trnrefid,clt_trn.trnname,currcm.rcmname,clt_trn.startdate,clt_trn.enddate,regstartdate,regenddate");#group_concat(curtra.traname) tras 
        if(isset($data['tmrefid']) && !empty($data['tmrefid'])){
            $this->db->where('tmrefid', $data['tmrefid']);
        }        
        if(isset($data['rlmrole']) && !empty($data['rlmrole'])){
            $this->db->where('rlmrole', $data['rlmrole']);
        }        
        $this->db->join('escrlm', 'escrlm.rlmrefid = clt_tra_tm.rlmrefid');
        $this->db->join('curtra', 'curtra.trarefid = clt_tra_tm.trarefid');
        
        $this->db->join('clt_trn','clt_trn.trntrarefid=curtra.trarefid');
        $this->db->join('currcm','currcm.rcmrefid=clt_trn.trnrcmrefid');
        #$this->db->group_by('escrlm.rlmrefid');
        $users = $this->db->get('clt_tra_tm')->result_array();
        return $users;
    }
    */
    
    public function tra_ti_list($data){
        //IF(tralogo is null, '', concat('logo-path/',tralogo)) as tralogo
        $query = "select trnrefid,trnname,curtra.trarefid,curtra.traname,tralogo, "
                . "tmrefid,escrlm.rlmrefid,clt_trn.startdate,clt_trn.enddate,regstartdate,regenddate,rcmname,chaptername "
                . "from escrlm "
                . "join clt_tra_tm ON clt_tra_tm.rlmrefid=escrlm.rlmrefid "
                . "join curtra ON curtra.trarefid=clt_tra_tm.trarefid "
                . "join clt_trn ON clt_trn.trntrarefid =curtra.trarefid "
                . "join currcm ON currcm.rcmrefid =clt_trn.trnrcmrefid "
                . "where tmrefid =".$data['tmrefid']." and escrlm.rlmrole = '".$data['rlmrole']."' "
                . "AND ( (CURDATE() BETWEEN clt_trn.`startdate` AND clt_trn.`enddate`) OR (CURDATE() BETWEEN clt_trn.`regstartdate` AND clt_trn.`regenddate`) ) ";
        $tis = $this->db->query($query)->result_array();        
        return $tis;
    }
    
    public function ti_details($data){
        $where = "1=1";
        if(isset($data['trnrefids']) && !empty($data['trnrefids'])){
            $where = $where." AND ti.trnrefid IN (".$data['trnrefids'].")";
        }
        if(isset($data['rlmrole']) && !empty($data['rlmrole'])){
            $where = $where." AND rlmrole='".$data['rlmrole']."'";
        }
        
        //COUNT(currcu.rcurefid) AS trainees,datediff(ti.enddate,ti.startdate) as total_training_days ,datediff(now(),ti.startdate) as day_of_training
        $query =    "SELECT ti.`startdate`,ti.`enddate`,ti.regstartdate,ti.regenddate, ti.trnrefid as trnrefid, ti.`trnname`, curtra.trarefid,curtra.traname ,
                tmrefid,clt_tra_tm.rlmrefid,rlmrole,tralogo,currcm.rcmname,chaptername,
                SUM(CASE WHEN  currcu.completionpercentage = 0  THEN 1 ELSE 0 END)  AS 'ns',
                SUM(CASE WHEN  currcu.completionpercentage > 0 AND currcu.completionpercentage < 50  THEN 1 ELSE 0 END)  AS 'low',
                SUM(CASE WHEN  currcu.completionpercentage >= 50 AND currcu.completionpercentage < 75  THEN 1 ELSE 0 END)  AS 'medium',
                SUM(CASE WHEN  currcu.completionpercentage >= 75 AND currcu.completionpercentage < 100  THEN 1 ELSE 0 END)  AS 'high',
                SUM(CASE WHEN  currcu.completionpercentage = 100   THEN 1 ELSE 0 END)  AS 'complete',
                case
                WHEN ((CURDATE() between ti.startdate and ti.enddate) && (CURDATE() between ti.regstartdate and ti.regenddate )) THEN  3
                  WHEN CURDATE() between ti.startdate and ti.enddate THEN  2
                  WHEN CURDATE() between ti.regstartdate and ti.regenddate THEN 1
                  ELSE 0 
                END type,count(distinct currcu.rcurefid) as ttl_reg,80 as expected
                FROM `currcu`
                join curtrb batch on (currcu.rcutrbrefid = batch.trbrefid)
                JOIN clt_trn ti on (batch.trbtrnrefid = ti.trnrefid)
                JOIN curtra on(ti.trntrarefid = curtra.trarefid)
                JOIN clt_tra_tm on(clt_tra_tm.trarefid = curtra.trarefid)
                JOIN escrlm on(escrlm.rlmrefid = clt_tra_tm.rlmrefid)
                JOIN currcm on(currcm.rcmrefid=ti.trnrcmrefid)
                WHERE ".$where." AND ti.isvisibleonclient=1 AND currcu.rcurole = 'TEACHER'  
                  AND ( (CURDATE()  BETWEEN ti.`startdate` AND ti.`enddate`) OR (CURDATE()  BETWEEN ti.`regstartdate` AND ti.`regenddate`) )
                GROUP BY ti.trnrefid";
                        #echo $query;
        $tis = $this->db->query($query)->result_array();
        return $tis;
    }
    
    public function user_roles($data) { //pre($data);
        $where = "1=1";
        if(isset($data['tirefids']) && !empty($data['tirefids'])){
            $where = $where." AND ti.trnrefid IN (".$data['tirefids'].")";
        }
        if(isset($data['rlmrole']) && !empty($data['rlmrole'])){
            $where = $where." AND rlmrole='".$data['rlmrole']."'";
        }
        
        $query =    "SELECT ti.`startdate`,ti.`enddate`,ti.regstartdate,ti.regenddate, ti.trnrefid as trnrefid, ti.`trnname`, curtra.trarefid,curtra.traname ,COUNT(currcu.rcurefid) AS trainees,datediff(ti.enddate,ti.startdate) as total_training_days ,datediff(now(),ti.startdate) as day_of_training,
tmrefid,clt_tra_tm.rlmrefid,rlmrole,IF(tralogo is null, '', concat('logo-path/',tralogo)) as tralogo,currcm.rcmname,chaptername,
SUM(CASE WHEN  currcu.completionpercentage = 0  THEN 1 ELSE 0 END)  AS 'ns',
SUM(CASE WHEN  currcu.completionpercentage > 0 AND currcu.completionpercentage < 50  THEN 1 ELSE 0 END)  AS 'low',
SUM(CASE WHEN  currcu.completionpercentage >= 50 AND currcu.completionpercentage < 75  THEN 1 ELSE 0 END)  AS 'medium',
SUM(CASE WHEN  currcu.completionpercentage >= 75 AND currcu.completionpercentage < 100  THEN 1 ELSE 0 END)  AS 'high',
SUM(CASE WHEN  currcu.completionpercentage = 100   THEN 1 ELSE 0 END)  AS 'complete',
case
WHEN ((CURDATE() between ti.startdate and ti.enddate) && (CURDATE() between ti.regstartdate and ti.regenddate )) THEN  3
  WHEN CURDATE() between ti.startdate and ti.enddate THEN  2
  WHEN CURDATE() between ti.regstartdate and ti.regenddate THEN 1
  ELSE 0 
END type

FROM `currcu`
join curtrb batch on (currcu.rcutrbrefid = batch.trbrefid)
JOIN clt_trn ti on (batch.trbtrnrefid = ti.trnrefid)
JOIN curtra on(ti.trntrarefid = curtra.trarefid)
JOIN clt_tra_tm on(clt_tra_tm.trarefid = curtra.trarefid)
JOIN escrlm on(escrlm.rlmrefid = clt_tra_tm.rlmrefid)
JOIN currcm on(currcm.rcmrefid=ti.trnrcmrefid)
WHERE ".$where." AND ti.isvisibleonclient=1 AND currcu.rcurole = 'TEACHER'  
   AND ( (CURDATE()  BETWEEN ti.`startdate` AND ti.`enddate`) OR (CURDATE()  BETWEEN ti.`regstartdate` AND ti.`regenddate`) ) 
GROUP BY ti.trnrefid";
        #echo $query;
        $tis = $this->db->query($query)->result_array();
        #pre($tis);die;
        return $tis;
      
  #AND ti.trnrefid IN (36,37,45,46,47,48,49,50,51,105,56)
 # AND ( (CURDATE()  BETWEEN ti.`startdate` AND ti.`enddate`) OR (CURDATE()  BETWEEN ti.`regstartdate` AND ti.`regenddate`) )   
   
    }
    
}
