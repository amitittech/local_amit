<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Training_instances_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_training_instances($data) { //pre($data);
        if (isset($data['trnrefid']) && !empty($data['trnrefid'])) {
            $this->db->where('clt_trn.trnrefid',$data['trnrefid']);
        }
        if (isset($data['trntrarefid']) && !empty($data['trntrarefid'])) {
            $this->db->where('clt_trn.trntrarefid',$data['trntrarefid']);
        }
        $this->db->where('clt_trn.isdeleted', 0);
        $this->db->where('clt_trn.isactive', 1);
        $trn = $this->db->get('clt_trn')->result_array();
        return $trn;
    }
}
