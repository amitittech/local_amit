<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Training_instances_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
    public function get_role($data){
        if(isset($data['role']) && !empty($data['role'])){
                $this->db->where('rlmrole', $data['role']);
        }
        $role = $this->db->get('escrlm')->row_array();
        return $role;
    }
	
    public function get_training_instance_batches($data){
        $this->db->where('trbtrnrefid', $data['trnrefid']);
        $batches = $this->db->get('curtrb')->result_array();
        return $batches;
    }

    /*
    public function get_training_instances4($data) { //pre($data);die;
        if (isset($data['trnrefid']) && !empty($data['trnrefid'])) {
            $this->db->where('clt_trn.trnrefid',$data['trnrefid']);
        }
        if (isset($data['trntrarefid']) && !empty($data['trntrarefid'])) {
            $this->db->where('clt_trn.trntrarefid',$data['trntrarefid']);
        }
        $this->db->where('clt_trn.isdeleted', 0);
        $this->db->where('clt_trn.isactive', 1);
        $trn = $this->db->get('clt_trn')->result_array();
        return $trn;
    }
    */
	
    public function get_training_instances($data) {
        if(isset($data['for_manage_rights']) && !empty($data['for_manage_rights'])){
            $this->db->select("GROUP_CONCAT(clttirefid) as tirefids");
            $this->db->where('clt_ti_tm.isdeleted', 0);
            $this->db->where('clt_ti_tm.clttmrefid', $data['for_manage_rights']);
            $already = 	$this->db->get('clt_ti_tm')->row_array();

            $this->db->select("GROUP_CONCAT(trarefid) as trarefids");
            $this->db->where('clt_tra_tm.isdeleted', 0);
            $this->db->where('clt_tra_tm.tmrefid',$data['for_manage_rights']);
            $my_tra = 	$this->db->get('clt_tra_tm')->row_array();

            if(!empty($already['tirefids'])){
                    $this->db->WHERE_NOT_IN('clt_trn.trnrefid',explode(',',$already['tirefids']));
            }
            if(!empty($my_tra['trarefids'])){
                    $this->db->WHERE_IN('clt_trn.trntrarefid',explode(',',$my_tra['trarefids']));
            }
            $this->db->where('clt_trn.createdby !=',$data['for_manage_rights']);
        }	
        
        $this->db->select('clt_trn.trnrefid,clt_trn.trnname,clt_trn.startdate,clt_trn.enddate,regstartdate,regenddate');
        if(isset($data['tirefids']) && !empty($data['tirefids'])){
            $this->db->WHERE_IN('clt_trn.trnrefid',explode(',',$data['tirefids']));
        }
        if (isset($data['trnrefid']) && !empty($data['trnrefid'])) {
            $this->db->where('clt_trn.trnrefid',$data['trnrefid']);
        }
        if (isset($data['trntrarefid']) && !empty($data['trntrarefid'])) {
            $this->db->where('clt_trn.trntrarefid',$data['trntrarefid']);
        }
        $this->db->where('clt_trn.isdeleted', 0);
        $this->db->where('clt_trn.isactive', 1);
        $this->db->join('curtra','curtra.trarefid=clt_trn.trntrarefid');
        #$this->db->join('currcm','currcm.rcmrefid=clt_trn.trnrcmrefid');
        
        $trn = $this->db->get('clt_trn')->result_array();
        #echo $this->db->last_query();
        return $trn;
    }
    
    
    public function ti_info($data) { 
        $tdy = '2018-04-10';
        $where = "('".$tdy."' between startdate and enddate) OR ('".$tdy."' between regstartdate and regenddate )";
        $this->db->select('clt_trn.trnrefid,clt_trn.trnname,clt_trn.startdate,clt_trn.enddate,regstartdate,regenddate');
        if(isset($data['tirefids']) && !empty($data['tirefids'])){
            $this->db->WHERE_IN('clt_trn.trnrefid',explode(',',$data['tirefids']));
        }
        
        $this->db->where('clt_trn.isdeleted', 0);
        $this->db->where($where);
        $this->db->where('clt_trn.isactive', 1);
        $this->db->join('curtra','curtra.trarefid=clt_trn.trntrarefid');
        #$this->db->join('currcm','currcm.rcmrefid=clt_trn.trnrcmrefid');
        
        $trn = $this->db->get('clt_trn')->result_array();
        #echo $this->db->last_query();
        return $trn;
    }
	
    public function add_rights($data){
        $this->db->where('clttirefid', $data['trnrefid']);
        $this->db->where('clttmrefid', $data['usmrefid']);
        $already = $this->db->get('clt_ti_tm')->row_array();
        if (empty($already)) {
            $inserting                  = array();
            $inserting['clttirefid']   = $data['trnrefid'];
            $inserting['clttmrefid']   = $data['usmrefid'];
            $inserting['createddate']   =$inserting['modifieddate']   = date('Y-m-d H:i:s');
            $inserting['createdby']     =$inserting['modifiedby']     = $data['login_id'];
            $inserting['isdeleted']     = 0;
            $inserting['isactive']      = 1;
            $this->db->insert('clt_ti_tm', $inserting);
            return 1;
        }else if($already['isdeleted'] == 1){
            $this->db->where("clttitmrefid",$already['clttitmrefid']);
            $this->db->update('clt_ti_tm', array("isdeleted"=>0,"isactive"=>1,"modifiedby"=>$data['login_id'],"modifieddate"=>date('Y-m-d H:i:s')));
            return 2;
        }else{
            return 3;
        }
        return false;        
    }
	
    public function list_of_mapping_rights($data) { //pre($data);die;
        $this->db->select('clttitmrefid,usmrefid,usmfirstname,usmlastname,usmmob,RIGHT(usmmob,10) as usm_mob,trnname,clt_ti_tm.createddate,clt_ti_tm.modifieddate');
        if(isset($data['tmrefid']) && !empty($data['tmrefid'])){
                $this->db->where('clt_ti_tm.clttmrefid', $data['tmrefid']);
        }
        if(isset($data['not_owned_tmrefid']) && !empty($data['not_owned_tmrefid'])){
            $this->db->where('clt_trn.createdby !=', $data['not_owned_tmrefid']);
        }
        $this->db->where('clt_ti_tm.isdeleted', 0);
        $this->db->where('clt_ti_tm.isactive', 1);
        $this->db->join('escusm','escusm.usmrefid=clt_ti_tm.clttmrefid');
        $this->db->join('clt_trn','clt_trn.trnrefid=clt_ti_tm.clttirefid');
        $this->db->order_by('clt_ti_tm.modifieddate','desc');
        $trn = $this->db->get('clt_ti_tm')->result_array();
        return $trn;
    }
	
    public function delete_instance_rights_mapping($data) {//pre($data);
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach ($data['mapping_ids'] as $m_id) {//echo $m_id;
            $this->db->where('clttitmrefid', $m_id);
            $exist = $this->db->get('clt_ti_tm')->row();
            if ($exist) {
                $this->db->where('clttitmrefid', $m_id);
                $this->db->update('clt_ti_tm', array('isdeleted' =>1, 'isactive' =>0, 'modifieddate' => date('Y-m-d H:i:s'), 'modifiedby' => $data['login_id']));
            }
        }
        // echo $this->db->last_query();
        return true;
    }
	
    public function certificate_approves($data) {  
        $role        = $this->get_role(array("role"=>$data['role'])); 
        if(empty($role)){
            return_data(false, 'Role could not match.', array());
        }        
        $trn        = $this->get_training_instances(array("trnrefid"=>$data['trnrefid'])); //pre($trn); die;
        if(empty($trn)){
            return_data(false, 'Training instace not found.', array());
        }
        $response   = array();
        $i          = 0;
        $batches = $this->get_training_instance_batches($data);
        if(empty($batches)){
            return_data(false, 'No batches available for this training instance.', array());
        }
        if($batches){ //pre($batches);
            foreach($batches as $batch){
                foreach ($data['user_mobiles'] as $user_mobile) { 
                    $i++;
                    if (empty($batch)) {
                        $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => "n/a", "mobile" => $user_mobile, "status" => "fail", "message" => "Batch not exist");
                        continue;
                    }
                    
                    $this->db->where('ulmusername', $user_mobile);
                    $user = $this->db->get('esculm')->row_array(); //echo $this->db->last_query()."<br>";  pre($user);
                    if (empty($user)) {
                        $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => $batch['batchname'], "mobile" => $user_mobile, "status" => "fail", "message" => "Invalid mobile number");
                        continue;
                    }
                    
                    $this->db->where('urmusmrefid', $user['ulmusmrefid']);
                    $this->db->where('urmrlmrefid', $role['rlmrefid']);
                    $already = $this->db->get('escurm')->row_array();
                    if (empty($already)) {
                        $inserting                  = array();
                        $inserting['urmusmrefid']   = $user['ulmusmrefid'];
                        $inserting['urmrlmrefid']   = $role['rlmrefid'];
                        $inserting['createddate']   = date('Y-m-d H:i:s');
                        $inserting['isdeleted']     = 0;
                        $inserting['isactive']      = 1;
                        $this->db->insert('escurm', $inserting);
                    }else if($already['isdeleted'] == 1){
                        $this->db->where("urmrefid",$already['urmrefid']);
                        $this->db->update('escurm', array("isdeleted"=>0,"isactive"=>1,"modifieddate"=>date('Y-m-d H:i:s')));
                    }
                    
                    
                    //$this->db->select("rlmrefid");
                    $this->db->where('trmtrbrefid', $batch['trbrefid']);
                    $this->db->where('trmusmrefid', $user['ulmusmrefid']);
                    $exist = $this->db->get('curtrm')->row_array();
                    if($exist){ 
                        if($exist['isdeleted']==0){
                            $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => $batch['batchname'], "mobile" => $user_mobile, "status" => "fail", "message" => "Record already exist");
                            continue;
                        }else{ 
                            $updating['isdeleted']      = 0;
                            $updating['isactive']       = 1;
                            $updating['modifiedby']     = $data['login_id'];
                            $updating['modifieddate']   = date('Y-m-d H:i:s');
                            $this->db->where('trmrefid', $exist['trmrefid']);
                            $this->db->update('curtrm', $updating);
                        }
                    }else{
						$inserting 					= array();
                        $inserting['trmtrbrefid']   = $batch['trbrefid'];
                        $inserting['trmusmrefid']   = $user['ulmusmrefid'];
                        $inserting['isdeleted']     = 0;
                        $inserting['isactive']       = 1;
                        $inserting['createdby']     =$inserting['modifiedby']     = $data['login_id'];
                        $inserting['createddate']   = $inserting['modifieddate']   = date('Y-m-d H:i:s');
                        $this->db->insert('curtrm', $inserting);
                    }
                    $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => $batch['batchname'], "mobile" => $user_mobile, "status" => "success", "message" => "Done Successfully");
                    
                }
            }
        }
        return $response;
    }
	
    public function certificate_approves_list($data){
        $this->db->select('curtrm.trmrefid,curtra.traname,clt_trn.trnname,curtrb.batchname,escusm.usmfirstname,escusm.usmlastname,escusm.usmmob,curtrm.createddate,curtrm.modifieddate,curtrm.trmrefid as mapping_id');
        $this->db->join('escusm','escusm.usmrefid=curtrm.trmusmrefid');
        $this->db->join('curtrb','curtrb.trbrefid=curtrm.trmtrbrefid');
        $this->db->join('clt_trn','clt_trn.trnrefid=curtrb.trbtrnrefid');
        $this->db->join('curtra','curtra.trarefid=clt_trn.trntrarefid');
        $this->db->where('curtrm.isdeleted', 0);
        $this->db->where('curtrm.isactive', 1);
        $this->db->order_by('curtrm.modifieddate','desc');
        $result = $this->db->get('curtrm')->result_array(); //pre($result);
        return $result;
    }
	
    public function delete_certificate_approve($input) { 
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_model->delete_certificate_approve($input);
        if ($result) {
            return array("status" => true, "message" => 'Data Removed successfully.', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
    public function ti_list($data) { //pre($data);die;
        $this->db->select("clt_trn.trnname,startdate,enddate,curtra.traname");
        if (isset($data['trarefid']) && !empty($data['trarefid'])) {
            $this->db->where('curtra.trarefid',$data['trarefid']);
        }
        if (isset($data['traname']) && !empty($data['traname'])) {
            $this->db->where('curtra.traname',$data['traname']);
        }
        if (isset($data['trnrefid']) && !empty($data['trnrefid'])) {
            $this->db->where('clt_trn.trnrefid',$data['trnrefid']);
        }        
        if (isset($data['trnname']) && !empty($data['trnname'])) {
            $this->db->where('clt_trn.trnname',$data['trnname']);
        }
        $this->db->where('clt_trn.isdeleted', 0);
        $this->db->where('clt_trn.isactive', 1);
        $this->db->join('curtra','curtra.trarefid=clt_trn.trntrarefid');
        $trn = $this->db->get('clt_trn')->result_array();
        return $trn;
    }
}
