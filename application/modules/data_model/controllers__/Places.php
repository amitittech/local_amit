<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Places extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Places_model');
        $this->load->helper("services");
        $this->load->library('session');
    }

    public function get_fav_places() { //user_id
        $this->validate_get_fav_places();
        $result = $this->Places_model->get_fav_places($this->input->post());
        if ($result) {
            return_data(true, 'Success.', $result);
        }  
        return_data(false, 'No record found.', array());
    }

    private function validate_get_fav_places() {
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }
    
     public function add_fav_place() { //user_id
        $this->validate_add_fav_place();
        if($this->input->post('action') == 1){
            $yes = $this->Places_model->check_already_fav_place($this->input->post());
            if($yes){
                return_data(false, 'Already exist in your favorite list.', array());
            }
            $result = $this->Places_model->add_fav_place($this->input->post());
            $message = "Added to your favorite list.";
        }else if($this->input->post('action') == 0){
            $result = $this->Places_model->un_fav_place($this->input->post());
             $message = "Removed from your favorite list.";
        } 
        if($result) {
            return_data(true, $message, $this->input->post());
        }  
        return_data(false, 'No record found.', $this->input->post());
    }

    private function validate_add_fav_place() {
        post_check();
        $this->form_validation->set_rules('action', 'action', 'trim|required');
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
        $this->form_validation->set_rules('place', 'place', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }


   

}
