<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Elasticsearch\ClientBuilder;
require APPPATH. 'third_party/elasticsearch/vendor/autoload.php';

		
class Elasticsearch extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    
    function __construct() {
        parent::__construct();
        //$this->load->model('Training_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }	
	
	
	
	public function get_settings(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}	
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->indices()->getSettings($input); //$client->index($input);
		echo json_encode($response);
	}
	
	public function get_mapping(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);
		if(!isset($input['index']) OR empty($input['index'])){
			//return_data("fail", "Please pass index.", array());
		}	
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->indices()->getMapping($input); //$client->index($input);
		echo json_encode($response);
	}
	
	public function testing(){
		$arr = [
			"appversion"=>"1.2",
			"opcode"=>"elasticsearch",
			"auth_token"=>"22f9814d-yfyfy",
			"searching_keyword"=>"hello",
			"searching_options"=>array("cl_wall"=>1,"cl_resources"=>0,"cl_trainings"=>1)
		];
		pre($arr);
		$wall 		= [["col1"=>"val1","col2"=>"val2"],["col1"=>"val1","col2"=>"val2"]];
		$resources 	= [["col1"=>"val1","col2"=>"val2"],["col1"=>"val1","col2"=>"val2"]];
		$trainings 	= [["col1"=>"val1","col2"=>"val2"],["col1"=>"val1","col2"=>"val2"]];
		
		$post['wall'] 		= $wall;
		$post['trainings'] 	= $trainings;
		$post['resources'] 	= $resources;
		
		$response['status'] 	= "success";
		$response['message'] 	= "Data displyed successfully.";
		$response['Result'] 	= $post;
		pre($response);
		echo json_encode($response);
		
	}
	
	public function sink_wall(){
		$tt = time();
		$this->db->select('chprefid,chptopic,chpcontent,chmname,chmlogo,chpqutrefid');
		$this->db->where('escchp.isdeleted',0);
		//$this->db->where('escchm.isdeleted',0);
		$this->db->join('escchm','escchm.chmrefid=escchp.chpchmrefid');		
		$result = $this->db->get('escchp')->result_array();
		$i =1;
		foreach($result as $each){	$i=$i+1;
			$body = array( 
				"post_id"				=>$each['chprefid'],
				"post_title"			=>$each['chptopic'],
				"post_description"		=>$each['chpcontent'],
				"channel_name"			=>$each['chmname'],
		"channel_logo"			=>(!empty($each['chmlogo']))?"http://www.qa.chalklit.in/chalklitone/resources/newhome/images/".$each['chmlogo']:"",
				"audiencesetid"			=>$each['chpqutrefid']
			);
			$params['index'] 	= 'cl_wall';
			$params['type'] 	= 'post';
			$params['id'] 		= $each['chprefid'];
			$params['body'] 	= $body;
			$client 			= ClientBuilder::create()->build();
			$response 			= $client->index($params);
			//pre($response);
		}
		$response = array("time_taken_in_seconds"=>time()-$tt,"total records"=>$i);
		echo json_encode($response);
		//{"time_taken_in_seconds":8,"total records":982}
    }
	
	
	
	public function sink_resources(){
		
		/*
		case
  when type = 'video' then concat('http:',thumbnailurl)
  else ifnull(thumbnailurl,'')    
end `postlogo`
		
		*/
		$tt = time();
		$this->db->select("rchrefid,title,description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,isfortraining,rcgrefid,groupname,case
  when type = 'video' then concat('http:',thumbnailurl)
  else ifnull(thumbnailurl,'')    
end postlogo");
		$this->db->where('currcg.isdeleted',0);
		$this->db->where('currch.isdeleted',0);
		$this->db->where('currcm.isfortraining',0);
		$this->db->where('currcm.isforreference',0); 
		$this->db->where('currcm.islive',1);
		$this->db->join('currcg','currcg.rcgrcmrefid=currcm.rcmrefid');
		$this->db->join('currch','currch.rchrcgrefid=currcg.rcgrefid');
		$result = $this->db->get('currcm')->result_array();
		//pre($result);die;
		$i =1;
		foreach($result as $each){				
			$i=$i+1;
			$params['index'] 	= 'cl_resources';
			$params['type'] 	= 'post';
			$params['id'] 		= $each['rchrefid'];
			$params['body']		= [
				"post_id"				=>(isset($each['rchrefid']) && !empty($each['rchrefid']))?$each['rchrefid']:"",
				"post_title"			=>(isset($each['title']) && !empty($each['title']))?$each['title']:"",
				"post_description"		=>(isset($each['description']) && !empty($each['description']))?$each['description']:"",				
				"chapter_id"			=>(isset($each['rcmrefid']) && !empty($each['rcmrefid']))?$each['rcmrefid']:"",
				"chapter_name"			=>(isset($each['chaptername']) && !empty($each['chaptername']))?$each['chaptername']:"",
				"class"					=>(isset($each['clmname']) && !empty($each['clmname']))?$each['clmname']:"",
				"subject"				=>(isset($each['sbmname']) && !empty($each['sbmname']))?$each['sbmname']:"",
				"language"				=>(isset($each['rcmlngcode']) && !empty($each['rcmlngcode']))?$each['rcmlngcode']:"",
				"module_id"				=>(isset($each['rcgrefid']) && !empty($each['rcgrefid']))?$each['rcgrefid']:"",
				"module_title"			=>(isset($each['groupname']) && !empty($each['groupname']))?$each['groupname']:"",
				"chapter_logo"			=>"",
				"module_logo"			=>"",
				"post_logo"				=>(isset($each['groupname']) && !empty($each['groupname']))?$each['groupname']:""
			];
			$client 			= ClientBuilder::create()->build();
			$response 			= $client->index($params);			
		}
		$response = array("time_taken_in_seconds"=>time()-$tt,"total records"=>$i);
		echo json_encode($response);
		//{"time_taken_in_seconds":28,"total records":3818}
    }
	
	public function sink_trainings(){
		$tt = time();
		$this->db->select("rchrefid,title,description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,rcgrefid,groupname,case
  when type = 'video' then concat('http:',thumbnailurl) else ifnull(thumbnailurl,'') end postlogo");
		$this->db->where('currcg.isdeleted',0);
		$this->db->where('currch.isdeleted',0);
		$this->db->where('currcm.isfortraining',1);
		$this->db->join('currcg','currcg.rcgrcmrefid=currcm.rcmrefid');
		$this->db->join('currch','currch.rchrcgrefid=currcg.rcgrefid');
		$result = $this->db->get('currcm')->result_array();
		$i =1;
		foreach($result as $each){	
			$q = "SELECT rcurcmrefid,CONCAT('[',GROUP_concat(distinct rcutrbrefid),']') as trbrefids from currcu where rcurcmrefid =".$each['rcmrefid']." and isactive = 1 group by rcurcmrefid order by rcurcmrefid";
			$res = $this->db->query($q)->row_array();
			if(!$res){
				continue;
			}
			$i=$i+1;
			$params['index'] 	= 'cl_trainings';
			$params['type'] 	= 'post';
			$params['id'] 		= $each['rchrefid'];
			$params['body']		= [
				"post_id"				=>(isset($each['rchrefid']) && !empty($each['rchrefid']))?$each['rchrefid']:"",
				"post_title"			=>(isset($each['title']) && !empty($each['title']))?$each['title']:"",
				"post_description"		=>(isset($each['description']) && !empty($each['description']))?$each['description']:"",				
				"chapter_id"			=>(isset($each['rcmrefid']) && !empty($each['rcmrefid']))?$each['rcmrefid']:"",
				"chapter_name"			=>(isset($each['chaptername']) && !empty($each['chaptername']))?$each['chaptername']:"",
				"class"					=>(isset($each['clmname']) && !empty($each['clmname']))?$each['clmname']:"",
				"subject"				=>(isset($each['sbmname']) && !empty($each['sbmname']))?$each['sbmname']:"",
				"language"				=>(isset($each['rcmlngcode']) && !empty($each['rcmlngcode']))?$each['rcmlngcode']:"",
				"module_id"				=>(isset($each['rcgrefid']) && !empty($each['rcgrefid']))?$each['rcgrefid']:"",
				"module_title"			=>(isset($each['groupname']) && !empty($each['groupname']))?$each['groupname']:"",
				"trbrefids"				=>(isset($res['trbrefids']) && !empty($res['trbrefids']))?$res['trbrefids']:"",
				"chapter_logo"			=>"",
				"module_logo"			=>"",
				"post_logo"				=>""
			];
			$client 			= ClientBuilder::create()->build();
			$response 			= $client->index($params);
		}
		//echo json_encode($response);
		$response = array("time_taken_in_seconds"=>time()-$tt,"total records"=>$i);
		echo json_encode($response);
		//{"time_taken_in_seconds":8,"total records":883}
    }
	
	
	
	public function bulk(){
		foreach($result as $each){
			$params['body'][] = [
				'index' => [
					'_index' => 'cl_resourses',
					'_type' => 'post',
				]
			];
			$params['body'][] = [
				'my_field' => 'my_value',
				'second_field' => 'some more values'
			];			
		}
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->bulk($params);	
		echo json_encode($response);
		
    }
		
    public function add_document(){
		$params 			= $this->validate_add_document();
		
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->index($params);
		echo json_encode($response);
    }	
	
	public function validate_add_document(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);		
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}
		if(!isset($input['type']) OR empty($input['type'])){
			return_data("type", "Please pass type.", array());
		}
		if(!isset($input['body']) OR empty($input['body'])){
			return_data("fail", "Please pass body parameters.", array());
		}	
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];		
		$params['body'] 	= $input['body'];
		if(isset($input['id']) && !empty($input['id'])){
			$params['id'] 		= $input['id'];
		}
		return $params;
	}
	
	public function get_document_by_id(){
		$params 			= $this->validate_get_document_by_id();
		
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->get($params);
		
		echo json_encode($response);
    }	
	
	public function validate_get_document_by_id(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);		
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}
		if(!isset($input['type']) OR empty($input['type'])){
			return_data("type", "Please pass type.", array());
		}
		if(!isset($input['id']) OR empty($input['id'])){
			return_data("fail", "Please pass id.", array());
		}	
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		$params['id'] 	= $input['id'];
		return $params;
	}
	
	public function delete_document_by_id(){
		$params 			= $this->validate_delete_document_by_id();
		
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->delete($params);
		
		echo json_encode($response);
    }	
	
	public function validate_delete_document_by_id(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);		
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}
		if(!isset($input['type']) OR empty($input['type'])){
			return_data("type", "Please pass type.", array());
		}
		if(!isset($input['id']) OR empty($input['id'])){
			return_data("fail", "Please pass id.", array());
		}	
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		$params['id'] 	= $input['id'];
		return $params;
	}
	
	public function search_documents_by_a_column(){
		$params 			= $this->validate_search_documents_by_a_column();
		//$params['body']['from'] = 0;
		//$params['body']['size'] = 3;
		$params['body']['query']['match_phrase'][$params['searching_col']] = $params['searching_val'];
		$params['body']['highlight']['fields']['content'] = '{}';
		unset($params['searching_col'],$params['searching_val']);
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->search($params);
		
		echo json_encode($response);
    }	
	
	public function validate_search_documents_by_a_column(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);		
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}
		if(!isset($input['type']) OR empty($input['type'])){
			return_data("fail", "Please pass type.", array());
		}
		if(!isset($input['searching_col']) OR empty($input['searching_col'])){
			return_data("fail", "Please pass searching_col.", array());
		}
		if(!isset($input['searching_val']) OR empty($input['searching_val'])){
			return_data("fail", "Please pass searching_val.", array());
		}
		return $input;
	}
	
	public function search_documents_with_and_operator(){
		$params 			= $this->validate_search_documents_with_and_operator();
				
		$client 			= ClientBuilder::create()->build();	
		$response 			= $client->search($params);
		
		echo json_encode($response);
    }	
	
	public function validate_search_documents_with_and_operator(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);		
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}
		if(!isset($input['type']) OR empty($input['type'])){
			return_data("fail", "Please pass type.", array());
		}
		if(!isset($input['multi_search']) OR empty($input['multi_search'])){
			return_data("fail", "Please pass multi_search.", array());
		}
		if(!is_array($input['multi_search'])){
			return_data("fail", "Please pass multi_search as an array.", array());
		}
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		foreach($input['multi_search'] as $each){
			$params['body']['query']['bool']['must'][]['match'][$each['searching_col']] = $each['searching_val'];
		}
		return $params;
	}
	
	public function create_index_with_mappings(){
		$params 			= $this->validate_create_index();
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->indices()->delete($params);
		echo json_encode($response);
    }

	public function create_index(){
		$params 			= $this->validate_create_index();
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->indices()->delete($params);
		echo json_encode($response);
    }	
	
	public function validate_create_index(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);		
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}		
		$params['index'] 	= $input['index'];
		return $params;
	}
	
	
	public function delete_index(){
		$params 			= $this->validate_delete_index();
		$client 			= ClientBuilder::create()->build();
		$response 			= $client->indices()->delete($params);
		echo json_encode($response);
    }	
	
	public function validate_delete_index(){
		$json 				= file_get_contents('php://input');
		$input 				= json_decode($json,true);		
		if(!isset($input['index']) OR empty($input['index'])){
			return_data("fail", "Please pass index.", array());
		}
		$params['index'] 	= $input['index'];
		return $params;
	}
	
	
	// ELASTICSEARCH METHODS ///////////////////////////////////////////////////////////
	
	function elastic_active_records($input,$action){
		$client 			= ClientBuilder::create()->build();
		return $response = $client->index($input);
	}
	
	function elastic_add_document($input){
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		$params['body'] 	= $input['body'];
		if(isset($input['id'])){
		$params['id'] 		= $input['id'];
		}
		$client 			= ClientBuilder::create()->build();
		return $response = $client->index($input);
	}
	
	function elastic_get_document_by_id($input){		
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		$params['id'] 		= $input['id'];
		$client 			= ClientBuilder::create()->build();
		return $response = $client->get($params);
	}
	
	function elastic_delete_document_by_id($input){		
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		$params['id'] 		= $input['id'];
		$client 			= ClientBuilder::create()->build();
		return $response = $client->delete($params);
	}
	
	function elastic_search_documents_by_a_column($input){		
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];		
		$params['body']['from'] 	= 0;		
		$params['body']['size'] 	= 2;		
		$params['body']['query']['match'][$input['searching_col']] = $input['searching_val'];
		
		$client 			= ClientBuilder::create()->build();
		return $response = $client->search($params);
	}
	
	function elastic_search_documents_with_and_operator($input){	//pre($input); die;	
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		foreach($input['multi_search'] as $each){
			$params['body']['query']['bool']['must'][]['match'][$each['searching_col']] = $each['searching_val'];
		}		
		$client 			= ClientBuilder::create()->build();	
		return $response = $client->search($params);
	}
	
    function elastic_delete_index($input){		
		//$params['index'] 	= $input['index'];
		$client 			= ClientBuilder::create()->build();
		return $response = $client->indices()->delete($input);
	}
	
}


