<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Elasticsearch\ClientBuilder;

require APPPATH . 'third_party/elasticsearch/vendor/autoload.php';

class Cl_search extends MX_Controller {
	
    function __construct() {
        parent::__construct();
        $this->load->helper('custom');
		$this->load->model('Log_records_model');
    }

    function cl_search_params($input){ 
		if (!isset($input['searching_options']['cl_wall'])
				OR!isset($input['searching_options']['cl_resources'])
				OR!isset($input['searching_options']['cl_trainings'])
				OR (empty($input['searching_options']['cl_wall']) 
						AND empty($input['searching_options']['cl_resources']) 
						AND empty($input['searching_options']['cl_trainings']))
			){
			return_data(false,'Please select atleast one searching option.',array());
		}
		
		$params['match_type'] 	= (isset($input['match_type']) && !empty($input['match_type'])) ? $input['match_type'] : 'match_phrase';
        $pre_tag 		= (isset($input['pre_tag']) && !empty($input['pre_tag']) && isset($input['post_tag']) && !empty($input['post_tag'])) ? $input['pre_tag'] : '<em>';
        $post_tag 	= (isset($input['pre_tag']) && !empty($input['pre_tag']) && isset($input['post_tag']) && !empty($input['post_tag'])) ? $input['post_tag'] : '</em>';
        $params['highlight']  	= [
            'pre_tags' 	=> [$pre_tag], 	// optional
            'post_tags' => [$post_tag],	// optional
            'fields' 	=> [
                "post_title" 		=> new \stdClass(),
                "post_description" 	=> new \stdClass()
            ],
            'require_field_match' 	=> true
        ];
		#$default_values['no_search_found']	= $no_search_found = "No posts found with searched text." . PHP_EOL . "Please note that this is a Beta version of Search feature, which currently allows searching for post text only. Make sure, you are searching for content inside posts to get the best results.";
		$default_values['no_search_found']	= $no_search_found = "No posts found matching your search text.";
        $default_values['final_response'] = [
                ["section" => "wall", "total" 		=> 0, "message" => $no_search_found, "result" => array()],
                ["section" => "resources", "total" 	=> 0, "message" => $no_search_found, "result" => array()],
                ["section" => "trainings", "total" 	=> 0, "message" => $no_search_found, "result" => array()]
        ];	
		$key = (isset($input['plateform']))?$input['plateform']:"";		
		$plateform_array = array("EPMOB1.0"=>"MOBILE","IOS-CHALKLIT"=>"MOBILE-IOS");
		if (array_key_exists($key,$plateform_array)){
			$default_values['platform'] =  $plateform_array[$key];
		}else{
			$default_values['platform'] = "WEB";
		}		
		return array("params"=>$params,"default_values"=>$default_values);
	}
	
	function cl_search_wall_params($cl_search_params,$input){//pre($cl_search_params);
		$params 						= array();
		$match_type  					= $cl_search_params['params']['match_type'];
		$params['body']['highlight'] 	= $cl_search_params['params']['highlight'];
		//$params['body']['sort'] 		= ["post_id" => [ "order" => "asc"]];
		$params['index'] 				= 'cl_wall';
		$params['type'] 				= 'post';
		$params['body']['from'] 		= (isset($input['offset']) && !empty($input['offset'])) ? $input['offset'] : 0;
		$params['body']['size'] 		= (isset($input['size']) && !empty($input['size'])) ? $input['size'] : 3;
		//$params['body']['query']['bool']['must'][$match_type][$searching_column]    = $input['searching_keyword']; 

		$query 	= "SELECT GROUP_CONCAT( distinct `qutrefid`) as audiencesetid FROM `clt_aud_usm` WHERE `usmrefid`=" . $input['login_id'];
		$res 	= $this->db->query($query)->row_array();
		$audiencesetids 														= explode(',', $res['audiencesetid']);
		//$params['body']['query']['bool']['filter']['terms']['audiencesetid'] 	= $audiencesetids;		
		
		//$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_title'] 		= $input['searching_keyword'];
		//$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_description'] = $input['searching_keyword'];
		
		//$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_title'] 		= array("query"=>$input['searching_keyword'],"fuzziness"=>"2","analyzer"=>"stopwords","operator" => "and","zero_terms_query"=> "all");
		//$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_description'] 	= array("query"=>$input['searching_keyword'],"fuzziness"=>"2","analyzer"=> "stopwords","operator" => "and","zero_terms_query"=> "all");
		
		$params['body']['query']['bool']['must']['multi_match']	=[
													'query' 	=> $input['searching_keyword'],
													'fields' 	=> ["post_title","post_description"],
													"operator"  => "or",
													"fuzziness"	=> "AUTO",
													"analyzer"	=> "english_stop",
													"minimum_should_match"=> '60%'													
												];
		$params['body']['query']['bool']['filter']['terms']['audiencesetid'] 	= $audiencesetids;
		
		#echo json_encode($params);
		#pre($params);		
		//$params['body']['query']['filter']['terms']['audiencesetid'] 	= $audiencesetids;		
		
		return $params;
	}
	
	function cl_search_resourses_params($cl_search_params,$input){
		$params 						= array();
		$match_type  					= $cl_search_params['params']['match_type'];
		$params['body']['highlight'] 	= $cl_search_params['params']['highlight'];
		//$params['body']['sort'] 		= ["post_id" => [ "order" => "asc"]];
		$params['index'] 				= 'cl_resources';
		$params['type'] 				= 'post';
		$params['body']['from'] 		= (isset($input['offset']) && !empty($input['offset'])) ? $input['offset'] : 0;
		$params['body']['size'] 		= (isset($input['size']) && !empty($input['size'])) ? $input['size'] : 3;
		//$params['body']['query']['bool']['must'][$match_type][$searching_column]    = $input['searching_keyword']; 

		#$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_title'] 		= $input['searching_keyword'];
		#$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_description'] 	= $input['searching_keyword'];
		
		$params['body']['query']['multi_match']	=[
													'query' 	=> $input['searching_keyword'],
													'fields' 	=> ["post_title","post_description"],
													"operator"  => "or",
													"fuzziness"	=> "AUTO",
													"analyzer"	=> "english_stop",
													"minimum_should_match"=> '50%'													
												];		
		return $params;
	}
	
	function cl_search_trainings_params($cl_search_params,$input){
		$params 						= array();
		$match_type  					= $cl_search_params['params']['match_type'];
		$params['body']['highlight'] 	= $cl_search_params['params']['highlight'];
		//$params['body']['sort'] 		= ["post_id" => [ "order" => "asc"]];
		$params['index'] 				= 'cl_trainings';
		$params['type'] 				= 'post';
		$params['body']['from'] 		= 0;	#(isset($input['offset']) && !empty($input['offset'])) ? $input['offset'] : 0;
		$params['body']['size'] 		= 10000;	#(isset($input['size']) && !empty($input['size'])) ? $input['size'] : 3;
		//$params['body']['query']['bool']['must'][$match_type][$searching_column]    = $input['searching_keyword']; 

		#$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_title'] 		= $input['searching_keyword'];
		#$params['body']['query']['bool']['must']['bool']['should'][][$match_type]['post_description'] 	= $input['searching_keyword'];
		
		$params['body']['query']['multi_match']	=[
													'query' 	=> $input['searching_keyword'],
													'fields' 	=> ["post_title","post_description"],
													"operator"  => "or",
													"fuzziness"	=> "AUTO",
													"analyzer"	=> "english_stop",
													"minimum_should_match"=> '50%'													
												];	
		return $params;
	}
	
	
	
	function prepare_response($response) {
		//$wall_posts 				= $response['hits']['hits'];
		$my_response['total'] 		= $response['hits']['total'];
		$my_response['message'] 	= "Searching records displayed successfully.";
		$my_response['result'] 		= $response['hits']['hits'];
		//$wall_response 			= array("section"=>"resources","total"=>$response['hits']['total'],"result"=>$response['hits']['hits']);
		return $my_response;
	}
	
	function prepare_logs_data_wall($input,$wall_posts,$platform) {
		$search_log['usmrefid'] 		= $input['login_id'];
		$search_log['searchphrase'] 	= $input['searching_keyword'];
		$search_log['searchsection'] 	= 'cl_wall';
		$search_log['searchresultcount']= $wall_posts;
		$search_log['platform'] 		= $platform;
		$search_log['searchedon'] 		= date('Y-m-d H:i:s');
		return $search_log;
	}
	
	function prepare_logs_data_resources($input,$resources_posts,$platform) {
		$search_log['usmrefid'] 		= $input['login_id'];
		$search_log['searchphrase'] 	= $input['searching_keyword'];
		$search_log['searchsection'] 	= 'cl_resources';
		$search_log['searchresultcount']= $resources_posts;
		$search_log['platform'] 		= $platform;
		$search_log['searchedon'] 		= date('Y-m-d H:i:s');
		return $search_log;
    }
	
	function prepare_logs_data_trainings($input,$trainings_posts,$platform) {
		$search_log['usmrefid'] 		= $input['login_id'];
		$search_log['searchphrase'] 	= $input['searching_keyword'];
		$search_log['searchsection'] 	= 'cl_resources';
		$search_log['searchresultcount']= $trainings_posts;
		$search_log['platform'] 		= $platform;
		$search_log['searchedon'] 		= date('Y-m-d H:i:s');
		return $search_log;
    }				
				
    //----------GET LIST OF Documents by searching a keyword----------- //    
    public function cl_search($input) {
		$wall_posts 		= $trainings_posts_count = $resources_posts = 0;
		$client 			= ClientBuilder::create()->build(); //pre($client); die;
        $cl_search_params 	= $this->cl_search_params($input);
        $final_response		= $cl_search_params['default_values']['final_response'];		
		#wall search 		
        if (isset($input['searching_options']['cl_wall']) && !empty($input['searching_options']['cl_wall'])) {
            $params 				= $this->cl_search_wall_params($cl_search_params,$input);            
            try {
                $response 			= $client->search($params); #pre($response);die;
            } catch (Exception $e) {
				#pre($e->getMessage());die;
				$error =  json_decode($e->getMessage(),true)['error']['reason'];//die;
                $error =  $e->getMessage();//die;
                //return array("status" => false, "message" => ELASTICSEARCH_ERROR_MESSAGE, "result" => array());
                return array("status"=>false,"message"=>"Exception : ".$error,"result"=>array());
            } 
			$wall_posts 					= $response['hits']['total'];			
            if (isset($response['hits']['total']) && !empty($response['hits']['total'])) {
				
				$final_response[0] 				=   $this->prepare_response($response);
				$final_response[0]['section']	= "wall";
            }			
            if (isset($input['offset']) && $input['offset'] == 0) {  
				$search_log = $this->prepare_logs_data_wall($input,$wall_posts,$cl_search_params['default_values']['platform']);		
                $this->create_logs_for_search_api($search_log);
            }
        }
		
		#resources search 		
        if (isset($input['searching_options']['cl_resources']) && !empty($input['searching_options']['cl_resources'])) {
			$params 				= $this->cl_search_resourses_params($cl_search_params,$input);												
            try {
                $response 			= $client->search($params);
            } catch (Exception $e) {
                return array("status" => false, "message" => ELASTICSEARCH_ERROR_MESSAGE, "result" => array());
                //return array("status"=>false,"message"=>"Exception : ".$e->getMessage(),"result"=>array());
            }
			$resources_posts 					= $response['hits']['total'];			
            if (isset($response['hits']['total']) && !empty($response['hits']['total'])) {
				
				$final_response[1] 				=   $this->prepare_response($response);
				$final_response[1]['section']	= "resources";
            }			
            if (isset($input['offset']) && $input['offset'] == 0) {  
				$search_log = $this->prepare_logs_data_resources($input,$resources_posts,$cl_search_params['default_values']['platform']);		
                $this->create_logs_for_search_api($search_log);
            }
        }
		
		# trainings search
        if (isset($input['searching_options']['cl_trainings']) && !empty($input['searching_options']['cl_trainings'])) {
			$params 				= $this->cl_search_trainings_params($cl_search_params,$input);             
            try {
                $response = $client->search($params);
            } catch (Exception $e) {
                return array("status" => false, "message" => ELASTICSEARCH_ERROR_MESSAGE, "result" => array());
                //return array("status"=>false,"message"=>"Exception : ".$e->getMessage(),"result"=>array());
            }
            //pre($params);            pre($response);
            $hits = [];
            if (isset($response['hits']['total']) && !empty($response['hits']['total'])) {
                $query = "SELECT GROUP_CONCAT(distinct `rcutrbrefid`) as batchids FROM `currcu` WHERE `isactive`=1 and `rcuusmrefid`=" . $input['login_id'];
                $res = $this->db->query($query)->row_array();
                //pre($input['login_id'] = 388);//pre($res);
                $batchids = explode(',', $res['batchids']);

                foreach ($response['hits']['hits'] as $hit) {
                    $intersect = array_intersect($batchids, $hit['_source']['trbrefids']);
                    if (!empty($intersect)) {
                        $int_values = array_map('intval', array_values(array_intersect($batchids, $hit['_source']['trbrefids'])));
                        $hit['_source']['trbrefids_users'] = implode('|', $batchids);
                        $hit['_source']['trbrefids_docs'] = implode('|', $hit['_source']['trbrefids']);
                        $hit['_source']['trbrefids_intersact'] = implode('|', $int_values);
                        $hit['_source']['trbrefids'] = $int_values;
                        $hit['_source']['login_id'] = $input['login_id'];
                        $hits[] = $hit;
                    }
                }

                $from = (isset($input['offset']) && !empty($input['offset'])) ? $input['offset'] : 0;
                $size = (isset($input['size']) && !empty($input['size'])) ? $input['size'] : 3;
                $trainings_posts = array_slice($hits, $from, $size);
                $final_response[2]['total'] = count($hits); //$response['hits']['total'];
                $final_response[2]['message'] = "Searching records displayed successfully.";
                $final_response[2]['result'] = $trainings_posts;
				$final_response[2]['section'] = "trainings";
				
				$trainings_posts_count = count($trainings_posts);
            }
            
			if (isset($input['offset']) && $input['offset'] == 0) {  
				$search_log = $this->prepare_logs_data_trainings($input,$trainings_posts_count,$cl_search_params['default_values']['platform']);	
                $this->create_logs_for_search_api($search_log);
            }
            unset($params['body']['query']['bool']['filter']);
        }
        if (empty($wall_posts) && empty($resources_posts) && empty($trainings_posts_count)) {
            return array("status" => false, "message" => $cl_search_params['default_values']['no_search_found'], "result" => array());
        }
        return array("status" => true, "message" => 'Search result displayed successfully.', "result" => $final_response);
    }
	
	//------- Logs for search api ---------------------------------------------------//    
    public function create_logs_for_search_api($log_data) {
        $result = $this->Log_records_model->create_logs_for_search_api($log_data);
        //$result                     = $this->create_elastic_logs_for_search_api($log_data);
        return true;
    }

}
