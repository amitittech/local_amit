<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class wall_channels extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Wall_model');
        $this->load->helper('custom');
    }
	
	 public function get_channel_list($input) {
        //$this->validate_sender_id_list();
        $result = $this->Wall_model->get_channel_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function master_channel_list($input) {//pre($input);
        //$this->validate_sender_id_list();
        $result = $this->Wall_model->master_channel_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
	public function add_channel($input) {
        //pre($input); pre($_FILES);die;
        $this->validate_add_channel($input);
        $result = $this->Wall_model->add_channel($input); //pre($result);
        if ($result) {
            return array("status" => true, "message" => "Channel creation has been done as following", "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_add_channel($inputs) { //pre($inputs);
        if (!isset($inputs['chmname']) OR empty($inputs['chmname'])) {
            return_data(false, 'chmname is required in array format.', array());
        }
        if (!isset($inputs['chmdesc']) OR empty($inputs['chmdesc'])) {
            return_data(false, 'chmdesc is required in array format.', array());
        }
        // if (!isset($inputs['chmusmrefids']) OR empty($inputs['chmusmrefids'])) {
        //     return_data(false, 'chmusmrefids is required in array format.', array());
        // }
    }

    public function delete_channels($input) {
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['channel_ids']) OR empty($input['channel_ids'])) {
            return array("status" => false, "message" => 'channel_ids is required.', "result" => array());
        }
        $result = $this->Wall_model->delete_channels($input);
        if ($result) {
            return array("status" => true, "message" => 'Selected Channels deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
}
