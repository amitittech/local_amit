<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role_mgmt extends MX_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Role_mgmt_model');
        $this->load->helper('custom');
    }

    public function users_list_by_role($input) {
        $result = $this->User_model->users_list_by_role($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function cl_fetch_master_data($inputs) {
        if (!isset($inputs['fetch_what']) OR empty($inputs['fetch_what'])) {
            return_data(false, 'Fetch what field is required.', array());
        }
        $result = $this->User_model->get_states($input);
        if ($result) {
            return array("status" => true, "message" => 'States fetched successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
    }
    
    /*
    public function user_roles($input) {        
        $result         = $this->Role_mgmt_model->user_roles($input);
        if ($result) { 
            $final = [];
            $tdy = date('Y-m-d');
            foreach($result as $res){       
                $ti = array("trnrefid"      => $res['trnrefid'],
                            "trnname"       => $res['trnname'],
                            "rcmname"       =>$res['rcmname'],
                            "startdate"     =>$res['startdate'],
                            "enddate"       =>$res['enddate'],
                            "regstartdate"  =>$res['regstartdate'],
                            "regenddate"    =>$res['regenddate'],
                            "type"          =>$res['type'],
                            "ns"            =>$res['ns'],
                            "low"           =>$res['low'],
                            "medium"        =>$res['medium'],
                            "high"          =>$res['high'],
                            "complete"      =>$res['complete'],);
                
                if(!in_array($res['trarefid'],$traids)){
                    $traids[]           = $res['trarefid'];
                    
                    $final[$res['trarefid']] = array("trarefid"=>$res['trarefid'],
                                                    "traname"=>$res['traname'],
                                                    "tralogo"=>$res['tralogo']);
                    
                }
                #if( ($res['startdate'] >= $tdy && $res['enddate'] >= $tdy) OR ($res['regstartdate'] >= $tdy && $res['regenddate'] >= $tdy) ){
                    
                    $final[$res['trarefid']]['ti'][] = $ti;
                #}
            }
            $final = array_values($final);
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $final);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    */
    
    /*public function user_roles($input) {    // pre($input);   die;
        $result         = $this->Role_mgmt_model->tra_ti_list($input);
        if ($result) { 
            foreach ($result as $each){
                $ti_ids[] = $each['trnrefid'];
            }
            $result1         = $this->Role_mgmt_model->ti_details(["trnrefids"=>implode(',',$ti_ids) ]);            
            if ($result1) { 
                $final = [];
                $tdy = date('Y-m-d');
                foreach($result1 as $res){       
                    $ti = array("trnrefid"      =>$res['trnrefid'],
                                "trnname"       =>$res['trnname'],
                                "rcmname"       =>$res['rcmname'],
                                "chaptername"   =>$res['chaptername'],
                                "startdate"     =>$res['startdate'],
                                "enddate"       =>$res['enddate'],
                                "regstartdate"  =>$res['regstartdate'],
                                "regenddate"    =>$res['regenddate'],
                                "type"          =>$res['type'],                                
                                "ttl_reg"       =>$res['ttl_reg'],
                                "expected"      =>$res['expected'],
                                "ns"            =>$res['ns'],
                                "low"           =>$res['low'],
                                "medium"        =>$res['medium'],
                                "high"          =>$res['high'],
                                "complete"      =>$res['complete']);
                    
                    if(!in_array($res['trarefid'],$traids)){
                        $traids[]           = $res['trarefid'];

                        $final[$res['trarefid']] = array("trarefid"=>$res['trarefid'],
                                                        "traname"=>$res['traname'],
                                                        "tralogo"=>$res['tralogo']);
                    }
                    #if( ($res['startdate'] >= $tdy && $res['enddate'] >= $tdy) OR ($res['regstartdate'] >= $tdy && $res['regenddate'] >= $tdy) ){
                        $final[$res['trarefid']]['ti'][] = $ti;
                    #}
                }
                $final = array_values($final);
                return array("status" => true, "message" => 'Data displyed successfully.', "result" => $final);
            }
            #return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result1);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    */
    
    public function user_roles($input) {    //pre($input);   die; 
        $sp = "call Get_TI_TRA_FOR_TM(".$input['tmrefid'].",'".$input['rlmrole']."')";
        $result = $this->db->query($sp)->result_array();
        //pre($result);
        //$result         = $this->Role_mgmt_model->tra_ti_list($input);
        if ($result) { 
            //pre($result);
            foreach ($result as $each){
                $final[$each['tra_id']]['trarefid'] = $each['tra_id'];
                $final[$each['tra_id']]['traname'] = $each['traname'];
                $final[$each['tra_id']]['tralogo'] = $each['tralogo'];
                $final[$each['tra_id']]['ti'][] = $each;
                #$ti_ids[] = $each['trnrefid'];
            }
             $final = array_values($final);
            //pre($final); die;
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $final);
            
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    
    public function get_rm_ti_glimpse_mewp($input) {
        #call ChalkLit_Get_Wall_Post_Data_For_TI('ABS/DSCERT/Nov/2018','N');
        $sp = "call ChalkLit_Get_Wall_Post_Data_For_TI('".$input[tiid]."','N')";
        $result = $this->db->query($sp)->result_array();
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    
    public function get_rm_ti_glimpse_metp($input) {
        $sp = "call ChalkLit_Get_Training_Post_Data_For_TI('".$input[tiid]."','N')";
        $result = $this->db->query($sp)->result_array();
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    public function get_rm_ti_glimpse_meu($input) {
        #call ChalkLit_Get_User_Activity_Data_For_TI('ABS/DSCERT/Nov/2018','N','USERS',25);
        $sp = "call ChalkLit_Get_User_Activity_Data_For_TI('".$input[tiid]."','N','USERS',0)";
        $result = $this->db->query($sp)->result_array();
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    public function get_rm_ti_glimpse_mes($input) {
        $sp = "call ChalkLit_Get_User_Activity_Data_For_TI('".$input[tiid]."','Y','SCHOOLS',0)";
        $result = $this->db->query($sp)->result_array();
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    
    
}
