<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sender_id_mapping extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Training_instances_model');
        $this->load->model('Training_model');
        $this->load->model('Courses_model');
        $this->load->model('Wall_model');
        $this->load->model('User_model');
        $this->load->model('Reports_model');
        $this->load->model('Log_records_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }
	
	////////////////////////SENDER ID MAPPING /////////////////////

    public function sender_id_mapping_list($input) {//user_id
        //$this->validate_sender_id_mapping_list();
        $result = $this->Training_model->sender_id_mapping_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping list displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function map_sender_id($inputs) { //pre($inputs);die;        
        $this->validate_map_sender_id($inputs);
        $result = $this->Training_model->map_sender_id($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_map_sender_id($inputs) {
        if (!isset($inputs['trainings']) OR empty($inputs['trainings'])) {
            return_data(false, 'trainings is required in array format.', array());
        }
        if (!isset($inputs['sender_ids']) OR empty($inputs['sender_ids'])) {
            return_data(false, 'sender_ids is required.', array());
        }
    }

    public function delete_sender_id_mapping($input) {
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_model->delete_sender_id_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    
	
}
