<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sender_id extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Training_instances_model');
        $this->load->model('Training_model');
        $this->load->model('Courses_model');
        $this->load->model('Wall_model');
        $this->load->model('User_model');
        $this->load->model('Reports_model');
        $this->load->model('Log_records_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }

	public function add_sender_id($input) {
        if (!isset($input['sendercode']) OR empty($input['sendercode'])) {
            return array("status" => false, "message" => 'sendercode is required.', "result" => array());
        }
        $result = $this->Training_model->add_sender_id($input);
        if ($result['message'] == "false") {
            return array("status" => false, "message" => 'Sender Id already exists', "result" => array());
        } elseif ($result['message'] == "true") {
            return array("status" => true, "message" => 'Data Saved successfully.', "result" => array('sendercode' => $result['sendercode']));
        } else {
            return array("status" => false, "message" => 'Something went wrong', "result" => array());
        }
    }

    public function edit_sender_id($input) {

        if (!isset($input['senderrefid']) OR empty($input['senderrefid'])) {
            return array("status" => false, "message" => 'senderrefid is required.', "result" => array());
        }
        if (!isset($input['sendercode']) OR empty($input['sendercode'])) {
            return array("status" => false, "message" => 'sendercode is required.', "result" => array());
        }
        $result = $this->Training_model->edit_sender_id($input);
        if ($result['message'] == 'true') {
            $data = $this->Training_model->get_sender_id($input);
            return array("status" => true, "message" => 'Data Updated successfully.', "result" => $data);
        } elseif ($result['message'] == 'false') {
            return array("status" => false, "message" => 'Sender code already exists.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function delete_sender_id($input) {

        if (!isset($input['senderrefid']) OR empty($input['senderrefid'])) {
            return array("status" => false, "message" => 'senderrefid is required.', "result" => array());
        }

        $result = $this->Training_model->delete_sender_id($input);
        if ($result) {
            return array("status" => true, "message" => 'Data Removed successfully.', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
    public function sender_id_list($input) {

        $result = $this->Training_model->sender_id_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
}
