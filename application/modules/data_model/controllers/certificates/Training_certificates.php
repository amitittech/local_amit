<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Training_certificates extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Training_instances_model');
        $this->load->model('Training_model');
        $this->load->model('Courses_model');
        $this->load->model('Wall_model');
        $this->load->model('User_model');
        $this->load->model('Reports_model');
        $this->load->model('Log_records_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }
	
	
    

    //////////////////////// CERTIFICATES TEMPLATES MAPPING /////////////////////

    public function list_of_mapping_certificates_to_trainings($input) {//user_id
        //$this->validate_certificates_mapping_list();
        $result = $this->Training_model->list_of_mapping_certificates_to_trainings($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping list displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function map_certificates_to_trainings($inputs) { //pre($inputs);die;        
        $this->validate_map_certificates_to_trainings($inputs);
        $result = $this->Training_model->map_certificates_to_trainings($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_map_certificates_to_trainings($inputs) {
        if (!isset($inputs['trainings']) OR empty($inputs['trainings'])) {
            return_data(false, 'trainings is required in array format.', array());
        }
        if (!isset($inputs['certificates']) OR empty($inputs['certificates'])) {
            return_data(false, 'certificates is required in array format.', array());
        }
    }

    public function delete_certificates_mapping($input) {
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_model->delete_certificates_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function certificates_list($input) {
        //$this->validate_sender_id_list();
        $result = $this->Training_model->certificates_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
	
	//----------   certificate approves   ----------- //
	
    public function certificate_approves($inputs) { 
        $this->validate_certificate_approves($inputs);
        $result = $this->Training_instances_model->certificate_approves($inputs);
        if ($result) {
            if ($result == true && !is_array($result)) {
                $result = array();
            }
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
    
    public function validate_certificate_approves($inputs) {        
        if (!isset($inputs['trarefid']) OR empty($inputs['trarefid'])) {
            return_data(false, 'trarefid is required.', array());
        }
        if (!isset($inputs['trnrefid']) OR empty($inputs['trnrefid'])) {
            return_data(false, 'trnrefid is required.', array());
        }
        if (!isset($inputs['user_mobiles']) OR empty($inputs['user_mobiles']) OR is_array($inputs['user_mobiles'])== false) {
            return_data(false, 'user_mobiles is required in array format.', array());
        }
    }
    
    public function certificate_approves_list($inputs) { 
        //$this->validate_certificate_approves($inputs);
        $result = $this->Training_instances_model->certificate_approves_list($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Records has been displyed successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
    
    public function delete_certificate_approve($input) { 
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_instances_model->delete_certificate_approve($input);
        if ($result) {
            return array("status" => true, "message" => 'Data Removed successfully.', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
}
