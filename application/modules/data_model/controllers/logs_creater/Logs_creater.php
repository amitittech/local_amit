<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logs_creater extends MX_Controller {
   
    function __construct() {
        parent::__construct();
        $this->load->helper('custom');
		$this->load->model('Log_records_model');
    }
	
	
	public function save_api_log_txt_file($inputs) {
        $file_name = 'api_request_logs';
        if (isset($inputs['opcode'])) {
            $file_name = $inputs['opcode'].'_'.$file_name;
        }
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . base_url($file_name) . PHP_EOL;
        $log = $log . "Request " . json_encode($inputs) . PHP_EOL . PHP_EOL;

        file_put_contents('logs/' . $file_name . '_' . date('d_m_Y') . '.txt', $log, FILE_APPEND);
    }
	
	//------- LOG for api---------------------------------------------------//    
    public function activity_log($active_user,$inputs,$response,$starting_time) {
		$responsesize 					= 0; 									#response size
        if (isset($response['result']) && !empty($response['result'])) {
            #$responsesize 				= mb_strlen($string, '8bit'); #strlen(json_encode($response['result'])); #mb_strlen(json_encode($response),'8bit');
            $responsesize 				= 2000;
        }
		$response_time 					= milliseconds() - $starting_time; 		#processing time
		$header_data 					= $_SERVER; #$this->input->request_headers(); #getallheaders(); #header values
		
		$activity_log['server'] 		= $header_data["SERVER_NAME"];
        $activity_log['ipaddress'] 		= (isset($header_data['REMOTE_ADDR'])) ? $header_data['REMOTE_ADDR'] : '192.168.0.1'; //SERVER_ADDR
        $activity_log['useragent'] 		= (isset($header_data['HTTP_USER_AGENT'])) ? $header_data['HTTP_USER_AGENT'] : 'useragent';
		$activity_log['createddate'] 	= milliseconds();
        $activity_log['resstatus'] 		= (isset($response['status']) && !empty($response['status'])) ? API_RESPONSE_STATUS_TRUE : API_RESPONSE_STATUS_FALSE;
        $activity_log['resmessage'] 	= $response['message'];
        $activity_log['responsesize'] 	= $responsesize;
        $activity_log['processingtime'] = $response_time;
        $activity_log['opcode'] 		= $inputs['opcode'];
        $activity_log['appversion'] 	= (isset($inputs['appversion'])) ? $inputs['appversion'] : 'appversion';
        $request_params 				= $inputs;
        unset($request_params['authtoken']);
        $activity_log['request'] 		= json_encode($request_params);
        $activity_log['usmid'] 			= $active_user['login_id'];
        
        $result 						= $this->Log_records_model->activity_log($activity_log);
        return true;
    }
	
}
