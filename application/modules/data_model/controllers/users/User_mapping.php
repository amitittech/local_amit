<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_mapping extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Training_model');
        $this->load->helper('custom');
    }
	
	//----------   MAP USERS TO TRAINING AGENCIES    ----------- //
    public function map_users_to_trainings($inputs) { //pre($inputs);die;        
        $this->validate_map_users_to_trainings($inputs);
        $result = $this->Training_model->map_users_to_trainings($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_map_users_to_trainings($inputs) {
        if (!isset($inputs['trainings']) OR empty($inputs['trainings'])) {
            return_data(false, 'trainings is required in array format.', array());
        }
        if (!isset($inputs['user_mobiles']) OR empty($inputs['user_mobiles'])) {
            return_data(false, 'user_mobiles is required in array format.', array());
        }
        if (!isset($inputs['user_role']) OR empty($inputs['user_role'])) {
            return_data(false, 'user_role is required.', array());
        }
    }

    //list_of_mapping_users_to_trainings
    public function list_of_mapping_users_to_trainings($input) {//user_id
        //$this->validate_list_of_mapping_users_to_trainings();
        $result = $this->Training_model->list_of_mapping_users_to_trainings($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping list displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function validate_list_of_mapping_users_to_trainings($inputs) {
        
    }

    public function delete_users_mapping($input) {
        //pre($input); die;
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_model->delete_users_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
}
