<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('User_model');
        $this->load->helper('custom');
    }

    public function users_list_by_role($input) {
        $result = $this->User_model->users_list_by_role($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function cl_fetch_master_data($inputs) {
        if (!isset($inputs['fetch_what']) OR empty($inputs['fetch_what'])) {
            return_data(false, 'Fetch what field is required.', array());
        }
        $result = $this->User_model->get_states($input);
        if ($result) {
            return array("status" => true, "message" => 'States fetched successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
    }
    
    public function user_roles($input) {
        $result = $this->User_model->users_list_by_role($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

}
