<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Carousel extends MX_Controller {
   
function __construct() {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if ($method == "OPTIONS") {
        //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
        //die('use only post method');
    }
    parent::__construct();
    
}
public function trn_state_wise_teaher($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($input['query'])->result_array();

        $i=0;
        foreach($data as $key => $value) {
          $resul_array[$i]['state'] = $value['statename'];
          $resul_array[$i]['teacher'] = intval($value['teacher']);
          $i++;
        }
        //print_r($data);die;
    return_data(true, 'state wise teachers fetched sucessfully', $resul_array);
    }
}
public function trn_state_wise_school_covered($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($input['query'])->result_array();

        $i=0;
        foreach ($data as $key => $value) {
          $resul_array[$i]['state'] = $value['statename'];
          $resul_array[$i]['school'] = intval($value['Total_School']);
          $i++;
        }
        
    return_data(true, 'state wise school count fetched sucessfully', $resul_array);
    }
}
public function users_activity_last_seven_days(){

    $query = "SELECT last_week.DAY , SUM(X.TotalUsers) TotalUsers
    FROM   (SELECT DATE_FORMAT(NOW() - INTERVAL 0 DAY, '%M %d') as DAY UNION  
    SELECT DATE_FORMAT(NOW() - INTERVAL 1 DAY, '%M %d') as DAY UNION 
    SELECT DATE_FORMAT(NOW() - INTERVAL 2 DAY, '%M %d') as DAY UNION 
    SELECT DATE_FORMAT(NOW() - INTERVAL 3 DAY, '%M %d') as DAY UNION 
    SELECT DATE_FORMAT(NOW() - INTERVAL 4 DAY, '%M %d') as DAY UNION 
    SELECT DATE_FORMAT(NOW() - INTERVAL 5 DAY, '%M %d') as DAY UNION 
    SELECT DATE_FORMAT(NOW() - INTERVAL 6 DAY, '%M %d') as DAY  
    ) last_week
    LEFT JOIN
    (SELECT DATE_FORMAT(curlrq.logdate,'%M %d') day,count(DISTINCT `usmrefid`) TotalUsers FROM  chalklit.`curlrq` WHERE `logdate` > DATE(CURDATE()) - INTERVAL 7 DAY AND logdate < (CURDATE() + INTERVAL 1 DAY) GROUP BY day
    UNION ALL
    SELECT DATE_FORMAT(json_activity_log.createddate,'%M %d') day, COUNT(DISTINCT `usmid` ) TotalUsers  FROM esocial_log.json_activity_log WHERE `createddate` > DATE(CURDATE()) - INTERVAL 7 DAY AND createddate < (CURDATE() + INTERVAL 1 DAY) GROUP BY day
    ) X ON X.day = last_week.day  GROUP BY last_week.day ORDER BY last_week.day;";


      $this->db->db_debug = false;

      if(!@$this->db->query($query)){
      $error = $this->db->error();
      return_data('false', $error['message'], array());
      }else{
      $data =   $this->db->query($query)->result_array();
      $i=0;
      foreach ($data as $key => $value) {
      $post_type[$i]['days'] = $value['DAY'];
      $post_type[$i]['count'] = intval($value['TotalUsers']);
      $i++;
      }
      return_data(true, 'Hour wise activity Fetched Sucessfully', $post_type);
      }
}

public function get_ongoing_trainings(){

        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
        $query = "SELECT ti.`startdate`,ti.`enddate`, ti.trnrefid as uuuid, ti.`trnname`, curtra.traname ,COUNT(currcu.rcurefid) AS trainees,datediff(ti.enddate,ti.startdate) as total_training_days ,datediff(now(),ti.startdate) as day_of_training,
SUM(CASE WHEN  currcu.completionpercentage = 0  THEN 1 ELSE 0 END)  AS 'ns',
SUM(CASE WHEN  currcu.completionpercentage > 0 AND currcu.completionpercentage < 50  THEN 1 ELSE 0 END)  AS 'low',
SUM(CASE WHEN  currcu.completionpercentage >= 50 AND currcu.completionpercentage < 75  THEN 1 ELSE 0 END)  AS 'medium',
SUM(CASE WHEN  currcu.completionpercentage >= 75 AND currcu.completionpercentage < 100  THEN 1 ELSE 0 END)  AS 'high',
SUM(CASE WHEN  currcu.completionpercentage = 100   THEN 1 ELSE 0 END)  AS 'complete'
FROM `currcu`
join curtrb batch on (currcu.rcutrbrefid = batch.trbrefid)
JOIN clt_trn ti on (batch.trbtrnrefid = ti.trnrefid)
JOIN curtra on(ti.trntrarefid = curtra.trarefid)
WHERE ti.isvisibleonclient=1 AND currcu.rcurole = 'TEACHER' AND (CURDATE()  BETWEEN ti.`startdate` AND ti.`enddate`) OR (ti.`startdate` > CURDATE())   GROUP BY ti.trnrefid";

        $this->db->db_debug = false;

        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
        }else{
        $trainings =   $CHALKLIT_SLAVE->query($query)->result_array();
		/*echo "<pre>";print_r($trainings);die;*/
		
		if($trainings){
        $i=0;
            foreach ($trainings as  $key => $training) {
				
				$msg='';
				if($training['total_training_days']==$training['day_of_training']){
					$msg = 'Last Day of Training';
				}elseif($training['day_of_training']==1){
					$msg = 'First Day of Training';
				}else{
					$totaldays =$training['total_training_days'];
					$day_of_training = $this->ordinal($training['day_of_training']);
					
					if(trim($day_of_training) < 0){
						
					 $msg = abs($day_of_training).' days remaining';
					}else{
					$msg = $day_of_training.' day of '.$totaldays.' days training';
					}
				}
				
               
                  $data[$i]['uuuid']                = intval($training['uuuid']);  
                  $data[$i]['trnname']              = $training['trnname'];
                  $data[$i]['traname']              = $training['traname'];
                  $data[$i]['trainees']             = $training['trainees'];
				  $data[$i]['msg']                  = $msg;
				  //$data[$i]['day_of_training']      = $training['day_of_training'];
                  $data[$i]['info']['Not Started']  = intval($training['ns']);
                  $data[$i]['info']['Low']          = intval($training['low']);
                  $data[$i]['info']['Medium']       = intval($training['medium']);
                  $data[$i]['info']['High']         = intval($training['high']);
                  $data[$i]['info']['Complete']     = intval($training['complete']);
                  $i++;
            }
			return_data(true, 'Ongoing Trainings fetched successfully', $data);
		}
			return_data(true, 'There are no Ongoing Trainings ', array());
      }

        
}

public function get_trn_partners(){

        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
        $query = "select count(distinct trntrarefid) as partners from clt_trn where isvisibleonclient = 1;";

        $this->db->db_debug = false;

        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
        }else{
        $partners =   $CHALKLIT_SLAVE->query($query)->row();
        }

        return_data(true, 'Partners  fetched successfully', $partners->partners);
}

public function activity_distribution_today($input){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
     $query = " SELECT  SUM(IFNULL(L.TotalLike, 0)) as likesCount , SUM(IFNULL(C.TotalComment,0)) AS commentCount , SUM(IFNULL(S.TotalShare,0)) AS shareCount  , SUM(IFNULL(V.TotalViews,0)) AS viewsCount , SUM(IFNULL(D.TotalDownloads,0)) AS downloadsCount  from escchp 
        LEFT OUTER JOIN 
        (
        SELECT escchp.chprefid AS postid ,count(esclom.lomrefid) as TotalLike from  escchp
        JOIN esclom ON esclom.lomobjrefid = escchp.chprefid
        WHERE esclom.lomobjtype='ESCCHP' AND  esclom.likedate >= CURDATE() 
        GROUP BY escchp.chprefid  
        ) L
        ON L.postid = escchp.chprefid
        LEFT JOIN
        (
        SELECT escchp.chprefid AS postid ,count(esccpc.cpcrefid) as TotalComment from  escchp
        LEFT JOIN esccpc ON esccpc.cpcchprefid = escchp.chprefid
        WHERE  esccpc.cpcdate >= CURDATE()    
        GROUP BY escchp.chprefid 
        ) C
        ON C.postid = escchp.chprefid

        LEFT JOIN
        (
        SELECT  escchp.chprefid as postid,count(eschso.hsorefid) as TotalShare from  escchp
        LEFT JOIN eschso ON eschso.hsoobjrefid = escchp.chprefid
        WHERE eschso.hsoobjtype='ESCCHP' AND eschso.hsoshareddate >= CURDATE()
        GROUP BY escchp.chprefid
        ) S
        ON S.postid = escchp.chprefid
        LEFT JOIN
        (
        SELECT  escchp.chprefid as postid, COUNT(escobt.obtrefid) AS TotalViews FROM escchp 
        LEFT JOIN escobt ON escobt.obtobjectid = escchp.chprefid 
        WHERE escobt.obtobjecttype ='ESCCHP' AND escobt.seendate >= CURDATE()
        GROUP BY escchp.chprefid 
        ) V
        ON V.postid = escchp.chprefid
        LEFT JOIN 
        ( SELECT eschdl.hdldomrefid as postid ,COUNT(eschdl.hdlrefid) as TotalDownloads FROM escchp
        LEFT JOIN  eschdl ON eschdl.hdldomrefid = escchp.chprefid  
        WHERE eschdl.downloaddate  >= CURDATE()
        GROUP BY escchp.chprefid
        ) D
        ON D.postid = escchp.chprefid";
    
        $this->db->db_debug = false;
        if(!@$CHALKLIT_SLAVE->query($query)){
            $error = $this->db->error();
            return_data('false', $error['message'], array());
        }else{
            $data =   $CHALKLIT_SLAVE->query($query)->row();
            $i=0;
            foreach ($data as $key => $value) {
                $resul_array[$i]['type'] = $key;
                $resul_array[$i]['count'] = intval($value);
                $i++;
            }
              return_data(true, 'Today activity distribution  fetched sucessfully', $resul_array);
        }
}

public function total_activity_last_seven_days(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT DATE_FORMAT(activityprofile.activitydate,'%M %d') AS day, SUM(totalactivities) AS activity FROM chalklit_analysis.activityprofile WHERE activityprofile.activitydate > DATE(CURDATE()) - INTERVAL 7 DAY AND activityprofile.activitydate < (CURDATE() + INTERVAL 1 DAY)  GROUP BY day;";

    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
    $error = $CHALKLIT_SLAVE->error();
    return_data('false', $error['message'], array());
    }else{
    $data =   $CHALKLIT_SLAVE->query($query)->result_array();

    $i=0;
    foreach ($data as $key => $value) {
        
    $activity[$i]['days']  = $value['day'];
    $activity[$i]['activity'] = intval($value['activity']);
    $i++;
    }
	foreach ($activity as $key => $part) {
	   $sort[$key] = strtotime($part['days']);
	}
    array_multisort($sort, SORT_ASC, $activity);
    return_data(true, 'Last Seven Days Activity fetched successfully', $activity);
    }
}
public function total_activity_today(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT today_hour as Hours , SUM(activity) as Activty from (
    (
    SELECT today_hours.today_hour ,COUNT(wall.activity) as activity
    FROM   ( 
    SELECT  0   as today_hour UNION 
    SELECT  1   as today_hour UNION 
    SELECT  2   as today_hour UNION 
    SELECT  3   as today_hour UNION 
    SELECT  4   as today_hour UNION 
    SELECT  5   as today_hour UNION 
    SELECT  6   as today_hour UNION 
    SELECT  7   as today_hour UNION 
    SELECT  8   as today_hour UNION 
    SELECT  9   as today_hour UNION 
    SELECT  10  as today_hour UNION 
    SELECT  11  as today_hour UNION 
    SELECT  12  as today_hour UNION
    SELECT  13  as today_hour UNION
    SELECT  14  as today_hour UNION
    SELECT  15  as today_hour UNION
    SELECT  16  as today_hour UNION
    SELECT  17  as today_hour UNION
    SELECT  18  as today_hour UNION
    SELECT  19  as today_hour UNION
    SELECT  20  as today_hour UNION
    SELECT  21  as today_hour UNION
    SELECT  22  as today_hour UNION
    SELECT  23  as today_hour 
    ) today_hours

    LEFT JOIN
    (
    ( SELECT hour(esclom.likedate) as Todayhour ,esclom.lomrefid as activity from esclom WHERE esclom.lomobjtype='ESCCHP' AND esclom.likedate >= CURDATE() AND esclom.likedate < (CURDATE() + INTERVAL 1 DAY) 
    )
    UNION ALL
    (SELECT hour(esccpc.cpcdate) as Todayhour ,esccpc.cpcrefid as activity from esccpc WHERE esccpc.cpcdate >= CURDATE() AND esccpc.cpcdate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Todayhour
    )
    UNION ALL
    (SELECT  hour(eschso.hsoshareddate ) AS Todayhour,eschso.hsorefid as activity from  eschso WHERE eschso.hsoobjtype='ESCCHP' AND eschso.hsoshareddate >=  CURDATE() AND eschso.hsoshareddate < (CURDATE() + INTERVAL 1 DAY) 
    )
    UNION ALL
    (SELECT   hour(escobt.seendate) AS Todayhour , escobt.obtrefid AS activity FROM escobt WHERE escobt.obtobjecttype ='ESCCHP' AND escobt.seendate >= CURDATE() AND escobt.seendate < (CURDATE() + INTERVAL 1 DAY) 
    ) 
    UNION ALL
    (
    SELECT  hour(eschdl.downloaddate ) AS Todayhour ,eschdl.hdlrefid as activity FROM eschdl WHERE eschdl.downloaddate >= CURDATE() AND eschdl.downloaddate < (CURDATE() + INTERVAL 1 DAY) 
    ) 
    ) wall on wall.Todayhour = today_hours.today_hour GROUP by today_hours.today_hour
    )
    UNION ALL
    (
    SELECT today_hours.today_hour,COUNT(content.activity) as activity FROM
    ( 
    SELECT  0   as today_hour UNION 
    SELECT  1   as today_hour UNION 
    SELECT  2   as today_hour UNION 
    SELECT  3   as today_hour UNION 
    SELECT  4   as today_hour UNION 
    SELECT  5   as today_hour UNION 
    SELECT  6   as today_hour UNION 
    SELECT  7   as today_hour UNION 
    SELECT  8   as today_hour UNION 
    SELECT  9   as today_hour UNION 
    SELECT  10  as today_hour UNION 
    SELECT  11  as today_hour UNION 
    SELECT  12  as today_hour UNION
    SELECT  13  as today_hour UNION
    SELECT  14  as today_hour UNION
    SELECT  15  as today_hour UNION
    SELECT  16  as today_hour UNION
    SELECT  17  as today_hour UNION
    SELECT  18  as today_hour UNION
    SELECT  19  as today_hour UNION
    SELECT  20  as today_hour UNION
    SELECT  21  as today_hour UNION
    SELECT  22  as today_hour UNION
    SELECT  23  as today_hour 
    ) today_hours

    LEFT JOIN

    (
    SELECT hour(currct.seendate) as h ,currct.rctrefid AS activity FROM currct WHERE currct.seendate  >=  CURDATE() AND currct.seendate < (CURDATE() + INTERVAL 1 DAY)
    UNION ALL
    SELECT hour(currcc.createddate) as h , currcc.rccrefid AS activity FROM currcc WHERE currcc.createddate  >=  CURDATE() AND currcc.createddate < (CURDATE() + INTERVAL 1 DAY)
    UNION ALL
    SELECT hour(currcr.ratingdate) AS h ,currcr.rcrrefid AS activity  FROM currcr WHERE currcr.ratingdate  >=  CURDATE() AND currcr.ratingdate < (CURDATE() + INTERVAL 1 DAY)
    ) content ON  content.h = today_hours.today_hour GROUP BY today_hour
    )

    ) x GROUP BY today_hour;
    ";
    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
    }else{

        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        //echo "<pre>"; print_r($data);die;
        $i=0;
        foreach ($data as $key => $value) {
        $current_hour = date('H');
        if($value['Hours'] > $current_hour ){

            continue;
        }
        $activity[$i]['hours']  = $this->beautifyTime($value['Hours']);
        $activity[$i]['activity'] = intval($value['Activty']);
        $i++;
        }
        
        return_data(true, 'Today Activity  fetched successfully', $activity);
    }
}
public function get_topics_covered(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "select count(distinct chaptername) `topics` from clt_trn left outer join currcm on (trnrcmrefid = rcmrefid) where isvisibleonclient = 1;";

    $this->db->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
    }else{
        $data =   $CHALKLIT_SLAVE->query($query)->row();
        return_data(true, 'Total  Topics covered fetched successfully', intval($data->topics));
    }
}
public function top_five_sources(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT cursrc.srcname AS source , COUNT(`rchrefid`) total_post FROM `currch` 
    JOIN cursrc on cursrc.srcrefid = currch.rchsrcrefid GROUP BY currch.rchsrcrefid ORDER BY total_post DESC ";
    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
    }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        
        $i=0;
        foreach ($data as $key => $value) {
            if($value['source']==''){
                $value['source']='Anonymous';
            }
        $source[$i]['source']  = $value['source'];
        $source[$i]['count'] = intval($value['total_post']);
        $i++;
        }
        
        return_data(true, 'Top Five Sources fetched successfully', $source);
    }
}
public function total_users_today(){

        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
        $query = "       SELECT today_hour as hours , SUM(IFNULL(x.users,0)) AS totalusers FROM
       (SELECT  0 as today_hour UNION 
        SELECT  1 as today_hour UNION 
        SELECT  2  as today_hour UNION 
        SELECT  3  as today_hour UNION 
        SELECT  4  as today_hour UNION 
        SELECT  5  as today_hour UNION 
        SELECT  6  as today_hour UNION 
        SELECT  7  as today_hour UNION 
        SELECT  8  as today_hour UNION 
        SELECT  9 as today_hour UNION 
        SELECT   10  as today_hour UNION 
        SELECT  11  as today_hour UNION 
        SELECT 12  as today_hour UNION
        SELECT   13  as today_hour UNION
        SELECT   14  as today_hour UNION
        SELECT   15  as today_hour UNION
        SELECT   16  as today_hour UNION
        SELECT    17  as today_hour UNION
        SELECT   18 as today_hour UNION
        SELECT    19  as today_hour UNION
        SELECT    20 as today_hour UNION
        SELECT    21  as today_hour UNION
        SELECT     22 as today_hour UNION
        SELECT     23  as today_hour 
        ) todayhour LEFT JOIN     
        ( SELECT hour(escusm.createddate) as Todayhour ,count(escusm.usmrefid) as users from escusm WHERE              escusm.createddate >= CURDATE() AND escusm.createddate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Todayhour) x
        on  x.Todayhour = todayhour.today_hour GROUP BY todayhour.today_hour ORDER BY todayhour.today_hour;";
        $this->db->db_debug = false;

        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
        }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        $i=0;
        foreach ($data as $key => $value) {
            if($value['hours'] > date('H')){
                continue;
            }
        $total_users[$i]['hours']  = $this->beautifyTime($value['hours']);
        $total_users[$i]['count'] = intval($value['totalusers']);
        $i++;
        }

        return_data(true, 'Chalklit total users fetched successfully', $total_users);
        }
}
public function total_users(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT count(*) AS Chalklit_Users from escusm A join esculm B on(A.usmrefid = B.ulmusmrefid) where A.isregisteredbyapp = 1 and B.isotpverified = 1";
    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
    }else{
        $data =   $CHALKLIT_SLAVE->query($query)->row();
        return_data(true, 'Chalklit total users fetched successfully', intval($data->Chalklit_Users));
    }
}
public function wall_activity_last_seven_days(){

        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);

        $query = "SELECT last_week.DAY , SUM(IFNULL(x.activity,0)) activity
        FROM   ( 
        SELECT  1  as DAY UNION 
        SELECT  2  as DAY UNION 
        SELECT  3  as DAY UNION 
        SELECT  4  as DAY UNION 
        SELECT  5  as DAY UNION 
        SELECT  6  as DAY UNION
        SELECT  7  as DAY  
        ) last_week
        LEFT JOIN
        (
        ( SELECT DATE_FORMAT(esclom.likedate,'%M %d') as Days ,count(esclom.lomrefid) as activity from esclom WHERE esclom.lomobjtype='ESCCHP' AND esclom.likedate >= CURDATE() - INTERVAL 7 DAY AND likedate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Days
        )
        UNION ALL
        (SELECT DATE_FORMAT(esccpc.cpcdate,'%M %d') as Days ,count(esccpc.cpcrefid) as activity from esccpc WHERE esccpc.cpcdate >= CURDATE() - INTERVAL 7 DAY AND cpcdate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Days
        )
        UNION ALL
        (SELECT  DATE_FORMAT(eschso.hsoshareddate,'%M %d' ) AS Days,count(eschso.hsorefid) as activity from  eschso WHERE eschso.hsoobjtype='ESCCHP' AND eschso.hsoshareddate >=  CURDATE() - INTERVAL 7 DAY AND hsoshareddate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Days
        )
        UNION ALL
        (SELECT   DATE_FORMAT(escobt.seendate,'%M %d') AS Days , COUNT(escobt.obtrefid) AS activity FROM escobt WHERE escobt.obtobjecttype ='ESCCHP' AND escobt.seendate >= CURDATE() - INTERVAL 7 DAY AND seendate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Days
        )
        UNION ALL
        (
        SELECT  DATE_FORMAT(eschdl.downloaddate,'%M %d') AS Days ,COUNT(eschdl.hdlrefid) as activity FROM eschdl WHERE eschdl.downloaddate >= CURDATE() - INTERVAL 7 DAY AND downloaddate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Days
        )
        ) x on x.Days = last_week.DAY GROUP BY last_week.DAY";

        $CHALKLIT_SLAVE->db_debug = false;
        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
        }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        //print_r($data);die;
        $i=0;
        foreach ($data as $key => $value) {
        $post_type[$i]['days']  = $value['DAY'];
        $post_type[$i]['count'] = intval($value['activity']);
        $i++;
        }
        return_data(true, 'Wall Weekely activity Fetched Sucessfully', $post_type);
        }
}
public function wall_total_activity_hourly_today(){

        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);

        $query = "SELECT today_hours.today_hour ,SUM(IFNULL(x.activity,0)) as TotalUsers
        FROM   ( 
        SELECT  0  as today_hour UNION 
        SELECT  1  as today_hour UNION 
        SELECT  2  as today_hour UNION 
        SELECT  3  as today_hour UNION 
        SELECT  4  as today_hour UNION 
        SELECT  5  as today_hour UNION 
        SELECT  6  as today_hour UNION 
        SELECT  7  as today_hour UNION 
        SELECT  8  as today_hour UNION 
        SELECT  9  as today_hour UNION 
        SELECT  10  as today_hour UNION 
        SELECT  11  as today_hour UNION 
        SELECT  12  as today_hour UNION
        SELECT  13  as today_hour UNION
        SELECT  14  as today_hour UNION
        SELECT  15  as today_hour UNION
        SELECT  16  as today_hour UNION
        SELECT  17  as today_hour UNION
        SELECT  18 as today_hour UNION
        SELECT  19  as today_hour UNION
        SELECT  20 as today_hour UNION
        SELECT  21  as today_hour UNION
        SELECT  22 as today_hour UNION
        SELECT  23  as today_hour  
        ) today_hours

        LEFT JOIN
        (
        ( SELECT hour(esclom.likedate) as Todayhour ,count(esclom.lomrefid) as activity from esclom WHERE esclom.lomobjtype='ESCCHP' AND esclom.likedate >= CURDATE() AND esclom.likedate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Todayhour
        )
        UNION ALL
        (SELECT hour(esccpc.cpcdate) as Todayhour ,count(esccpc.cpcrefid) as activity from esccpc WHERE esccpc.cpcdate >= CURDATE() AND esccpc.cpcdate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Todayhour
        )
        UNION ALL
        (SELECT  hour(eschso.hsoshareddate ) AS Todayhour,count(eschso.hsorefid) as activity from  eschso WHERE eschso.hsoobjtype='ESCCHP' AND eschso.hsoshareddate >=  CURDATE() AND eschso.hsoshareddate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Todayhour
        )
        UNION ALL
        (SELECT   hour(escobt.seendate) AS Todayhour , COUNT(escobt.obtrefid) AS activity FROM escobt WHERE escobt.obtobjecttype ='ESCCHP' AND escobt.seendate >= CURDATE() AND escobt.seendate < (CURDATE() + INTERVAL 1 DAY) GROUP BY Todayhour
        )
        UNION ALL
        (
        SELECT  hour(eschdl.downloaddate ) AS Todayhour ,COUNT(eschdl.hdlrefid) as activity FROM eschdl WHERE eschdl.downloaddate >= CURDATE() GROUP BY Todayhour
        )
        ) x on x.Todayhour = today_hours.today_hour GROUP BY today_hours.today_hour;";

        $CHALKLIT_SLAVE->db_debug = false;
        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
        }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        //print_r($data);die;
        $i=0;
        foreach ($data as $key => $value) {
        $post_type[$i]['hours']  = intval($value['today_hour']);
        $post_type[$i]['count'] = intval($value['TotalUsers']);
        $i++;
        }
        return_data(true, 'Wall Hourly activity Fetched Sucessfully', $post_type);
        }
}

public function activity_distribution(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT activityprofile.activitytype as Type , SUM(totalactivities) AS activity FROM chalklit_analysis.activityprofile  GROUP BY activityprofile.activitytype;";

    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
    $error = $CHALKLIT_SLAVE->error();
    return_data('false', $error['message'], array());
    }else{
    $data =   $CHALKLIT_SLAVE->query($query)->result_array();
    $i=0;
    foreach ($data as $key => $value) {
        
    $activity[$i]['type']  = $value['Type'];
    $activity[$i]['activity'] = intval($value['activity']);
    $i++;
    }

    return_data(true, 'Topics fetched successfully', $activity);
    }
}

public function wall_activity_distribution_today(){

      $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
      $query = " SELECT  SUM(IFNULL(L.TotalLike, 0)) as likes , SUM(IFNULL(C.TotalComment,0)) AS comment , SUM(IFNULL(S.TotalShare,0)) AS share  , SUM(IFNULL(V.TotalViews,0)) AS views , SUM(IFNULL(D.TotalDownloads,0)) AS downloads  from escchp 

      LEFT OUTER JOIN 
      (SELECT escchp.chprefid AS postid ,count(esclom.lomrefid) as TotalLike from  escchp
      JOIN esclom ON esclom.lomobjrefid = escchp.chprefid
      WHERE esclom.lomobjtype='ESCCHP' AND  esclom.likedate >= CURDATE() AND esclom.likedate < (CURDATE() + INTERVAL 1 DAY) GROUP BY escchp.chprefid  
      ) L
      ON L.postid = escchp.chprefid
      LEFT JOIN
      (
      SELECT escchp.chprefid AS postid ,count(esccpc.cpcrefid) as TotalComment from  escchp
      LEFT JOIN esccpc ON esccpc.cpcchprefid = escchp.chprefid
      WHERE  esccpc.cpcdate >= CURDATE() AND esccpc.cpcdate < (CURDATE() + INTERVAL 1 DAY)
      GROUP BY escchp.chprefid 
      ) C
      ON C.postid = escchp.chprefid
      LEFT JOIN
      (
      SELECT  escchp.chprefid as postid,count(eschso.hsorefid) as TotalShare from  escchp
      LEFT JOIN eschso ON eschso.hsoobjrefid = escchp.chprefid
      WHERE eschso.hsoobjtype='ESCCHP' AND eschso.hsoshareddate >= CURDATE() AND eschso.hsoshareddate < (CURDATE() + INTERVAL 1 DAY)
      GROUP BY escchp.chprefid
      ) S
      ON S.postid = escchp.chprefid
      LEFT JOIN
      (
      SELECT  escchp.chprefid as postid, COUNT(escobt.obtrefid) AS TotalViews FROM escchp 
      LEFT JOIN escobt ON escobt.obtobjectid = escchp.chprefid 
      WHERE escobt.obtobjecttype ='ESCCHP' AND escobt.seendate >= CURDATE() AND escobt.seendate < (CURDATE() + INTERVAL 1 DAY)
      GROUP BY escchp.chprefid 
      ) V
      ON V.postid = escchp.chprefid
      LEFT JOIN 
      ( SELECT eschdl.hdldomrefid as postid ,COUNT(eschdl.hdlrefid) as TotalDownloads FROM escchp
      LEFT JOIN  eschdl ON eschdl.hdldomrefid = escchp.chprefid  
      WHERE eschdl.downloaddate  >= CURDATE() AND eschdl.downloaddate < (CURDATE() + INTERVAL 1 DAY)
      GROUP BY escchp.chprefid
      ) D
      ON D.postid = escchp.chprefid
      ";


      $CHALKLIT_SLAVE->db_debug = false;

      if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $CHALKLIT_SLAVE->error();
      return_data('false', $error['message'], array());
      }else{
      $data =   $CHALKLIT_SLAVE->query($query)->row();
      $i=0;
      foreach ($data as $key => $value) {
      $post_type[$i]['activity'] = $key;
      $post_type[$i]['count'] = intval($value);
      $i++;
      }
      return_data(true, 'Wall Activity Distribution Fetched Sucessfully', $post_type);
      }
}
public function users_active_last_seven_days(){

        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);

        $query = "SELECT DATE_FORMAT(activeusers.activitydate,'%M %d') AS day, COUNT(DISTINCT userid) AS activeusers FROM esocial_log.activeusers WHERE activeusers.activitydate > DATE(CURDATE()) - INTERVAL 8 DAY AND activeusers.activitydate < (CURDATE() + INTERVAL 1 DAY)  GROUP BY day;";
        


        $CHALKLIT_SLAVE->db_debug = false;

        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
        }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        $i=0;
        foreach ($data as $key => $value) {
        $post_type[$i]['days'] = $value['day'];
        $post_type[$i]['count'] = intval($value['activeusers']);
        $i++;
        }
		foreach ($post_type as $key => $part) {
	    $sort[$key] = strtotime($part['days']);
	    }
        array_multisort($sort, SORT_ASC, $post_type);
        return_data(true, 'Last seven Days  Active Users Fetched Sucessfully', $post_type);
        }
}
public function users_activity_hourly_today(){

        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);

        $query = " SELECT today_hours.today_hour, COUNT(DISTINCT x.UserId) AS TotalUsers  FROM
        ( 
        SELECT  0  as today_hour UNION 
        SELECT  1  as today_hour UNION 
        SELECT  2  as today_hour UNION 
        SELECT  3  as today_hour UNION 
        SELECT  4  as today_hour UNION 
        SELECT  5  as today_hour UNION 
        SELECT  6  as today_hour UNION 
        SELECT  7  as today_hour UNION 
        SELECT  8  as today_hour UNION 
        SELECT  9  as today_hour UNION 
        SELECT  10 as today_hour UNION 
        SELECT  11 as today_hour UNION 
        SELECT  12 as today_hour UNION
        SELECT  13 as today_hour UNION
        SELECT  14 as today_hour UNION
        SELECT  15 as today_hour UNION
        SELECT  16 as today_hour UNION
        SELECT  17 as today_hour UNION
        SELECT  18 as today_hour UNION
        SELECT  19 as today_hour UNION
        SELECT  20 as today_hour UNION
        SELECT  21 as today_hour UNION
        SELECT  22 as today_hour UNION
        SELECT  23 as today_hour 
        ) today_hours

        LEFT JOIN
        (
        SELECT  `usmrefid` UserId ,hour(curlrq.logdate) AS today_hour  FROM  chalklit.`curlrq` WHERE `logdate`>=  CURDATE() AND logdate < (CURDATE() + INTERVAL 1 DAY) 
        union
        SELECT  usmid  UserId ,hour(json_activity_log.createddate) AS today_hour FROM esocial_log.json_activity_log WHERE json_activity_log.createddate >=  CURDATE() AND json_activity_log.createddate < (CURDATE() + INTERVAL 1 DAY)
        ) x ON x.today_hour = today_hours.today_hour GROUP BY today_hours.today_hour   ORDER BY today_hour ASC;";


        $CHALKLIT_SLAVE->db_debug = false;

        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
        }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        
        $i=0;
        foreach ($data as $key => $value) {

          $current_hour = date('H');
          if($value['today_hour'] > $current_hour ){
              continue;
          }
        $post_type[$i]['hours'] = $this->beautifyTime($value['today_hour']);
        $post_type[$i]['count'] = intval($value['TotalUsers']);
        $i++;
        }
        return_data(true, 'Hour wise user activity Fetched Sucessfully', $post_type);
        }
}
public function wall_state_wise_activity(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);

    $query = "SELECT clt_mat_state.statename , SUM(totallikes+totalcomments+totalshares+totalviews+totaldownloads) as activity FROM clt_mat_state  JOIN
    (

    select escusm.usmstaterefid , IFNULL(L.likes,0) as totallikes , IFNULL(C.comments,0) AS totalcomments , IFNULL(S.shares,0) AS totalshares ,IFNULL(V.tviews,0) as totalviews , IFNULL(D.downloads,0) AS totaldownloads from escusm 
    LEFT join
    (select escusm.usmstaterefid ,COUNT(esclom.lomrefid) as likes from escusm join esclom on     esclom.lomusmrefid = escusm.usmrefid  GROUP by escusm.usmstaterefid
    ) L ON L.usmstaterefid =  escusm.usmstaterefid

    LEFT JOIN 
    (select escusm.usmstaterefid ,COUNT(esccpc.cpcrefid) as comments from escusm join esccpc on esccpc.cpcusmrefid = escusm.usmrefid GROUP by escusm.usmstaterefid
    ) C ON  C.usmstaterefid = escusm.usmstaterefid

    LEFT JOIN 
    (select escusm.usmstaterefid ,COUNT(eschso.hsorefid) as shares from escusm join eschso on eschso.hsousmrefid = escusm.usmrefid GROUP by escusm.usmstaterefid
    ) S ON  S.usmstaterefid = escusm.usmstaterefid

    LEFT JOIN 
    (select escusm.usmstaterefid ,COUNT(escobt.obtrefid) as tviews from escusm join escobt on escobt.obtrefid = escusm.usmrefid GROUP by escusm.usmstaterefid
    ) V ON  V.usmstaterefid = escusm.usmstaterefid

    LEFT JOIN 
    (select escusm.usmstaterefid ,COUNT(eschdl.hdlrefid) as downloads from escusm join eschdl on eschdl.hdlrefid = escusm.usmrefid GROUP by escusm.usmstaterefid
    ) D ON  D.usmstaterefid = escusm.usmstaterefid


    WHERE escusm.usmstaterefid IS NOT NULL
    GROUP BY escusm.usmstaterefid
    )x on x.usmstaterefid = clt_mat_state.staterefid GROUP BY clt_mat_state.staterefid ORDER BY activity DESC LIMIT 0,6 ;
    ";
    $CHALKLIT_SLAVE->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
    $error = $CHALKLIT_SLAVE->error();
    return_data('false', $error['message'], array());
    }else{
    $data =   $CHALKLIT_SLAVE->query($query)->result_array();
    $i=0;
    foreach ($data as $key => $value) {
    if($value['statename']=='National Capital Territory of Delhi'){
    $value['statename'] = 'Delhi';
    }
    $post_type[$i]['state'] = $value['statename'];
    $post_type[$i]['count'] = intval($value['activity']) ;
    $i++;
    }
    return_data(true, 'Wall State wise activity Fetched Sucessfully', $post_type);
    }
}
public function content_chapter_count(){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query  = "SELECT count(*) as total_chapter FROM `currcm` WHERE `isfortraining`=0 AND `isforreference`=0 AND  islive =1;";
    $data   = $CHALKLIT_SLAVE->query($query)->row();
    return_data(true, 'Chapter count fetched Sucessfully', intval($data->total_chapter));
      
      /*
    $CHALKLIT_SLAVE->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $this->db->error();
      return_data('false', $error['message'], array());
    }else{
      
    }*/
}
public function get_wall_post_type(){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT SUM(CASE WHEN  currch.type = 'video' THEN 1 ELSE 0 END)  AS 'video',SUM(CASE WHEN currch.type = ''  AND currch.thumbnailurl != '' THEN 1 ELSE 0 END)  AS 'image',SUM(CASE WHEN currch.type = ''  AND currch.thumbnailurl = ''  AND currch.downloadurl != '' THEN 1 ELSE 0 END)  AS 'document',SUM(CASE WHEN currch.type = ''  AND currch.thumbnailurl = '' AND currch.downloadurl = '' THEN 1 ELSE 0 END)  AS 'text' FROM currch;";
    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $this->db->error();
      return_data('false', $error['message'], array());
    }else{
      $data =   $CHALKLIT_SLAVE->query($query)->row();
      
      $i=0;
       foreach ($data as $key => $value) {
        $post_type[$i]['Type'] = $key;
        $post_type[$i]['Count'] = intval($value) ;
        $i++;
      }
      return_data(true, 'Wall Post Type Fetched Sucessfully', $post_type);
    }
}
public function source_count(){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT COUNT(*) AS sources from cursrc WHERE `isactive`=1;";
    $this->db->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $this->db->error();
      return_data('false', $error['message'], array());
    }else{
      $data =   $CHALKLIT_SLAVE->query($query)->row();
      return_data(true, 'Wall Source count fetched sucessfully', intval($data->sources));
    }
}
public function wall_post(){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT count(*) AS total_post from escchp  WHERE createddate >= '2016-04-01';";
    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $this->db->error();
      return_data('false', $error['message'], array());
    }else{
      $data =   $CHALKLIT_SLAVE->query($query)->row();
      return_data(true, 'Wall post count fetched sucessfully', intval($data->total_post));
    }
}
public function most_active_channels($input){
        $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
        $query="SELECT escchm.chmname AS channel, SUM( TotalLikes + TotalComment + TotalShare + TotalViews +TotalDownloads ) AS Activity FROM escchm
        LEFT  JOIN

        (
        SELECT escchp.chpchmrefid, escchp.chprefid , IFNULL(L.TotalLike,0) AS TotalLikes,     IFNULL(C.TotalComment,0) AS TotalComment, IFNULL(S.TotalShare,0) AS TotalShare, IFNULL(V.TotalViews,0) AS TotalViews , IFNULL(D.TotalDownloads,0) AS TotalDownloads from escchp 

        LEFT OUTER JOIN 
        (
        SELECT escchp.chprefid AS postid ,count(esclom.lomrefid) as TotalLike from  escchp
        JOIN esclom ON esclom.lomobjrefid = escchp.chprefid
        WHERE esclom.lomobjtype='ESCCHP'  
        GROUP BY escchp.chprefid  
        ) L
        ON L.postid = escchp.chprefid
        LEFT JOIN
        (
        SELECT escchp.chprefid AS postid ,count(esccpc.cpcrefid) as TotalComment from  escchp
        LEFT JOIN esccpc ON esccpc.cpcchprefid = escchp.chprefid
        GROUP BY escchp.chprefid 
        ) C
        ON C.postid = escchp.chprefid

        LEFT JOIN
        (
        SELECT  escchp.chprefid as postid,count(eschso.hsorefid) as TotalShare from  escchp
        LEFT JOIN eschso ON eschso.hsoobjrefid = escchp.chprefid
        WHERE eschso.hsoobjtype='ESCCHP'  
        GROUP BY escchp.chprefid
        ) S
        ON S.postid = escchp.chprefid
        LEFT JOIN
        (
        SELECT  escchp.chprefid as postid, COUNT(escobt.obtrefid) AS TotalViews FROM escchp 
        LEFT JOIN escobt ON escobt.obtobjectid = escchp.chprefid 
        WHERE escobt.obtobjecttype ='ESCCHP' GROUP BY escchp.chprefid 
        ) V
        ON V.postid = escchp.chprefid
        LEFT JOIN 
        ( SELECT eschdl.hdldomrefid as postid ,COUNT(eschdl.hdlrefid) as TotalDownloads FROM escchp
        LEFT JOIN  eschdl ON eschdl.hdldomrefid = escchp.chprefid  

        GROUP BY escchp.chprefid
        ) D

        ON D.postid = escchp.chprefid
        ) post
        on post.chpchmrefid = escchm.chmrefid
        GROUP BY escchm.chmname HAVING  Activity > 0 ORDER BY Activity DESC LIMIT 0,3";


        $this->db->db_debug = false;

        if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $this->db->error();
        return_data('false', $error['message'], array());
        }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();

        if($data){

        $i=0;
        foreach ($data as $key => $value) {
        $region_data[$i]['channel'] = $value['channel'];
        $region_data[$i]['count'] = intval($value['Activity']) ;
        $i++;
        }
        }
        return_data(true, 'Most Active Channels fetched  sucessfully', $region_data);
        }
}
public function trn_count($input){ #pre($input);
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);   
    #$SLAVE_DB->from('escchm');
    #$query = $SLAVE_DB->get();
    $query = "SELECT COUNT(*) as training_count FROM `clt_trn` WHERE `isvisibleonclient`=1;";
    $CHALKLIT_SLAVE->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());     
    }else{
      $training_count =   $CHALKLIT_SLAVE->query($query)->row();
        return_data(true, 'Total training fetched sucessfully', $training_count->training_count);
    }
}
public function trn_total_teachers($input){
        
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "select count(distinct rcuusmrefid) teacher from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn on(trbtrnrefid = trnrefid) where isvisibleonclient = 1;";
    $CHALKLIT_SLAVE->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $CHALKLIT_SLAVE->error();
      return_data('false', $error['message'], array());     
    }else{
      $teacher_count =   $CHALKLIT_SLAVE->query($query)->row();
      return_data(true, 'Total Teachers fetched sucessfully', $teacher_count->teacher);
    }
}
public function state_wise_training_last_28_days($input){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query="select count(*)`totaltrn`,statename from clt_trn A join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 and (A.regstartdate >= date_add(date(now()), Interval -28 day) or A.regenddate >= date_add(date(now()), Interval -28 day) or A.startdate >= date_add(date(now()), Interval -28 day) or A.enddate >= date_add(date(now()), Interval -28 day)) group by A.trnstaterefid order by statename;";
    $CHALKLIT_SLAVE->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
       $error = $CHALKLIT_SLAVE->error();
       return_data('false', $error['message'], array());
    }else{
      $data =   $CHALKLIT_SLAVE->query($query)->result_array();
      if($data){
        $i=0;
        foreach ($data as $key => $value) {
				if($value['statename']=='Odisha'){
				$value['statename'] = 'Orissa';
				}
				if($value['statename'] == "National Capital Territory of Delhi"){
				$value['statename'] = "Delhi";
				}
          $region_data[$i]['state'] = $value['statename'];
          $region_data[$i]['trn'] = intval($value['totaltrn']) ;
          $i++;         
        }
      }
    return_data(true, 'State wise training fetched last 28 days sucessfully', $region_data);
    }
}
public function statewise_teacher_trained($input){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query ="select statename ,count(distinct rcuusmrefid) teacher from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn A on(trbtrnrefid = trnrefid) join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 group by A.trnstaterefid order by statename;";
    $CHALKLIT_SLAVE->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $CHALKLIT_SLAVE->error();
      return_data('false', $error['message'], array());     
    }else{
      $data =   $CHALKLIT_SLAVE->query($query)->result_array();
      $i=0;
      foreach($data as $key => $value) {
        if($value['statename'] == "National Capital Territory of Delhi"){
          $value['statename'] = "Delhi";
        }
		if($value['statename']=='Odisha'){
		   $value['statename'] = 'Orissa';
	    }
        $resul_array[$i]['state'] = $value['statename'];
        $resul_array[$i]['teacher'] = intval($value['teacher']);
        $i++;
      }
    return_data(true, 'state wise teacher trained fetched sucessfully', $resul_array);
    }
}
public function trn_state_school_covered($input){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "select statename ,count(distinct usmschoolname) AS school from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn A on(trbtrnrefid = trnrefid) join clt_mat_state B on(A.trnstaterefid = B.staterefid) join escusm on(rcuusmrefid = usmrefid) where isvisibleonclient = 1 and ifnull(usmschoolname,'') <> '' group by A.trnstaterefid order by statename;";
    $CHALKLIT_SLAVE->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $CHALKLIT_SLAVE->error();
      return_data('false', $error['message'], array());     
    }else{
      $data =   $CHALKLIT_SLAVE->query($query)->result_array();     
      $i=0;
      foreach ($data as $key => $value) {
                if($value['statename'] == "National Capital Territory of Delhi"){
                    $value['statename'] = "Delhi";
                }
				if($value['statename']=='Odisha'){
					$value['statename'] = 'Orissa';
				}
        $region_data[$i]['state'] = $value['statename'];
        $region_data[$i]['school'] = intval($value['school']);
        $i++;
      }
       // echo "<pre>";    print_r($region_data);die;
    return_data(true, 'Post by source fetched sucessfully', $region_data);
    }
}
public function trn_state_wise_count($input){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query="select count(*)`totaltrn`,statename from clt_trn A join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 group by A.trnstaterefid order by statename;";
    $CHALKLIT_SLAVE->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $CHALKLIT_SLAVE->error();
      return_data('false', $error['message'], array());
    }else{
      $data =   $CHALKLIT_SLAVE->query($query)->result_array();
      $i=0;
      foreach ($data as $key => $value) {
        if($value['statename'] == "National Capital Territory of Delhi"){
          $value['statename'] = "Delhi";
        }
		if($value['statename']=='Odisha'){
		 $value['statename'] = 'Orissa';
		}
        $region_data[$i]['state'] = $value['statename'];
        $region_data[$i]['trn'] = intval($value['totaltrn']) ;
        $i++;
      }
      return_data(true, 'State wise training fetched sucessfully', $region_data);
    }
}
public function statewise_teacher_trained_last_28_days($input){
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
      $query = "select statename , count(distinct rcuusmrefid) as teacher from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn A on(trbtrnrefid = trnrefid) join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 and (A.regstartdate >= date_add(date(now()), Interval -28 day) or A.regenddate >= date_add(date(now()), Interval -28 day) or A.startdate >= date_add(date(now()), Interval -28 day) or A.enddate >= date_add(date(now()), Interval -28 day)) group by A.trnstaterefid order by statename;";
      $CHALKLIT_SLAVE->db_debug = false;
    if(!@$CHALKLIT_SLAVE->query($query)){
      $error = $this->db->error();
      return_data('false', $error['message'], array());          
    }else{
      $data =   $this->db->query($query)->result_array();
            $i=0;
      foreach($data as $key => $value) {
        if($value['statename'] == "National Capital Territory of Delhi"){
          $value['statename'] = "Delhi";
        }
		if($value['statename']=='Odisha'){
			$value['statename'] = 'Orissa';
		}
        $resul_array[$i]['state'] = $value['statename'];
        $resul_array[$i]['teacher'] = intval($value['teacher']);
        $i++;
      }
    return_data(true, 'state wise teacher trained fetched sucessfully', $resul_array);
    }
}
	
	public function user_activity_last_seven_days(){
	
    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
	
	$query = "SELECT
    SUM(if(activityprofile.appsection = 'Wall',activityprofile.totalactivities,0)) `Wall`,
    SUM(if(activityprofile.appsection = 'Training',activityprofile.totalactivities,0)) `Training`,
    SUM(if(activityprofile.appsection = 'Lesson',activityprofile.totalactivities,0)) `Lesson`,
    DATE_FORMAT(activityprofile.activitydate ,'%M %d') AS activity_date FROM chalklit_analysis.activityprofile WHERE activityprofile.activitydate >  CURDATE() - INTERVAL 7 DAY 
    GROUP BY activity_date";	
	$CHALKLIT_SLAVE->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
    $error = $CHALKLIT_SLAVE->error();
    return_data('false', $error['message'], array());
    }else{
    $data =   $CHALKLIT_SLAVE->query($query)->result_array();
	//print_r($data);die;
    $i=0;
    foreach ($data as $key => $value) {
        
    $activity[$i]['activity_date']            = $value['activity_date'];
    $activity[$i]['Wall']         = (int) $value['Wall'];
	$activity[$i]['Training']         = (int) $value['Training'];
	$activity[$i]['Lesson']         = (int) $value['Lesson'];
	
	
	
    $i++;
    }
	
	foreach ($activity as $key => $part) {
	   $sort[$key] = strtotime($part['activity_date']);
	}
	array_multisort($sort, SORT_ASC, $activity);
		
    return_data(true, 'Last seven days activity fetched successfully', $activity);
    }
}
public function top_sources(){

    $CHALKLIT_SLAVE = $this->load->database('chalklit_slave', TRUE);
    $query = "SELECT cursrc.srcname AS source , COUNT(`rchrefid`) total_post FROM `currch` 
    JOIN cursrc on cursrc.srcrefid = currch.rchsrcrefid GROUP BY currch.rchsrcrefid ORDER BY total_post DESC LIMIT 0,20 ";
    $this->db->db_debug = false;

    if(!@$CHALKLIT_SLAVE->query($query)){
        $error = $CHALKLIT_SLAVE->error();
        return_data('false', $error['message'], array());
    }else{
        $data =   $CHALKLIT_SLAVE->query($query)->result_array();
        
        $i=0;
        foreach ($data as $key => $value) {
            if($value['source']==''){
                $value['source']='Anonymous';
            }
        $source[$i]['source']  = $value['source'];
        $source[$i]['count'] = intval($value['total_post']);
        $i++;
        }
        
        return_data(true, 'Top Five Sources fetched successfully', $source);
    }
}
public function beautifyTime($value){

    if($value=='0'){
        return "0 AM";
    }elseif($value=='1' || $value=='01'){
        return "1 AM";
    }elseif($value=='2'|| $value=='02'){
        return "2 AM";
    }elseif($value=='3'|| $value=='03' ){
        return "3 AM";
    }elseif($value=='4'){
        return "4 AM";
    }elseif($value=='5'){
        return "5 AM";
    }elseif($value=='6'){
        return "6 AM";
    }elseif($value=='7'){
        return "7 AM";
    }elseif($value=='8'){
        return "8 AM";
    }elseif($value=='9'){
        return "9 AM";
    }elseif($value=='10'){
        return "10 AM";
    }elseif($value=='11'){
        return "11 AM";
    }elseif($value=='12'){
        return "12 AM";
    }elseif($value=='13'){
        return "1 PM";
    }elseif($value=='14'){
        return "2 PM";
    }elseif($value=='15'){
        return "3 PM";
    }elseif($value=='16'){
        return "4 PM";
    }elseif($value=='17'){
        return "5 PM";
    }elseif($value=='18'){
        return "6 PM";
    }elseif($value=='19'){
        return "7 PM";
    }elseif($value=='20'){
        return "8 PM";
    }elseif($value=='21'){
        return "9 PM";
    }elseif($value=='22'){
        return "10 PM";
    }elseif($value=='23'){
        return "11 PM";
    }else{
        return $value;
    }
}
public  function ordinal($number){
		$ends = array('th','st','nd','rd','th','th','th','th','th','th');
		if ((($number % 100) >= 11) && (($number%100) <= 13)){
			return $number. 'th';
		}elseif($number < 0){
			return $number;
		}else{
			return $number. $ends[$number % 10];
		}
		
	}
       
}
