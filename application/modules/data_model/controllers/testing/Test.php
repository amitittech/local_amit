<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();        
        $this->load->helper('custom_helper');
        $this->load->helper('push');
        $this->load->helper('array'); 
        $this->load->library('email');       
    }
	
	public function file_get(){
		$url 		= "/var/log/kibana/kibana.stdout";"http://localhost:5601/";
		$data 		= file_get_contents($url); 
		pre($data);
	}
	
	public function kibana_stderr_log(){
		$url 		= "/var/log/kibana/kibana.stderr";
		$data 		= file_get_contents($url); 
		pre($data);
	}
	
	public function kibana_stdout_log(){
		$url 		= "/var/log/kibana/kibana.stdout";
		$data 		= file_get_contents($url); 
		pre($data);
		
	}

	public function curl(){
		#$url 		= "http://localhost:9200/.kibana";
		$ch 		= curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$result 	= curl_exec($ch);
		$httpCode 	= curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return $result;
	}
    
    public function index() {
		$testing = ["timing" => date('Y-m-d H:i:s')];
		$this->db->db_select('esocial_log');
		$this->db->insert('cronjob',$testing);		
		mail("meittech10@gmail.com","my subject","my subject");
		$this->db->db_select('chalklit');
		echo "Sent";
	}
	
	
    public function push() { 
        //$this->Notification_model->reallocation_push_to_user();
        $input                  = $this->input->post();           
        $message                = "This is the Klosebuyer testing notification.";
        $details['message']     = $message;
        $details['push_type']   = 1;
        generatePush(1,'eAk4iVwsL3Y:APA91bHLQVtJI-hFpXx5AdVJoRXjEndVpKgJBljfZBdZkoUdhN2S2gibhNBfQFVqbVTMaq2QnOalWykMRlR8BibFIEXgC097OJn6S62GVCBOlrMIrkVgnHoyOA6Q1JjImqLCyJLfZech',$details);
        //echo 'Success';
                
    }

     public function send_mail(){
        $to=$subject=$message=$from='';

        
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'mygmail@gmail.com';
        $config['smtp_pass']    = '*******';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'text'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);


        $this->email->from($from, 'test');
        $this->email->to($to); 

        $this->email->subject($subject);
        $this->email->message($message);  

        $this->email->send();

        echo $this->email->print_debugger();
        //$this->load->view('email_view');
    }



}