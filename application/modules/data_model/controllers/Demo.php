<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('custom');
    }

    public function demo() {
        $inputs 					= check_inputs();
		$response 					= call_api("POST",site_url().'/'.$inputs['opcode'],json_encode($inputs));
		$response 					= json_decode($response);
		pre($response);
    }
	
}
