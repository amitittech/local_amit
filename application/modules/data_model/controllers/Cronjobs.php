<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Elasticsearch\ClientBuilder;
require APPPATH . 'third_party/elasticsearch/vendor/autoload.php';

class Cronjobs extends MX_Controller {
    
    function __construct() {
        parent::__construct();   
		$this->load->helper('custom');
    }

	/*
    public function testing_method() {
		$testing = ["timing" => date('Y-m-d H:i:s')];
		$this->db->db_select('esocial_log');
		$this->db->insert('cronjob',$testing);		
		mail("meittech10@gmail.com","my subject","my subject");
		$this->db->db_select('chalklit');
		echo "Sent";
	}
	*/
	
    public function synchronize_elasticsearch_with_database() {
		$this->delete_indexes();
		$this->create_indexes();
		$response	= $this->synchronize_documents();		
		#$response 	= json_encode($response);		
		
		$logdata 	= json_encode(["timing" => date('Y-m-d H:i:s'),"cronjob" =>"synchronize_elasticsearch_with_database","response" =>$response],JSON_PRETTY_PRINT). PHP_EOL . PHP_EOL . "*******************************************************************";
		file_put_contents('logs/cronjobs/cronjob_synch_elastic_WallTrainingResourcs.txt', $logdata, FILE_APPEND);
		/*
		$this->db->db_select('esocial_log');
		$this->db->insert('cronjob',$testing);		
		$this->db->db_select('chalklit');
		
		echo $response;
		*/
	}
	
	function delete_indexes() {
		
        $client 	= ClientBuilder::create()->build();
		
		//delete walls
		$exist 		= $client->indices()->exists(array("index"=>"cl_wall"));
		if($exist){
			$response 	= $client->indices()->delete(array("index"=>"cl_wall"));
		}
        
		//delete trainings
		$exist 		= $client->indices()->exists(array("index"=>"cl_trainings"));
		if($exist){
			$response 	= $client->indices()->delete(array("index"=>"cl_trainings"));
		}
		
		//delete resources
		$exist 		= $client->indices()->exists(array("index"=>"cl_resources"));
		if($exist){
			$response 	= $client->indices()->delete(array("index"=>"cl_resources"));
		}        
    }
	
	function create_indexes() {
		//create wall
		$wall 		= $this->get_wall_mapping_structure();
		$this->create_index_with_mappings($wall);
		
		//create trainings
		$trainings 	= $this->get_trainings_mapping_structure();
        $this->create_index_with_mappings($trainings);
		
		//create resources
		$resources 	= $this->get_resources_mapping_structure();
        $this->create_index_with_mappings($resources);		
    }
	
	function get_wall_mapping_structure() {
		$mapping_json ='{
			"index_name"	: "cl_wall",
			"type_name"	:"post",
			"number_of_shards":"5",
			"number_of_replicas":"1",
			"fields" : [
				{"field_name":"post_id","field_type":"integer"},
				{"field_name":"post_title","field_type":"text"},
				{"field_name":"post_description","field_type":"text"},
				{"field_name":"channel_name","field_type":"text"},
				{"field_name":"channel_logo","field_type":"keyword"},
				{"field_name":"audiencesetid","field_type":"integer"}
			]
		}';
		return json_decode($mapping_json,true);
	}
	
	function get_trainings_mapping_structure() {
		$mapping_json ='{
			"index_name"	: "cl_trainings",
			"type_name"	:"post",
			"number_of_shards":"5",
			"number_of_replicas":"1",
			"fields" : [
				{"field_name":"post_id","field_type":"integer"},
				{"field_name":"post_title","field_type":"text"},
				{"field_name":"post_description","field_type":"text"},
				{"field_name":"chapter_id","field_type":"integer"},
				{"field_name":"chapter_name","field_type":"text"},
				{"field_name":"class","field_type":"text"},
				{"field_name":"subject","field_type":"text"},
				{"field_name":"language","field_type":"text"},
				{"field_name":"module_id","field_type":"integer"},
				{"field_name":"module_title","field_type":"text"},
				{"field_name":"trbrefids","field_type":"object"},
				{"field_name":"chapter_logo","field_type":"keyword"},
				{"field_name":"module_logo","field_type":"keyword"},
				{"field_name":"post_logo","field_type":"keyword"}
			]
		}';
		return json_decode($mapping_json,true);
	}
	
	function get_resources_mapping_structure() {
		$mapping_json = '{
			"index_name"	: "cl_resources",
			"type_name"	:"post",
			"number_of_shards":"5",
			"number_of_replicas":"1",
			"fields" : [
				{"field_name":"post_id","field_type":"integer"},
				{"field_name":"post_title","field_type":"text"},
				{"field_name":"post_description","field_type":"text"},
				{"field_name":"chapter_id","field_type":"integer"},
				{"field_name":"chapter_name","field_type":"text"},
				{"field_name":"class","field_type":"text"},
				{"field_name":"subject","field_type":"text"},
				{"field_name":"language","field_type":"text"},
				{"field_name":"module_id","field_type":"integer"},
				{"field_name":"module_title","field_type":"text"},
				{"field_name":"chapter_logo","field_type":"keyword"},
				{"field_name":"module_logo","field_type":"keyword"},
				{"field_name":"post_logo","field_type":"keyword"}
			]
		}';
		return json_decode($mapping_json,true);
	}
	
	public function create_index_with_mappings($input) {
        foreach($input['fields'] as $each){
            $properties[$each['field_name']] = ["type"=>$each['field_type']]; 
        }
        $params = [
			'index' => $input['index_name'],
			'body' => [
				'settings' => [
					'number_of_shards' => $input['number_of_shards'],
					'number_of_replicas' => $input['number_of_replicas'],
					"analysis" => [
						"analyzer" => [
							"english_stop" => [
								"type" 		=> "standard",
								"stopwords" => "_english_"
							]
						]
					]	
				],
				'mappings' => [
					$input['type_name'] => [
						'_source' => [
							'enabled' => true
						],
						'properties' =>$properties
					]
				]
			]
		];                        
        $client 	= ClientBuilder::create()->build();
        $response 	= $client->indices()->create($params);
        //echo json_encode($response);
    }
	
	public function create_search_log_index() {
		$client 	= ClientBuilder::create()->build();
		$exist 		= $client->indices()->exists(array("index"=>"cl_search"));
		pre($exist); die;
		if($exist){
			//$response 	= $client->indices()->delete(array("index"=>"cl_trainings"));
		}
		
		$mapping_json ='{
			"index_name"		: "cl_search",
			"type_name"			:"clt_his_search",
			"number_of_shards"	:"5",
			"number_of_replicas":"1",
			"fields" 			: [
				{"field_name":"searchrefid","field_type":"integer"},
				{"field_name":"usmrefid","field_type":"integer"},
				{"field_name":"platform","field_type":"text"},
				{"field_name":"searchphrase","field_type":"text"},
				{"field_name":"searchsection","field_type":"text"},
				{"field_name":"searchresultcount","field_type":"integer"},
				{"field_name":"searchedon","field_type":"date","format":"yyyy-MM-dd HH:mm:ss"}
			]
		}';
		$input = json_decode($mapping_json,true);
		foreach($input['fields'] as $each){
            $properties[$each['field_name']] = ["type"=>$each['field_type']]; 
        }
        $params = [
			'index' => $input['index_name'],
			'body' => [
				'settings' => [
					'number_of_shards' => $input['number_of_shards'],
					'number_of_replicas' => $input['number_of_replicas']
				],
				'mappings' => [
					$input['type_name'] => [
						'_source' => [
							'enabled' => true
						],
						'properties' =>$properties
					]
				]
			]
		];                        
        //$client 	= ClientBuilder::create()->build();
        $response 	= $client->indices()->create($params);		
	}
	
	
	
	public function synchronize_documents() {
		$wall 		= $this->synchronize_wall();
		$trainings 	= $this->synchronize_trainings();
		$resources 	= $this->synchronize_resources();
		
		return array("wall"=>$wall,"trainings"=>$trainings,"resources"=>$resources);
	}

    public function synchronize_wall() {
        $tt 	= time();
		
		$query  = "SELECT chprefid,chptopic,chpcontent,chmname,chmlogo,chpqutrefid FROM `escchp` join `escchm` ON `escchm`.`chmrefid`=`escchp`.`chpchmrefid` join escqut ON escqut.qutrefid=escchp.chpqutrefid where escchp.isdeleted=0 and escchp.isactive=1 and (escchp.expirydate > now() OR escchp.expirydate is NULL) and (escqut.expirydate > now() OR escqut.expirydate is NULL)";
		$result = $this->db->query($query)->result_array();	
		
        #$this->db->select('chprefid,chptopic,chpcontent,chmname,chmlogo,chpqutrefid');
        #$this->db->where('escchp.isdeleted', 0);
        //$this->db->where('escchm.isdeleted',0);
        #$this->db->join('escchm', 'escchm.chmrefid=escchp.chpchmrefid');
        #$result = $this->db->get('escchp')->result_array();
		
        $i 		= 1;
        foreach ($result as $each) {
            $i 	= $i + 1;
            $body = array(
                "post_id" 		=> (int)$each['chprefid'],
                "post_title" 	=> $each['chptopic'],
                "post_description" => $each['chpcontent'],
                "channel_name" 	=> $each['chmname'],
                "channel_logo" 	=>(!empty($each['chmlogo']))?"http://www.qa.chalklit.in/chalklitone/resources/newhome/images/".$each['chmlogo']:"",
                "audiencesetid" => (int)$each['chpqutrefid']
            );
			
            $params['index'] 	= 'cl_wall';
            $params['type'] 	= 'post';
            $params['id'] 		= $each['chprefid'];
            $params['body'] 	= $body;
            
            $client 			= ClientBuilder::create()->build();
            $response 			= $client->index($params);
        }
        $response = array("time_taken_in_seconds" => time() - $tt, "total records" => $i);
		return $response;
        //echo json_encode($response);
        //{"time_taken_in_seconds":8,"total records":982}
    }

    public function synchronize_resources() {
        /*
          case
          when type = 'video' then concat('http:',thumbnailurl)
          else ifnull(thumbnailurl,'')
          end `postlogo`
         */
        $tt = time();
        $this->db->select("rchrefid,title,description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,isfortraining,rcgrefid,groupname,case
  when type = 'video' then concat('http:',thumbnailurl)
  else ifnull(thumbnailurl,'')    
end postlogo");
        $this->db->where('currcg.isdeleted', 0);
        $this->db->where('currch.isdeleted', 0);
        $this->db->where('currcm.isfortraining', 0);
        $this->db->where('currcm.isforreference', 0);
        $this->db->where('currcm.islive', 1);
        $this->db->join('currcg', 'currcg.rcgrcmrefid=currcm.rcmrefid');
        $this->db->join('currch', 'currch.rchrcgrefid=currcg.rcgrefid');
        $result = $this->db->get('currcm')->result_array();
        $i 		= 1;
        foreach ($result as $each) {
            $i 	= $i + 1;
            $params['index'] = 'cl_resources';
            $params['type'] = 'post';
            $params['id'] = $each['rchrefid'];
            $params['body'] = [
                "post_id" => (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
                "post_title" => (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
                "post_description" => (isset($each['description']) && !empty($each['description'])) ? $each['description'] : "",
                "chapter_id" => (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
                "chapter_name" => (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
                "class" => (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
                "subject" => (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
                "language" => (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
                "module_id" => (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
                "module_title" => (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
                "chapter_logo" => "",
                "module_logo" => "",
                "post_logo" => (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : ""
            ];
            $client = ClientBuilder::create()->build();
            $response = $client->index($params);
        }
        $response = array("time_taken_in_seconds" => time() - $tt, "total records" => $i);
		return $response;
        //echo json_encode($response);
        //{"time_taken_in_seconds":28,"total records":3818}
    }

    public function synchronize_trainings() {
        $tt = time();
        $this->db->select("rchrefid,title,description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,rcgrefid,groupname,case
  when type = 'video' then concat('http:',thumbnailurl) else ifnull(thumbnailurl,'') end postlogo");
        $this->db->where('currcg.isdeleted', 0);
        $this->db->where('currch.isdeleted', 0);
        $this->db->where('currcm.isfortraining', 1);
        $this->db->where('`currcm`.`rcmrefid` IN (select distinct rcurcmrefid from currcu A join curtrb B on (A.rcutrbrefid = B.trbrefid) where A.isactive = 1 and B.isdeleted = 0)',NULL,FALSE);
        $this->db->join('currcg', 'currcg.rcgrcmrefid=currcm.rcmrefid');
        $this->db->join('currch', 'currch.rchrcgrefid=currcg.rcgrefid');
        $result = $this->db->get('currcm')->result_array();
        $i 		= 1;
        foreach ($result as $each) {
            //$q = "SELECT distinct rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $each['rcmrefid'] . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";
            $q  				= "SELECT rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $each['rcmrefid'] . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";           
            
            $res 				= $this->db->query($q)->row_array();
            if (!$res OR empty($res['trbrefids'])) {
                continue;
            }
            $trb 				= (object)array_map('intval', explode(',', $res['trbrefids']));
            $i 					= $i + 1;
            $params['index'] 	= 'cl_trainings';
            $params['type'] 	= 'post';
            $params['id'] 		= $each['rchrefid'];
            $params['body'] 	= [
                "post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
                "post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
                "post_description" 	=> (isset($each['description']) && !empty($each['description'])) ? $each['description'] : "",
                "chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
                "chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
                "class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
                "subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
                "language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
                "module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
                "module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
                "trbrefids" 		=> $trb,
                "chapter_logo" 		=> "",
                "module_logo" 		=> "",
                "post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : ""
            ];
            $client = ClientBuilder::create()->build();
            $response = $client->index($params);
        }
        $response = array("time_taken_in_seconds" => time() - $tt, "total records" => $i);
		return $response;
        //echo json_encode($response);
        //{"time_taken_in_seconds":8,"total records":883}
    }
	
}