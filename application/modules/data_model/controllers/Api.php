<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }

    public function index() {

        $start_time = milliseconds();
        $inputs     = check_inputs(); //pre($inputs);
        $done = modules::run('data_model/logs_creater/Logs_creater/save_api_log_txt_file', $inputs); #creating log file
        #$saved 					= $this->save_api_log($inputs); 		#creating log file
        $active_user = $this->check_authentication($inputs); #AUTH TOKEN HANDLING #login_id 
        $inputs['login_id'] = $active_user['login_id'];

        $op = $this->opcode_handling($inputs);   #OPCODE HANDLING		

        $response = modules::run('data_model/' . $op['directory'] . '/' . $op['controller_class'] . '/' . $inputs['opcode'], $inputs);

        #$header_data 				= $_SERVER; #$this->input->request_headers(); #getallheaders(); #header values
        #$done = modules::run('data_model/Logs_creater/Logs_creater/activity_log', $_SERVER, $inputs, $response, $start_time);
        #$loged 					= $this->activity_log($active_user,$inputs,$response,$start_time);
        if ($response['status'] == true) {
            return_data(API_RESPONSE_STATUS_TRUE, $response['message'], $response['result']);
        }
        return_data('fail', $response['message'], (isset($response['result'])) ? $response['result'] : []);
    }

    //-------------**  CHECK AUTHTOKEN/SESSION FOR AUTHENTICATION PURPOSE  **-----------------------------//
    public function check_authentication($input) {
        if (isset($input['login_id'])) {
            return array("login_id" => $input['login_id']);
        } else {
            if (!isset($input['authtoken'])) {
                if (isset($input['opcode']) && $input['opcode'] == "cl_search") {
                    return_data('fail', ELASTICSEARCH_ERROR_MESSAGE, array());
                }
                return_data('fail', "You are not athorised to use this api.", array());
            } else {
                $this->db->where('accesstoken', $input['authtoken']);
                $exist = $this->db->get('esculm')->row_array();
                if ($exist) {
                    return array('login_id' => $exist['ulmusmrefid']);
                } else {
                    if (isset($input['opcode']) && $input['opcode'] == "cl_search") {
                        return_data('fail', ELASTICSEARCH_ERROR_MESSAGE, array());
                    }
                    return_data('fail', "You are not athorised to use this api.", array());
                }
            }
        }
        return_data('fail', "You are not athorised to use this api.", array());
    }

    function opcode_handling($inputs) {
        if ((!isset($inputs['opcode']) OR empty($inputs['opcode']))) {
            return_data(false, 'opcode was not matched', array());
        }
        #$mapping['myopcode'] 					= "directory/controller/method";
        $directory['search'] = ['Cl_search'];
        $directory['certificates'] = ['Training_certificates'];
        $directory['courses'] = ['Training_courses', 'Course_mapping'];
        $directory['sender_id'] = ['Sender_id', 'Sender_id_mapping'];
        $directory['trainings'] = ['Trainings', 'Training_visibility'];
        $directory['users'] = ['Users', 'User_mapping', 'Manage_rights'];
        $directory['wall'] = ['Wall_channels', 'Wall_posting_users'];
        $directory['report'] = ['Report'];
        $directory['story'] = ['Story'];
        $directory['carousel'] = ['Carousel'];
        $directory['custom'] = ['Custom'];
        $directory['roles'] = ['Role_mgmt'];

        $classes['Cl_search'] = ['cl_search'];
        $classes['Training_certificates'] = ['map_certificates_to_trainings', 'list_of_mapping_certificates_to_trainings', 'delete_certificates_mapping', 'certificates_list', 'certificate_approves', 'certificate_approves_list', 'delete_certificate_approve'];
        $classes['Training_courses'] = ["get_training_courses"];
        $classes['Course_mapping'] = ['training_n_courses_mapping', 'training_n_courses_mapping_list', 'delete_courses_training_mapping'];
        $classes['Sender_id'] = ['sender_id_list', 'add_sender_id', 'delete_sender_id', 'edit_sender_id'];
        $classes['Sender_id_mapping'] = ['sender_id_mapping_list', 'map_sender_id', 'delete_sender_id_mapping'];
        $classes['Trainings'] = ['ti_training_user_operation', 'get_training_agencies','ti_info', 'training_list', 'add_training', 'edit_training', 'get_training_instances', 'get_training_modules', 'clt_get_comments_by_object', 'clt_get_data_tra', 'ti_training_user_operation','tra_ti_list','get_tra_selected_report','get_tra_aggregated_report'];
        $classes['Training_visibti_training_user_operationility'] = ['get_training_toggle_visibility', 'update_training_toggle_visibility'];
        $classes['Users'] = ['users_list_by_role', 'cl_fetch_master_data'];
        $classes['User_mapping'] = ['map_users_to_trainings', 'list_of_mapping_users_to_trainings', 'delete_users_mapping'];
        $classes['Manage_rights'] = ['add_rights', 'list_of_mapping_rights', 'delete_instance_rights_mapping'];
        $classes['Wall_channels'] = ['get_channel_list', 'master_channel_list', 'edit_channel', 'add_channel', 'delete_channels'];
        $classes['Wall_posting_users'] = ['get_wall_posting_users', 'add_wall_poster', 'delete_wall_posters'];
        $classes['Report'] = ['get_report', 'report1'];
        $classes['Story'] = ['get_story'];
        $classes['Carousel'] = ['trn_count', 'trn_total_teachers', 'state_wise_training_last_28_days', 'statewise_teacher_trained', 'statewise_teacher_trained_last_28_days', 'trn_state_school_covered', 'trn_state_wise_count', 'activity_distribution_today', 'most_active_channels', 'wall_post', 'source_count', 'get_wall_post_type', 'content_chapter_count', 'wall_state_wise_activity', 'users_activity_hourly_today', 'users_active_last_seven_days', 'wall_activity_distribution_today', 'wall_total_activity_hourly_today', 'wall_activity_last_seven_days', 'total_users', 'total_users_today', 'top_five_sources', 'get_topics_covered', 'total_activity_today', 'total_activity_last_seven_days', 'activity_distribution', 'get_trn_partners', 'get_ongoing_trainings', 'user_activity_last_seven_days', 'top_sources', 'activity_distribution'];
        $classes['Custom'] = ['get_kibana_custom_filters', 'user_retention'];
        $classes['Role_mgmt'] = ['user_roles','get_rm_ti_glimpse_mewp','get_rm_ti_glimpse_metp','get_rm_ti_glimpse_meu','get_rm_ti_glimpse_mes'];
        foreach ($classes as $key => $class) {
            if (in_array($inputs['opcode'], $class)) {
                foreach ($directory as $key1 => $dir) {
                    if (in_array($key, $dir)) {
                        return array("directory" => $key1, "controller_class" => $key);
                    }
                }
            }
        }
        return_data(false, 'opcode was not matched', array());
    }

}
