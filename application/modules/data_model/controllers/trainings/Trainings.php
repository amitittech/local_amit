<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trainings extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
		$this->load->model('Training_model');
		$this->load->helper('custom');
		$this->load->model('Reports_model');
		$this->load->model('Training_instances_model');
    }
	
	//----------   GET LIST OF TRAINING Instances     ------------//
    public function get_training_instances($input) {
        //$this->validate_get_training_instances();
        $result = $this->Training_instances_model->get_training_instances($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    
    public function ti_info($input) {
        //$this->validate_get_training_instances();
        $result = $this->Training_instances_model->ti_info($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
    //----------GET LIST OF TRAINING AGENCIES----------- //    
    public function get_training_agencies($input) {
		//$this->validate_get_training_agencies();
        $result = $this->Training_model->get_training_agencies($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

	public function training_list($input) {
        $result = $this->Training_model->training_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
	public function add_training($inputs) {
        if (empty($_FILES['trasplash']['name'])) {
            return_data(false, 'trasplash is required in array format.', array());
        }
        if (empty($_FILES['tralogo']['name'])) {
            return array("status" => false, "message" => 'tralogo is required in array format.', "result" => array());
        }
        if (!isset($inputs['traname']) OR empty($inputs['traname'])) {
            return array("status" => false, "message" => 'traname is required.', "result" => array());
        }

        $result = $this->Training_model->get_training($inputs);
        if ($result) {
            return_data('fail', 'Training already exist.', array());
        }
        $result = $this->Training_model->add_training($inputs);
        if ($result['status'] == 'success') {
            $result = $this->Training_model->get_training($inputs);
            return array("status" => true, "message" => 'Training created successfully.', "result" => $result);
        } elseif ($result['status'] == 'fail') {
            return array("status" => false, "message" => $result['message'], "result" => array());
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function edit_training($inputs) {
        if (!isset($inputs['trarefid']) OR empty($inputs['trarefid'])) {
            return array("status" => false, "message" => 'trarefid is required.', "result" => array());
        }
        if (!isset($inputs['traname']) OR empty($inputs['traname'])) {
            return array("status" => false, "message" => 'training field is required.', "result" => array());
        }

        $exit = $this->Training_model->get_by_trarefid($inputs);

        if (!empty($exit)) {
            $check_duplicate = $this->Training_model->get_training($inputs);
            if ($check_duplicate) {
                return array("status" => false, "message" => 'Training already exists.', "result" => array());
            }
            $result = $this->Training_model->edit_training($inputs);

            if ($result['status'] == 'success') {
                return array("status" => true, "message" => 'Training created successfully.', "result" => $result['result']);
            } elseif ($result['status'] == 'fail') {
                return array("status" => false, "message" => $result['message'], "result" => array());
            }
        } else {
            return array("status" => false, "message" => 'trarefid not matched.', "result" => array());
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
	
	public function get_training_modules($inputs){

        if(!isset($inputs['traningId']) OR empty($inputs['traningId'])){
            return array("status"=>false,"message"=>'Traning Id field is required.',"result"=>array());
        } 

       //$training = $this->Reports_model->checkTraning($inputs['traningId']);
        $Modules = $this->Reports_model->getTrainingInstanceModules($inputs['traningId']);
        
        if($Modules){

            return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>$Modules);
        }
          return array("status"=>false,"message"=>'No module found for this Training Instance.',"result"=>array());
    }

    

    public function deleteUser($inputs){

        foreach($inputs['users'] as  $userid){

           $data['isactive'] = '0';
           $data['deactivedate'] = date('Y-m-d H:i:s');
           $data['extrainfo'] = 'Deleted from panel';
           $this->db->where('rcurefid',$userid);
           $update = $this->db->update('currcu',$data);
        }
        return true;
    }
    public function updateUser($inputs){

        foreach($inputs['users'] as  $userid){

           $data['rcurole'] = trim($inputs['rcurole']);
           $data['extrainfo'] = 'Updated from panel';
           $this->db->where('rcurefid',$userid);
           $update = $this->db->update('currcu',$data);
        }
        return true;
    }

    public function clt_get_comments_by_object($inputs){

       
        if(!isset($inputs['objtype']) OR empty($inputs['objtype'])){
            return array("status"=>false,"message"=>'objtype  field is required.',"result"=>array());
        }
        if(!isset($inputs['objid']) OR empty($inputs['objid'])){
            return array("status"=>false,"message"=>'objid  field is required.',"result"=>array());
        }
        if(!isset($inputs['fromdate']) OR empty($inputs['fromdate'])){
            return array("status"=>false,"message"=>'fromdate  field is required.',"result"=>array());
        }
        if(!isset($inputs['todate']) OR empty($inputs['todate'])){
            return array("status"=>false,"message"=>'todate  field is required.',"result"=>array());
        }
        $objtype  = $inputs['objtype'];
        $objid    = $inputs['objid'];
        $fromdate = $inputs['fromdate'];
        $todate   = $inputs['todate'];
       
        $this->db->db_debug = false;
        $query = $this->db->query("CALL ChalkLit_Get_Comments_By_Object('$objtype','$objid','$fromdate','$todate')");

        if(!@$this->db->query($query)){

          return array("status"=>false,"message"=>'Server error.',"result"=>array());  
        }
        $comments =  $query->result_array();
        $i=0;$result_array=[];
        foreach ($comments as $key => $value) {

            $result_array[$i]['user_name']         = $value['User Name'];
            $result_array[$i]['mobile']            = intval($value['Mobile']);
            $result_array[$i]['training_instance'] = $value['Training Instance'];
            $result_array[$i]['super_batch_name']  = $value['SB Name'];
            $result_array[$i]['batch_name']        = $value['Batch Name'];
            $result_array[$i]['module_number']     = intval($value['Module Number']);
            $result_array[$i]['post_number']       = intval($value['Post Number']);
            $result_array[$i]['comment_date']      = $value['Date of Comment'];
            $result_array[$i]['comment']           = $value['Comment'];
            $result_array[$i]['url']               = $value['URL'];
            $result_array[$i]['category']          = $value['Category'];
            $result_array[$i]['is_committed']      = $value['Is Committed'];
            $i++;
        }
        // print_r($result_array);die;
        return array("status" => true, "message" => 'Comments Fetched successfully.', "result" => $result_array);
    }

    public function clt_get_data_tra($inputs){

        if( isset($inputs['traid'])){

                    $traInfo                         =  $this->Training_model->get_training_agency($inputs['traid']);
                    $course                          =  $this->Training_model->getTraCourses($inputs['traid']);
                    $certificate                     =  $this->Training_model->getTraCertificateMapping($inputs['traid']);
                    $sender                          =  $this->Training_model->getTraSenderMapping($inputs['traid']);
                    $user                            =  $this->Training_model->getTraUserMapping($inputs['traid']);
                    $data['name']                    = $traInfo['traname'];
                    $data['tralogo']                 = TARGET_PATH_TO_DISPLAY_TRA_LOGO.$traInfo['tralogo'];
                    $data['trasplash']               = TARGET_PATH_TO_DISPLAY_TRA_SPLASH.$traInfo['trasplash'];
                    $data['tra_course']              = $course;
                    $data['tra_certificate']         = $certificate;
                    $data['tra_sender']              = $sender;
                    $data['tra_user']                = $user;

                return array("status" => true, "message" => 'Training agencies data fetched sucessfully.', "result" => array($data) );
        }
           $userid     =  $this->Training_model->getUserByAuthToken($inputs['authtoken']);
           $tralist    =  $this->Training_model->getAllTraAssignToUser($userid);

      if($tralist){

            $i=0;
            foreach ($tralist as $key => $tra) {

                $traInfo                         =  $this->Training_model->get_training_agency($tra['trarefid']);
                $course                          =  $this->Training_model->getTraCourses($tra['trarefid']);
                $certificate                     =  $this->Training_model->getTraCertificateMapping($tra['trarefid']);
                $sender                          =  $this->Training_model->getTraSenderMapping($tra['trarefid']);
                $user                            =  $this->Training_model->getTraUserMapping($tra['trarefid']);
                $numer_of_ti                     =  $this->Training_model->getTrnInstance($tra['trarefid']);
                $numerof_teacher_trained         =  $this->Training_model->getTrnTotalTeacherTrained($tra['trarefid']);

                $data[$i]['name']                = $traInfo['traname'];
                $data[$i]['tralogo']             = TARGET_PATH_TO_DISPLAY_TRA_LOGO.$traInfo['tralogo'];
                $data[$i]['trasplash']           = TARGET_PATH_TO_DISPLAY_TRA_SPLASH.$traInfo['trasplash'];
                $data[$i]['tra_course']          = $course;
                $data[$i]['tra_certificate']     = $certificate;
                $data[$i]['tra_sender']          = $sender;
                $data[$i]['tra_user']            = $user;
                $data[$i]['number_of_ti']        = intval($numer_of_ti['ti']);
                $data[$i]['teacher_trained']     = intval($numerof_teacher_trained['trainee']);

                $i++;
            }
                return array("status" => true, "message" => 'Training agencies data fetched sucessfully.', "result" => array('tra'=>$data) );
        }
      return array("status" => false, "message" => 'No Training agencies assign to this user', "result" => array()); 
    }


    public function ti_training_user_operation($input){

        if(!isset($input['operation']) OR empty($input['operation'])){
            return array("status"=>false,"message"=>'operation field is required.',"result"=>array());
        }
        if(!isset($input['users']) OR empty($input['users'])){
            return array("status"=>false,"message"=>'users field is required.',"result"=>array());
        }
        if(!isset($input['rcurole']) OR empty($input['rcurole'])){
            return array("status"=>false,"message"=>'rcurole field is required.',"result"=>array());
        }

        if($input['operation']=='delete'){

            if($this->Training_model->deleteUser($input)){

                return array("status"=>true,"message"=>'Deleted sucessfully.',"result"=>array());
            }
        }elseif($input['operation']=='update'){

            if($this->Training_model->updateUser($input)){

                return array("status"=>true,"message"=>'Updated sucessfully.',"result"=>array());
            }
        }
    }
    
    
    //----------GET LIST OF TRAINING Instances----------- //
    public function tra_ti_list($input) {
        //$this->validate_get_training_instances();
        
        $ti_list = $this->Training_instances_model->ti_list($input);
        
        
       
        #$result = $this->Training_instances_model->get_training_instances($input);
        if ($ti_list) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $ti_list);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    public function get_tra_selected_report($input){


        if(!isset($input['courses_string']) OR empty($input['courses_string'])){
            return array("status"=>false,"message"=>'Course field is required.',"result"=>array());
        }
        if(!isset($input['tras']) OR empty($input['tras'])){
            return array("status"=>false,"message"=>'Training Agency field is required.',"result"=>array());
        }
        if(!isset($input['fromdate']) OR empty($input['fromdate'])){
            return array("status"=>false,"message"=>'From date field is required.',"result"=>array());
        }
        if(!isset($input['todate']) OR empty($input['todate'])){
            return array("status"=>false,"message"=>'to date field is required.',"result"=>array());
        }

       $data = $this->db->query("call ChalkLit_Get_TI_Data('$input[fromdate]','$input[todate]','$input[tras]','$input[courses_string]','SELECTED');")->result_array();
        return array("status" => true, "message" => 'Data displyed successfully.', "result" => $data);
    }
    public function get_tra_aggregated_report($input){


        if(!isset($input['courses_string']) OR empty($input['courses_string'])){
            return array("status"=>false,"message"=>'Course field is required.',"result"=>array());
        }
        if(!isset($input['tra']) OR empty($input['tra'])){
            return array("status"=>false,"message"=>'Training Agency field is required.',"result"=>array());
        }
        if(!isset($input['fromdate']) OR empty($input['fromdate'])){
            return array("status"=>false,"message"=>'From date field is required.',"result"=>array());
        }
        if(!isset($input['todate']) OR empty($input['todate'])){
            return array("status"=>false,"message"=>'to date field is required.',"result"=>array());
        }

       $data = $this->db->query("call ChalkLit_Get_TI_Data('$input[fromdate]','$input[todate]','$input[tra]','$input[courses_string]','AGGREGATED');")->result_array();
       return array("status" => true, "message" => 'Data displyed successfully.', "result" => $data);
    }
}
