<div>
   <div class="col-lg-12">
      <section class="panel">
         <header class="panel-heading">
            Send Email
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" method= "POST" enctype="multipart/form-data">
               <div class="form-group">
                  <label for="to_email">To</label>
                  <input type="text" value="<?php if(isset($user['email'])){ echo $user['email'];}?>" class="form-control" name = "to_email" id="to_email" placeholder="Enter email" readonly="">
<!--                   <span>* for multiple email separated by comma.</span>-->
                  <span class="text-danger"><?php echo form_error('title');?></span>
               </div>
               <div class="form-group">
                  <label for="subject">Subject</label>
                  <input type="text" value="<?php echo set_value('subject');?>" class="form-control" name = "subject" id="subject" placeholder="Enter subject">
                  <span class="text-danger"><?php echo form_error('subject');?></span>
               </div>
<!--               <div class="form-group">
                  <label for="cc_email">cc</label>
                  <input type="text" value="<?php echo set_value('email');?>" class="form-control" name = "cc_email" id="cc_email" placeholder="Enter email">
                  <span>* for multiple email separated by comma.</span>
                  <span class="text-danger"><?php echo form_error('title');?></span>
               </div>-->
             <!--   <div class="form-group">
                  <label for="message">Message</label>
                  <textarea class="form-control" id="message" name = "message" rows="3" placeholder="Enter Message"><?php //echo set_value('message');?></textarea>
                  <span>* multiple email comma seperated.</span>
                  <span class="text-danger"><?php //echo form_error('message');?></span>
               </div> -->
                <div class="form-group">
                 <label class="col-sm-12 control-label col-sm-2">Message</label>
                 <div class="col-sm-12">
                     <textarea placeholder="Enter Message" class="form-control ckeditor" name="message" rows="6"><?php echo set_value('message');?></textarea>
                 </div>
                 <span class="text-danger"><?php echo form_error('message');?></span>
              </div>
              <div class="clearfix"></div>
              <br>
               
               <button type="submit" class="btn btn-info">SEND</button>
            </form>
         </div>
      </section>
   </div>
   <div class="clearfix"></div>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >
			
						$( function() {
					$( ".dpd1" ).datetimepicker();
					$( ".dpd2" ).datetimepicker();
					 }); // datepicker closed                
               </script>
EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
