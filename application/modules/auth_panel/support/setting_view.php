<div class="col-lg-12">
  <section class="panel">
    <div class="panel-body">
      <form  method="post">
		<center><strong>Settings and Prices</strong></center>
               <input type="hidden" name="id" value="<?php if(isset($result)){ echo $result[0]['id']; }?>"> 
            <div class="form-group">
                <label for="default_distance_to_find_stores">Default distance per searching</label>
                <input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['default_distance_to_find_stores'];}?>"  id="default_distance_to_find_stores" name="default_distance_to_find_stores" placeholder="Enter Distance">
                <span class="text-danger"><?php echo form_error('default_distance_to_find_stores');?></span>
            </div>
            
			<div class="form-group">
                <label for="ranking_price_1st">Price Per Hit For Ranking</label>
                <input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['ranking_price_1st'];}?>"  id="ranking_price_1st" name="ranking_price_1st" placeholder="Enter ranking price">
                <span class="text-danger"><?php echo form_error('ranking_price_1st');?></span>
            </div>
			<div class="form-group">
                <label for="banner_price_per_day">Banner price per day</label>
                <input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['banner_price_per_day'];}?>"  id="banner_price_per_day" name="banner_price_per_day" placeholder="Enter banner price">
                <span class="text-danger"><?php echo form_error('banner_price_per_day');?></span>
            </div>
			
			<div class="form-group">
                <label for="ranking_price_1st">Free Package Price</label>
				<div class="row">
					<div class="col-md-4">
					<label for="free_package_1month_price">1 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['free_package_1month_price'];}?>"  id="free_package_1month_price" name="free_package_1month_price" placeholder="Enter free package price">
					<span class="text-danger"><?php echo form_error('free_package_1month_price');?></span>
					</div>
					
					<div class="col-md-4">
					<label for="free_package_6month_price">3 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['free_package_6month_price'];}?>"  id="free_package_6month_price" name="free_package_6month_price" placeholder="Enter free package price">
					<span class="text-danger"><?php echo form_error('free_package_6month_price');?></span>
					</div>
					
					<div class="col-md-4">
					<label for="free_package_12month_price">12 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['free_package_12month_price'];}?>"  id="free_package_12month_price" name="free_package_12month_price" placeholder="Enter free package price">
					<span class="text-danger"><?php echo form_error('free_package_12month_price');?></span>
					</div>
				</div>
		   </div>
			
			<div class="form-group">
                <label for="standard_package_price">Standard Package Price</label>
				<div class="row">
					<div class="col-md-4">
					<label for="standard_package_1month_price">1 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['standard_package_1month_price'];}?>"  id="standard_package_1month_price" name="standard_package_1month_price" placeholder="Enter standard package price">
					<span class="text-danger"><?php echo form_error('standard_package_1month_price');?></span>
					</div>
					
					<div class="col-md-4">
					<label for="standard_package_3month_price">3 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['standard_package_3month_price'];}?>"  id="standard_package_3month_price" name="standard_package_3month_price" placeholder="Enter standard package price">
					<span class="text-danger"><?php echo form_error('standard_package_3month_price');?></span>
					</div>
					
					<div class="col-md-4">
					<label for="standard_package_12month_price">12 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['standard_package_12month_price'];}?>"  id="standard_package_12month_price" name="standard_package_12month_price" placeholder="Enter standard package price">
					<span class="text-danger"><?php echo form_error('standard_package_12month_price');?></span>
					</div>
				</div>
		   </div>
		   
		   <div class="form-group">
                <label for="plus_package_price">Plus Package Price</label>
				<div class="row">
					<div class="col-md-4">
					<label for="plus_package_1month_price">1 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['plus_package_1month_price'];}?>"  id="plus_package_1month_price" name="plus_package_1month_price" placeholder="Enter plus package price">
					<span class="text-danger"><?php echo form_error('plus_package_1month_price');?></span>
					</div>
					
					<div class="col-md-4">
					<label for="plus_package_3month_price">3 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['plus_package_3month_price'];}?>"  id="plus_package_3month_price" name="plus_package_3month_price" placeholder="Enter plus package price">
					<span class="text-danger"><?php echo form_error('plus_package_3month_price');?></span>
					</div>
					
					<div class="col-md-4">
					<label for="plus_package_12month_price">12 Month Price</label>
					<input type="text" class="form-control" value="<?php if(isset($result)){ echo $result[0]['plus_package_12month_price'];}?>"  id="plus_package_12month_price" name="plus_package_12month_price" placeholder="Enter plus package price">
					<span class="text-danger"><?php echo form_error('plus_package_12month_price');?></span>
					</div>
				</div>
		   </div>
			
			
            <div  class="form-group">
                <button type="submit" class="btn btn-primary"><?php if(isset($result)){ echo "Update"; } else { echo "Submit";}?></button>
            </div>
      </form>
    </div>
    </section>
</div>

<div id="map" style="display:none"></div>
<?php
$admin_url = site_url('auth_panel');
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-video-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"Support/ajax_all_support_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );
						// Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-video-list').val() !="" && $('#max-date-video-list').val() != "" ){
                                var dates = $('#min-date-video-list').val()+','+$('#max-date-video-list').val();
                                dataTable.columns(8).search(dates).draw();
                            } 
                            if($('#min-date-video-list').val() =="" || $('#max-date-video-list').val() == "" ){
                                var dates = "";
                                dataTable.columns(8).search(dates).draw();
                            }  
                        }); 
                   } );
				   
				   $('#min-date-video-list').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true
						
					});
					$('#max-date-video-list').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true
						
					});
               </script>

EOD;

echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>











