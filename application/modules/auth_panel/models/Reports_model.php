<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }    

    public function getTraining($traname) { 

        $this->db->where('trnrefid',$traname);
         $result = $this->db->get('clt_trn')->row();
         return $result;
    }
    public function checkTraning($trnrefid) { 

        $this->db->where('trnrefid',$trnrefid);
         $result = $this->db->get('clt_trn')->row();
         return $result;
    }
    public function getModule($rcgrefid) { 

        $this->db->where('rcgrefid',$rcgrefid);
         $result = $this->db->get('currcg')->row_array();
         return $result;
    }

    public function getAllModules($trnrefid){

        $this->db->select('rcgrefid');
        $this->db->where('currcg.rcgrcmrefid',$trnrcmrefid);
        $modules = $this->db->get('currcg')->result_array();
        return $modules;
    }

    public function getTrainingInstanceModules($trnrefid){

      $data=  $this->checkTraning($trnrefid);
      $this->db->select('rcgrefid,groupname,rchname');
      $this->db->where('currcg.rcgrcmrefid',$data->trnrcmrefid);
      $modules = $this->db->get('currcg')->result_array();
      return $modules;

    }

    public function check_test_available($rcgrefid){


        $this->db->select('porrefid,porpolrefid');
        $this->db->where('escpor.porobjectid',$rcgrefid);
        $test = $this->db->get('escpor')->result_array();

        return $test;

    }

    public function getallActiveTrainings(){
        $this->db->where('isactive',TRUE);
        $this->db->where('isdeleted',FALSE);
        $this->db->select('trnrefid,trnname,trnrcmrefid');
        $result = $this->db->get('clt_trn')->result_array();
         return $result;

    }


     public function get_question_options($obj_id,$training_end_date){

       $this->db->select("escpol.polrefid,escpol.polquestion");
        $this->db->where('escpor.porobjectid',$obj_id);
        $this->db->where('escpor.isattached',TRUE);
        $this->db->join('escpor','escpor.porpolrefid=escpol.polrefid');
        $questions = $this->db->get('escpol')->result_array();

      $this->db->select("escusm.usmrefid,escusm.usmfirstname,escusm.usmlastname,escusm.usmmob,escpol.polrefid,escpol.polquestion,escpom.pomoption,escpom.iscorrect");
        $this->db->join('escusm','escusm.usmrefid=escprm.prmusmrefid');
        $this->db->join('escpol','escpol.polrefid=escprm.prmpolrefid');
        $this->db->join('escpor','escpor.porpolrefid=escprm.prmpolrefid');
        $this->db->join('escpom','escpom.pomrefid=escprm.prmpomrefid');
        //$this->db->limit(100,10);
        $this->db->where('escpor.porobjectid',$obj_id);
         $this->db->where('escpor.isattached',TRUE);
        $this->db->where('escprm.createddate <=',$training_end_date);
        $options = $this->db->get('escprm')->result_array();
        if($options){
          foreach($options as $option){
              $new_array[$option['usmrefid']]['name']   = $option['usmfirstname'];
              $new_array[$option['usmrefid']]['mobile'] = $option['usmmob'];
              $new_array[$option['usmrefid']]['options'][$option['polrefid']]['value'] = $option['iscorrect'];
          }
          $options = $new_array;
        }
        return $options;

        // $echo = "";
        // foreach ($options as $value) { //return $value;
         
        //   $echo =  $echo. "<br>". $value['name'].' # '.$value['mobile'].' # ';
        //   foreach ($questions as $key1 => $value1) {
        //     if(isset($value['options'][$value1['polrefid']])){
        //       $echo =   $echo . " ". $value['options'][$value1['polrefid']]['value']." ";
        //     }else{
        //       $echo = $echo." - ";  
        //     }
            
        //   }
        // }
        // return $echo;
        //return $options;
    }

    public function get_questions($obj_id){
        $this->db->select("escpol.polrefid,escpol.polquestion");
        $this->db->where('escpor.porobjectid',$obj_id);
        $this->db->where('escpor.isattached',TRUE);
        $this->db->join('escpor','escpor.porpolrefid=escpol.polrefid');
        $questions = $this->db->get('escpol')->result_array();
        return $questions;
    }

    public function getallUserAnswerThisQuestion($ques_id){

        $data = array();$i=0;
        $this->db->select('pomoption,prmusmrefid,usmfirstname,usmmob');
        $this->db->where('escprm.prmpolrefid',$ques_id);
        $this->db->join('escpom','escpom.pomrefid=escprm.prmpomrefid');
        $this->db->join('escusm','escusm.usmrefid=escprm.prmusmrefid');
        $all_users = $this->db->get('escprm')->result_array();
        return $all_users;

    }

    public function checkModuleDateWithTrainingInstanceEnddate($trnrefid){

    $training_instance =   $this->checkTraning($trnrefid);
    
    return $training_instance->enddate;

    }
    
    
    
}
