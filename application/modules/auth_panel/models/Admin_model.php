<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    /*

    function check_admin_role_authorization($input){
        $this->db->Where("urmusmrefid",$input['ulmusmrefid']);
        $this->db->Where("escrlm.rlmrole", 'CL-Admin');
        $this->db->Where("escurm.isactive", '1');
        $this->db->Where("escurm.isdeleted", '0');
        $this->db->Where("escrlm.isactive", '1');
        $this->db->Where("escrlm.isdeleted", '0');
        $this->db->join('escrlm','escrlm.rlmrefid=escurm.urmrlmrefid');
        $result     = $this->db->get('escurm')->row_array();
        return $result;
    }
    
    function check_admin_chalklit_right_authorization($input){        
        $this->db->Where("escoea.isactive", '1');
        $this->db->Where("escoea.isdeleted", '0');
        $this->db->Where("escuep.uepusmrefid",$input['ulmusmrefid']);
        $this->db->Where("escoea.oeaattributename", 'RIGHT-CHALKLIT-SYSTEM');
        $this->db->join('escoea','escoea.oearefid=escuep.uepoearefid');
        $result     = $this->db->get('escuep')->row_array();
        return $result;
    }
    
    public function admin_login($input) {//pre($input);echo md5($input['password']);
        $response['status']         = false;
        $response['message']        = 'Authentication failed';
        $response['result']         = array(); 
            
        $this->db->Where("ulmusername",$input['username']);
        $this->db->Where("ulmpassword", md5($input['password']));
        $this->db->Where("isactive", '1');
        $result     = $this->db->get('esculm')->row();
        if($result){
            $response['message']    = 'Authorization failed';
            $result1 = $this->check_admin_role_authorization(array("ulmusmrefid"=>$result->ulmusmrefid));
            if($result1){
                $result1 = $this->check_admin_chalklit_right_authorization(array("ulmusmrefid"=>$result->ulmusmrefid));
                if($result1){   
                    $response['status']     = true;
                    $response['message']    = 'Login successfully'; 
                    $response['result']     = $result; 
                }                
            }
        }
        return $response;
    }
    */
    
    public function admin_login($input) {//pre($input);echo md5($input['password']);
        $response['status']         = false;
        $response['message']        = 'Authentication failed';
        $response['result']         = array(); 
        
        $this->db->select("group_concat(rlmrole) as roles,esculm.*");
        $this->db->Where("ulmusername",$input['username']);
        $this->db->Where("ulmpassword", md5($input['password']));
        $this->db->Where("esculm.isactive", '1');
        $this->db->join('escurm','escurm.urmusmrefid=esculm.ulmusmrefid');
        $this->db->join('escrlm','escrlm.rlmrefid = escurm.urmrlmrefid');
        $result     = $this->db->get('esculm')->row();
        if($result){
            //'escoea','escoea.oearefid=escuep.uepoearefid'
            $result->roles        = explode(',',$result->roles);
            $response['status']     = true;
            $response['message']    = 'Login successfully'; 
            $response['result']     = $result; 
        }
        return $response;
    }

    /*
    select group_concat(rlmrole),esculm.*
    from esculm 
    JOIN escurm ON escurm.urmusmrefid=esculm.ulmusmrefid
    JOIN escrlm ON escrlm.rlmrefid = escurm.urmrlmrefid
    where ulmusername = '9910144235' AND ulmpassword=md5('rustam');
    */
    
    public function insert_admin($data) {
        $data['creation_time'] = time();
        $this->db->insert('backend_user', $data);
        //echo $this->db->last_query(); die;
        return $this->db->insert_id();
    }

    public function update_admin($data) {
        $this->db->where('id', $data['id']);
        $this->db->update('backend_user', $data);
        return $this->db->affected_rows();
    }

    public function get_admin_list() {
        $result = $this->db->get('backend_user')->result_array();
        return $result;
    }

    public function get_admin_by_id($id) {
        $this->db->where('id', $id);
        $result = $this->db->get('backend_user')->row_array();
        return $result;
    }

    public function delete_admin($id) {
        $this->db->where('id', $id);
        $result = $this->db->update('backend_user', array('status' => 3));
        //$result = $this->db->delete('backend_user');
        return $result;
    }

}
