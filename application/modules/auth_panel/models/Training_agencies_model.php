<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Training_agencies_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    public function get_training_agencies_list() {
        //$this->db->where('status',1);
        $result = $this->db->get('curtra')->result_array();
        return $result;
    }
    
    public function course_mapping($data) {
        $data['isactive'] = 1;          
        $data['creation_time'] = time();          
        $this->db->insert('category', $data);
        //echo $this->db->last_query(); die;
        return $this->db->insert_id();
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    public function insert_tertiary_category($data) {
        $data['creation_time'] = time();          
        $this->db->insert('tertiary_category', $data);
        //echo $this->db->last_query(); die;
        return $this->db->insert_id();
    }
    
     public function insert_sub_category($data) {
        $data['creation_time'] = time();          
        $this->db->insert('sub_category', $data);
        //echo $this->db->last_query(); die;
        return $this->db->insert_id();
    }

    public function update_category($data) {
        $this->db->where('id', $data['id']);
        $this->db->update('category', $data);
        return $this->db->affected_rows();
    }
      public function update_sub_category($data) {
        $this->db->where('id', $data['id']);
        $this->db->update('sub_category', $data);
        return $this->db->affected_rows();
    }

     public function update_tertiary_category($data) {
        $this->db->where('id', $data['id']);
        $this->db->update('tertiary_category', $data);
        return $this->db->affected_rows();
    }

    

     public function get_sub_category_list($id) {
         $this->db->where('status',1);
         $this->db->where('main_cat_id',$id);
        $result = $this->db->get('sub_category')->result_array();
        return $result;
    }

    public function get_category_by_id($id) {
        $this->db->where('id', $id);
        $result = $this->db->get('category')->row_array();
        return $result;
    }

     public function get_sub_category_by_id($id){
        $this->db->where('id', $id);
        $result = $this->db->get('sub_category')->row_array();
        return $result;
    }
    public function get_tertiary_category_by_id($id){
        $this->db->where('id', $id);
        $result = $this->db->get('tertiary_category')->row_array();
        return $result;
    }

    public function delete_category($id) {
        $this->db->where('id', $id);
        $this->db->update('category', array('status'=>3));
        return true;
    }

     public function delete_subcategory($id) {
        $this->db->where('id', $id);
        $this->db->update('sub_category', array('status'=>3));
        return true;
    }
    public function delete_tertiary_category($id) {
        $this->db->where('id', $id);
        $this->db->update('tertiary_category',array('status'=>3));
        return true;
    }
}
