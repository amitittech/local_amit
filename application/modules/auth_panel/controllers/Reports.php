<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('custom');
        $this->load->model("Reports_model");
    }


    public function index(){
        $view_data['page']      = 'report';
        $data['file_url']       = API_BASE_URL; 
        $data['opcode']         = 'get_training_instances';
        $data['login_id']       = $this->session->userdata('active_backend_user_id');
        $response               = file_curl_contents($data); 
        $view_data['training']  = $response['result'];
        $data['page_data']      = $this->load->view('reports/index', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajax_get_modules(){
        $data['file_url']   	= API_BASE_URL; 
        $data['opcode']     	= 'get_training_modules';
        $data['traningId']  	= $_GET['trnrefid'];
        $data['login_id']   	= $this->session->userdata('active_backend_user_id');
        $response           	= file_curl_contents($data); 
        echo json_encode($response['result']);
    }

    public function getReports(){
        $data['file_url']             = API_BASE_URL; 
        $data['opcode']               = 'get_report';
        $data['object_type']          = 'TRN';
        $data['object_entity_id']     = $_POST['trnname'];
        $data['moduleid']             = $_POST['rcgrefid'];
        $data['login_id']             = $this->session->userdata('active_backend_user_id');
        $response                     = file_curl_contents($data); 
        if( count( $response['result'] ) > '0' ){
            if( count( $response['result']['user_answer'] ) > '0' ){
              $this->load->view('reports/ajax_get_reports',array("result"=>$response,"ques"=>$response['result']['ques']));
            }else{
               echo $msg = '<p><h4  style="text-align:center;color:red">No users Available for this training Instance<h4/><p/>';
            } 			 
        }else{
            echo $msg = '<p><h4  style="text-align:center;color:red">'.$response['message'].'<h4/><p/>';
        }
    }

}
