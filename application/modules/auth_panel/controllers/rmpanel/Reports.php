<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('custom');
        $this->load->model("Courses_model");
        $this->load->model("Training_agencies_model");
    }
        
   public function aggregatedReport(){

        $rmId              = $this->session->userdata('active_backend_user_id');
       
        $this->db->select('curtra.trarefid,curtra.traname,clt_tra_tm.tmrefid');
        $this->db->where('clt_tra_tm.tmrefid',$rmId);                
        $this->db->join('curtra','curtra.trarefid=clt_tra_tm.trarefid');
        $this->db->order_by("curtra.traname", "asc");
        $tra_list = $this->db->get('clt_tra_tm')->result_array();
        $view_data['page']              = 'aggregated_report';
        $view_data['result']            = $tra_list;
        $data['page_data']              = $this->load->view('rmpanel/reports/aggregated_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
 
    public function ajaxGetTraCourses(){

        $input = $this->input->post();
        $trarefid =  $input['trarefid'];
        $this->db->select('currcm.chaptername,currcm.rcmrefid');
        $this->db->where('clt_tra_rcm.trarefid',$trarefid);                
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid');
        $this->db->order_by("currcm.chaptername", "asc");
        $course_list = $this->db->get('clt_tra_rcm')->result_array();

        if($course_list){ 
         
            foreach ($course_list as $row) {
                echo '<option value="'.$row['chaptername'].'">'.$row['chaptername'].'</option>';
            }
        }else{
            echo '<option value="">Course not available</option>';
        }
    }

    public function getAggregatedReport(){

       $input = $this->input->post();
       $trarefid = $input['tra_agency'];
       $this->db->select('curtra.traname');
       $this->db->where('curtra.trarefid',$trarefid);                
       $tra = $this->db->get('curtra')->row_array();
       $courses =  $input['courses'];
       $courses_string = implode(", ", $courses);
       
        $data['file_url']              = API_BASE_URL; 
        $data['opcode']                = 'get_tra_aggregated_report';
        $data['login_id']              = $this->session->userdata('active_backend_user_id');
        $data['courses_string']        = $courses_string;
        $data['tra']                   = $tra['traname'];
        $data['fromdate']              = $input['fromdate'];
        $data['todate']                = $input['todate'];

        if($input['checkboxval'] == 'ALL'){
            $data['courses_string'] = 'All';
        }
        $response                      = file_curl_contents($data);
       
       $this->load->view('rmpanel/reports/ajax_get_aggregated_report_list',array("result"=>$response['result'])); 
    }

   public function selectedReport(){

        $rmId = $this->session->userdata('active_backend_user_id');
        $this->db->select('curtra.trarefid,curtra.traname,clt_tra_tm.tmrefid');
        $this->db->where('clt_tra_tm.tmrefid',$rmId);                
        $this->db->join('curtra','curtra.trarefid=clt_tra_tm.trarefid');
        $this->db->order_by("curtra.traname", "asc");
        $tra_list = $this->db->get('clt_tra_tm')->result_array();
        $view_data['page']              = 'aggregated_report';
        $view_data['result']            = $tra_list;
        $data['page_data']              = $this->load->view('rmpanel/reports/selected_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajaxGetSelectedTraCourses(){

        $input = $this->input->post();
        $traids =  implode("," , $input['trarefid']);
        $this->db->select('DISTINCT(currcm.chaptername),currcm.rcmrefid');
        $this->db->where_in('trarefid', $input['trarefid']);               
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid');
        $this->db->order_by("currcm.chaptername", "asc");
        $course_list = $this->db->get('clt_tra_rcm')->result_array();
        if($course_list){ 
            foreach ($course_list as $row) {
                echo '<option value="'.$row['chaptername'].'">'.$row['chaptername'].'</option>';
            }
        }else{
            echo '<option value="">Course not available</option>';
        }
    }

    public function getSelectedReport(){

       $input = $this->input->post();
      // echo "<pre>";print_r($input);die;
       $trarefid = $input['tra_agency'];
       $this->db->select('curtra.traname');
       $this->db->where_in('curtra.trarefid',$trarefid);                
       $tra = $this->db->get('curtra')->result_array();
       foreach ($tra as $row ) {
            $tra_agency[] =  $row['traname'];
        }
        $courses =  $input['courses'];
        $courses_string = implode(" , ", $courses);
        $tras =   implode(" , " , $tra_agency);
        $data['file_url']              = API_BASE_URL; 
        $data['opcode']                = 'get_tra_selected_report';
        $data['login_id']              = $this->session->userdata('active_backend_user_id');
        $data['courses_string']        = $courses_string;
        $data['tras']                  = $tras;
        $data['fromdate']              = $input['fromdate'];
        $data['todate']                = $input['todate'];
        if($input['checkboxval'] == 'ALL'){
            $data['tras']  = 'ALL';
        }
        if($input['courseval'] == 'ALL'){
            $data['courses_string']        = 'ALL';
        }

        $response                      = file_curl_contents($data);
        $this->load->view('rmpanel/reports/ajax_get_selected_report_list',array("result"=>$response['result']));
    }

	
	
}
