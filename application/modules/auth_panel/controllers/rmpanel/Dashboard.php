<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->model("Admin_model");
        //$this->load->model("backend_user");
        //$this->load->model('web_user_model');
        $this->load->helper('custom');
    }

    public function index($id = '') {
        //pre($this->session->userdata());
        #$user_data = $this->session->userdata('active_user_data');
        
        $view_data['page'] = 'dashboard_rm';

        $data['page_data'] = $this->load->view('admin/dashboard_rm', $view_data, TRUE);
        $data['page_title'] = "welcome page";
        echo modules::run('auth_panel/template/call_default_template', $data);
    }
    
    function ajax_tra(){
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'user_roles';
        $data['rlmrole'] 	= "CL-Relationship Manager";
        $data['login_id'] 	= $data['tmrefid'] 	= $this->session->userdata('active_backend_user_id');        
        $response               = file_curl_contents($data);
        $this->load->view('auth_panel/admin/ajax_rm_dashboard',array("result"=>$response));
    }
    
    function ajax_ti(){
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'ti_info';
        $data['login_id'] 	= $data['tmrefid'] 	= $this->session->userdata('active_backend_user_id'); 
        $data['tirefids'] 	= $_POST['ti_ids'];
        $response               = file_curl_contents($data);
        echo json_encode($response);
        #pre($response);
        #$this->load->view('auth_panel/admin/ajax_rm_dashboard',array("result"=>$response));
    }

    public function index1($id = '') {
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'user_roles';
        $data['rlmrole'] 	= "CL-Relationship Manager";
        $data['login_id'] 	= $data['tmrefid'] 	= $this->session->userdata('active_backend_user_id');        
        $response               = file_curl_contents($data);
        $view_data['tras']      = $response['result'];
        #$user_data             = $this->session->userdata('active_user_data');        
        $view_data['page']      = 'dashboard_rm';
        $data['page_data']      = $this->load->view('rmpanel/dashboard/dashboard', $view_data, TRUE);
        $data['page_title']     = "welcome page";
        echo modules::run('auth_panel/template/call_default_template', $data);
    }
    /*
    public function glimpse($id = '') {
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'glimpse';
        $data['tiid']           = 129;
        $data['login_id'] 	= $data['tmrefid'] 	= $this->session->userdata('active_backend_user_id');        
        $response               = file_curl_contents($data);
        $view_data['result']    = $response['result'];
        #$user_data             = $this->session->userdata('active_user_data');        
        $view_data['page']      = 'glimpse';
        $data['page_data']      = $this->load->view('admin/glimpse', $view_data, TRUE);
        $data['page_title']     = "welcome page";
        echo modules::run('auth_panel/template/call_default_template', $data);
    }*/
    
    function ajax_display_glimpse_headings(){ 
        $ti_info['traname']         = $_POST['traname'];
        $ti_info['tralogo']         = $_POST['tralogo'];
        $ti_info['tcname']          = $_POST['tcname'];
        $ti_info['tiname']          = $_POST['tiname'];
        $ti_info['startdate']       = $_POST['startdate'];
        $ti_info['enddate']         = $_POST['enddate'];
        $ti_info['participants']         = $_POST['participants'];
        
        $this->load->view('auth_panel/rmpanel/dashboard/glimpse_headings',array('ti_info'=>$ti_info));
    }
    
    function ajax_get_glimpse_mewp(){        
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'get_rm_ti_glimpse_mewp';
        $data['login_id'] 	= $this->session->userdata('active_backend_user_id'); 
        $data['tiid']           = $_POST['tiid'];
        $response               = file_curl_contents($data);
        $this->load->view('auth_panel/rmpanel/dashboard/glimpse_mewp',array("result"=>$response));
    }
    
    function ajax_get_glimpse_metp(){        
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'get_rm_ti_glimpse_metp';
        $data['login_id'] 	= $this->session->userdata('active_backend_user_id'); 
        $data['tiid']           = $_POST['tiid'];
        $response               = file_curl_contents($data);
        $this->load->view('auth_panel/rmpanel/dashboard/glimpse_metp',array("result"=>$response));
    }
    function ajax_get_glimpse_meu(){        
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'get_rm_ti_glimpse_meu';
        $data['login_id'] 	= $this->session->userdata('active_backend_user_id'); 
        $data['tiid']           = $_POST['tiid'];
        $response               = file_curl_contents($data);
        $this->load->view('auth_panel/rmpanel/dashboard/glimpse_meu',array("result"=>$response));
    }
    
    function ajax_get_glimpse_mes(){        
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'get_rm_ti_glimpse_mes';
        $data['login_id'] 	= $this->session->userdata('active_backend_user_id'); 
        $data['tiid']           = $_POST['tiid'];
        $response               = file_curl_contents($data);
        $this->load->view('auth_panel/rmpanel/dashboard/glimpse_mes',array("result"=>$response));
    }
    
    

}
