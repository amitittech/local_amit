<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->model("Admin_model");
        //$this->load->model("backend_user");
        //$this->load->model('web_user_model');
        $this->load->helper('custom');
    }

    public function index($id = '') {
        //pre($this->session->userdata());
        $user_data = $this->session->userdata('active_user_data');
        
        $view_data['page'] = 'dashboard';

        $data['page_data'] = $this->load->view('admin/WELCOME_PAGE_SUPER_USER', $view_data, TRUE);
        $data['page_title'] = "welcome page";
        echo modules::run('auth_panel/template/call_default_template', $data);
    }

    public function user_list() {
        $data['page_title'] = "user list";
        $data['page_data'] = $this->load->view('admin/user_list/user_list', '', TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function create_backend_user() {

        if (isset($_GET['xfxr'])) {
            $id = $_GET['xfxr'];
            //pre($id); die;
            $view_data['result'] = $this->db->get_where('business_user', array('id' => $id))->row_array();
            //pre($view_data['result']);die;
        }
        if ($this->input->post()) {

            //pre($this->input->post()); die;
//            if ($_POST['business_name'] == '') {
//                echo json_encode(array('status' => false,'field_id' => 'business_name', 'message' => 'Please enter business name.'));
//                die;
//            }
            if ($_POST['owner_first_name'] == '') {
                echo json_encode(array('status' => false,'field_id' => 'owner_first_name', 'message' => 'Please enter First name.'));
                die;
            }
            if ($_POST['owner_last_name'] == '') {
                echo json_encode(array('status' => false,'field_id' => 'owner_last_name', 'message' => 'Please enter Last name.'));
                die;
            }
            if ($_POST['email'] == '') {
                echo json_encode(array('status' => false,'field_id' => 'email', 'message' => 'Please enter email address.'));
                die;
            }
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                echo json_encode(array('status' => false,'field_id' => 'email', 'message' => 'Please enter valid email address.'));
                die;
            }
            if ($_POST['mobile'] == '') {
                echo json_encode(array('status' => false,'field_id' => 'mobile', 'message' => 'Please enter mobile number.'));
                die;
            }
            if (strlen($_POST['mobile']) <= 9) {
                echo json_encode(array('status' => false,'field_id' => 'mobile', 'message' => 'Please enter atleast 10 digits mobile number.'));
                die;
            }
            
//            if ($_POST['location'] == '') {
//                echo json_encode(array('status' => false, 'message' => 'Please enter location.'));
//                die;
//            }

//            if ($_POST['category'] == '') {
//                echo json_encode(array('status' => false, 'message' => 'Please enter atleast one category.'));
//                die;
//            }
//            if ($_POST['username'] == '') {
//                echo json_encode(array('status' => false, 'message' => 'Please enter username.'));
//                die;
//            }
            if ($_POST['password'] == '') {
                echo json_encode(array('status' => false,'field_id' => 'login_pwd', 'message' => 'Please enter password.'));
                die;
            }
            if (strlen($_POST['password']) <= 7) {
                echo json_encode(array('status' => false,'field_id' => 'login_pwd', 'message' => 'Please enter atleast 8 digits password.'));
                die;
            }
           
            if ($_POST['c_password'] == '') {
                echo json_encode(array('status' => false,'field_id' => 'c_password', 'message' => 'Please retype your password.'));
                die;
            }
            if ($_POST['password'] != $_POST['c_password']) {
                echo json_encode(array('status' => false,'field_id' => 'c_password', 'message' => 'Confirm password did not matched.'));
                die;
            }
            if ($this->input->post('id')) {
                $input_data = $this->input->post();
                $user_detail = $this->db->get_where('business_user', array('id' => $this->input->post('id')))->row_array();
                if ($user_detail['password'] == $this->input->post('password')) {
                    $input_data['password'] = $input_data['password'];
                } else {
                    $input_data['password'] = md5($input_data['password']);
                }
                $input_data['updated_time'] = time();
                unset($input_data['business_name_other'], $input_data['c_password']);
                $this->db->where('id', $this->input->post('id'));
                $this->db->update('business_user', $input_data);
                page_alert_box('success', 'Business user Updated', 'Business user has been updated successfully');
                echo json_encode(array('status' => true, 'message' => 'Successfully add business user'));

                $log_data = array("subject" => $this->session->userdata('active_backend_user_id'), "subject_type" => "admin", "module" => "bussiness user", "request_string" => json_encode($this->input->post()), "action" => 'update', "object_id" => $this->input->post('id'), "object_name" => $input_data['business_name'], "table_name" => "business_user");
                create_logs($log_data);

                die;
            } else {
                //$this->db->where('username', $_POST['username']);
                $this->db->where('mobile', $_POST['mobile']);
                $this->db->or_where('email', $_POST['email']);
                $result = $this->db->get('business_user')->row_array();
                //echo $this->db->last_query();
                if ($_POST['email'] == $result['email']) {
                    echo json_encode(array('status' => false,'field_id' => 'email', 'message' => 'email aleady exits.'));
                    die;
                }
                if ($_POST['mobile'] == $result['mobile']) {
                    echo json_encode(array('status' => false,'field_id' => 'mobile', 'message' => 'Mobile No already Exist'));
                    die;
                }
//                if ($_POST['username'] == $result['username']) {
//                    echo json_encode(array('status' => false, 'message' => 'username aleady exits.'));
//                    die;
//                }
                $input_data = $this->input->post();
                //$input_data['category'] = implode(',', $this->input->post('category'));
                $input_data['password'] = md5($input_data['password']);
                $input_data['tokken'] = rand(1000, 999999) . '_' . time();
                $input_data['creation_time'] = $input_data['updated_time'] = time();
                $input_data['status'] = 1;
                unset($input_data['business_name_other'], $input_data['c_password']);
                //pre($input_data); die;
                $this->db->insert('business_user', $input_data);
                $insert_id = $this->db->insert_id();
                $this->buy_free_membership($insert_id);
                $log_data = array("subject" => $this->session->userdata('active_backend_user_id'), "subject_type" => "admin", "module" => "bussiness user", "request_string" => json_encode($this->input->post()), "action" => 'add', "object_id" => $insert_id, "object_name" => $input_data['email'], "table_name" => "business_user");
                create_logs($log_data);

                page_alert_box('success', 'Business user inserted', 'Business user has been inserted successfully');
                echo json_encode(array('status' => true, 'message' => 'Successfully add business user'));
                die;
            }
        }
        $view_data['page'] = 'create_backend_user';
        $data['page_data'] = $this->load->view('admin/backend_user/create_new_backend_user', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function business_user_list() {
        $view_data['page'] = 'business_user_list';
        $data['page_title'] = "Business User List";
        $data['page_data'] = $this->load->view('admin/business_user/business_user_list', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajax_business_user_list() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'business_name',
            1 => 'owner_first_name',
            2 => 'owner_last_name',
            3 => 'email',
            4 => 'mobile',
        );

        $query = "SELECT count(id) as total
                    FROM business_user
                    WHERE status != 3";
        $query = $this->db->query($query);
        $query = $query->row_array();
        $totalData = (count($query) > 0) ? $query['total'] : 0;
        $totalFiltered = $totalData;

        $sql = "SELECT *,bu.id as id,pg.permission_group_name, 
                    case bu.status
                    when '1' then 'Active'
                    when '2' then 'Blocked'
                    when '3' then 'Delete'
                    when '4' then 'Pending'
                    end as user_state
                    FROM business_user as bu LEFT JOIN 
                    backend_user_role_permissions as burp ON 
                    bu.id = burp.user_id LEFT JOIN 
                    permission_group as pg ON 
                    burp.permission_group_id = pg.id 
                    WHERE  bu.status != 3 ";

        /* $sql = "SELECT * ,
          case status
          when '0' then 'Active'
          when '1' then 'Blocked'
          end as user_state
          FROM backend_user   where status != 2 "; */

        // getting records as per search parameters
        
        
        if (!empty($requestData['columns'][0]['search']['value'])) {   //name
            $sql .= " AND business_name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
        }
        if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
            $sql .= " AND owner_first_name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
        }
        if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
            $sql .= " AND owner_last_name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
            $sql .= " AND email LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
            $sql .= " AND mobile LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
            $sql .= " AND status LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
        }

        $query = $this->db->query($sql)->result();

        $totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

        $sql .= " ORDER BY bu.id " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

        $result = $this->db->query($sql)->result();
        $data = array();

        foreach ($result as $r) {  // preparing an array
            $nestedData = array();


            //$nestedData[] = $r->business_name;
            $nestedData[] = $r->owner_first_name;
            $nestedData[] = $r->owner_last_name;
            $nestedData[] = $r->email;
            $nestedData[] = $r->mobile;


            $nestedData[] = $r->user_state;
            $action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "admin/create_backend_user?xfxr=" . $r->id . "'>Edit</a>&nbsp;"
                    . "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete?')\" href='" . AUTH_PANEL_URL . "admin/update_business_user/" . $r->id . "/3'>Delete</a>&nbsp;";
            if ($r->user_state == 'Blocked') {
                $action .= "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "admin/update_business_user/" . $r->id . "/1'>Unblock</a>";
            }
            if ($r->user_state == 'Active') {
                $action .= "<a class='btn-xs btn  bold btn-warning' href='" . AUTH_PANEL_URL . "admin/update_business_user/" . $r->id . "/2'>Block</a>";
            }
            if ($r->user_state == 'Pending') {
                $action .= "<a class='btn-xs bold btn btn-primary' href='" . AUTH_PANEL_URL . "admin/update_business_user/" . $r->id . "/1'>Active</a>";
            }
            $nestedData[] = $action;

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }

    public function delete_backend_user($id) {

        $this->db->where('id', $id);
        $delete_user = $this->db->delete('business_user');
        if ($delete_user == true) {
            page_alert_box('success', 'Business user Deleted', 'Business user has been Deleted successfully');
        } else {
            page_alert_box('success', 'Business user Not Deleted', 'Business user has not been deleted successfully');
        }
        redirect(AUTH_PANEL_URL . 'admin/business_user_list');
    }

    public function update_business_user($id, $status) {
        $this->db->where('id', $id);
        $update_data = $this->db->update('business_user', array('status' => $status));
        if ($update_data == true) {
            $this->db->where('id', $id);
            $result = $this->db->get('business_user')->row_array();
            if ($status == 1) {
                $log_data = array("subject" => $this->session->userdata('active_backend_user_id'), "subject_type" => "admin", "module" => "bussiness user", "action" => 'unblock', "object_id" => $result['id'], "object_name" => $result['business_name'], "table_name" => "business_user");
                create_logs($log_data);
                page_alert_box('success', 'User active', 'Business user has been active successfully');
            }
            if ($status == 2) {
                $log_data = array("subject" => $this->session->userdata('active_backend_user_id'), "subject_type" => "admin", "module" => "bussiness user", "action" => 'block', "object_id" => $result['id'], "object_name" => $result['business_name'], "table_name" => "business_user");
                create_logs($log_data);
                page_alert_box('success', 'User blocked', 'Business user has been blocked successfully');
            }
            if ($status == 3) {
                $log_data = array("subject" => $this->session->userdata('active_backend_user_id'), "subject_type" => "admin", "module" => "bussiness user", "action" => 'delete', "object_id" => $result['id'], "object_name" => $result['business_name'], "table_name" => "business_user");
                create_logs($log_data);
                page_alert_box('success', 'User deleted', 'Business user has been deleted successfully');
            }
        } else {
            $this->session->set_flashdata('error_message', 'User not Deleted');
        }
        redirect(AUTH_PANEL_URL . 'admin/business_user_list');
    }

    public function get_dashboard_by_search() {
        $date = $this->input->post('date');
        if ($date == 'today') {
            $today = date("Y-m-d");
            $query = $this->db->query("SELECT * FROM users where date(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' )) = '$today'")->result_array();
        } elseif ($date == 'yesterday') {
            $yesterday = date("Y-m-d", strtotime("-1 days"));
            $query = $this->db->query("SELECT * FROM users where date(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' )) = '$yesterday'")->result_array();
        } elseif ($date == 'Week') {
            $week = date('Y-m-d', strtotime("-7 days"));
            $today = date("Y-m-d");
            $query = $this->db->query("SELECT * FROM users where date(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' )) BETWEEN '$week' AND '$today'")->result_array();
        } elseif ($date == 'Month') {
            $month = date('Y-m-d', strtotime("-1 Months"));
            $today = date("Y-m-d");
            $query = $this->db->query("SELECT * FROM users where date(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' )) BETWEEN '$month' AND '$today'")->result_array();
        } elseif ($date == 'Year') {
            $year = date('Y-m-d', strtotime("-1 year"));
            $today = date("Y-m-d");
            $query = $this->db->query("SELECT * FROM users where date(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' )) BETWEEN '$year' AND '$today'")->result_array();
        }
        $total = 0;
        $dams_student = 0;
        $non_dams_student = 0;
        $result = array();
        foreach ($query as $key => $sql) {
            $total = $total + 1;
            if ($sql['dams_tokken'] != '') {
                $dams_student = $dams_student + 1;
            } else {
                $non_dams_student = $non_dams_student + 1;
            }
        }
        $result['total_student'] = $total;
        $result['dams_student'] = $dams_student;
        $result['non_dams_student'] = $non_dams_student;
        $html = '<div class=col-lg-3><section class=panel><div class=panel-body><a href=#><span class="fa fa-2x fa-users"></span></a><div class=task-thumb-details><h1><a href=#>Student Summary</a></h1></div></div><table class="personal-task table table-hover"><tr><td><i class="fa fa-tasks"></i><td>Total Student<td>' . $result['total_student'] . '<tr><td><i class="fa fa-tasks"></i><td>DAMS Student<td>' . $result['dams_student'] . '<tr><td><i class="fa fa-tasks"></i><td>NON DAMS Student<td>' . $result['non_dams_student'] . '</table></section></div><div class=col-lg-3><section class=panel><div class=panel-body><a href=#><span class="fa fa-2x fa-users"></span></a><div class=task-thumb-details><h1><a href=#>Faculty Summary</a></h1></div></div><table class="personal-task table table-hover"><tr><td><i class="fa fa-tasks"></i><td>Total Faculty<td>N/A<tr><td><i class="fa fa-tasks"></i><td>DAMS Faculty<td>N/A<tr><td><i class="fa fa-tasks"></i><td>NON DAMS Faculty<td>N/A</table></section></div><div class=col-lg-3><section class=panel><div class=panel-body><a href=#><span class="fa fa-2x fa-users"></span></a><div class=task-thumb-details><h1><a href=#>Member Summary</a></h1></div></div><table class="personal-task table table-hover"><tr><td><i class="fa fa-tasks"></i><td>Student<td>N/A<tr><td><i class="fa fa-tasks"></i><td>Faculty<td>N/A<tr><td><i class="fa fa-tasks"></i><td>HOD<td>N/A</table></section></div><div class=col-lg-3><section class=panel><div class=panel-body><a href=#><span class="fa fa-2x fa-users"></span></a><div class=task-thumb-details><h1><a href=#>Course Summary</a></h1></div></div><table class="personal-task table table-hover"><tr><td><i class="fa fa-tasks"></i><td>Total Course<td>N/A<tr><td><i class="fa fa-tasks"></i><td>DAMS Faculty Course<td>N/A<tr><td><i class="fa fa-tasks"></i><td>Non DAMS Faculty Course<td>N/A<tr><td><i class="fa fa-tasks"></i><td>Today Rated Course<td>N/A</table></section></div><div class=col-lg-3><section class=panel><div class=panel-body></div><table class="personal-task table table-hover"><tr><td><i class="fa fa-tasks"></i><td>Total Revenue<td>N/A<tr><td><i class="fa fa-tasks"></i><td>Order<td>N/A<tr><td><i class="fa fa-tasks"></i><td>Recent Order<td>N/A<tr><td><i class="fa fa-tasks"></i><td>Pending Order<td>N/A</table></section></div>';
        echo json_encode(array('status' => true, 'html' => $html));
        die;
    }

    public function make_permission_group() {
        //set message on delete group..
        if ($this->session->userdata('delete_permission_group') != '' && $this->session->userdata('delete_permission_group') == 'Yes') {

            $data['page_toast'] = 'Information Deleted  successfully.';
            $data['page_toast_type'] = 'success';
            $data['page_toast_title'] = 'Action performed.';
            $this->session->unset_userdata('delete_permission_group');
        }
        //post data for add group..
        if ($_POST) {
            $this->form_validation->set_rules('permission_id[]', 'Permission', 'required');
            $this->form_validation->set_rules('permission_group_name', 'Permission Group Name', 'required');

            if ($this->form_validation->run() != FALSE) {

                if (!empty($_POST['permission_id'])) {
                    $permission_ids = implode(',', $_POST['permission_id']);
                } else {
                    $permission_ids = '';
                }
                $insert_data = array('permission_group_name' => $_POST['permission_group_name'], 'permission_fk_id' => $permission_ids);
                $this->db->insert('permission_group', $insert_data);

                $data['page_toast'] = 'Information Inserted  successfully.';
                $data['page_toast_type'] = 'success';
                $data['page_toast_title'] = 'Action performed.';
            }
        }
        //if data not post to add..
        $view_data['page'] = 'permission_group';
        $view_data['permission_lists'] = $this->backend_user->get_permission_list();
        $data['page_data'] = $this->load->view('admin/backend_user/make_permission_group', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajax_get_permission_group_list() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'id',
            1 => 'permission_group_name'
        );

        $query = "SELECT count(id) as total
								FROM permission_group where 1= 1
								";
        $query = $this->db->query($query);
        $query = $query->row_array();
        $totalData = (count($query) > 0) ? $query['total'] : 0;
        $totalFiltered = $totalData;

        $sql = "SELECT * FROM permission_group   where 1= 1 ";

        // getting records as per search parameters
        if (!empty($requestData['columns'][0]['search']['value'])) {   //name
            $sql .= " AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
        }
        if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
            $sql .= " AND permission_group_name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
        }


        $query = $this->db->query($sql)->result();

        $totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

        $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

        $result = $this->db->query($sql)->result();
        $data = array();
        foreach ($result as $r) {  // preparing an array
            $nestedData = array();

            $nestedData[] = $r->id;
            $nestedData[] = $r->permission_group_name;
            $action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "admin/edit_permission_group/" . $r->id . "'>Edit</a>&nbsp;"
                    . "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete this group?')\" href='" . AUTH_PANEL_URL . "admin/delete_permission_group/" . $r->id . "'>Delete</a>&nbsp;";

            $nestedData[] = $action;

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }

    public function edit_permission_group($id) {
        if ($_POST) {
            //echo "<pre>";print_r($_POST);die;
            $this->form_validation->set_rules('permission_id[]', 'Permission', 'required');
            $this->form_validation->set_rules('permission_group_name', 'Permission Group Name', 'required');

            if ($this->form_validation->run() != FALSE) {

                if (!empty($_POST['permission_id'])) {
                    $permission_ids = implode(',', $_POST['permission_id']);
                } else {
                    $permission_ids = '';
                }
                $update_data = array('permission_group_name' => $_POST['permission_group_name'], 'permission_fk_id' => $permission_ids);

                $this->db->where('id', $id);
                $this->db->update('permission_group', $update_data);

                $data['page_toast'] = 'Information Updated successfully.';
                $data['page_toast_type'] = 'success';
                $data['page_toast_title'] = 'Action performed.';
            }
        }

        $view_data['page'] = 'permission_group';
        $view_data['permission_lists'] = $this->backend_user->get_permission_list();
        $view_data['permission_detail'] = $this->backend_user->get_permission_detail_by_id($id);
        $data['page_data'] = $this->load->view('admin/backend_user/edit_permission_group', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function delete_permission_group($id) {
        $this->db->where('id', $id)
                ->delete('permission_group');
        $this->session->set_userdata('delete_permission_group', 'Yes');
        redirect('auth_panel/admin/make_permission_group');
    }
    
    
    function buy_free_membership($business_user_id){
           $input_data['validity'] = 1;
           $m_plan = $this->db->get_where('membership_package',array('id'=>1))->row_array();
           $input_data['membership_id'] = 1;
           $input_data['user_id'] = $business_user_id;
           $input_data['no_of_stores'] = $input_data['pending_stores'] = $m_plan['no_of_stores'];
           $input_data['is_store_unlimited'] = 0;
           if($m_plan['no_of_stores']=='multiple'){
               $input_data['is_store_unlimited'] = 1;
           }
           $input_data['no_of_discounts'] = $input_data['pending_discounts'] = $m_plan['no_of_discounts'];
           $input_data['is_discount_unlimited'] = 0;
           if($m_plan['no_of_discounts']=='multiple'){
                $input_data['is_discount_unlimited'] = 1;
           }
           $input_data['no_of_coupons'] = $input_data['pending_coupons'] = $m_plan['no_of_coupans'];
           $input_data['is_coupon_unlimited'] = 0;
           if($m_plan['no_of_coupans']=='multiple'){
               $input_data['is_coupon_unlimited'] = 1;
           }
           $input_data['events'] = $m_plan['events'];
           $input_data['responds_to_comment'] = $m_plan['responds_to_comment'];
           $input_data['send_notifications'] = $m_plan['send_notifications'];
           $input_data['business_reports'] = $m_plan['business_reports'];
           $input_data['email_suuport'] = $m_plan['email_support'];
           $input_data['dop'] = time();
           $input_data['price'] = 0;
           $input_data['modified'] =  date('Y-m-d H:i:s');
           //pre($input_data); die;
           $this->db->insert('membership_buy',$input_data);
            
           return true;
    }

}
