<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends MX_Controller {

function __construct() {

    parent::__construct();
    /* !!!!!! Warning !!!!!!!11
     *  admin panel initialization
     *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
     */
    $this->load->helper('aul');
    modules::run('auth_panel/auth_panel_ini/auth_ini');
    $this->load->library('session');
    
    $this->load->helper(array('form', 'url'));
    $this->load->helper('custom');
    
  }          
    
    /////////////////////  //////////////
    
 public function subject_list() {
    $view_data['page']              = 'subject_list';
    $view_data['currentuser']          = $this->session->userdata('active_backend_user_id');
    $data['page_data']              = $this->load->view('subject/subject_list', $view_data, TRUE);
    echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
 }
}
