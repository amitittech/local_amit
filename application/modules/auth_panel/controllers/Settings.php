<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul', 'url');
       modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->model("backend_user");
        $this->load->helper('custom');
    }

    public function set_price() {
		 $settings = $this->db->get('settings')->result_array();
        //($this->session->userdata('active_backend_user_id'));pre($ranking); die;
        if ($this->input->post()) {
            $input_data = $this->input->post();
            //pre($input_data); die;
            if (!empty($settings)) {
                $id = $settings[0]['id'];
				$this->db->where('id',$id);
                $this->db->update('settings',$input_data );
                
            }else{
                $this->db->insert('settings', $input_data);
                $id = $this->db->insert_id();
            }
            //$log_data = array("subject"=>$this->session->userdata('active_backend_user_id'),"subject_type"=>"admin","module"=>"ranking","request_string"=>"price = ".$input_data['price'][0],"action"=>"set price","object_id"=>$id,"object_name"=>"ranking","table_name"=>"ranking_master");        
            //create_logs($log_data);
            //pre($insert_data); //die;
            page_alert_box('success', 'settings Updated', 'settings has been updated successfully');
            redirect('auth_panel/Settings/set_price');
        }
        if (!empty($settings)) {
            $view_data['result'] = $settings;
        }

        $view_data['page'] = 'setting_view';
        $data['page_data'] = $this->load->view('support/setting_view', $view_data, TRUE);
        
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
	
	 

}
