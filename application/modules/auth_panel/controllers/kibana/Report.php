<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->helper('custom');
    }

    public function active_users() {
        $view_data['page'] = "active_users";
        $data['page_title'] = "Active Users Report";
        $data['page_data'] = $this->load->view('auth_panel/kibana/active_users', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function activity_report() {
        $view_data['page'] = "activity_report";
        $data['page_title'] = "Activity Report";
        $data['page_data'] = $this->load->view('auth_panel/kibana/activity_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    public function training_report() {
        $view_data['page'] = "training_report";
        $data['page_title'] = "Training Report";
        $data['page_data'] = $this->load->view('auth_panel/kibana/training_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

	public function user_activity_wall() {
        $view_data['page'] = "user_activity_wall";
        $data['page_title'] = "Activity Report On Wall";
        $data['page_data'] = $this->load->view('auth_panel/kibana/activity_report_wall', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
	
	public function user_activity_training() {
        $view_data['page'] = "user_activity_training";
        $data['page_title'] = "Activity Report On Training";
        $data['page_data'] = $this->load->view('auth_panel/kibana/activity_report_training', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
	
	public function user_activity_resources() {
        $view_data['page'] = "user_activity_resources";
        $data['page_title'] = "Activity Report For Teacher Tools";
        $data['page_data'] = $this->load->view('auth_panel/kibana/activity_report_resources', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
	
	public function activity_analytics() {
        $view_data['page'] = "activity_analytics";
        $data['page_title'] = "Activity Analytics";
        $data['page_data'] = $this->load->view('auth_panel/kibana/activity_analytics', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
	

}
