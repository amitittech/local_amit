<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wall extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('custom');
        $this->load->helper('file_upload');
        $this->load->model("Courses_model");
        $this->load->model("Wall_model");
        $this->load->model("Training_agencies_model");
    }

    public function channels() {
        $view_data = array();
        $mydata['file_url']        		= API_BASE_URL;
        $mydata['opcode']          		= 'get_wall_posting_users';
        $mydata['order_by']          	= 'escusm.usmfirstname';
        $mydata['order_in']          	= 'asc';
        //$mydata['attribute_name']  	= 'RIGHT_POST_EPC';
        $mydata['login_id']        		= $this->session->userdata('active_backend_user_id');
        //$mydata['appversion']    		= '1.0';
        //$mydata['assignedby']    		= $mydata['usmid']  = $this->session->userdata('active_backend_user_id');
        //$mydata['start']         		= "";

        $response                       = file_curl_contents($mydata);
        $view_data['users']             = $response['result'];

        $view_data['page']              = 'channels';
        $data['page_data']              = $this->load->view('wall/channels', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajax_master_channels_list() { 
        $data['file_url']   = API_BASE_URL;
        $data['opcode']     = 'master_channel_list';
        $data['login_id']   = $this->session->userdata('active_backend_user_id');
        $response           = file_curl_contents($data);
        $this->load->view('wall/ajax_channels_list',array("result"=>$response['result']));     
    }

  /*  public function ajax_create_channel() {
        if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            // $logo_image = rand(100000, 999999) . $_FILES['image']['name'];

            $logo_image=date('YmdHis').rand(100,999);
            if (move_uploaded_file($_FILES['image']['tmp_name'], TARGET_PATH_TO_UPLOAD_CHANNEL_LOGO . $logo_image)) {
                $insert_data['logo_image'] = $logo_image;
                //echo "The file ".basename($_FILES['image']['name'])." has been uploaded";
            }
        }
        $input = $this->input->post();
        $file_url = API_BASE_URL;
        $insert_data['opcode'] = 'add_channel';
        $insert_data['login_id'] = $this->session->userdata('active_backend_user_id');
        $insert_data['chmname'] = $input['chmname'];
        $insert_data['chmdesc'] = $input['chmdesc'];
        $insert_data['chmusmrefids'] = $input['chmusmrefids'];
        $insert_data['image'] = (array) $_FILES['image'];
        $response = call_api("POST", $file_url, json_encode($insert_data)); //pre($response);die;
		$tr = "<tr><th>#</th><th>Channel Name</th><th>User</th><th>Result</th><th>Message</th></tr>";        
		if($response){
			$res = json_decode($response,true);
            foreach($res['result'] as $r){
               $tr  = $tr."<tr><td>".$r['s_no']."</td><td>".$r['chmname']."</td><td>".$r['usmname']."</td><td>".$r['status']."</td><td>".$r['message']."</td></tr>";
            }
        }
        echo $tr;
        //echo json_encode($response);   
    }
	*/
	
	public function ajax_create_channel() {

        if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            // $logo_image = rand(100000, 999999) . $_FILES['image']['name'];
            $logo_image=date('YmdHis').rand(100,999);
            if (move_uploaded_file($_FILES['image']['tmp_name'], TARGET_PATH_TO_UPLOAD_CHANNEL_LOGO . $logo_image)) {
                $insert_data['logo_image'] = $logo_image;
                //echo "The file ".basename($_FILES['image']['name'])." has been uploaded";
            }
        }
        $input = $this->input->post();
        $file_url = API_BASE_URL;
        $insert_data['opcode'] = 'add_channel';
        $insert_data['login_id'] = $this->session->userdata('active_backend_user_id');
        $insert_data['chmname'] = $input['chmname'];
        $insert_data['chmdesc'] = $input['chmdesc'];
        //$insert_data['chmusmrefids'] = $input['chmusmrefids'];
        $insert_data['image'] = (array) $_FILES['image'];
        $response = call_api("POST", $file_url, json_encode($insert_data)); 

                
        if($response){

            echo $response;
        }
          
    }
    public function ajax_delete_channels() {
        $input      = $this->input->post();
        //pre($input); die;
        $file_url   = API_BASE_URL;
        $data['opcode'] = 'delete_channels';
        $data['login_id'] = $this->session->userdata('active_backend_user_id');
        $data['channel_ids'] = $input['channel_ids'];

        $response = call_api("POST", $file_url, json_encode($data));
        // Call a api to delete the records
        //file_curl_contents($data);
        //page_alert_box('success', 'Channel deleted', 'Selected channels have been deleted successfully');
        //redirect('auth_panel/Training/courses_traing_mapping');
    }

    public function ajax_edit_channel(){ 
		$this->db->where('chmrefid !=',$_POST['chmrefid']);
		$this->db->where('chmusmrefid',$_POST['usmrefid']);
		$this->db->where('chmname',$_POST['chmname']);
		$exist = $this->db->get('escchm')->row();
		//pre($exist);
		if($exist){
			$response['status'] 	= false;
			$response['message'] = 'Channel already exist.';
		}else{
			$update['modifieddate'] = date('Y-m-d H:i:s');
			$update['chmname'] 		= $_POST['chmname'];
			if(!empty($_FILES['chmlogo']) ){
				if(!empty($_FILES["chmlogo"]["type"])){
					// $fileName = rand(100000,999999).$_FILES['chmlogo']['name'];
                    $fileName=date('YmdHis').rand(100,999);
					$valid_extensions = array("jpeg", "jpg", "png");
					$temporary = explode(".", $_FILES["chmlogo"]["name"]);
					$file_extension = end($temporary);
					if((($_FILES["chmlogo"]["type"] == "image/png") || ($_FILES["chmlogo"]["type"] == "image/jpg") || ($_FILES["chmlogo"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
						$sourcePath = $_FILES['chmlogo']['tmp_name'];
						move_uploaded_file($sourcePath,TARGET_PATH_TO_UPLOAD_CHANNEL_LOGO.$fileName);
						$update['chmlogo'] = $fileName;
					}
				}
			} 
			$this->db->where('chmrefid',$_POST['chmrefid']);
			$update = $this->db->update('escchm',$update);
			$response['status'] 	= true;
			$response['message'] = 'Channel upadted successfully.';
		}
		echo json_encode($response);
    }
	
	public function wall_posters() {
        $view_data['page']              = 'wall_posters';
        $data['page_data']              = $this->load->view('wall/wall_posters', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajax_wall_posters_list() { 
        $data['file_url']   = API_BASE_URL;
        $data['opcode']     = 'get_wall_posting_users';
		//$data['attribute_name']  = 'RIGHT_POST_EPC';
        $data['login_id']   = $this->session->userdata('active_backend_user_id');
        $response           = file_curl_contents($data);
        $this->load->view('wall/ajax_wall_posters',array("result"=>$response['result']));     
    }
		
	public function add_wall_poster() {
        $input                    = $this->input->post();
        $input['user_mobiles']    = explode(',',$input['user_mobiles']);
        $input['opcode']          = 'add_wall_poster';
        $input['role']            = 'CL-Admin';
        $input['login_id']        = $this->session->userdata('active_backend_user_id');
        $response                 = call_api("POST",site_url().'/data_model/Api/index',json_encode($input)); //pre($response) ;
        
        $tr = "<tr><th>#</th><th>Mobile</th><th>Result</th><th>Message</th></tr>";        
        if($response){ 
            $res = json_decode($response,true);
            if($res['status'] == true){
                $n=0;
                foreach($res['result'] as $r){ $n++;
                    $tr  = $tr."<tr><td>".$n."</td><td>".$r['mobile']."</td><td>".$r['status']."</td><td>".$r['message']."</td></tr>";
                }
            }else{
                $tr  = $tr.'<tr><td colspan="5">'.$res['message'].'</td></tr>';
            }
        }else{
            $tr  = $tr.'<tr><td colspan="5">Something went wrong</td></tr>';
        }
        echo $tr;
    }
	
	public function ajax_delete_wall_posters() {
        $input      = $this->input->post();
        $file_url   = API_BASE_URL;
        $data['opcode'] = 'delete_wall_posters';
        $data['login_id'] = $this->session->userdata('active_backend_user_id');
        $data['delete_ids'] = $input['delete_ids'];

        $response = call_api("POST", $file_url, json_encode($data));
    }
	
	public function get_all_channel(){

		$data   = $this->input->post();
		$userid =$data['userid'];
        $sql = "SELECT * FROM `escchm` WHERE `chmusmrefid`='1'  AND `isactive`=1 AND `isdeleted`=0 GROUP BY `chmname` ORDER BY `chmname`";
        $query = $this->db->query($sql);
		$last_query=    $this->db->last_query();
        $channels =  $query->result_array();
		$this->load->view('wall/select_dropdown',array("result"=>$channels,"userid"=>$userid)); 
        
   }

    public function manage_channel(){
echo  'sd'; die;
        $data = $this->input->post();
        $userid= $data['id'];


        $this->db->where('chmusmrefid',$userid);
        $this->db->where('isdeleted',0);
        $check_poster =  $this->db->get('escchm')->result_array();

        if($check_poster){

        $this->db->where('chmusmrefid',$userid);
        $update['isdeleted'] = 1;
        $update = $this->db->update('escchm',$update);
        $last=   $this->db->last_query();

        }
       
        if(isset($data['channel'])){

                foreach($data['channel'] as $key => $value) {

                $this->db->where('chmusmrefid',$userid);
                $this->db->where('chmname',$value);
                $this->db->where('isdeleted',1);
                $checkchannel = $this->db->get('escchm')->row();
                $last1=   $this->db->last_query();

                if($checkchannel){

                $update_data=array();
                $update_data['isactive'] = 1;
                $update_data['isdeleted'] = 0;
                $update_data['modifieddate'] = date('Y-m-d H:i:s');
                $this->db->where('chmusmrefid',$userid);
                $this->db->where('chmname',$value);
                $checkchannel = $this->db->update('escchm',$update_data);

                }else{

                $this->db->where('chmname',$value);
                $channel_data =  $this->db->get('escchm')->row();
                // echo "<pre>";print_r($channel_data);die;
                $inserting = array();
                $inserting['chmname']       = $channel_data->chmname;
                $inserting['chmdesc']       = $channel_data->chmdesc;
                $inserting['chmusmrefid']   = $userid;
                $inserting['chmlogo']       = $channel_data->chmlogo;
                $inserting['createdate']   = date('Y-m-d H:i:s');
                $inserting['isdeleted']     = 0;
                $inserting['isactive']      = 1;
                $this->db->insert('escchm', $inserting);
                $last=    $this->db->last_query();
                }

                }
        }
            $response= array('msg'=>'sucess');
            echo json_encode($response);

    } 

}
