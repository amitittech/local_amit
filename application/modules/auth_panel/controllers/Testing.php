<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Testing extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('custom');
        $this->load->model("Courses_model");
        $this->load->model("Training_agencies_model");
    }
        
    public function index() {
        // pre($this->session->userdata('active_backend_user_id')); die;
        $view_data = array();
        if ($this->input->post()) {
            $input = $this->input->post();
            if(isset($input['mapping_btn'])){
                $insert_data['file_url']        = site_url().'/data_model/Api/index';
                $insert_data['opcode']          = 'training_n_courses_mapping';
                $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
                $insert_data['trarefids']       = implode(',',$input['trarefids']);
                $insert_data['rcmrefids']       = implode(',',$input['rcmrefids']);
                file_curl_contents($insert_data);
                page_alert_box('success', 'Training Agencies And Courses Mapping ', 'Mapping has been done successfully');  
                redirect('auth_panel/Training/courses_traing_mapping');
            }else if(isset($input['delete_btn'])){
                page_alert_box('success', 'Mapping Deleted', 'Mapping has been deleted successfully');  
                redirect('auth_panel/Training/courses_traing_mapping');
            }
        }
        $input                          = $this->input->post();              
        $insert_data['file_url']        = site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'get_training_agencies';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        //$insert_data['appversion']      = '1.0';
        //$insert_data['assignedby']      = $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$insert_data['start']         = "";
        $response                       = file_curl_contents($insert_data);
        $view_data['training_agencies'] = $response['Result'];
        
        $insert_data['file_url']        = site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'get_training_courses';
        //$insert_data['appversion']      = '1.0';
        //$insert_data['assignedby']      = $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$insert_data['start']         = "";
        $response                         = file_curl_contents($insert_data);
        $view_data['courses']           = $response['Result'];
        
        //$view_data['training_agencies'] = $this->Training_agencies_model->get_training_agencies_list();
        //$view_data['courses']           = $this->Courses_model->get_courses_list();        
        $view_data['page']              = 'courses_traing_mapping';
        $data['page_data']              = $this->load->view('testing', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
     
    public function ajax_courses_traing_mapped_list() {
        // storing  request (ie, get/post) global array to a variable
        //user_panel = site_url('auth_panel/');
        $requestData = $_REQUEST;
        
        //PRE($_SERVER); die;
        $columns = array(
            // datatable column index  => database column name
            0 => 'sno',
            1 => 'agency',
            2 => 'course'
        );
        //$this->db->select('count(trarcmrefid) as total');   
        //$this->db->where('isactive',1);
        //$this->db->where('isdeleted',0);
        //$query                          = $this->db->get('clt_tra_rcm')->row_array();
        //$totalData                      = (count($query) > 0) ? $query['total'] : 0;
        //$totalFiltered                  = $totalData;
        
        $input                          = $this->input->post();              
        $insert_data['file_url']        = site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'training_n_courses_mapping_list';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        $response                         = file_curl_contents($insert_data);
        //pre($response);die;
        $totalData                      = (count($response['Result']) > 0) ? count($response['Result']) : 0;
        $totalFiltered                  = $totalData;
            
        $insert_data['start']           = $requestData['start'];
        $insert_data['length']          = $requestData['length'];
        $response                       = file_curl_contents($insert_data);
        //pre($response);die;
        $result                         = $response['Result'];
        /*        
        $select = "clt_tra_rcm.trarcmrefid as mapping_id,clt_tra_rcm.trarefid,clt_tra_rcm.rcmrefid,curtra.traname,currcm.rcmname";
        $this->db->select($select);
        $this->db->where('clt_tra_rcm.isactive',1);
        $this->db->where('clt_tra_rcm.isdeleted',0);                
        $this->db->where('curtra.isdeleted',0);                
        //$this->db->where('currcm.isdeleted',0);                
        $this->db->join('curtra','curtra.trarefid=clt_tra_rcm.trarefid');
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid');
        $this->db->limit($requestData['length'], $requestData['start']);
        $result = $this->db->get('clt_tra_rcm')->result();
        */
        $data = array();
        $i = $requestData['start'];
        foreach ($result as $r) {  // preparing an array
            $rcmversionlabel = (!empty($r['rcmversionlabel']))?' - '.$r['rcmversionlabel']:"";
            $i++;
            $nestedData     = array();
            $nestedData[]   = $i;
            $nestedData[]   = $r['traname'];
            $nestedData[]   = $r['chaptername'].$rcmversionlabel;
            //$nestedData[] = "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete?')\" href='" . base_url() . "index.php/auth_panel/Training/delete_courses_training_mapping/" . $r->mapping_id . "'>Delete</a>&nbsp;";
            $nestedData[]   = '<input value="'.$r['mapping_id'].'" type="checkbox" name="mapping_ids[]" class="mapping_ids">';
            //$nestedData[] = "<button class='btn-xs bold btn btn-danger delete_mapping' mapping__row_id='$r->mapping_id' onclick=\"return confirm('Are you sure you want to delete?')\" >Delete entry</button>&nbsp;";
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }
       
}
