<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Training extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('custom');
        $this->load->model("Courses_model");
        $this->load->model("Training_agencies_model");
    }
        
    public function courses_traing_mapping() {
        $view_data 						= array();
        $input                          = $this->input->post();              
        $insert_data['file_url']        = API_BASE_URL;//site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'get_training_agencies';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        //$insert_data['appversion']      = '1.0';
        //$insert_data['assignedby']      = $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$insert_data['start']         = "";
        $response                       = file_curl_contents($insert_data);
        $view_data['training_agencies'] = $response['result'];
        
        $insert_data['file_url']        = API_BASE_URL;//site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'get_training_courses';
        //$insert_data['appversion']      = '1.0';
        //$insert_data['assignedby']      = $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$insert_data['start']         = "";
        $response                         = file_curl_contents($insert_data);
        $view_data['courses']           = $response['result'];
        
        //$view_data['training_agencies'] = $this->Training_agencies_model->get_training_agencies_list();
        //$view_data['courses']           = $this->Courses_model->get_courses_list();        
        $view_data['page']              = 'courses_traing_mapping';
        $data['page_data']              = $this->load->view('training/courses_traing_mapping', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    public function ajax_course_mapping_list(){
        $data['file_url'] 	= API_BASE_URL;#site_url() . '/data_model/Api/index';
        $data['opcode'] 	= 'training_n_courses_mapping_list';
        $data['login_id'] 	= $this->session->userdata('active_backend_user_id');
        $response 			= file_curl_contents($data);$this->load->view('training/ajax_training_n_courses_mapping_list',array("result"=>$response));
    }
    
    public function ajax_courses_mapping() { 
        $input                          = $this->input->post();
        //pre($input); die;
        //$insert_data['file_url']        = site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'training_n_courses_mapping';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        $insert_data['trainings']       = $input['trainings'];//implode(',',$input['trarefids']);
        $insert_data['courses']       = $input['courses'];//implode(',',$input['rcmrefids']);
        //$response                       = file_curl_contents_with_array($insert_data);
        $response                       = call_api("POST",API_BASE_URL,json_encode($insert_data));
        $tr = "<tr><th>#</th><th>Training</th><th>Course</th><th>Result</th><th>Message</th></tr>";        
		if($response){
			$n=0;
            //$res = $response;
            $res = json_decode($response,true);
            //pre($res['result']);die;
            foreach($res['result'] as $r){ $n++;
               $tr  = $tr."<tr><td>".$n."</td><td>".$r['training_name']."</td><td>".$r['course_name']."</td><td>".$r['status']."</td><td>".$r['message']."</td></tr>";
            }
        }
        echo $tr;
        //pre($response); ;
        //pre($insert_data);			pre($response); die('working');
        //page_alert_box('success', 'Training Agencies And Courses Mapping ', 'Mapping has been done successfully');
        //return $response;
        
        //redirect('auth_panel/Training/courses_traing_mapping');
    }
    
    public function ajax_courses_traing_mapped_list() {
        $requestData    = $_REQUEST;
        $columns = array(
            // datatable column index  => database column name
            0 => 'trarcmrefid',
            1 => 'traname',
            2 => 'chaptername'
        );
        $input                          = $this->input->post();              
        $insert_data['file_url']        = API_BASE_URL;//site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'training_n_courses_mapping_list';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        $response                       = file_curl_contents($insert_data);
        
        $totalData                      = (count($response['result']) > 0) ? count($response['result']) : 0;
        $totalFiltered                  = $totalData;
                
        $insert_data['start']           = $requestData['start'];
        $insert_data['length']          = $requestData['length'];
        $insert_data['order_by_column'] = $columns[$requestData['order'][0]['column']];
        $insert_data['order_by_order']  = $requestData['order'][0]['dir'];
        //pre($insert_data);
        $response                       = file_curl_contents($insert_data);
        //pre($response);die;
        $result                         = $response['result'];
        if(empty($result)){
            $result = [];
        }
        //pre($result); die;
        /*        
        $select = "clt_tra_rcm.trarcmrefid as mapping_id,clt_tra_rcm.trarefid,clt_tra_rcm.rcmrefid,curtra.traname,currcm.rcmname";
        $this->db->select($select);
        $this->db->where('clt_tra_rcm.isactive',1);
        $this->db->where('clt_tra_rcm.isdeleted',0);                
        $this->db->where('curtra.isdeleted',0);                
        //$this->db->where('currcm.isdeleted',0);                
        $this->db->join('curtra','curtra.trarefid=clt_tra_rcm.trarefid');
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid');
        $this->db->limit($requestData['length'], $requestData['start']);
        $result = $this->db->get('clt_tra_rcm')->result();
        */
        $data = array();
        $i = $requestData['start'];
        foreach ($result as $r) {  // preparing an array
            $rcmversionlabel = (!empty($r['rcmversionlabel']))?' - '.$r['rcmversionlabel']:"";
            $i++;
            $nestedData     = array();
            $nestedData[]   = $i;
            $nestedData[]   = $r['traname'];
            $nestedData[]   = $r['chaptername'].$rcmversionlabel;
            //$nestedData[] = "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete?')\" href='" . base_url() . "index.php/auth_panel/Training/delete_courses_training_mapping/" . $r->mapping_id . "'>Delete</a>&nbsp;";
            $nestedData[]   = '<input value="'.$r['mapping_id'].'" type="checkbox" name="mapping_ids[]" class="mapping_ids">';
            //$nestedData[] = "<button class='btn-xs bold btn btn-danger delete_mapping' mapping__row_id='$r->mapping_id' onclick=\"return confirm('Are you sure you want to delete?')\" >Delete entry</button>&nbsp;";
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }
    
    /*
     public function ajax_courses_traing_mapped_list() {
        // storing  request (ie, get/post) global array to a variable
        //user_panel = site_url('auth_panel/');
        $requestData = $_REQUEST;
        $columns = array(
            // datatable column index  => database column name
            0 => 'sno',
            1 => 'agency',
            2 => 'course'
        );
        $this->db->select('count(trarcmrefid) as total');   
        $this->db->where('isactive',1);
        $this->db->where('isdeleted',0);
        $query = $this->db->get('clt_tra_rcm')->row_array();
        $totalData = (count($query) > 0) ? $query['total'] : 0;
        $totalFiltered = $totalData;
        $select = "clt_tra_rcm.trarcmrefid as mapping_id,clt_tra_rcm.trarefid,clt_tra_rcm.rcmrefid,curtra.traname,currcm.rcmname";
        $this->db->select($select);
        $this->db->where('clt_tra_rcm.isactive',1);
        $this->db->where('clt_tra_rcm.isdeleted',0);                
        $this->db->where('curtra.isdeleted',0);                
        //$this->db->where('currcm.isdeleted',0);                
        $this->db->join('curtra','curtra.trarefid=clt_tra_rcm.trarefid');
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid');
        $this->db->limit($requestData['length'], $requestData['start']);
        $result = $this->db->get('clt_tra_rcm')->result();
        $data = array();
        $i = 0;
        foreach ($result as $r) {  // preparing an array
            $i++;
            $nestedData     = array();
            $nestedData[]   = $i;
            $nestedData[]   = $r->traname;
            $nestedData[]   = $r->rcmname;
            $nestedData[]   = "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete?')\" href='" . base_url() . "index.php/auth_panel/Training/delete_courses_training_mapping/" . $r->mapping_id . "'>Delete</a>&nbsp;";
            //$nestedData[]   = "<button class='btn-xs bold btn btn-danger delete_mapping' mapping__row_id='$r->mapping_id' onclick=\"return confirm('Are you sure you want to delete?')\" >Delete entry</button>&nbsp;";
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }
    */    
	
	
    
    public function delete_courses_training_mapping($id) {          
        //pre($this->input->post()); die;
        $this->db->where('trarcmrefid',$id);
        $exist = $this->db->get('clt_tra_rcm')->row();
        if($exist){
            $this->db->where('trarcmrefid',$id);
            $this->db->update('clt_tra_rcm',array('isdeleted'=>1,'isactive'=>0));
        }
        //page_alert_box('success', 'Mapping deleted', 'Mapping has been deleted successfully');
        redirect('auth_panel/Training/courses_traing_mapping');
    }
    
    public function delete_selected_courses_traing_mapping() {          
        $input                          = $this->input->post();              
        $insert_data['file_url']        = API_BASE_URL; #site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'delete_courses_training_mapping';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        $insert_data['mapping_ids']       = implode(',',$input['mapping_ids']);
        
        // Call a api to delete the records
        file_curl_contents($insert_data);
        
        //page_alert_box('success', 'Mapping deleted', 'Mapping has been deleted successfully');
        redirect('auth_panel/Training/courses_traing_mapping');
    }
    
    public function ajax_delete_courses_training_mapping() {        
        $input                          = $this->input->post(); 
        //pre($input); //die;
        $file_url        = site_url().'/data_model/Api/index';
        $my_data['opcode']          = 'delete_courses_training_mapping';
        $my_data['login_id']        = $this->session->userdata('active_backend_user_id');
        $my_data['mapping_ids']     = $input['mapping_ids'];
        
        // Call a api to delete the records
        //file_curl_contents($my_data);
		$response                       = call_api("POST",$file_url,json_encode($my_data));
        
        //page_alert_box('success', 'Mapping deleted', 'Mapping has been deleted successfully');
        //redirect('auth_panel/Training/courses_traing_mapping');
    }
    
    
    public function traing_agencies() {
       // pre($this->session->userdata('active_backend_user_id'));die;
        // pre($this->session->userdata('active_backend_user_id')); die;
        //$view_data['training_agencies'] = $this->Training_agencies_model->get_training_agencies_list();
        //$view_data['courses']           = $this->Courses_model->get_courses_list();        
        $view_data['page']              = 'traing_agencies';
        $data['page_data']              = $this->load->view('training/traing_agencies_list', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    public function ajax_traing_agencies_list() {
        // storing  request (ie, get/post) global array to a variable
        //user_panel = site_url('auth_panel/');
        $requestData = $_REQUEST;
        $columns = array(
            // datatable column index  => database column name
            0 => 'sno',
            1 => 'agency'
        );
        $input                          = $this->input->post();              
        $insert_data['file_url']        = site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'get_training_agencies';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        //$insert_data['appversion']      = '1.0';
        //$insert_data['assignedby']      = $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$insert_data['start']         = "";
        //pre($insert_data);
        $response                         = file_curl_contents($insert_data);
        //pre($response);die;
        $totalData                      = (count($response['result']) > 0) ? count($response['result']) : 0;
        $totalFiltered                  = $totalData;
            
        $insert_data['start']           = $requestData['start'];
        $insert_data['length']          = $requestData['length'];
        $response                       = file_curl_contents($insert_data);
        $result                         = $response['result'];
        
        
        //$select = "trarefid,traname";
        //$this->db->select($select);                
        //$this->db->where('curtra.isdeleted',0);                
        //$this->db->limit($requestData['length'], $requestData['start']);
        //$result = $this->db->get('curtra')->result();//currcm
        $data = array();
        $i = $requestData['start'];
        foreach ($result as $r) {  // preparing an array
            $i++;
            $nestedData     = array();
            $nestedData[]   = $i;
            $nestedData[]   = $r['traname'];
            //$nestedData[]   = "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete?')\" href='" . base_url() . "index.php/auth_panel/Training/delete_courses_training_mapping/" . $r->mapping_id . "'>Delete</a>&nbsp;";
            //$nestedData[]   = '<input value="'.$r->mapping_id.'" type="checkbox" name="mapping_ids[]" class="mapping_ids">';
            //$nestedData[]   = "<button class='btn-xs bold btn btn-danger delete_mapping' mapping__row_id='$r->mapping_id' onclick=\"return confirm('Are you sure you want to delete?')\" >Delete entry</button>&nbsp;";
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }
    
    
    public function traing_courses() { //pre($this->session->userdata());die;
        // pre($this->session->userdata('active_backend_user_id')); die;
        //$view_data['training_agencies'] = $this->Training_agencies_model->get_training_agencies_list();
        //$view_data['courses']           = $this->Courses_model->get_courses_list();        
        $view_data['page']              = 'traing_courses';
        $data['page_data']              = $this->load->view('training/traing_courses_list', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    public function ajax_traing_courses_list() {
        // storing  request (ie, get/post) global array to a variable
        //user_panel = site_url('auth_panel/');
        $requestData = $_REQUEST;
        $columns = array(
            // datatable column index  => database column name
            0 => 'sno',
            1 => 'agency'
        );
          
        $input                          = $this->input->post();              
        $insert_data['file_url']        = site_url().'/data_model/Api/index';
        $insert_data['opcode']          = 'get_training_courses';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        //$insert_data['start']         = "";
        $response                         = file_curl_contents($insert_data);
        //pre($response);die;
        $totalData                      = (count($response['result']) > 0) ? count($response['result']) : 0;
        $totalFiltered                  = $totalData;
            
        
        $insert_data['start']           = $requestData['start'];
        $insert_data['length']          = $requestData['length'];
        $response                       = file_curl_contents($insert_data);
        $result                         = $response['result'];
        
        $data = array();
        $i = $requestData['start'];
        foreach ($result as $r) {  // preparing an array
            $i++;
            $rcmversionlabel = (!empty($r['rcmversionlabel']))?' - '.$r['rcmversionlabel']:"";
            $nestedData     = array();
            $nestedData[]   = $i;
            $nestedData[]   = $r['chaptername'].$rcmversionlabel;
            //$nestedData[]   = "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete?')\" href='" . base_url() . "index.php/auth_panel/Training/delete_courses_training_mapping/" . $r->mapping_id . "'>Delete</a>&nbsp;";
            //$nestedData[]   = '<input value="'.$r->mapping_id.'" type="checkbox" name="mapping_ids[]" class="mapping_ids">';
            //$nestedData[]   = "<button class='btn-xs bold btn btn-danger delete_mapping' mapping__row_id='$r->mapping_id' onclick=\"return confirm('Are you sure you want to delete?')\" >Delete entry</button>&nbsp;";
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }
    
    
    
    
    public function visibility() {
		$view_data['page']              = 'visibility';
        $data['page_data']              = $this->load->view('training/toggle_visibility', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    public function ajax_visibility_list() {
        $requestData = $_REQUEST;
        $columns = array(
            0 => 'sno',
            1 => 'agency'
        );
        $input                          = $this->input->post();              
        $my_data['file_url']        = API_BASE_URL;
        $my_data['opcode']          = 'get_training_toggle_visibility';
        $my_data['login_id']        = $this->session->userdata('active_backend_user_id');
        //$my_data['appversion']      = '1.0';
        //$my_data['assignedby']      = $my_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$my_data['start']         = "";
        //pre($my_data);
        $response                         = file_curl_contents($my_data);
        $totalData                      = (count($response['result']) > 0) ? count($response['result']) : 0;
        $totalFiltered                  = $totalData;
            
        $my_data['start']           = $requestData['start'];
        $my_data['length']          = $requestData['length'];
        $response                       = file_curl_contents($my_data);
        $result                         = $response['result'];        
        
        $data = array();
        $i = $requestData['start'];
        foreach ($result as $r) {  // preparing an array
            $btn = '<button type="button"  class="btn btn-danger col-md-6 change_button"  onclick="abc(this,'.$r['trnrefid'].','.$r['isvisibleonclient'].')">OFF</button>';
            if($r['isvisibleonclient']==1){
                $btn = '<button type="button"  class="btn btn-success col-md-6 change_button" onclick="abc(this,'.$r['trnrefid'].','.$r['isvisibleonclient'].')" >ON</button>';
            }
            $i++;
            $nestedData     = array();
            $nestedData[]   = $i;
            $nestedData[]   = $r['trnname'];
            $nestedData[]   = $btn;
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }
	
	public function update_visibility() {        
        $input                          = $this->input->post();
		//pre($input); die;
        $data['file_url']        		= API_BASE_URL;
        $data['opcode']          		= 'update_training_toggle_visibility';
        $data['login_id']        		= $this->session->userdata('active_backend_user_id');
        $data['trnrefid']     			= $input['trnrefid'];
        $data['isvisibleonclient']     	= ($input['isvisibleonclient']==1)?0:1;	
		if($data['isvisibleonclient']==1){
			echo $btn = '<button type="button" class="btn btn-success col-md-6 change_button" onclick="abc(this,'.$input['trnrefid'].',1)">ON</button>';
		}else{
			echo $btn = '<button type="button" class="btn btn-danger col-md-6 change_button" onclick="abc(this,'.$input['trnrefid'].',0)">OFF</button>';
		}
		
			
		// Call a api to delete the records
        file_curl_contents($data);
		
		
        return $btn;
        //page_alert_box('success', 'Training Visibility', 'Visibility has been updated successfully');
        //redirect('auth_panel/Training/courses_traing_mapping');
    }
	
	//--------Users Mapping-------------------------------------//
    
    public function users_mapping() {
        $view_data                      = array();
        $input                          = $this->input->post();              
        $my_array['file_url']        	= API_BASE_URL;
        $my_array['opcode']          	= 'get_training_agencies';
        $my_array['login_id']        	= $this->session->userdata('active_backend_user_id');
        //$my_array['appversion']      	= '1.0';
        //$my_array['assignedby']      	= $my_array['usmid']  = $this->session->userdata('active_backend_user_id');
        //$my_array['start']         	= "";
        $response                       = file_curl_contents($my_array);
        $view_data['training_agencies'] = $response['result'];
        
            
        $view_data['page']              = 'users_mapping';
        $data['page_data']              = $this->load->view('training/users_mapping', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    public function ajax_users_mapping_list() { 
        $data['file_url']   			= API_BASE_URL;
        $data['opcode']     			= 'list_of_mapping_users_to_trainings';
        $data['login_id']   			= $this->session->userdata('active_backend_user_id');
        $response           			= file_curl_contents($data);
        $this->load->view('training/users_mapping_ajax_page',array("result"=>$response['result']));     
    }
    
    public function ajax_users_mapping() {
        //pre($this->input->post());       die;
        $input                          = $this->input->post();
        $insert_data['opcode']          = 'map_users_to_trainings';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        $insert_data['trainings']       = $input['trainings'];
        $insert_data['user_mobiles']    = $input['users_mobiles'];
        $insert_data['user_role']       = $input['user_role'];
        //pre($insert_data);die;
        $response                       = call_api("POST",API_BASE_URL,json_encode($insert_data));
        #pre($response);
        $tr = "<tr><th>#</th><th>Training</th><th>User Mobile</th><th>User Role</th><th>Result</th><th>Message</th></tr>";        
        if($response){
            $n=0;
            $res =$response;
            #$res = json_decode($response,true);
            foreach($res['result'] as $r){ $n++; 				
               $tr  = $tr."<tr><td>".$n."</td><td>".$r['training_name']."</td><td>".$r['mobile']."</td><td>".$r['role']."</td><td>".$r['status']."</td><td>".$r['message']."</td></tr>";
            }
        }
        echo $tr;
    }

    public function ajax_delete_users_mapping() {        
        $input                          = $this->input->post();
		$data['opcode']          		= 'delete_users_mapping';
        $data['login_id']        		= $this->session->userdata('active_backend_user_id');
        $data['mapping_ids']     		= $input['mapping_ids'];
		$response                       = call_api("POST",API_BASE_URL,json_encode($data));
    }
    
	
	///////////////////// SENDER ID MAPPING //////////////
	
	public function sender_id_mapping() {
		// pre($this->session->userdata('active_backend_user_id')); 
		//die;
		
		$view_data 						= array();             
        $my_array['file_url']        	= API_BASE_URL; #site_url().'/data_model/Api/index';
        $my_array['opcode']          	= 'get_training_agencies';
        $my_array['login_id']        	= $this->session->userdata('active_backend_user_id');
        //$my_array['appversion']      	= '1.0';
        //$my_array['assignedby']      	= $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$my_array['start']         	= "";
        $response                       = file_curl_contents($my_array);
        $view_data['training_agencies'] = $response['result'];
		
		//$my_array['file_url']        	= site_url().'/data_model/Api/index';
        $my_array['opcode']          	= 'sender_id_list';
        //$my_array['login_id']        	= $this->session->userdata('active_backend_user_id');
        //$my_array['appversion']      	= '1.0';
        //$my_array['assignedby']      	= $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$my_array['start']         	= "";
        $response                       = file_curl_contents($my_array);
        $view_data['sender_ids'] 		= $response['result'];
		
	/*	           
        $my_data['file_url']        	= site_url().'/data_model/Api/index';
        $my_data['opcode']          	= 'sender_id_mapping_list';
        $my_data['login_id']        	= $this->session->userdata('active_backend_user_id');
        //$insert_data['appversion']    = '1.0';
        //$insert_data['assignedby']    = $insert_data['usmid']  = $this->session->userdata('active_backend_user_id');
        //$insert_data['start']         = "";
        $response                       = file_curl_contents($my_data);
        $view_data['mappings'] 			= $response['result'];
        */
            
        $view_data['page']              = 'sender_id_mapping';
        $data['page_data']              = $this->load->view('training/sender_id_mapping', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
     
    
    public function ajax_sender_mapping_list(){
        $data['file_url'] 			= API_BASE_URL;
        $data['opcode'] 			= 'sender_id_mapping_list';
        $data['login_id'] 			= $this->session->userdata('active_backend_user_id');
        $response 					= file_curl_contents($data);
        $this->load->view('training/ajax_sender_id_mapping',array("result"=>$response));
    }
	
    public function ajax_map_sender_id() {
        //pre($this->input->post());       die;
        $input                          = $this->input->post();
        $insert_data['opcode']          = 'map_sender_id';
        $insert_data['login_id']        = $this->session->userdata('active_backend_user_id');
        $insert_data['trainings']       = $input['trainings'];
        $insert_data['sender_ids']     = $input['sender_ids'];
        //pre($insert_data);die;
        $response                       = call_api("POST",API_BASE_URL,json_encode($insert_data));
		$tr = "<tr><th>#</th><th>Training Name</th><th>Sender-Id</th><th>Result</th><th>Message</th></tr>";        
		if($response){
			$n=0;
            //$res = $response;
            $res = json_decode($response,true);
            //pre($res['result']);die;
            foreach($res['result'] as $r){ $n++;
               $tr  = $tr."<tr><td>".$n."</td><td>".$r['training_name']."</td><td>".$r['sender_id_name']."</td><td>".$r['status']."</td><td>".$r['message']."</td></tr>";
            }
        }
        echo $tr;
        //file_curl_contents($insert_data);
        //page_alert_box('success', 'Training Agencies And SENDER-IDS Mapping ', 'Mapping has been done successfully');
        //echo json_encode($response);
        //return $response;
    }

    public function ajax_delete_sender_id_mapping() {        
        $input                          = $this->input->post();
		$data['opcode']          		= 'delete_sender_id_mapping';
        $data['login_id']        		= $this->session->userdata('active_backend_user_id');
        $data['mapping_ids']     		= $input['mapping_ids'];
        
		$response                       = call_api("POST",API_BASE_URL,json_encode($data));
        // Call a api to delete the records
        //file_curl_contents($data);
        
        //page_alert_box('success', 'Mapping deleted', 'Mapping has been deleted successfully');
        //redirect('auth_panel/Training/courses_traing_mapping');
    }
	
	
	
    /////////////////////  Certificate Templates MAPPING //////////////

    public function certificate_mapping() {
        $view_data = array();
        $my_array['file_url']        	= API_BASE_URL;
        $my_array['opcode']          	= 'get_training_agencies';
        $my_array['login_id']        	= $this->session->userdata('active_backend_user_id');
        //$my_array['appversion']      	= '1.0';
        //$my_array['assignedby']      	= $my_array['usmid']  = $this->session->userdata('active_backend_user_id');
        //$my_array['start']         	= "";
        $response                       = file_curl_contents($my_array);
        $view_data['training_agencies'] = $response['result'];

        //$my_array['file_url']        	= site_url() . '/data_model/Api/index';
        $my_array['opcode']          	= 'certificates_list';
        //$my_array['login_id']        	= $this->session->userdata('active_backend_user_id');
        //$my_array['appversion']      	= '1.0';
        //$my_array['assignedby']      	= $my_array['usmid']  = $this->session->userdata('active_backend_user_id');
        //$my_array['start']         	= "";
        $response                       = file_curl_contents($my_array);
        $view_data['certificates']      = $response['result'];
        
        $view_data['page']              = 'certificate_mapping';
        $data['page_data']              = $this->load->view('training/certificate_mapping/certificate_mapping', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
    
    public function ajax_certificates_mapping_list() { 
        $data['file_url']   			= API_BASE_URL;
        $data['opcode']     			= 'list_of_mapping_certificates_to_trainings';
        $data['login_id']   			= $this->session->userdata('active_backend_user_id');
        $response           			= file_curl_contents($data);
        $this->load->view('training/certificate_mapping/certificate_mapping_ajax_page',array("result"=>$response['result']));     
    }
    
    public function ajax_map_certificates() {
        $input 							= $this->input->post();
        $insert_data['opcode'] 			= 'map_certificates_to_trainings';
        $insert_data['login_id'] 		= $this->session->userdata('active_backend_user_id');
        $insert_data['trainings'] 		= $input['trainings'];
        $insert_data['certificates'] 	= $input['certificates'];
        $response 						= call_api("POST",API_BASE_URL, json_encode($insert_data));
		$tr = "<tr><th>#</th><th>Training</th><th>Certificate</th><th>Result</th><th>Message</th></tr>";        
		if($response){
			$n=0;
            $res = json_decode($response,true);
			foreach($res['result'] as $r){ $n++; 				
               $tr  = $tr."<tr><td>".$n."</td><td>".$r['training_name']."</td><td>".$r['certificate_name']."</td><td>".$r['status']."</td><td>".$r['message']."</td></tr>";
            }
        }
        echo $tr;
    }

    public function ajax_delete_certificate_mapping() {
        $input 							= $this->input->post();
        $data['opcode'] 				= 'delete_certificates_mapping';
        $data['login_id'] 				= $this->session->userdata('active_backend_user_id');
        $data['mapping_ids'] 			= $input['mapping_ids'];
        $response 						= call_api("POST",API_BASE_URL, json_encode($data));
    }
	
    public function training_agencies_list(){
		$view_data['page']              = 'training_agencies_list';
		$data['page_data']              = $this->load->view('training/training_agencies_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
	
    public function ajax_training_agencies_list() {  
		$data['file_url']   			= API_BASE_URL; 
		$data['opcode']     			= 'get_training_agencies';
		$data['login_id']   			= $this->session->userdata('active_backend_user_id');
		$response           			= file_curl_contents($data);
		$this->load->view('training/ajax_training_agencies_list',array("result"=>$response));         
    }
	
    public function save_training(){
		if(!empty($_POST['traname']) || !empty($_FILES['trasplash']['name']) || !empty($_FILES['tralogo']['name'])){
			$msg = '';
            $this->db->where('traname',$_POST['traname']);
            $result = $this->db->get('curtra')->row_array();
            if($result){
                $msg='exists';
                echo $msg;die;
            }
			$uploadedFile = '';
            if(!empty($_FILES["trasplash"]["type"])){
                //$fileName = rand(100000,999999).$_FILES['trasplash']['name'];
                $random_three_digit = rand(100,999);
                $fileName=date('YmdHis').$random_three_digit;
                $valid_extensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $_FILES["trasplash"]["name"]);
                $file_extension = end($temporary);
                if((($_FILES["trasplash"]["type"] == "image/png") || ($_FILES["trasplash"]["type"] == "image/jpg") || ($_FILES["trasplash"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                    $sourcePath = $_FILES['trasplash']['tmp_name'];
                    $path =  TARGET_PATH_TO_UPLOAD_TRA_SPLASH;
                    move_uploaded_file($sourcePath,$path.$fileName);
                    $data['trasplash'] = $fileName;
                    
                }
            }
            if(!empty($_FILES["tralogo"]["type"])){
                //$fileName = rand(100000,999999).$_FILES['tralogo']['name'];
                $random_three_digit = rand(100,999);
                $fileName=date('YmdHis').$random_three_digit;
                $valid_extensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $_FILES["tralogo"]["name"]);
                $file_extension = end($temporary);
                if((($_FILES["tralogo"]["type"] == "image/png") || ($_FILES["tralogo"]["type"] == "image/jpg") || ($_FILES["tralogo"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                    $sourcePath = $_FILES['tralogo']['tmp_name'];
                    $path = TARGET_PATH_TO_UPLOAD_TRA_LOGO;
                    move_uploaded_file($sourcePath,$path.$fileName);
                    $data['tralogo'] = $fileName;
                }
            }
            $data['traname'] = $_POST['traname'];
            $data['createdby'] = $this->session->userdata('active_backend_user_id');
            $data['isactive'] = TRUE;
            $data['isdeleted'] = 0;
            $data['createddate']   = date('Y-m-d H:i:s');
            $result = $this->db->insert('curtra',$data);
            if($result){
                $msg = 'success';
				echo  $msg;die;
            }
		}else{
			$msg = 'fail';
			echo $msg;die;
		}
	}
	public function edit_training(){  
		$trarefid 		= $_POST['trarefid'];
		if(isset($_POST['traname']) && !empty($_POST['traname'])){
		   $this->db->where('traname',$_POST['traname']);
		   $result = $this->db->get('curtra')->row();
		}
		if($result) {
			if($result->trarefid  != $_POST['trarefid']){     
				$msg= 'exists';
				echo $msg;die;
			}
		}
		if(!empty($_FILES['trasplash']) ){
			$uploadedFile = '';
			if(!empty($_FILES["trasplash"]["type"])){

				$fileName = rand(100000,999999).$_FILES['trasplash']['name'];
				$valid_extensions = array("jpeg", "jpg", "png");
				$temporary = explode(".", $_FILES["trasplash"]["name"]);
				$file_extension = end($temporary);
				if((($_FILES["trasplash"]["type"] == "image/png") || ($_FILES["trasplash"]["type"] == "image/jpg") || ($_FILES["trasplash"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
					$sourcePath = $_FILES['trasplash']['tmp_name'];
					$path =  TARGET_PATH_TO_UPLOAD_TRA_SPLASH;
					move_uploaded_file($sourcePath,$path.$fileName);
					$data['trasplash'] = $fileName;
					
				}
			}
		}       
		if( !empty($_FILES['tralogo'])){ 
			if(!empty($_FILES["tralogo"]["type"])){
				$fileName = rand(100000,999999).$_FILES['tralogo']['name'];
				$valid_extensions = array("jpeg", "jpg", "png");
				$temporary = explode(".", $_FILES["tralogo"]["name"]);
				$file_extension = end($temporary);
				if((($_FILES["tralogo"]["type"] == "image/png") || ($_FILES["tralogo"]["type"] == "image/jpg") || ($_FILES["tralogo"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
					$sourcePath = $_FILES['tralogo']['tmp_name'];
					 $path = TARGET_PATH_TO_UPLOAD_TRA_LOGO;
					move_uploaded_file($sourcePath,$path.$fileName);
					$data['tralogo'] = $fileName;
					
				}
			}
		}        
            

		if(isset($data['tralogo'] )){
			$update['tralogo'] = $data['tralogo'];
		}
		if(isset($data['trasplash'])){
			$update['trasplash'] = $data['trasplash'];
		}
		if(isset($_POST['traname'])){
			$update['traname'] = $_POST['traname'];
		}
		$update['modifieddate'] = date('Y-m-d H:i:s');

		$this->db->where('trarefid',$trarefid);
		$update = $this->db->update('curtra',$update);
		if($update){
			echo 'success';die;
		}
	}
	
	public function agency_info(){

        $input   = $this->input->post();
        $data['file_url']              = API_BASE_URL; 
        $data['opcode']                = 'clt_get_data_tra';
        $data['traid']                 = $input['traid'];
        $data['login_id']              = $this->session->userdata('active_backend_user_id');
        $response                      = file_curl_contents($data);
        //print_r( $response);die;
        $this->load->view('training/traing_agencies_info',array("result"=>$response));    
    }
    public function aggregatedReport(){

        $rmId              = $this->session->userdata('active_backend_user_id');
       
        $this->db->select('curtra.trarefid,curtra.traname,clt_tra_tm.tmrefid');
        $this->db->where('clt_tra_tm.tmrefid',$rmId);                
        $this->db->join('curtra','curtra.trarefid=clt_tra_tm.trarefid');
        $this->db->order_by("curtra.traname", "asc");
        $tra_list = $this->db->get('clt_tra_tm')->result_array();
        $view_data['page']              = 'aggregated_report';
        $view_data['result']            = $tra_list;
        $data['page_data']              = $this->load->view('rmpanel/reports/aggregated_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }
 
    public function ajaxGetTraCourses(){

        $input = $this->input->post();
        $trarefid =  $input['trarefid'];
        $this->db->select('currcm.chaptername,currcm.rcmrefid');
        $this->db->where('clt_tra_rcm.trarefid',$trarefid);                
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid');
        $this->db->order_by("currcm.chaptername", "asc");
        $course_list = $this->db->get('clt_tra_rcm')->result_array();

        if($course_list){ 
         
            foreach ($course_list as $row) {
                echo '<option value="'.$row['chaptername'].'">'.$row['chaptername'].'</option>';
            }
        }else{
            echo '<option value="">Course not available</option>';
        }
    }

    public function getAggregatedReport(){

       $input = $this->input->post();
       $trarefid = $input['tra_agency'];
       $this->db->select('curtra.traname');
       $this->db->where('curtra.trarefid',$trarefid);                
       $tra = $this->db->get('curtra')->row_array();
       $courses =  $input['courses'];
       $courses_string = implode(", ", $courses);
       
        $data['file_url']              = API_BASE_URL; 
        $data['opcode']                = 'get_tra_aggregated_report';
        $data['login_id']              = $this->session->userdata('active_backend_user_id');
        $data['courses_string']        = $courses_string;
        $data['tra']                   = $tra['traname'];
        $data['fromdate']              = $input['fromdate'];
        $data['todate']                = $input['todate'];

        if($input['checkboxval'] == 'ALL'){
            $data['courses_string'] = 'All';
        }
        $response                      = file_curl_contents($data);
       
       $this->load->view('rmpanel/reports/ajax_get_aggregated_report_list',array("result"=>$response['result'])); 
    }

   public function selectedReport(){

        $rmId = $this->session->userdata('active_backend_user_id');
        $this->db->select('curtra.trarefid,curtra.traname,clt_tra_tm.tmrefid');
        $this->db->where('clt_tra_tm.tmrefid',$rmId);                
        $this->db->join('curtra','curtra.trarefid=clt_tra_tm.trarefid');
        $this->db->order_by("curtra.traname", "asc");
        $tra_list = $this->db->get('clt_tra_tm')->result_array();
        $view_data['page']              = 'aggregated_report';
        $view_data['result']            = $tra_list;
        $data['page_data']              = $this->load->view('rmpanel/reports/selected_report', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajaxGetSelectedTraCourses(){

        $input = $this->input->post();
        $traids =  implode("," , $input['trarefid']);
        $this->db->select('DISTINCT(currcm.chaptername),currcm.rcmrefid');
        $this->db->where_in('trarefid', $input['trarefid']);               
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid');
        $this->db->order_by("currcm.chaptername", "asc");
        $course_list = $this->db->get('clt_tra_rcm')->result_array();
        if($course_list){ 
            foreach ($course_list as $row) {
                echo '<option value="'.$row['chaptername'].'">'.$row['chaptername'].'</option>';
            }
        }else{
            echo '<option value="">Course not available</option>';
        }
    }

    public function getSelectedReport(){

       $input = $this->input->post();
      // echo "<pre>";print_r($input);die;
       $trarefid = $input['tra_agency'];
       $this->db->select('curtra.traname');
       $this->db->where_in('curtra.trarefid',$trarefid);                
       $tra = $this->db->get('curtra')->result_array();
       foreach ($tra as $row ) {
            $tra_agency[] =  $row['traname'];
        }
        $courses =  $input['courses'];
        $courses_string = implode(" , ", $courses);
        $tras =   implode(" , " , $tra_agency);
        $data['file_url']              = API_BASE_URL; 
        $data['opcode']                = 'get_tra_selected_report';
        $data['login_id']              = $this->session->userdata('active_backend_user_id');
        $data['courses_string']        = $courses_string;
        $data['tras']                  = $tras;
        $data['fromdate']              = $input['fromdate'];
        $data['todate']                = $input['todate'];
        if($input['checkboxval'] == 'ALL'){
            $data['tras']  = 'ALL';
        }
        if($input['courseval'] == 'ALL'){
            $data['courses_string']        = 'ALL';
        }

        $response                      = file_curl_contents($data);
        $this->load->view('rmpanel/reports/ajax_get_selected_report_list',array("result"=>$response['result']));
    }

	
	
}
