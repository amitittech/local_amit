<?php
//commited
defined('BASEPATH') OR exit('No direct script access allowed');

class Generic_interface extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('custom');
        $this->load->helper('file_upload');
        require_once APPPATH . 'third_party/PHPExcel.php';
        $this->phpexcel = new PHPExcel();
        $this->load->library('PHPExcel');
    }

    public function index() {
        $contents   = file_get_contents(GENERIC_INTERFACE_JSON_QUERY_PATH);
		//$contents = trim($contents);
		$contents = trim(preg_replace('/\s\s+/', ' ', $contents));
        $inputs     = json_decode($contents,true);
		if($inputs){		
			foreach($inputs as $input ){
				$view_data['queries'][] = array("name"=>$input['name']);
			}
		}
		//pre($view_data);die;
        $view_data['page'] = 'generic_interface';
        $data['page_data'] = $this->load->view('generic_interface/generic_interface', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    public function ajax_get_input_boxes() {
        $contents   = file_get_contents(GENERIC_INTERFACE_JSON_QUERY_PATH);
        $inputs     = json_decode($contents,true); 
		$inputs     = $inputs[$_POST['name']];
        $this->load->view('generic_interface/ajax_generic_interface_input_boxes',$inputs);
        //return $response;
    }     

	public function ajax_get_query_result() {	
		#pre($_POST);
        $contents       		= file_get_contents(GENERIC_INTERFACE_JSON_QUERY_PATH);
        $contents       		= json_decode($contents,true); 
        $query_data     		= $contents[$_POST['name']];
        $query_data_inputs      = $contents[$_POST['name']]['inputs'];
        $display_columns      	= (isset($_POST['display_columns']))?$_POST['display_columns']:array();
        $query                  = $query_data['query'];
        $input_box_values       = json_decode($_POST['values'],true);
        for($i=0;$i<count($input_box_values);$i++){ 
            $query = str_replace("{".$query_data_inputs[$i]['name']."}",$input_box_values[$i][$query_data_inputs[$i]['name']],$query);
        }
        $format_button  		= (isset($query_data['format_button']) && !empty($query_data['format_button']))?$query_data['format_button']:"";
        $result =$this->db->query($query)->result_array();
        $url        = base_url()."index.php/auth_panel/Generic_interface/download_records?query=".base64_encode($query);
        $this->load->view('generic_interface/generic_result_datatable',array("result"=>$result,"query"=>$query,"url"=>$url,"display_columns"=>$display_columns,"format_button"=>$format_button));        
        //echo json_encode(array("query"=>$query,"url"=>$url,"html"=>$html));        
    }
	
	
	//////////////////////////////////////////////////////////////////////////////////////

	/*
    public function ajax_get_query() {
        $contents       = file_get_contents(GENERIC_INTERFACE_JSON_QUERY_PATH);
        $contents       = json_decode($contents,true); //pre($_POST); //pre($inputs); //die;
        $query_data     = $contents[$_POST['name']];
        $query_data_inputs     = $contents[$_POST['name']]['inputs'];
        $query          = $query_data['query'];
        $input_box_values       = json_decode($_POST['values'],true);
        for($i=0;$i<count($input_box_values);$i++){ 
            $query = str_replace("{".$query_data_inputs[$i]['name']."}",$input_box_values[$i][$query_data_inputs[$i]['name']],$query);
        }
        $tr = "<tr>";
        $result =$this->db->query($query)->row_array();
        if ($result) {            
            $tr = $tr."<th>S.No.</th>";
            foreach ($result as $key => $each) {
                $tr = $tr."<th>".$key."</th>";
            }            
        }else{
            $tr = $tr."<th>No result found.</th>";
        }
        $tr = $tr."</tr>";
        $url = base_url()."index.php/auth_panel/Generic_interface/download_records?query=".base64_encode($query);

        echo json_encode(array("query"=>$query,"url"=>$url,"tr"=>$tr));

        //$this->load->view('generic_interface/ajax_generic_interface_input_boxes',$inputs);
        //return $response;
    }      
    
    
    public function ajax_execute_query() { //die('efew');	
        $requestData        = $_REQUEST;
        $result             = $this->db->query($requestData['query'])->result_array();
        
        $totalData          = (count($result) > 0) ? count($result) : 0;
        $totalFiltered      = $totalData;

        $insert_data['start']   = $requestData['start'];
        $insert_data['length']  = $requestData['length'];
        $requestData['query']   = $requestData['query']. " limit ".$requestData['start'].",".$requestData['length'];
        $result                 = $this->db->query($requestData['query'])->result_array();
        $i                      = $requestData['start'];
        $data                   = [];
        if ($result) {
            foreach ($result as $r) {  $i++;
                $nestedData = array();
                $nestedData[] = $i;
                foreach ($r as $each) {
                    $nestedData[] = $each;
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        echo json_encode($json_data);  // send data as json format
    }
    
    public function download_records() {
        $sql        = base64_decode($_GET['query']);
        $result     = $this->db->query($sql)->result_array();
        if (!$result)
            return false;
        $sheet = $this->phpexcel->getActiveSheet();
        
        $i = 1;
        
        $letters = range('A','Z');
        foreach ($result as $each) {
            $j = 0; 
			if($i == 1){
				foreach($each as $key=>$value){ 
					$sheet->setCellValue($letters[$j].$i, $key); $j++;
				}
				$i++;
				$j = 0;
				//setCellValue('A1', 'Name')->setCellValue('B1', 'Phone')->setCellValue('C1', 'Email');
			}
			foreach($each as $key=>$value){ 
				$sheet->setCellValue($letters[$j].$i, $value); $j++;
			}          
            $i++;
        }
        $writer = new PHPExcel_Writer_Excel5($this->phpexcel);
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="file.xls"');
        $writer->save('php://output');
    }    
    */
    
    

}
