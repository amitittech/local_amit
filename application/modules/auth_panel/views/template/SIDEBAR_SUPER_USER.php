<?php
#pre(array("id"=>1));
#pre($this->session->active_user_data->roles);
$roles = $this->session->active_user_data->roles;//explode(',',$this->session->active_user_data->roles);

if (isset($page) && !empty($page)) {
    $training_agencies = $training_instance = $wall = $generic_interface = $master_data = $reports = $kibana_reports = $activity_report = $training_report = $active_users = $user_activity = $user_activity_wall = $user_activity_training = $user_activity_resources = "";
    $active_pages = ['dashboard', 'training_agencies', 'traing_courses', 'courses_traing_mapping', 'sender_id_mapping', 'users_mapping', 'certificate_mapping', 'visibility', 'certificate_approves', 'channels', 'generic_interface', 'sender_id', 'training_agencies_list', 'sender_ids_list', 'manage_view_rights', 'wall_posters', 'active_users', 'activity_report', 'training_report', 'user_activity_wall', 'user_activity_training', 'user_activity_resources', 'subject_list', 'activity_analytics'];
    foreach ($active_pages as $each) {
        if ($page == $each) {
            ${$each} = 'active';
            if (in_array($page, ['courses_traing_mapping', 'sender_id_mapping', 'users_mapping', 'certificate_mapping'])) {
                $training_agencies = 'active';
            } else if (in_array($page, ['visibility', 'certificate_approves', 'manage_view_rights'])) {
                $training_instance = 'active';
            } else if (in_array($page, ['channels', 'wall_posters'])) {
                $wall = 'active';
            } else if (in_array($page, ['generic_interface'])) {
                $generic_interface = 'active';
            } else if (in_array($page, ['sender_ids_list', 'training_agencies_list', 'subject_list'])) {
                $master_data = 'active';
            } else if (in_array($page, ['activity_report', 'training_report', 'active_users'])) {
                $kibana_reports = 'active';
            } else if (in_array($page, ['user_activity_wall', 'user_activity_training', 'user_activity_resources'])) {
                $kibana_reports = $user_activity = 'active';
            }
        } else {
            ${$each} = '';
        }
    }
}


$sidebar_url = array('web_user/all_user_list');
$sidebar_url[] = 'admin/index';
//$sidebar_url[] = 'admin/create_backend_user';
$sidebar_url[] = 'Training/traing_agencies';
$sidebar_url[] = 'Courses/traing_courses';
$sidebar_url[] = 'Courses/courses_traing_mapping';
$sidebar_url[] = 'Training/toggle_visibility';

if (is_array($sidebar_url)) {
    $sidebar_url = implode("','", $sidebar_url);
    $sidebar_url = "'" . $sidebar_url . "'";
}
$session_userdata = $this->session->userdata();
?>
<aside>
    <div id="sidebar"  class="nav-collapse " >
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <?php if(in_array('CL-Admin', $roles)){ ?>
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:;" class="dcjq-parent active-class">
                    <i class="fa fa-sitemap"></i>
                    <span>System Admin </span>
                <span class="dcjq-icon"></span></a>
                <ul class="sub" style="display: block;">
                    <li>
                        <a class="<?php echo $dashboard; ?>" href="<?php echo AUTH_PANEL_URL . 'admin/index'; ?>">
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent <?php echo $training_agencies; ?>">
                            <span>TRA Mapping</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <li class="<?php echo $courses_traing_mapping; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training/courses_traing_mapping'; ?>">Courses</a>
                            </li>
                            <li class="<?php echo $sender_id_mapping; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training/sender_id_mapping'; ?>">Sender-ID</a>
                            </li>
                            <li class="<?php echo $users_mapping; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training/users_mapping'; ?>">Users</a>
                            </li>
                            <li class="<?php echo $certificate_mapping; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training/certificate_mapping'; ?>">Certificates</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent <?php echo $training_instance; ?>">
                            <span>Trng Instances</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <li class="<?php echo $visibility; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training/visibility'; ?>">Visibility</a>
                            </li>
                             <li class="<?php echo $certificate_approves; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training_instance/certificate_approves'; ?>">Cert. Approver</a>
                            </li>
                            <li class="<?php echo $manage_view_rights; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training_instance/manage_view_rights'; ?>">View Rights</a>
                            </li>
                        </ul>                       
                    </li>				
                    <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent <?php echo $wall; ?>">
                            <span>Wall</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <li class="<?php echo $channels; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Wall/channels'; ?>">Channels</a>
                            </li>
                            <li class="<?php echo $wall_posters; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Wall/wall_posters'; ?>">Wall Posters</a>
                            </li>
                        </ul>
                    </li>

                    <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent <?php echo $generic_interface; ?>">
                            <span>Generic Interface</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <li class="<?php echo $generic_interface; ?>"><a href="<?php echo AUTH_PANEL_URL . 'Generic_interface/index'; ?>">Generic Interface</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent <?php echo $master_data; ?>">
                            <span>Master Data</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <li class="<?php echo $sender_id; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Sender_id/index'; ?>">Sender-ID</a>
                            </li>
                            <li class="<?php echo $training_agencies_list; ?>"><a href="<?php echo AUTH_PANEL_URL . 'Training/training_agencies_list'; ?>">Training Agencies</a>
                            </li>
                            <li class="<?php echo $subject_list; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'Subject/subject_list'; ?>">Manage Subjects</a>
                            </li>
                        </ul>
                    </li>		
                    <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:0;" class="dcjq-parent <?php echo $kibana_reports; ?>">
                            <span>Kibana Reports</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <li class="<?php echo $active_users; ?>">
                                <a href="<?php echo AUTH_PANEL_URL . 'kibana/Report/active_users'; ?>">Active Users </a>
                            </li>
                            <li class="sub-menu dcjq-parent-li ">
                                <a href="boxed_page.html" class="dcjq-parent <?php echo $user_activity; ?>">Activities<span class="dcjq-icon"></span></a>
                                <ul class="sub" style="display: none;">
                                    <!--<li class="<?php echo $user_activity_wall; ?>">
                                        <a href="<?php echo AUTH_PANEL_URL . 'kibana/Report/user_activity_wall'; ?>">Wall</a>
                                    </li>
                                    <li class="<?php echo $user_activity_training; ?>">
                                        <a href="<?php echo AUTH_PANEL_URL . 'kibana/Report/user_activity_training'; ?>">Training</a>
                                    </li>
                                    <li class="<?php echo $user_activity_resources; ?>">
                                        <a href="<?php echo AUTH_PANEL_URL . 'kibana/Report/user_activity_resources'; ?>">Teacher Tools</a>
                                    </li>
                                    -->
                                    <li class="<?php echo $activity_analytics; ?>">
                                        <a href="<?php echo AUTH_PANEL_URL . 'kibana/Report/activity_analytics'; ?>">Activity Analytics</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
            
                </ul>
            </li>
                  
            <?php } if(in_array('CL-Relationship Manager', $roles)){ ?>
                    
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:0;" class="dcjq-parent active-class">
                    <i class="fa fa-dashcube"></i>
                    <span>RM Panel</span>
                    <span class="dcjq-icon"></span>
                </a>
                <ul class="sub" style="display: block;">
                    <li>
                        <a class="<?php echo $dashboard; ?>" href="<?php echo AUTH_PANEL_URL . 'rmpanel/Dashboard/index1'; ?>">
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="sub-menu dcjq-parent-li ">
                        <a href="boxed_page.html" class="dcjq-parent active-class">Reports<span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li class="active-class">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training/selectedReport'; ?>">Selected </a>
                            </li>
                            <li class="active-class">
                                <a href="<?php echo AUTH_PANEL_URL . 'Training/aggregatedReport'; ?>">Aggregated</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </li>
            
            
            
            <?php } if(in_array('CL-Training Manager', $roles)){ ?>
                    
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:0;" class="dcjq-parent active-class">
                    <i class="fa fa-dashcube"></i>
                    <span>TM Panel</span>
                    <span class="dcjq-icon"></span>
                </a>
                <ul class="sub" style="display: block;">
                    
                </ul>
            </li>
            
            
             <?php } if(in_array('CL-Training Lead', $roles)){ ?>
                    
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:0;" class="dcjq-parent active-class">
                    <i class="fa fa-dashcube"></i>
                    <span>TL Panel</span>
                    <span class="dcjq-icon"></span>
                </a>
                <ul class="sub" style="display: block;">
                    
                </ul>
            </li>
            
            
            
             <?php } if(in_array('CL-L1 User', $roles)){ ?>
                    
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:0;" class="dcjq-parent active-class">
                    <i class="fa fa-dashcube"></i>
                    <span>Mentor</span>
                    <span class="dcjq-icon"></span>
                </a>
                <ul class="sub" style="display: block;">
                    
                </ul>
            </li>
            
            
            <?php } ?>
            
            
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>