<?php
$result = $result['result'];
?>
<div id="meu_dt_less_data_div" class="dataTables_wrapper form-inline meu_dt_less_data_div" role="grid">
    <table id="meu_dt_less_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th title="User Name">Name</th>
                <th title="School Name">School</th>
                <th title="Session Time">Session Time</th>
                <th title="Total Activities for Wall and Training">TA (Wall + TI)</th>
                <th title="Total Comments">C</th>
                <th title="Long Comments">LC</th>
                <th title="Training Completion %age">Training Completion %age</th>
                
                <th class="hidden" title="Likes">L</th>
                <th class="hidden" title="Views">V</th>
                <th class="hidden" title="Share">S</th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>        

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                if ($i == 3) {
                    break;
                }
                $i++;
                $ta  = $row['totalactivity']."|".$row['views']."|".$row['likes']."|".$row['comments']."|".$row['shares'];
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['name']; ?></td>
                    <td class=""><?php echo $row['schoolname']; ?></td>
                    <td class=""><?php echo $row['sessiontime']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['totalactivity'].'</a>'; ?></td>
                    <td class=""><?php echo $row['comments']; ?></td>
                    <td class=""><?php echo $row['longcomments']; ?></td>
                    <td class=""><?php echo $row['trncompletion']; ?></td>
                    
                    <td class="hidden"><?php echo $row['likes']; ?></td>
                    <td class="hidden"><?php echo $row['views']; ?></td>
                    <td class="hidden"><?php echo $row['shares']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="meu_dt_all_data_div" class="dataTables_wrapper form-inline meu_dt_all_data_div" role="grid">
    <table  id="meu_dt_all_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th title="User Name">Name</th>
                <th title="School Name">School</th>
                <th title="Session Time">Session Time</th>
                <th title="Total Activities for Wall and Training">TA (Wall + TI)</th>
                <th title="Total Comments">C</th>
                <th title="Long Comments">LC</th>
                <th title="Training Completion %age">Training Completion %age</th>
                
                <th class="hidden" title="Likes">L</th>
                <th class="hidden" title="Views">V</th>
                <th class="hidden" title="Share">S</th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                $i++;
                $ta  = $row['totalactivity']."|".$row['views']."|".$row['likes']."|".$row['comments']."|".$row['shares'];
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['name']; ?></td>
                    <td class=""><?php echo $row['schoolname']; ?></td>
                    <td class=""><?php echo $row['sessiontime']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['totalactivity'].'</a>'; ?></td>
                    <td class=""><?php echo $row['comments']; ?></td>
                    <td class=""><?php echo $row['longcomments']; ?></td>
                    <td class=""><?php echo $row['trncompletion']; ?></td>
                    
                    <td class="hidden"><?php echo $row['likes']; ?></td>
                    <td class="hidden"><?php echo $row['views']; ?></td>
                    <td class="hidden"><?php echo $row['shares']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>
    //$("#meu_dt_less_data").DataTable();    
    var table_meu_1 = $("#meu_dt_less_data").DataTable({
        destroy: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5', 
                text: 'Download Excel', 
                exportOptions: {columns: [':visible'],
                modifier:{search : 'none'}}
            },
        ],
        responsive : true
    });    
    $('#meu_dt_less_data thead tr:eq(1) th').each( function (i) {  
        if(i==1 || i==2){
            //var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search" />' );

            $( 'input', this ).on( 'keyup change', function () { //alert('working');
                if ( table_meu_1.column(i).search() !== this.value ) {
                    table_meu_1
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        }
    });
    
    var table_meu_2 = $("#meu_dt_all_data").DataTable({
        destroy: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5', 
                text: 'Download Excel', 
                exportOptions: {columns: [':visible'],
                modifier:{search : 'none'}}
            },
        ],
        responsive : true
    });
    $('#meu_dt_all_data thead tr:eq(1) th').each( function (i) {  
        if(i==1 || i==2){
            //var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search" />' );

            $( 'input', this ).on( 'keyup change', function () { //alert('working');
                if ( table_meu_2.column(i).search() !== this.value ) {
                    table_meu_2
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        }
    });

    $('#meu_dt_all_data_div').hide();

    <?php if(count($result) > 3){ ?>
    $('#meu_see_more_btn').show();
    $('#meu_see_more_text').show();
    $('#meu_see_less_text').hide();
    <?php } ?>
  
</script>
