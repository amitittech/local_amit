<?php
$result = $result['result'];
?>
<div id="mes_dt_less_data_div" class="dataTables_wrapper form-inline mes_dt_less_data_div" role="grid">
    <table id="mes_dt_less_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th title="School Name">Name</th>
                <th title="Total Activities">TA</th>
                <th title="Total Comments">C</th>
                <th title="Long Comments">LC</th>
                <th title="Unique Users">U</th>
                <th title="Total users in school">Users in school</th>
                
                <th class="hidden" title="Likes">L</th>
                <th class="hidden" title="Views">V</th>
                <th class="hidden" title="Share">S</th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                if ($i == 3) {
                    break;
                }
                $i++;
                $ta  = $row['totalactivity']."|".$row['views']."|".$row['likes']."|".$row['comments']."|".$row['shares'];
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['schoolname']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['totalactivity'].'</a>'; ?></td>
                    <td class=""><?php echo $row['comments']; ?></td>
                    <td class=""><?php echo $row['longcomments']; ?></td>
                    <td class=""><?php echo $row['activeusers']; ?></td>
                    <td class=""><?php echo $row['users']; ?></td>
                    
                     <td class="hidden"><?php echo $row['likes']; ?></td>
                    <td class="hidden"><?php echo $row['views']; ?></td>
                    <td class="hidden"><?php echo $row['shares']; ?></td>
                </tr>
                
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="mes_dt_all_data_div" class="dataTables_wrapper form-inline mes_dt_all_data_div" role="grid">
    <table id="mes_dt_all_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th title="School Name">Name</th>
                <th title="Total Activities">TA</th>
                <th title="Total Comments">C</th>
                <th title="Long Comments">LC</th>
                <th title="Unique Users">U</th>
                <th title="Total users in school">Users in school</th>
                
                <th class="hidden" title="Likes">L</th>
                <th class="hidden" title="Views">V</th>
                <th class="hidden" title="Share">S</th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th></th>
                <th></th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                $i++;
                $ta  = $row['totalactivity']."|".$row['views']."|".$row['likes']."|".$row['comments']."|".$row['shares'];
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['schoolname']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['totalactivity'].'</a>'; ?></td>
                    <td class=""><?php echo $row['comments']; ?></td>
                    <td class=""><?php echo $row['longcomments']; ?></td>
                    <td class=""><?php echo $row['activeusers']; ?></td>
                    <td class=""><?php echo $row['users']; ?></td>
                    
                    <td class="hidden"><?php echo $row['likes']; ?></td>
                    <td class="hidden"><?php echo $row['views']; ?></td>
                    <td class="hidden"><?php echo $row['shares']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>

    var table_mes_1 = $("#mes_dt_less_data").DataTable({
        destroy: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5', 
                text: 'Download Excel', 
                exportOptions: {columns: [':visible'],
                modifier:{search : 'none'}}
            },
        ],
        responsive : true
    });
    $('#mes_dt_less_data thead tr:eq(1) th').each( function (i) {  
        if(i==1){
            //var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search" />' );

            $( 'input', this ).on( 'keyup change', function () { //alert('working');
                if ( table_mes_1.column(i).search() !== this.value ) {
                    table_mes_1
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        }
    });
    
    var table_mes_2 = $("#mes_dt_all_data").DataTable({
        destroy: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5', 
                text: 'Download Excel', 
                exportOptions: {columns: [':visible'],
                modifier:{search : 'none'}}
            },
        ],
        responsive : true
    });
    $('#mes_dt_all_data thead tr:eq(1) th').each( function (i) {  
        if(i==1){
            //var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search" />' );

            $( 'input', this ).on( 'keyup change', function () { //alert('working');
                if ( table_mes_2.column(i).search() !== this.value ) {
                    table_mes_2
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        }
    });

    $('#mes_dt_all_data_div').hide();

    <?php if(count($result) > 3){ ?>
    $('#mes_see_more_btn').show();
    $('#mes_see_more_text').show();
    $('#mes_see_less_text').hide();
    <?php } ?>
</script>
<script>
    //$("#paginatedTable").on("click", ".test .toggleTest", function ...);
    $(".table").on("click",".ta_popup",function () {
        var ta = $(this).attr('value').split('|');
        var av = "<td>"+ta[0]+"</td><td>"+ta[1]+"</td><td>"+ta[2]+"</td><td>"+ta[3]+"</td><td>"+ta[4]+"</td>"
        $("#ta_values_tbody").html(av);
        $("#ta_values").modal("toggle");
    });
    
    $("#ta_popup_metpp").click(function () {
        var ta = $("#ta_popup_metp").attr('value').split('|');
        var av = "<td>"+ta[0]+"</td><td>"+ta[1]+"</td><td>"+ta[2]+"</td><td>"+ta[3]+"</td><td>"+ta[4]+"</td>"
        $("#ta_values_tbody").html(av);
        $("#ta_values").modal("toggle");
    });
       
</script>