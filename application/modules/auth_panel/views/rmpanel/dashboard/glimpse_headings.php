<style>
.loader {
  margin: 10px auto;  
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<div class="panel panel-primary">     
    <div class="panel-heading">
        <a href="javascript:void(0)" style=" color: whitesmoke; margin: 0; padding: 0" ><i class="fa fa-arrow-left" id="back_to_tra"> Go Back</i> </a>
        <br>
        <img src="<?php echo TARGET_PATH_TO_DISPLAY_TRA_LOGO.$ti_info['tralogo']; ?>" width="80px" style="margin-right: 20px;">
        <?php echo $ti_info['traname']; ?>

        <span style="float:right">
            Participants &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    : <?php echo $ti_info['participants']; ?> <br>
            Training period : <?php echo date('d/M/y',strtotime($ti_info['startdate'])).' <b>-</b> '.date('d/M/y',strtotime($ti_info['enddate'])); ?>
        </span>
        <h4>  <span style="margin-left: 20px; margin-right: 100px;"> <?php echo $ti_info['tcname']; ?> </span><span> <?php echo $ti_info['tiname']; ?></span></h4>
    </div>

    <div class="panel-body">
        <section class="panel panel-success" style=" border: 1px solid green;">
            <header class="panel-heading" style="min-height: 50px;">
                Most engaging wall post (By Activity)
                <button id="mewp_see_more_btn" type="mewp" style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary see_more_btn" >
                    <span id="mewp_see_more_text" class="col-md-3 see_more_text"> See more..</span>
                    <span id="mewp_see_less_text" class="col-md-3 see_less_text"> See less.. &nbsp;&nbsp;</span>
                </button>
            </header>
            <div class="panel-body">
                <div class="adv-table mewp_dt_div_div">
                    <div class="dataTables_wrapper form-inline" role="grid">
                        <table class="table table-striped table-bordered nowrap display  " style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>TA</th>
                                    <th>C</th>
                                    <th>LC</th>
                                    <th>MC</th>
                                    <th>MSF-C</th>
                                    <th>TRA-C</th>
                                    <th>U</th>
                                    <!-- <th>Update</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="9"><p class="loader"></p></td></tr>
                            </tbody>
                           
                            
                        </table>
                    </div> 
                </div>
            </div>
        </section>
    </div>
    
    <div class="panel-body">
        <section class="panel panel-success" style=" border: 1px solid green;">
            <header class="panel-heading" style="min-height: 50px;">
                Most engaging Training post (By Activity)
                <button id="metp_see_more_btn" type="metp" style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary see_more_btn" >
                    <span id="metp_see_more_text" class="col-md-3 see_more_text"> See more..</span>
                    <span id="metp_see_less_text" class="col-md-3 see_less_text"> See less.. &nbsp;&nbsp;</span>
                </button>
            </header>
            <div class="panel-body">
                <div class="adv-table metp_dt_div_div">
                    <div class="dataTables_wrapper form-inline" role="grid">
                        <table class="table table-striped table-bordered nowrap display  " style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>TA</th>
                                    <th>C</th>
                                    <th>LC</th>
                                    <th>MC</th>
                                    <th>MSF-C</th>
                                    <th>TRA-C</th>
                                    <th>U</th>
                                    <!-- <th>Update</th> -->
                                </tr>
                            </thead>

                            <tbody>
                                <tr><td colspan="9"><p class="loader"></p></td></tr>
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </section>
    </div>


    <div class="panel-body">
        <section class="panel panel-success" style=" border: 1px solid green;">
            <header class="panel-heading" style="min-height: 50px;">
                    Most engaging users (By Activity)
                <button id="meu_see_more_btn" type="meu" style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary see_more_btn" >
                    <span id="meu_see_more_text" class="col-md-3 see_more_text"> See more..</span>
                    <span id="meu_see_less_text" class="col-md-3 see_less_text"> See less.. &nbsp;&nbsp;</span>
                </button>
            </header>
            <div class="panel-body">
                <div class="adv-table meu_dt_div_div">
                    <div class="dataTables_wrapper form-inline" role="grid">
                        <table class="table table-striped table-bordered nowrap display " style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>TA</th>
                                    <th>C</th>
                                    <th>LC</th>
                                    <th>MC</th>
                                    <th>MSF-C</th>
                                    <th>TRA-C</th>
                                    <th>U</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="9"><p class="loader"></p></td></tr>
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </section>
    </div>
    
    <div class="panel-body ">
        <section class="panel panel-success" style=" border: 1px solid green;">
            <header class="panel-heading" style="min-height: 50px;">
                Most engaging Schools (By Activity)
                <button id="mes_see_more_btn" type="mes" style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary see_more_btn" >
                    <span id="mes_see_more_text" class="col-md-3 see_more_text"> See more..</span>
                    <span id="mes_see_less_text" class="col-md-3 see_less_text"> See less.. &nbsp;&nbsp;</span>
                </button>
            </header>
            <div class="panel-body">
                <div class="adv-table mes_dt_div_div">
                    <div class="dataTables_wrapper form-inline" role="grid">
                        <table class="table table-striped table-bordered nowrap display " style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>TA</th>
                                    <th>C</th>
                                    <th>LC</th>
                                    <th>MC</th>
                                    <th>MSF-C</th>
                                    <th>TRA-C</th>
                                    <th>U</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="9"><p class="loader"></p></td></tr>
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </section>
    </div>
    
</div>

<div id="ta_values" class="modal fade"  role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #4EC95E;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Total Activities</h4>
            </div>
            <div class="modal-body">
                <p>
                <table class="table table-bordered" style="border:1px solid black">
                    <thead>
                        <tr><th>Total Activities</th><th>View</th><th>Like</th><th>Share</th><th>Comments</th></tr>
                    </thead>
                    <tbody>
                        <tr id="ta_values_tbody"></tr>
                    </tbody>
                </table>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(".datatable").DataTable({  });
    $('.see_more_btn').hide();
    $("#back_to_tra").click(function () {
        $('#tra_parent_div').show();
        $('#glimpse_parent_div').hide();
    });
    $(".see_more_btn").click(function () { //alert('dd');
        var type = $(this).attr('type')
        $(this).children('.see_less_text').toggle();
        $(this).children('.see_more_text').toggle(); 
        
        $("#"+type+"_dt_all_data_div").toggle(); 
        $("#"+type+"_dt_less_data_div").toggle(); 
    });
    
    $(".ta_popup").click(function () {
        var ta = $(this).attr('value').split('|');
        var av = "<td>"+ta[0]+"</td><td>"+ta[1]+"</td><td>"+ta[2]+"</td><td>"+ta[3]+"</td><td>"+ta[4]+"</td>"
        $("#ta_values_tbody").html(av);
        $("#ta_values").modal("toggle");
    });
    
    
</script>

