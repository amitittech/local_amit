<?php
$result = $result['result'];
?>
<style>
.expandname{
    white-space: nowrap;
    width: auto !important;                  
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>
<div id="mewp_dt_less_data_div" class="dataTables_wrapper form-inline mewp_dt_less_data_div" role="grid">
    <table id="mewp_dt_less_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th title="Post Title">Title</th>
                <th class="expandtitle" title="Total Activities">TA</th>
                <th title="Total Comments">C</th>
                <th title="Long Comments">LC</th>
                <th title="Mentor Comments">MC </th>
                <th title="MSF Member Comments">MSF-C</th>
                <!--<th>TRA-C</th> -->
                <th title="Unique Users">U</th>
                
                <th class="hidden" title="Likes">L</th>
                <th class="hidden" title="Views">V</th>
                <th class="hidden" title="Share">S</th>
                
            </tr>
        </thead>
       
        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) { //pre($row);die;
                if ($i == 3) {
                    break;
                }
                $ta  = $row['totalactivity']."|".$row['views']."|".$row['likes']."|".$row['comments']."|".$row['shares'];
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['posttitle']; ?></td>
                    <td class=""><?php echo '<a stylt="padding 10px" href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['totalactivity'].'</a>'; ?></td>
                    <td class=""><?php echo $row['comments']; ?></td>
                    <td class=""><?php echo $row['longcomments']; ?></td>
                    <td class=""><?php echo $row['mentorcomments']; ?></td>
                    <td class=""><?php echo $row['msfcomments']; ?></td>
                 <!--   <td class=""><?php //echo 'N/A'; //$row['tra_c']; ?></td> -->
                    <td class=""><?php echo $row['uniqueusers']; ?></td>
                    
                    <td class="hidden"><?php echo $row['likes']; ?></td>
                    <td class="hidden"><?php echo $row['views']; ?></td>
                    <td class="hidden"><?php echo $row['shares']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="mewp_dt_all_data_div" class="dataTables_wrapper form-inline mewp_dt_all_data_div" role="grid">
    <table id="mewp_dt_all_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th title="Post Title">Title</th>
                <th class="expandtitle" title="Total Activities">TA</th>
                <th title="Total Comments">C</th>
                <th title="Long Comments">LC</th>
                <th title="Mentor Comments">MC </th>
                <th title="MSF Member Comments">MSF-C</th>
                <th>TRA-C</th>
                <th title="Unique Users">U</th>
                
                <th class="hidden" title="Likes">L</th>
                <th class="hidden" title="Views">V</th>
                <th class="hidden" title="Share">S</th>
                
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                $ta  = $row['totalactivity']."|".$row['views']."|".$row['likes']."|".$row['comments']."|".$row['shares'];
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['posttitle']; ?></td>
                    <td class=""><?php echo '<a stylt="padding 10px" href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['totalactivity'].'</a>'; ?></td>
                    <td class=""><?php echo $row['comments']; ?></td>
                    <td class=""><?php echo $row['longcomments']; ?></td>
                    <td class=""><?php echo $row['mentorcomments']; ?></td>
                    <td class=""><?php echo $row['msfcomments']; ?></td>
                    <td class=""><?php echo 'N/A'; //$row['tra_c']; ?></td>
                    <td class=""><?php echo $row['uniqueusers']; ?></td>
                    
                    <td class="hidden"><?php echo $row['likes']; ?></td>
                    <td class="hidden"><?php echo $row['views']; ?></td>
                    <td class="hidden"><?php echo $row['shares']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>
    //$("datatable").DataTable();
    //$("#mewp_dt_less_data").DataTable();
    var table_mewp_1 = $("#mewp_dt_less_data").DataTable({
        destroy: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5', 
                text: 'Download Excel', 
                exportOptions: {columns: [':visible'],
                modifier:{search : 'none'}}
            },
        ],
        responsive : true
    });
    $('#mewp_dt_less_data thead tr:eq(1) th').each( function (i) {  
        if(i==1){
            //var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search" />' );

            $( 'input', this ).on( 'keyup change', function () { //alert('working');
                if ( table_mewp_1.column(i).search() !== this.value ) {
                    table_mewp_1
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        }
    });

    //$("#mewp_dt_all_data").DataTable();
    var table_mewp_2 = $("#mewp_dt_all_data").DataTable({
        destroy: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5', 
                text: 'Download Excel', 
                exportOptions: {columns: [':visible'],
                modifier:{search : 'none'}}
            },
        ],
        responsive : true
    });
    $('#mewp_dt_all_data thead tr:eq(1) th').each( function (i) {  
        if(i==1){
            //var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search" />' );

            $( 'input', this ).on( 'keyup change', function () { //alert('working');
                if ( table_mewp_2.column(i).search() !== this.value ) {
                    table_mewp_2
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        }
    });

    $('#mewp_dt_all_data_div').hide();

    <?php if(count($result) > 3){ ?>
    $('#mewp_see_more_btn').show();
    $('#mewp_see_more_text').show();
    $('#mewp_see_less_text').hide();
    <?php } ?>
</script>
<script>
     
</script>


