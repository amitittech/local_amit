<?php #pre($tras);    ?>

<style>
    .loader {
        margin: 70px auto;  
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>


<div id="glimpse_parent_div">    
</div>

<div class="col-sm-12 appendtra" id="tra_parent_div">
    <?php foreach ($tras as $tra) { ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <img src="<?php echo TARGET_PATH_TO_DISPLAY_TRA_LOGO . $tra['tralogo']; ?>" width="80px" style="margin-right: 20px;">
                <?php echo $tra['traname']; ?>
            </div>
            <?php if (isset($tra['ti'][0]['ti_id']) && !empty($tra['ti'][0]['ti_id'])) { ?>
                <div class="panel-body" id="trarefid_<?php echo $tra['trarefid']; ?>">
                    <?php
                    foreach ($tra['ti'] as $ti) {

                        #if( ($ti['startdate'] >= $tdy && $ti['enddate'] >= $tdy) OR ($ti['regstartdate'] >= $tdy && $ti['regenddate'] >= $tdy) ){
                        $ti_ids[] = $ti['ti_id'];
                        $mydata[$ti['ti_id']] = array(
                            "phase" => $ti['phase'],
                            "enrolled" => $ti['enrolled'],
                            "expected" => $ti['expected'],
                            "ns" => $ti['ns'],
                            "low" => $ti['low'],
                            "medium" => $ti['medium'],
                            "high" => $ti['high'],
                            "completed" => $ti['completed']
                        );
                        ?>

                        <input id="ti_traname_<?php echo $ti['ti_id']; ?>" type="hidden" value="<?php echo $tra['traname']; ?>">
                        <input id="ti_tralogo_<?php echo $ti['ti_id']; ?>" type="hidden" value="<?php echo $tra['tralogo']; ?>">
                        <input id="ti_trnname_<?php echo $ti['ti_id']; ?>" type="hidden" value="<?php echo $ti['tiname']; ?>">
                        <input id="ti_tcname_<?php echo $ti['ti_id']; ?>" type="hidden" value="<?php echo $ti['chaptername']; ?>">
                        <input id="ti_startdate_<?php echo $ti['ti_id']; ?>" type="hidden" value="<?php echo $ti['startdate']; ?>">
                        <input id="ti_enddate_<?php echo $ti['ti_id']; ?>" type="hidden" value="<?php echo $ti['enddate']; ?>">
                        <input id="ti_reg_users_<?php echo $ti['ti_id']; ?>" type="hidden" value="<?php echo $ti['enrolled']; ?>">
                        <div class="col-sm-6" >

                            <div class="panel panel-success"style="border: 1px solid">
                                <div class="panel-heading"><?php echo $ti['tiname']; ?>
                                    <?php if ($ti['phase'] == 'mp' OR $ti['phase'] == 'rp') { ?>
                                        <button style="float:right" id="rp_save_pdf_<?php echo $ti['ti_id']; ?>"><i class="fa fa-download">Registration</i></button>&nbsp;
                                    <?php } if ($ti['phase'] == 'mp' OR $ti['phase'] == 'tp') { ?>
                                            <button style="float:right" id="tp_save_pdf_<?php echo $ti['ti_id']; ?>"><i class="fa fa-download">Training</i></button>
                                        <?php } ?>
                                        <br><small> <?php echo $ti['chaptername']; ?></small></div>
                                    <div class="panel-body tra_panel_body loader" style=""  > <center>
                                            <?php
                                            $col = 12;
                                            if ($ti['phase'] == 'mp') {
                                                $col = 6;
                                            }
                                            if ($ti['phase'] == 'mp' OR $ti['phase'] == 'rp') {
                                                ?>
                                                <div class="html col-md-<?php echo $col; ?>" style="">                                    
                                                    <div class="" id="trnrefid_bar_<?php echo $ti['ti_id']; ?>">
                                                    </div>
                                                    <div class="">
                                                        <button class="btn btn-primary glimpse_btn " id="btn_bar_<?php echo $ti['ti_id']; ?>" style="display:none">GLIMPSE</button>
                                                    </div>                                    
                                                </div>
                                                <?php
                                            }
                                            if ($ti['phase'] == 'mp' OR $ti['phase'] == 'tp') {
                                                ?>
                                                <div class="col-md-<?php echo $col; ?>" style="" >
                                                    <div class="" id="trnrefid_pie_<?php echo $ti['ti_id']; ?>">
                                                    </div>
                                                    <div>
                                                        <button class="btn btn-primary glimpse_btn" id="btn_pie_<?php echo $ti['ti_id']; ?>" style="display:none">GLIMPSE</button>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <?php //}
                        }
                        ?>
                    </div>
        <?php } else { ?>
                    <div class="panel-body" id="trarefid_<?php echo $tra['trarefid']; ?>">
                        <h4> No training instance in registration or training phase.</h4>
                    </div>
            <?php } ?>
            </div>  
        <?php }
        ?>
    <!-- <input type="text" id="ti_ids" value="<?php echo implode(',', $ti_ids); ?>"> -->
    </div>



    <?php
#pre($tras); die;
    $adminurl = AUTH_PANEL_URL; #$loginid = [];
    $loginid = $this->session->active_user_data->ulmusmrefid; #pre($loginid);
    $custum_js = <<<EOD
        
        
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
            
    <script type="text/javascript" language="javascript" >
        jQuery(document).ready(function() {
            $('.glimpse_btn').show();
            $('.tra_panel_body').removeClass( "loader" );
            //display_tra();
        });
    </script>
    
    <script>
        jQuery(document).ready(function() {
            $('.glimpse_btn').click(function(){ //alert('wdew');
                $('#tra_parent_div').hide();
                $('#glimpse_parent_div').show();

                var tiid_arr    = $(this).attr('id').split('_');
                var tiid        = tiid_arr[2]; //alert(tiid);
        
                var traname     = $('#ti_traname_'+tiid).val();  //alert('traname_'+tiid); //alert(traname);
                var tralogo     = $('#ti_tralogo_'+tiid).val(); 
                var tcname      = $('#ti_tcname_'+tiid).val();  
                var tiname      = $('#ti_trnname_'+tiid).val(); 
                var startdate   = $('#ti_startdate_'+tiid).val(); 
                var enddate     = $('#ti_enddate_'+tiid).val(); 
                var participants= $('#ti_reg_users_'+tiid).val(); 
                $.ajax({
                    url : "$adminurl"+"rmpanel/Dashboard/ajax_display_glimpse_headings",
                    type: "post",  
                    data: { tiid  :   tiid,
                            traname : traname,
                            tralogo : tralogo,
                            tcname  : tcname,
                            tiname  : tiname,
                            startdate : startdate,
                            enddate   : enddate,
                            participants : participants    },
                    success: function (headings) {	
                        $("#glimpse_parent_div").html(headings); //exit;
                        $.ajax({
                            url : "$adminurl"+"rmpanel/Dashboard/ajax_get_glimpse_mewp",
                            type: "post",  
                            data: { tiid  :   tiid },
                            success: function (wall_post) {                                
                                //$('.glimpse_mewp_div').removeClass( "loader" );
                                $(".mewp_dt_div_div").html(wall_post);
                                $.ajax({
                                    url : "$adminurl"+"rmpanel/Dashboard/ajax_get_glimpse_metp",
                                    type: "post",  
                                    data: { tiid  :   tiid },
                                    success: function (training_post) {	//alert(training_post);
                                        //$('.glimpse_metp_div').removeClass( "loader" );
                                        $(".metp_dt_div_div").html(training_post);
                                        $.ajax({
                                            url : "$adminurl"+"rmpanel/Dashboard/ajax_get_glimpse_meu",
                                            type: "post",  
                                            data: { tiid  :   tiid },
                                            success: function (users) {	//alert(users);
                                                //$('.glimpse_meu_div').removeClass( "loader" );
                                                $(".meu_dt_div_div").html(users);
                                                $.ajax({
                                                    url : "$adminurl"+"rmpanel/Dashboard/ajax_get_glimpse_mes",
                                                    type: "post",  
                                                    data: { tiid  :   tiid },
                                                    success: function (schools) {	//alert(schools);
                                                        //$('.glimpse_mes_div').removeClass( "loader" );
                                                        $(".mes_dt_div_div").html(schools);

                                                    }
                                                });        
                                            }
                                        });
                                    }
                                });
                            }
                        });
                        
                    }
                });
            });
        });
    </script>
EOD;

    echo modules::run('auth_panel/template/add_custum_js', $custum_js);
    ?>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?" async="" defer="defer"></script>

    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>

    <?php
    if (!empty($ti_ids)) {
        foreach ($ti_ids as $ti_id) {
            if ($mydata[$ti_id]['phase'] == 'mp' OR $mydata[$ti_id]['phase'] == 'rp') { //die('working'); 
                ?>
                <script>
                    google.charts.load("current", {packages: ['controls', "corechart", "bar"]});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ["Element", "Density", {role: "style"}],
                            ["Registered", <?php echo $mydata[$ti_id]['enrolled']; ?>, "#b87333"],
                            ["Expected", <?php echo $mydata[$ti_id]['expected']; ?>, "gold"]
                        ]);

                        var view = new google.visualization.DataView(data);
                        view.setColumns([0, 1,
                            {calc: "stringify",
                                sourceColumn: 1,
                                type: "string",
                                role: "annotation"},
                            2]);

                        var options = {
                            title: "Training Registration", width: '100%', height: '100%',
                            bar: {groupWidth: "95%"}, legend: {position: "none"},
                        };
                        var container = document.getElementById("trnrefid_bar_<?php echo $ti_id; ?>");
                        var chart = new google.visualization.ColumnChart(container);
                        chart.draw(view, options);
                        var btnSave = document.getElementById("rp_save_pdf_<?php echo $ti_id; ?>");

                        google.visualization.events.addListener(chart, 'ready', function () {
                            btnSave.disabled = false;
                        });

                        btnSave.addEventListener('click', function () {
                            var doc = new jsPDF();
                            doc.addImage(chart.getImageURI(), 0, 0);
                            doc.save('chart.pdf');
                        }, false);
                    }
                </script>
                <?php
            }
            if ($mydata[$ti_id]['phase'] == 'mp' OR $mydata[$ti_id]['phase'] == 'tp') {
                ?>
                <script>
                    // Load google charts
                    google.charts.load('current', {'packages': ['controls', 'corechart']});
                    google.charts.setOnLoadCallback(drawChart);

                    // Draw the chart and set the chart values
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Task', 'training done'],
                            ['NS', <?php echo $mydata[$ti_id]['ns']; ?>],
                            ['Low', <?php echo 1 + $mydata[$ti_id]['low']; ?>],
                            ['Medium', <?php echo 1 + $mydata[$ti_id]['medium']; ?>],
                            ['High', <?php echo 1 + $mydata[$ti_id]['high']; ?>],
                            ['Completed', <?php echo 1 + $mydata[$ti_id]['completed']; ?>]
                        ]);
                        // Optional; add a title and set the width and height of the chart
                        var options = {'title': 'Training details', 'width': '100%', 'height': '100%', legend: {position: "none"}};
                        // Display the chart inside the <div> element with id="piechart"


                        var chart = new google.visualization.PieChart(document.getElementById("trnrefid_pie_<?php echo $ti_id; ?>"));
                        chart.draw(data, options);

                        var btnSave = document.getElementById("tp_save_pdf_<?php echo $ti_id; ?>");

                        google.visualization.events.addListener(chart, 'ready', function () {
                            btnSave.disabled = false;
                        });

                        btnSave.addEventListener('click', function () {
                            var doc = new jsPDF();
                            doc.addImage(chart.getImageURI(), 0, 0);
                            doc.save('chart.pdf');
                        }, false);
                    }
                </script>
                <?php
            }
        }
        ?> <?php }
    ?>
