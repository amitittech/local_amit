<section class="panel ">
    
        <header class="panel-heading ">
            Report
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span>
            
        
       
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                    <div class = "table-responsive"> 
                    <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                        <thead>
                            <tr>
                            <th>S.No</th>
                            <th class="limited" title="Training">Training</th>
                            <th class="limited" title="Training Agency">Training Agency</th>
                            <th class="limited" title="Training Course">Course</th>
                            <th class="limited" title="Registration Start Date">Reg Start-Date</th>
                            <th class="limited" title="Registration End Date">Reg End-Date</th>
                            <th class="limited" title="Training Start Date">Trn Start-Date</th>
                            <th class="limited" title="Training End Date">Trn End-Date</th>
                            <th class="limited" title="Registration Days">Reg Days</th>
                            <th class="limited" title="Training Days">Trn Days</th>
                            <th class="limited" title="Batches">Btaches</th>
                            <th class="limited" title=" Users Invited">Invited</th>
                            <th class="limited" title="Enrolled">Enrolled</th>
                            <th class="limited" title="Completion">Completion</th>
                            <th class="limited" title="Certified">Certified</th>
                            <th class="limited" title="Schoolsenrolled">Schoolsenrolled</th>
                             <th class="limited" title="Students Reached">Students</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                
                                <th></th>
                                <th>Training</th>
                                <th>Agency</th>
                                <th>Course</th>
                            </tr>
                        </thead>
                        
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php if($result){


                             $i=0; foreach($result as $row){ $i++; ?>
                            <tr class="gradeA odd">
                                <td class=""><?php echo $i; ?></td>
                                <td class=""><?php echo $row['trnname']; ?></td>
                                <td class=""><?php echo $row['traname']; ?></td>
                                <td class="limited" title="<?php echo $row['trncourse']; ?>">
                                    <?php echo $row['trncourse']; ?>
                                </td>
                                <td class=""><?php echo $row['regstartdate']; ?></td>
                                <td class=""><?php echo $row['regenddate']; ?></td>
                                <td class=""><?php echo $row['startdate']; ?></td>
                                <td class=""><?php echo $row['enddate']; ?></td>
                                <td class=""><?php echo $row['regdays']; ?></td>
                                <td class=""><?php echo $row['trndays']; ?></td>
                                <td class=""><?php echo $row['batches']; ?></td>
                                <td class=""><?php echo $row['invited']; ?></td>
                                <td class=""><?php echo $row['enrolled']; ?></td>
                                <td class=""><?php echo $row['completion']; ?></td>
                                <td class=""><?php echo $row['certified']; ?></td>
                                <td class=""><?php echo $row['schoolsenrolled']; ?></td>
                                 <td class=""><?php echo $row['studentsreached']; ?></td>
                            </tr>
                            <?php }}?>
                        </tbody>
                    </table>
                  </div>

                </div>
            </div>
        </div>
    </section>
