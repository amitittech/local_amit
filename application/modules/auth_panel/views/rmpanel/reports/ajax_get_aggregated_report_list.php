<section class="panel">
    
        <header class="panel-heading ">
            Report
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span>
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                    <div class = "table-responsive"> 
                    <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                        <thead>
                            <tr>
                            <th class="limited" title="Training Agency">S.No</th>
                            <th class="limited" title="Training Agency">Agency</th>
                            <th class="limited" title="Training course name">Course</th>
                            <th class="limited" title="Training Conducted">Trn conducted</th>
                            <th class="limited" title="Total Training Courses">Courses</th>
                            <th class="limited" title="Registration Days">Reg Days</th>
                            <th class="limited" title="Training Days">Trn Days</th>
                            <th class="limited" title="Training Batches">Batches</th>
                            <th class="limited" title="Trainee Invited ">Invited</th>
                            <th class="limited" title="Enrolled">Enrolled</th>
                            <th class="limited" title="Completion Percentage">Completion</th>
                            <th class="limited" title="Certificate Issued">Certified</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Agency</th>
                                <th>Course</th>
                            </tr>
                        </thead>
                        
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php $i=0; foreach($result as $row){ $i++; ?>
                            <tr class="gradeA odd">
                                
                                 <td class=""><?php echo $i; ?></td>
                                <td class=""><?php echo $row['traname']; ?></td>
                                
                                <td class="limited" title="<?php echo $row['trncoursesname']; ?>">
                                    <?php echo $row['trncoursesname']; ?>
                                </td>
                                <td class=""><?php echo $row['trnconducted']; ?></td>
                                <td class=""><?php echo $row['trncourses']; ?></td>
                                <td class=""><?php echo $row['regdays']; ?></td>
                                <td class=""><?php echo $row['trndays']; ?></td>
                                <td class=""><?php echo $row['batches']; ?></td>
                                <td class=""><?php echo $row['invited']; ?></td>
                                <td class=""><?php echo $row['enrolled']; ?></td>
                                <td class=""><?php echo $row['completion']; ?></td>
                                <td class=""><?php echo $row['certified']; ?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                  </div>

                </div>
            </div>
        </div>
    </section>
