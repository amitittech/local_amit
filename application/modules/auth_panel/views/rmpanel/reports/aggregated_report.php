<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >
<style type="text/css">

.loader {
  margin: 70px auto;  
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 70px;
  height: 70px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
  .limited{
    white-space: nowrap;
    width: auto !important;                  
    overflow: hidden;
    text-overflow: ellipsis;
}  
    .td-limit {
    max-width: 70px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
td {word-wrap: break-word;}
.modal
    {
    position: fixed;
    z-index: 999;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    background-color: Black;
    filter: alpha(opacity=60);
    opacity: 0.6;
    -moz-opacity: 0.8;
    }
.center
{
    z-index: 1000;
    margin: 300px auto;
    padding: 10px;
    width: 80px;
   /* background-color: White;*/
    border-radius: 20px;
    filter: alpha(opacity=100);
    opacity: 1;
    -moz-opacity: 1;
}
.center img
{
    height: 100px;
    width: 100px;
}

</style>
<div class="modal" id="loader" style="display: none">
    <div class="center">
        <img alt="" src="<?php echo base_url('resources/ajax-loader.gif');?>" />
    </div>
</div>
<div class="col-lg-12">
    <section class="panel panel-success">
        <header class="panel-heading"> Aggregated Report
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
               
            </span>
        </header>
        <div class="panel-body">
            <form role="form" id="agregatedform" method= "POST" enctype="multipart/form-data">
                <div class="row">
                    <div style="margin-left: 18px;" class="col-md-6">
                      <div class="form-group" style="position:relative;">
                            <label for="exampleInputEmail1">Trainging Agencies<span style="color: red">*</span></label>
                             <select class="form-control" placeholder="" name="trarefid" id="tra_agency" required="true" >
                                <option selected="selected" value="">Select Training Agency</option>
                                <?php foreach ($result as $each) {
                                ?>
                                <option value="<?php echo $each['trarefid']; ?>"><?php echo $each['traname']; ?></option>
                                <?php } ?>
                            </select>
                            <span class="tra_agency_eMsg text-danger error"><?php echo form_error('trarefid'); ?></span>
                       </div>
                    </div>
                </div>
                <div class="row">
                    <div style="margin-left: 18px;" class="col-md-6 col-sm-6 col-xs-12"">
                            <div class="form-group " style="position:relative;">
                                <label for="exampleInputEmail1">Courses<span style="color: red">*</span></label>
                                <select id="courses" class="form-control js-example-basic-multiple all_empty" name="courseid[]"  required="true" multiple="">
                                </select>
                                <span class="courses_eMsg text-danger error"><?php echo form_error('rcmrefid'); ?></span>
                           </div>
                    </div> 
                    <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group " style="position:relative;">
                              <label for="exampleInputEmail1">Select All<span style="color: red"></span></label>
                              <input class="form-control" value='ALL' type="checkbox" id="checkbox" style="width: 25px;" >
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div style="margin-left: 18px;" class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group" style="position:relative;">
                                <label for="exampleInputEmail1">From Date<span style="color: red">*</span></label>
                                <input type="text" class="form-control" id="fromdate" name="fromdate" required="true" />
                                <span class="fromdate_errorMsg text-danger error"><?php echo form_error('rcmrefid'); ?></span>
                            </div>   
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group" style="position:relative;">
                                <label for="exampleInputEmail1">To Date<span style="color: red">*</span></label>
                                <input type="text" class="form-control" id="todate" name="todate" required="true" />
                                <span class="todate_eMsg text-danger error"><?php echo form_error('rcmrefid'); ?></span>
                            </div>   
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">         
                     <div  class="form-group">                        
                        <button name="mapping_btn" id="get_report" value="123" type="button" class="btn btn-info create_mapping ">Submit</button>
                     </div>
                </div>    
            </form>
        </div>
    </section>
</div>
<div class="col-lg-12 put_table">
    <div class="put_loader"></div>
</div>

<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
$apppath = base_url();
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    
    
    <script type="text/javascript" language="javascript" >
       
        $('#tra_agency').on('change', function() {
        $('.tra_agency_eMsg').html('');
        document.getElementById("checkbox").checked = false;
        var traid =  $(this).find(":selected").val();

            $.ajax({
                    url : "$adminurl"+"Training/ajaxGetTraCourses",
                    type: "post",  
                    data: { trarefid:  traid }  ,
                    success: function (response) {
                             $('#loader').hide();
                              $('#courses').html(response);
                    },
                    beforeSend: function() {
                     $('#loader').show();
                    },
            });
        }); 
        $('#courses').on('change', function() {
        $('.courses_eMsg').html('');
        }); 

        $("select").on("select2:unselect", function (evt) {
        
        $(this).parent().parent().siblings().children().children('input').prop('checked', false);
        });
        $("#checkbox").click(function(){

            if($("#checkbox").is(':checked') ){
                $("#courses > option").prop("selected","selected");
                $("#courses").trigger("change");
                }else{
                $("#courses > option").removeAttr("selected");
                $("#courses").trigger("change");
            }
        });

        $(function(){
            $("#fromdate").datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                     $('.fromdate_errorMsg').html('');
                    var date2 = $('#fromdate').datepicker('getDate');
                    date2.setDate(date2.getDate() + 10);
                    $('#todate').datepicker('setDate', date2);
                    $('#todate').datepicker('option', 'minDate', date2);
                }
            });
        });
        $(function(){    
            $("#todate").datepicker({
                dateFormat: "yy-mm-dd",
                onClose: function () {
                    var dt1 = $('#fromdate').datepicker('getDate');
                    var dt2 = $('#todate').datepicker('getDate');
                    if (dt2 <= dt1) {
                        var minDate = $('#todate').datepicker('option', 'minDate');
                        $('#todate').datepicker('setDate', minDate);
                    }
                }
            });
        });

$("#get_report").click(function(){ 
    
    var tra_agency = $('#tra_agency').val();
    var courses = $('#courses').val();
    var todate = $('#todate').val();
    var fromdate = $('#fromdate').val();
    var checkboxval = '';
    if(tra_agency == '' ){
       $('.tra_agency_eMsg').html('Please select Training Agency');
        return false;
    }
    if(courses == '' || courses== null  ){
       $('.courses_eMsg').html('Minimum one Training Course is required');
        return false;
    }
    if(fromdate == ''){
         $('.fromdate_errorMsg').html('This field is required.');
        return false;
    }
    if(todate == '' ){
       $('.todate_eMsg').html('This field is required.');
        return false;
    }
    if($('#checkbox').prop("checked") == true){
        checkboxval = $('#checkbox').val();
    }
    $.ajax({
            type:'POST',
            url : "$adminurl"+"Training/getAggregatedReport",
            data: { tra_agency:  tra_agency , courses: courses , todate: todate , fromdate: fromdate , checkboxval:checkboxval },
            success:function(response){
                $('.put_loader').removeClass('loader');
                $('.put_table').html(response);
                $('#example thead tr:eq(1) th').each( function (i) {

                if( i == 1 || i == 2){
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    });
                }
                });
                 
                var table = $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [{ 
                    extend:'excelHtml5', 
                    title: 'Aggregated report',
                    text: 'Download Excel',
                    exportOptions: {stripHtml: false}
                },],
                responsive: true,
                "ordering": true,
                
                
                });

            },
            beforeSend: function() {
                    $('.put_table').empty().append(' <div class="put_loader"></div>');
                 $('.put_loader').addClass('loader');

            },
          
    });
});
</script>   
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>