<?php //print_r($users); die;?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >

<?php //pre($result);die;  ?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">Create a new channel
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data" class="formdata" id="formdata">
                <fieldset>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Channel Name<span style="color: red">*</span></label>
                        <input  type="text" id="chmname" name="chmname" maxlength="100" class="form-control all_empty"  value=""  name="chmname" placeholder="Enter your channel name">
                        <span class="text-danger error"><?php echo form_error('chmname'); ?></span> 
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Channel Logo<span style="color: red">*</span></label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div>
                                <span class="btn btn-primary btn-file">
                                     <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select a channel logo </span>
                                    <span class="fileupload-exists old"><i class="fa fa-undo"></i> Change</span> 
                                     <input type="file"  name="image" class="default all_empty" id="chmlogo" enctype="multipart/form-data" />
                                    <!-- <input type="file" class="default all_empty"  id="chmlogo" name="image" enctype="multipart/form-data" /> -->
                                    <span class="text-danger error femsg"><?php echo form_error('chmlogo'); ?></span>
                                </span>
                            </div>
                        </div>  
                        
                        <span class="text-danger error"><?php echo form_error('chmlogo'); ?></span> 
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Channel Description<span style="color: red">*</span></label>
                        <textarea   id="chmdesc" class="form-control all_empty"  name="chmdesc" placeholder="Enter your channel description"></textarea>
                        <span class="text-danger error"><?php echo form_error('chmdesc'); ?></span> 
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Wall poster<span style="color: red">*</span></label>
                        <select class="form-control js-example-basic-multiple all_empty" placeholder="" name="chmusmrefids[]" id="my_cat" required="" multiple="multiple">
                            <?php foreach ($users as $each) {
                                ?>
                                <option value="<?php echo $each['usmrefid']; ?>"><?php echo $each['usmfirstname'].' '.$each['usmlastname'].' ('.$each['usmmob'].')'; ?></option>
                            <?php } ?>
                        </select>
                        <span class="text-danger error"><?php echo form_error('chmusmrefids'); ?></span> 
                    </div>
                </fieldset>

                <div  class="form-group">
                    <button name="mapping_btn" value="submit" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>

<div id="myModall" class="modal fade response_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Channel creation has been done as following </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered res_res"></table>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--
<div id="created_modal" class="modal fade created_modal"  role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: green;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title success">Create Channel</h4>
            </div>
            <div class="modal-body">
                <p>A new channel has been created successfully..</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
-->
<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Selected Channels has been deleted successfully..</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<!--- update channel----->
<div class="modal fade" id="myModal" role="dialog" style="overflow-y:auto;overflow:auto,padding-right:0px !important">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelupdate">Update Channel</h4>
      </div>
      <div class="modal-body">
        <p class="ustatusMsg"></p>
        <form enctype="multipart/form-data" id="updatefrm" >
			<input type="hidden"  id="usmrefid" name="usmrefid">
			<input type="hidden"  id="chmrefid" name="chmrefid">
			<div class="form-group">
				<label for="name">Channel Name</label>
				<input type="text" class="form-control" id="updatename" name="chmname" placeholder="Enter name" required />
				<p class="updnamemsg"></p>
			</div>
			
			<div class="form-group">
			  <label for="email">Channel Logo <small style="color: red;">(The file(png,jpg,jpeg) size can not exceed 10kb and resolution should be 100x100.)</small></label>
				<input type="file" class="form-control" id="chnlogo" name="chmlogo" />
				<p class="uf1emsg"></p>
				<p> <img height="50px" width="50px" class="modal_logo_img" src=""> </p>
			</div>
			<input type="submit" name="submit"  class="btn btn-info  submitBtn" value="Update"/>
        </form>
      </div>
     
    </div>
  </div>
</div>
<!--------   end update modal --->
<!--- update sucess message modal  --->
<div class="modal fade update_sucess_modal" id="update_sucess_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Update Channel</h4>
            </div>
            <div class="modal-body">
                <p>Channels has been Update successfully..</p>
            </div>
         </div>
    </div>
</div>
<div class="col-sm-12 put_table hidden_section"></div>

<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
        jQuery(document).ready(function() {

            // create mapping
            $(".create_mapping").click(function(){         
                var form = $('#formdata')[0];
                var data = new FormData(form);   
                $(".error").html('');
                var chmlogo             = $("#chmlogo").val();
                var chmname             = $("#chmname").val();
                var chmdesc             = $("#chmdesc").val();
                var users               = $("#my_cat").val();
				var any_error 			= "";				
				if(chmlogo == null || chmlogo == ""){
                    $("#chmlogo").focus();
                    $("#chmlogo").siblings('.error').html('Please choose a logo image.'); //return false;
					any_error = 1;
                }
				if(users == null){
                    $("#my_cat").focus();
                    $("#my_cat").siblings('.error').html('Please select a user to be mapped.'); //return false;
					any_error = 1;
                }
				if(chmdesc == ""){
                    $("#chmdesc").focus();
                    $("#chmdesc").siblings('.error').html('Please enter description for this channel.'); //return false;	
					any_error = 1;
                }
                if(chmname == ""){
                    $("#chmname").focus();
                    $("#chmname").siblings('.error').html('Please enter your channel name.'); //return false;	
					any_error = 1;					
                }
                $(".create_mapping").prop("disabled", false);
                //if(confirm('Are you sure to map the records?') == false){
                   //return false;
                //}
				if(any_error){
					return false;
				}
                $.ajax({
                    url         : "$adminurl"+"Wall/ajax_create_channel",
                    enctype     : 'multipart/form-data',
                    processData : false,
                    contentType : false,
                    cache       : false,
                    type        : "post",  
                    async       : false,
                    data        : data,
					beforeSend: add_spin("create_mapping","processing..."),
                    success: function (data) {
						remove_spin("create_mapping","Submit");
						$('.res_res').html(data);
                        $(".response_modal").modal("toggle");
						
                        $("#btnSubmit").prop("disabled", true);						
                        $('.select2-selection__rendered').html('');
                        $("#my_cat").val('');
                        $("#chmname").val('');
                        $("#chmdesc").val('');
                        $('input[type=file]').val(null);
                        $('.fileupload-new').show();
                         $('.old').hide();
                        $("#created_modal").modal("toggle");
                        ajax_get_datatable();

                    }
                });   
            }); 
        
            
            ajax_get_datatable();
        });	
    </script>
    <script>       
    var channel_table;
        function ajax_get_datatable(){        
            $.ajax({
                url : "$adminurl"+"Wall/ajax_master_channels_list", 
                type: "post", 
                data: {},
                success: function (data) { //alert(data);
                    $('.put_table').html(data);
                    //$('.hidden_section').show();
                    channel_table = $('#example').DataTable({
                        stateSave: true,
                        responsive: true,

                        //"pageLength": 25,
                        //"lengthMenu": [[25, 50, 100], [25, 50, 100]],
                        "columnDefs":[{
                                "targets":[3, 4],
                                "orderable":false
                        }],
                    });
                    new $.fn.dataTable.FixedHeader(channel_table);
                }
            }); 
        }
    </script>
    <script>
        $(".all_empty").change(function() {
                $(this).siblings('.error').html('');	
        });
        $(".all_empty").keyup(function() {
                $(this).siblings('.error').html('');	
        });
    </script>

    <script>
    $("#updatefrm").on('submit', function(e){
		e.preventDefault();   
        $.ajax({
            type: 'POST',
             url: "$adminurl" + "Wall/ajax_edit_channel",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#updatefrm').css("opacity",".5");
            },
            success: function(data){
                var data = JSON.parse(data);
                if(data.status == true){
                    $("#myModal").modal("toggle");
					$(".update_sucess_modal").modal("toggle");
				}else{
					$('.updnamemsg').html('<span style="color:#EA4335">'+data.message+'</span>');
				}
                $('#updatefrm').css("opacity","");
                $(".submitBtn").removeAttr("disabled");
                ajax_get_datatable();
            }
        });
    });
	
    $('#myModal').on('show.bs.modal', function (e) {
       var opener=e.relatedTarget;//
       var usmrefid =$(opener).attr('usmrefid');
       var chmname =$(opener).attr('chmname');
       var chmrefid=$(opener).attr('chmrefid');
       var chmlogo=$(opener).attr('chmlogo');
       $('#updatefrm').find('[name="usmrefid"]').val(usmrefid);
       $('#updatefrm').find('[name="chmrefid"]').val(chmrefid);
       $('#updatefrm').find('[name="chmname"]').val(chmname);
       $('.modal_logo_img').attr('src',chmlogo);
       
       
   });
   $("#myModal").on("hidden.bs.modal", function(){
      $(".uemsg").html("");
      $(".uf1emsg").html("");
      $(".uf2emsg").html("");
      $(".ustatusMsg").html("");
      $('#updatefrm')[0].reset();
   });
   $("#chnlogo").change(function() {
       
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
            
            $('.uf1emsg').html('<span style="color:red;">Please select a valid image file (JPEG/JPG/PNG).</span>');
            $("#chnlogo").val('');
            return false;
        }

        var sizeKB = this.files[0].size / 1024;
        if(sizeKB > 10){
             $('.uf1emsg').html('<span style="color:red;">File size must not exceed 10kb.</span>');
             $("#chnlogo").val('');
              return false;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
          var image = new Image();
           image.src = e.target.result;
           image.onload = function () {
                      var height = this.height;
                      var width = this.width;
                      if (height != 100 || width != 100) {
                         
                          $('.uf1emsg').html('<span style="color:red;">Image resolution must be 100x100.</span>');
                          $("#chnlogo").val('');
                          return false;
                      }
                      $('.uf1emsg').html('');
                      return true;
                };
        }
    });
    $("#chmlogo").change(function() {

        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
            
            $('.femsg').html('<span style="color:red;">Please select a valid image file (JPEG/JPG/PNG).</span>');
            $("#chmlogo").val('');
            return false;
        }

        var sizeKB = this.files[0].size / 1024;
        if(sizeKB > 10){
             $('.femsg').html('<span style="color:red;">File size must not exceed 10kb.</span>');
             $("#chmlogo").val('');
              return false;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
          var image = new Image();
           image.src = e.target.result;
           image.onload = function () {
                      var height = this.height;
                      var width = this.width;
                      if (height != 100 || width != 100) {
                         
                          $('.femsg').html('<span style="color:red;">Image resolution must be 100x100.</span>');
                          $("#chmlogo").val('');
                          return false;
                      }
                      return true;
                };
        }

    });
    
  
   </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>