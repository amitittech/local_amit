

<?php 
//pre($result); die;
if(!isset($result) OR empty($result)){ ?>
    <section class="panel">
        <header class="panel-heading">
            Query Result
        </header>
        <div class="panel-body">
            No Record found
        </div>
    </section>
<?php die; } ?>

<section class="panel">    
    <header class="panel-heading">
        Channels List
        <!-- <span class="tools pull-right">

            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span> -->
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Channel</th>
                            <!-- <th>User</th>
                            <th>Mobile</th> -->
                            <th>Logo</th>
                          <!--   <th>Action</th> -->
                            
                            <th width="100" class="hello">
                                <button type="button" name="delete_btn" class="btn-xs bold btn btn-danger delete_mapping">DELETE</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=0; foreach($result as $row){ $i++; ?>
                        <tr class="gradeA odd">
                            <td class=""><?php echo $i; ?></td>
                            <td class=""><?php echo $row['chmname']; ?></td>
                            <!-- <td class=""><?php echo $row['usmfirstname'] . ' ' . $row['usmlastname']; ?></td>
                            <td class=""><?php echo $row['usmmob']; ?></td> -->
                            <td class="">                                
                                <img class="logo_img" height="50px" width="50px" src="<?php echo TARGET_PATH_TO_DISPLAY_CHANNEL_LOGO.$row['chmlogo']; ?>" /> 
                                <!-- <img class="logo_img" height="50px" width="50px" src="<?php echo base_url('resources/wall/logo/'.$row['chmlogo']); ?>" /> -->                                    
                                </td>                            
                            <!-- <td>
                                    <button class="btn btn-primary edit_btn" data-toggle="modal" data-target="#myModal" chmlogo="<?php echo base_url('resources/wall/logo/'.$row['chmlogo']);?>" usmrefid="<?php echo $row['chmusmrefid']; ?>" chmrefid="<?php echo $row['chmrefid']; ?>" chmname="<?php echo $row['chmname'];?>"  >Edit</button>
                                 </td> -->
                            <td class=""><?php echo '<input value="' . $row['chmrefid'] . '" type="checkbox" name="ch_ids[]" class="ch_ids">'; ?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    $(".delete_mapping").click(function(){ //allert('ds');
        var checkedNum = $('input[name="ch_ids[]"]:checked').length;
        if (!checkedNum) {
            alert('Please first select any channel to be deleted.');
            return false;
        }
        if(confirm('Are you sure to delete?') == false){

        $('tbody tr td input[type="checkbox"]').each(function(){
        $(this).prop('checked', false);
        });
           return false;
        }
        // var ch_ids = [];
        // $('input[name="ch_ids[]"]:checked').each(function(){      
        //     ch_ids.push($(this).val());
        // });  
        var ch_ids = [];
         channel_table.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(i,v){
            ch_ids.push($(v).val());
         }) 
          
        $.ajax({
            url : "<?php echo AUTH_PANEL_URL; ?>"+"Wall/ajax_delete_channels",
            type: "post", 
            data: { channel_ids:  ch_ids},
            success: function (data) {
                $(".delete_modal").modal("toggle");
                ajax_get_datatable();
            },
        });        
    }); 
</script>