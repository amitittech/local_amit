<style type="text/css">
    .modal-ku {
  width: 750px;
  margin: auto;
}
</style>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Wall Poster
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">
                <div class="form-group" >
                    <label for="sel1">Users Mobiles <span style="color: red">*</span><small> (Multiple numbers can be enetered by comma(,) seperated )</small></label>

                    <input type="text" onkeypress="return validate_phones(event)" class="form-control all_empty" value="<?php
                    if (set_value('user_mobiles') != '') {
                        echo set_value('user_mobiles');
                    }
                    ?>" id="users_mobile" name="users_mobile" placeholder="Enter Users Mobile Number ..." required="">
                    <span class="text-danger error"><?php echo form_error('users_mobile'); ?></span>
                </div> 
                <div  class="form-group">
                    <button name="mapping_btn" value="123" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>

<div class="col-sm-12 put_table hidden_section"></div>


<div id="myModal" class="modal fade response_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Wall poster has been added successfully. </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered res_res"></table>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>


<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Wall Poster Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Wall Poster user has been removed successfully..</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="manage_channel" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 style="text-align: center;" class="modal-title" id="">Manage Channel</h4>
            </div>
             <form  id="manage_channel_form">
            <div class="modal-body">
               
                <div id="datac"></div>
              
            </div>
            <div class="modal-footer">
                 <input type="submit" class="btn btn-info" name="submit" value="Submit" />
            </div>
            </form> 
        </div>
    </div>
</div>

<div class="modal fade update_manage_modal" id="update_manage_modal" role="dialog"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Update Channel for Poster </h4>
            </div>
            <div class="modal-body">
                <p>Poster Updated sucessfully</p>
            </div>
        </div>
    </div>
</div>
 

<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
     
   <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
   
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {

           
                $(".create_mapping").click(function(){ 
                var any_error = "";
                $(".error").html('');
                         
                var mobiles     = $("#users_mobile").val();             
                var regrExpr = /^\d{10,10}(,\d{10,10})*$/;
                if (!regrExpr.test($("#users_mobile").val())) {
                    $("#users_mobile").focus();
                    $("#users_mobile").siblings('.error').html('Please enter 10 digits number.');
                    return false;
                }
                if (!mobiles) {
                    $("#users_mobile").focus();
                    $("#users_mobile").siblings('.error').html('Please enter user mobile numbers.');
                    any_error = 1;;
                }               
                if(any_error){
                    return false;
                }
                if(mobiles){ 
                    if(confirm('Are you sure to create these records?') == false){
                        return false;
                    }   
                    $.ajax({
                        url : "$adminurl"+"Wall/add_wall_poster",
                        type: "post",  
                        data: { user_mobiles:mobiles},
                        beforeSend: add_spin("create_mapping","processing..."),                     
                        success: function (data) { //alert('success');
                            remove_spin("create_mapping","Submit");
                            $('.res_res').html(data);
                            $(".response_modal").modal("toggle");
                            $('.select2-selection__rendered').html('');
                            $(".all_empty").val('');
                            ajax_get_datatable();
                        },
                    });
                }
            });             
            ajax_get_datatable();
       } );                
    </script>
    <script>  
    var my_table_obj;
        function ajax_get_datatable(){        
            $.ajax({
                url : "$adminurl"+"Wall/ajax_wall_posters_list", 
                type: "post", 
                data: {},
                success: function (data) { //alert(data);                   
                    $('.put_table').html(data);
                     my_table_obj = $('#example').DataTable({
                        responsive: true,
                        "columnDefs":[{                                
                        "targets":[3],
                         "orderable":false                        
                         }],
                        "pageLength": 30,
                       
                    });
                    new $.fn.dataTable.FixedHeader(my_table_obj);
                }
            }); 
        }
    </script>
    
    <script>
        $(".all_empty").change(function() {
                $(this).siblings('.error').html('');    
        });
        $(".all_empty").keyup(function() {
                $(this).siblings('.error').html('');    
        });
    </script>
        
    <script>
        function validate_phones(event) {
            var key = window.event ? event.keyCode : event.which; //alert(key);
            if (key == 8 || key == 39 || key == 44) {
                return true;
            }else if (key < 48 || key > 57  ) {
                return false;
            }
            else return true;
        };
    </script>
    <script type="text/javascript" language="javascript" >
    jQuery(document).ready(function() {
        $('#manage_channel_form').on('submit', function(event){
         event.preventDefault();
         var form_data = $('#manage_channel_form').serialize();
         var user=    $('#huser').val();
            $.ajax({
                 url : "$adminurl"+"Wall/manage_channel", 
                type: "post",
                data:$('#manage_channel_form').serialize() + "&id=" + user,
                success:function(data)
                {   var msg = JSON.parse(data);
                    if(msg='sucess'){   
                    $("#manage_channel").modal("toggle");
                    $(".update_manage_modal").modal("toggle");
                    }
                  
                }
            });
          
          
         });
     });             
</script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>

<script>
    function add_spin(){
        $('.create_mapping').html("<i class='fa fa-spinner fa-spin submit_query_spin'></i> Processing Query...");
        $('.create_mapping').addClass('disabled');
    }
    
    function remove_spin(){
        $('.create_mapping').html("Submit");
        $('.create_mapping').removeClass('disabled');
        //$('.submit_query_spin').addClass('hidden');
    }
</script>   