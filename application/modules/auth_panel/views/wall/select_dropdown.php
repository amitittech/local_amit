
<?php $total_channels =  count($result);?>
<div class="container">
    <h3 id="check_channel" data-attrib = "<?php if($total_channels> 1){?> true <?php }else{ ?> false <?php } ?>   " style="margin-left: 324px" >Total Channels - <?php echo $total_channels; ?> </h3>
    <table  class="table table-striped" style="width: 67%;">
    <tr>
        <th style="text-align: center;">Select checkbox</th>
        <th style="text-align: center;">Channel</th>
        <th style="text-align: center;">Logo</th>
        
    </tr>
     <?php foreach ($result as $key => $value) { 
         $CI =& get_instance();
         $CI->load->model('Wall_model');
         $result= $CI->Wall_model->check_channel_poster($value['chmname'],$userid);
       ?>
    <tr>
        <td style="text-align: center;" align="center">
            <input id=huser type="hidden" name="user" value="<?php echo $userid;?>">
            <input  type="checkbox" class="case" name="channel[]" value="<?php echo $value['chmname']; ?>" 
            <?php if($result){ ?> checked <?php }   ?> />
         </td>
        <td style="text-align: center;"><?php echo $value['chmname'] ?></td>
        <td style="text-align: center;"><img class="logo_img" height="50px" width="50px" src="<?php echo TARGET_PATH_TO_DISPLAY_CHANNEL_LOGO.$value['chmlogo']; ?>"  /></td>
        
    </tr>
    <?php } ?>

    </table>
</div>