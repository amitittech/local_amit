

<?php 
//pre($result); die;
if(!isset($result) OR empty($result)){ ?>
    <section class="panel">
        <header class="panel-heading">
            Wall Posters
        </header>
        <div class="panel-body">
            No Record found
        </div>
    </section>
<?php die; } ?>

<section class="panel">    
    <header class="panel-heading">
        Wall Posters List
        <!-- <span class="tools pull-right">

            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span> -->
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                <table id="example" class="table table-striped table-bordered nowrap display my_table" style="width:100%">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>User</th>
                            <th>Mobile</th>
							<!--
                            <th width="100" class="hello">
                                <button type="button" name="delete_btn" class="btn-xs bold btn btn-danger delete_mapping">DELETE</button>
                            </th-->
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=0; foreach($result as $row){ $i++; ?>
                        <tr class="gradeA odd">
                            <td class=""><?php echo $i; ?></td>
                            <td class=""><?php echo $row['usmfirstname'] . ' ' . $row['usmlastname']; ?></td>
							<td class=""><?php echo $row['usmmob']; ?></td>
                           <?php /* <td class=""><?php echo '<input value="' . $row['urmrefid'] . '" type="checkbox" name="usm_ids[]" class="usm_ids">'; ?></td> <?php */ ?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    $(".delete_mapping").click(function(){ //allert('ds');
        var checkedNum = $('input[name="usm_ids[]"]:checked').length;
        if (!checkedNum) {
            alert('Please first select any entry to be deleted.');
            return false;
        }
        if(confirm('Are you sure to delete?') == false){
			$('tbody tr td input[type="checkbox"]').each(function(){
				$(this).prop('checked', false);
			});
			return false;
        }
		var usm_ids = [];
			my_table_obj.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(i,v){
			usm_ids.push($(v).val());
         })   
        $.ajax({
            url : "<?php echo AUTH_PANEL_URL; ?>"+"Wall/ajax_delete_wall_posters",
            type: "post", 
            data: { delete_ids:  usm_ids},
            success: function (data) {
                $(".delete_modal").modal("toggle");
                ajax_get_datatable();
            },
        });        
    }); 
</script>