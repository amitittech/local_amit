
<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >
<style type="text/css">
    th ,td {text-align:center}
    .modal {    overflow-y: auto;}.modal-open {    overflow: auto;}.modal-open[style] {    padding-right: 0px !important;}
</style>
<!-- Modal -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>

            </div>

            <!-- save Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form enctype="multipart/form-data" id="fupForm" >
                    <div class="form-group">
                        <label for="name">Name<span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="name" name="traname" placeholder="Enter name" required />
                        <p class="emsg"></p>
                    </div>
                    <div class="form-group">
                        <label for="email">Training Agency Logo<span style="color: red">*</span> <small style="color: red;">(The file(png,jpeg,jpg) size can not exceed 10kb and resolution should be 100x100.)</small></label>

                        <input type="file" class="form-control" id="file" name="tralogo"  required />
                        <p class="f1emsg"></p>
                    </div>
                    <div class="form-group">
                        <label for="file">Training Agency Splash<span style="color: red">*</span> <small style="color: red;">(The file(png,jpeg,jpg) size can not exceed 50kb and resolution should be 512x640.)</small></label>
                        <input type="file" class="form-control" id="file2" name="trasplash" required />
                        <p class="f2emsg"></p>
                    </div>
                    <input type="submit" name="submit" class="btn btn-info  submitBtn" value="SAVE"/>
                </form>
            </div>
            <!-- save Modal Body ends--> 



        </div>
    </div>
</div>

<!--- save sucess message modal  --->
<div class="modal fade save_modal" id="save_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Save Training Agency</h4>
            </div>
            <div class="modal-body">
                <p>Training Agency has been saved successfully..</p>
            </div>
        </div>
    </div>
</div>
<!--- save sucess modal ends  --->
<!--- update sucess message modal  --->
<div class="modal fade update_sucess_modal" id="update_sucess_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Update Training Agency</h4>
            </div>
            <div class="modal-body">
                <p>Training Agency has been Update successfully..</p>
            </div>
        </div>
    </div>
</div>
<!--- save sucess modal ends  --->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabelupdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelupdate">Update Training Agency</h4>
            </div>

            <div class="modal-body">
                <p class="ustatusMsg"></p>
                <form enctype="multipart/form-data" id="updatefrm" >
                    <input type="hidden"  id="trarefid" name="trarefid">
                    <div class="form-group">
                        <label for="name">Name<span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="updatename" name="traname" placeholder="Enter name" required />
                        <p class="uemsg"></p>
                    </div>
                    <div class="form-group">
                        <label for="email">Training Agency Logo<span style="color: red">*</span> <small style="color: red;">(The file (png,jpg,jpeg) size can not exceed 10kb and resolution should be 100x100.)</small></label>
                        <input type="file" class="form-control" id="ufile1" name="tralogo"   />
                        <!-- <p style="font-style: italic;">upload file(png,jpg,jpeg) max 10 kb size.</p> -->
                        <p class="uf1emsg"></p>
                        <p> <img height="50px" width="50px" class="modal_logo_img" src=""> </p>
                    </div>
                    <div class="form-group">
                        <label for="file">Training Agency Splash<span style="color: red">*</span> <small style="color: red;">(The file (png,jpg,jpeg) size can not exceed 50kb and resolution should be 100x400.)</small></label>
                        <input type="file" class="form-control" id="ufile2" name="trasplash"  />
                        <p class="uf2emsg"></p>
                        <p> <img height="50px" width="50px" class="modal_splash_img" src=""> </p>

                    </div>
                    <input type="submit" name="submit"  class="btn btn-info  submitBtn" value="Update"/>
                </form>
            </div>

        </div>
    </div>
</div>
<div class="modal fade infoModal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabelupdate">
    <div class="modal-dialog" role="document" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelupdate" style="text-align: center;">Training Agency Info</h4>
            </div>
            <div class="modal-body agencyinfo">

            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 put_table"></div>
<div class="loading" id="loading" ></div>
<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>

<?php
$adminurl = AUTH_PANEL_URL;

$custum_js = <<<EOD

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    
 <script type="text/javascript" language="javascript" >

$(document).ready(function() {
  $(".infoModal").on("hidden.bs.modal", function() {
    $(".agencyinfo").html("");
  });
   
});



	jQuery(document).ready(function() {
        ajax_get_datatable();
  });

	function ajax_get_datatable() {
		$.ajax({
			url: "$adminurl" + "Training/ajax_training_agencies_list",
			type: "post",
			data: {},
			success: function(data) {
				$('.put_table').html(data);
				var table = $('#example').DataTable({
					responsive: true,
					"columnDefs":[{
						"targets":[2,3],
						"orderable":false
					}],
				});
				new $.fn.dataTable.FixedHeader(table);
			},
		});
	}
</script>


<script>

 $("#fupForm").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
             url: "$adminurl" + "Training/save_training",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#fupForm').css("opacity",".5");
            },
            success: function(msg){
                $('.statusMsg').html('');
                if(msg == 'success'){
                    $('#fupForm')[0].reset();
                    $(".save_modal").modal("toggle");
                        $("#modalForm").modal("toggle");
                    $('.statusMsg').html('<span style="font-size:18px;color:#34A853">Training submitted successfully.</span>');
                }else if (msg == 'exists'){
                    $('.emsg').html('<span style="color:#EA4335">Training already exists.</span>');
                }else{
                    $('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
                }
                $('#fupForm').css("opacity","");
                $(".submitBtn").removeAttr("disabled");
                ajax_get_datatable();
            }
        });
    });
    
    
  //file type validation
  $("#file").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
            $('.f1emsg').html('<span style="color:red;">Please select a valid image file (JPEG/JPG/PNG).</span>');
            $("#file").val('');
            return false;
        }
        var sizeKB = this.files[0].size / 1024;
        if(sizeKB > 10){
             $('.f1emsg').html('<span style="color:red;">File size must not exceed 10kb.</span>');
             $("#file").val('');
              return false;
        }

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
          var image = new Image();
           image.src = e.target.result;
           image.onload = function () {
                      var height = this.height;
                      var width = this.width;
                      if (height != 100 || width != 100) {
                         
                          $('.f1emsg').html('<span style="color:red;">Image resolution must be 100x100.</span>');
                          $("#file").val('');
                          return false;
                      }
                      $('.f1emsg').html('');
                      return true;
                };
         }
  });

     $("#file2").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var height = this.height;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
           
            $('.f2emsg').html('<span style="color:red;">Please select a valid image file (JPEG/JPG/PNG).</span>');
            $("#file2").val('');
            return false;
        }
        var sizeKB = this.files[0].size / 1024;
        if(sizeKB > 50){
             $('.f2emsg').html('<span style="color:red;">File size must not exceed 50kb.</span>');
              $("#file2").val('');
              return false;
        }

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
          var image = new Image();
           image.src = e.target.result;
           image.onload = function () {
                      var height = this.height;
                      var width = this.width;
                      if (height != 640 || width != 512) {
                         
                          $('.f2emsg').html('<span style="color:red;">Image resolution must be 512x640.</span>');
                          $("#file2").val('');
                          return false;
                      }
                      $('.f2emsg').html('');
                      return true;
                };
         }

    });

  $('#myModal').on('show.bs.modal', function (e) {
  
       var opener=e.relatedTarget;//
    
       var traname =$(opener).attr('traname');
       var trarefid=$(opener).attr('trarefid');
       var tralogo=$(opener).attr('tralogo');
       var trasplash=$(opener).attr('trasplash');
       $('#updatefrm').find('[name="trarefid"]').val(trarefid);
       $('#updatefrm').find('[name="traname"]').val(traname);
       $('.modal_logo_img').attr('src',tralogo);
       $('.modal_splash_img').attr('src',trasplash);
       
    });
});


 $("#updatefrm").on('submit', function(e){
    e.preventDefault();
        $.ajax({
            type: 'POST',
             url: "$adminurl" + "Training/edit_training",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#updatefrm').css("opacity",".5");
            },
            success: function(msg){
               $('.ustatusMsg').html('');
                if(msg == 'success'){
                    $('#updatefrm')[0].reset();
                    $(".update_sucess_modal").modal("toggle");
                        $("#myModal").modal("toggle");
                    $('.ustatusMsg').html('<span style="font-size:18px;color:#34A853">Training submitted successfully.</span>');
                }else if (msg == 'exists'){
                    $('.uemsg').html('<span style="color:#EA4335">Training already exists.</span>');
                }else{
                    $('.ustatusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
                }
                $('#updatefrm').css("opacity","");
                $(".submitBtn").removeAttr("disabled");
                ajax_get_datatable();
                
            }
        });
    });
    $("#modalForm").on("hidden.bs.modal", function(){
      $(".eMsg").html("");
      $(".f1emsg").html("");
      $(".f2emsg").html("");
     $(".statusMsg").html("");
     $('#fupForm')[0].reset();
   });
   $("#myModal").on("hidden.bs.modal", function(){
      $(".uemsg").html("");
      $(".uf1emsg").html("");
      $(".uf2emsg").html("");
     $(".ustatusMsg").html("");
     $('#updatefrm')[0].reset();
   });
   $("#ufile1").change(function() {
       
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
            
            $('.uf1emsg').html('<span style="color:red;">Please select a valid image file (JPEG/JPG/PNG).</span>');
            $("#ufile1").val('');
            return false;
        }

        var sizeKB = this.files[0].size / 1024;
        if(sizeKB > 10){
             $('.uf1emsg').html('<span style="color:red;">File size must not exceed 10kb.</span>');
             $("#ufile1").val('');
              return false;
        }

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
          var image = new Image();
           image.src = e.target.result;
           image.onload = function () {
                      var height = this.height;
                      var width = this.width;
                      if (height != 100 || width != 100) {
                         
                          $('.uf1emsg').html('<span style="color:red;">Image resolution must be 100x100.</span>');
                          $("#ufile1").val('');
                          return false;
                      }
                       $('.uf1emsg').html('');
                      return true;

                };
        }
    });
    $("#ufile2").change(function() {
       
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
            $('.uf2emsg').html('<span style="color:red;">Please select a valid image file (JPEG/JPG/PNG).</span>');
            $("#ufile2").val('');
            return false;
        }
        var sizeKB = this.files[0].size / 1024;
        if(sizeKB > 50){
             $('.uf1emsg').html('<span style="color:red;">File size must not exceed 50kb.</span>');
             $("#uf2emsg").val('');
              return false;
        }

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
          var image = new Image();
           image.src = e.target.result;
           image.onload = function () {
                      var height = this.height;
                      var width = this.width;

                      if (height != 100 || width != 400) {
                          $('.uf2emsg').html('<span style="color:red;">Image resolution must be 100x400.</span>');
                          $("#ufile2").val('');
                          return false;
                      }
                      $('.uf2emsg').html('');
                      return true;
                };
         }
    });
    $('#name').keyup(function(){
    this.value = this.value.toUpperCase();
     $('.emsg').val('');
   });
    $('#updatename').keyup(function(){
    this.value = this.value.toUpperCase();
   });
) ;
});
 
               
   
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>






