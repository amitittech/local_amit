<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<style type="text/css">
    .bs-example{
        margin: 20px;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php //pre($result);die;  ?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Mapping Between Training Agencies And Their Courses

            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputEmail1">Trainging Agencies<span style="color: red">*</span></label>
                    <?php
                    if (set_value('trarefids') != '') {
                        $category1 = set_value('trarefids');
                    } else {
                        //$category1 = '';
                        //if(!empty($result['trarefid'])){$category1 = $result['trarefid'];}else{ //$category1='';}
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple" placeholder="mm/dd/yyyy" name="trarefids[]" id="my_cat" required="" multiple="multiple">
                        <option value="">Select Agencies</option>
                        <?php foreach ($training_agencies as $each) {
                            ?>
                            <option value="<?php echo $each['trarefid']; ?>"><?php echo $each['traname']; ?></option>
                        <?php } ?>
                    </select>
                    <span class="text-danger"><?php echo form_error('trarefid'); ?></span>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Courses<span style="color: red">*</span></label>
                    <?php
                    if (set_value('rcmrefids') != '') {
                        $category1 = set_value('rcmrefids');
                    } else {
                        $category1 = '';
                        if (!empty($result['rcmrefids'])) {
                            $category1 = $result['rcmrefids'];
                        } else {
                            $category1 = '';
                        }
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple" name="rcmrefids[]" id="sub_cats" required="" multiple="">
                        <option value=""> select Courses </option>
                        <?php if (!empty($courses)) {
                            foreach ($courses as $each) {
                                ?>
                                <option value="<?php echo $each['rcmrefid']; ?>"> <?php echo $each['rcmname']; ?></option>
                        <?php } } ?>

                    </select>
                    <span class="text-danger"><?php echo form_error('rcmrefid'); ?></span>
                </div>   

                <div  class="form-group">
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>
<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
//$adminurl = BUSINESS_PANEL_URL;
$adminurl = base_url('index.php');
$custum_js = <<<EOD
             
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
 <script type="text/javascript" language="javascript" >
    jQuery('form').submit(function(){//alert();
        $(this).find(':submit').attr( 'disabled','disabled' );
    });
    $('#my_cat').change( function(){
       
    });
</script>

EOD;

echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>