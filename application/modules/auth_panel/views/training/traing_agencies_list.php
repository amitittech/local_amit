<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >

<?php /* //pre($result);die;  ?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Training Agencies
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputEmail1">Trainging Agencies<span style="color: red">*</span></label>
                    <?php
                    if (set_value('trarefids') != '') {
                        $category1 = set_value('trarefids');
                    } else {
                        //$category1 = '';
                        //if(!empty($result['trarefid'])){$category1 = $result['trarefid'];}else{ //$category1='';}
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple" placeholder="mm/dd/yyyy" name="trarefids[]" id="my_cat" required="" multiple="multiple">
                        <option value="">Select Agencies</option>
                        <?php foreach ($training_agencies as $each) {
                            ?>
                            <option value="<?php echo $each['trarefid']; ?>"><?php echo $each['traname']; ?></option>
                        <?php } ?>
                    </select>
                    <span class="text-danger"><?php echo form_error('trarefid'); ?></span>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Courses<span style="color: red">*</span></label>
                    <?php
                    if (set_value('rcmrefids') != '') {
                        $category1 = set_value('rcmrefids');
                    } else {
                        $category1 = '';
                        if (!empty($result['rcmrefids'])) {
                            $category1 = $result['rcmrefids'];
                        } else {
                            $category1 = '';
                        }
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple" name="rcmrefids[]" id="sub_cats" required="" multiple="">
                        <option value=""> select Courses </option>
                        <?php
                        if (!empty($courses)) {
                            foreach ($courses as $each) {
                                ?>
                                <option value="<?php echo $each['rcmrefid']; ?>"> <?php echo $each['rcmname']; ?></option>
    <?php }
}  ?>

                    </select>
                    <span class="text-danger"><?php echo form_error('rcmrefid'); ?></span>
                </div>   

                <div  class="form-group">
                    <button name="mapping_btn" type="submit" class="btn btn-info">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>
 <?php */ ?>
<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            List of Training Agencies
        </header>
        <div class="panel-body">
            <div class="adv-table">
             <!--   <form name="form2" action="<?php // echo AUTH_PANEL_URL. 'Training/delete_selected_courses_traing_mapping'; ?>" method="post">
           -->     <table  class="display table table-bordered table-striped" id="all-video-grid">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Training Agency</th>
                           <!-- <th width="100"><button type="submit" name="delete_btn" class="btn-xs bold btn btn-danger delete_mapping"  >DELETE</button>&nbsp;</th> -->
                        </tr>
                    </thead>
                    <thead>
                        <!--
                        <tr>
                            <th align="center"></th>
                        </tr>-->
                    </thead>
                </table>
              <!-- </form> -->
            </div>
        </div>
    </section>
</div>
<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {
           var table = 'all-video-grid';
           var dataTable = jQuery("#"+table).DataTable( {
                "processing": true,
                "pageLength": 25,
                "lengthMenu": [[25, 50, 100], [25, 50, 100]],
                "serverSide": true,
                "order": [[ 0, "desc" ]],
                "ajax":{
                    url :"$adminurl"+"Training/ajax_traing_agencies_list/", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                       jQuery("."+table+"-error").html("");
                       jQuery("#"+table+"_processing").css("display","none");
                    }
               }
           } );
           jQuery("#"+table+"_filter").css("display","none");
           $('.search-input-text').on( 'keyup click', function () {   // for text boxes
               var i =$(this).attr('data-column');  // getting column index
               var v =$(this).val();  // getting search input value
               dataTable.columns(i).search(v).draw();
           } );
            $('.search-input-select').on( 'change', function () {   // for select box
                var i =$(this).attr('data-column');
                var v =$(this).val();
                dataTable.columns(i).search(v).draw();
            } );
            // Re-draw the table when the a date range filter changes
            $('.date-range-filter').change(function() {
                if($('#min-date-video-list').val() !="" && $('#max-date-video-list').val() != "" ){
                    var dates = $('#min-date-video-list').val()+','+$('#max-date-video-list').val();
                    dataTable.columns(8).search(dates).draw();
                } 
                if($('#min-date-video-list').val() =="" || $('#max-date-video-list').val() == "" ){
                    var dates = "";
                    dataTable.columns(8).search(dates).draw();
                }  
            }); 
       } );				   
    </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>