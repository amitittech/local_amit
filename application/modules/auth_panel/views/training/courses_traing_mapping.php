<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >

<?php #pre(APPPATH);die;  ?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Mapping Between Training Agencies And Their Courses
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">
                <div class="form-group" style="position:relative;">
                    <label for="exampleInputEmail1">Trainging Agencies<span style="color: red">*</span></label>
                    <?php
                    if (set_value('trarefids') != '') {
                        $category1 = set_value('trarefids');
                    } else {
                        //$category1 = '';
                        //if(!empty($result['trarefid'])){$category1 = $result['trarefid'];}else{ //$category1='';}
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple all_empty" placeholder="mm/dd/yyyy" name="trarefids[]" id="my_cat" required="" multiple="multiple">
                        <?php foreach ($training_agencies as $each) {
                            ?>
                            <option value="<?php echo $each['trarefid']; ?>"><?php echo $each['traname']; ?></option>
                        <?php } ?>
                    </select>
                    <a href="javascript:;" class="fa fa-chevron-down" style="position: absolute;top: 32px;right: 10px;"></a>
                    <span class="text-danger error"><?php echo form_error('trarefid'); ?></span> 
                </div>

                <div class="form-group" style="position:relative;">
                    <label for="exampleInputEmail1">Courses<span style="color: red">*</span></label>
                    <?php
                    if (set_value('rcmrefids') != '') {
                        $category1 = set_value('rcmrefids');
                    } else {
                        $category1 = '';
                        if (!empty($result['rcmrefids'])) {
                            $category1 = $result['rcmrefids'];
                        } else {
                            $category1 = '';
                        }
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple all_empty" name="rcmrefids[]" id="sub_cats" required="" multiple="">
                        <?php
                        if (!empty($courses)) {
                            foreach ($courses as $each) {
                                $version = (!empty($each['rcmversionlabel'])) ? ' - ' . $each['rcmversionlabel'] : "";
                                ?>
                                <option value="<?php echo $each['rcmrefid']; ?>"> <?php echo $each['chaptername'] . $version; ?></option>
                                <?php
                            }
                        }
                        ?>

                    </select>
                    <a href="javascript:;" class="fa fa-chevron-down" style="position: absolute;top: 32px;right: 10px;"></a>
                    <span class="text-danger error"><?php echo form_error('rcmrefid'); ?></span>
                </div>   

                <div  class="form-group">
                    <button name="mapping_btn" value="123" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>
<div class="col-lg-12 put_table">

</div>

 <div  class="form-group">
     <button name="generatepdf" value="123" type="button" class="btn btn-info generatepdf" id="generatepdf">generatepdf</button>
</div>


<div id="myModal" class="modal fade response_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Mapping has been done as following </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered res_res"></table>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Mapping has been deleted successfully..</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
$apppath = base_url();
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {          
            ajax_get_datatable_mapping();
            // create mapping
            $(".create_mapping").click(function(){ //alert('hello'); exit; 
                $(".error").html('');
                var trainings   = $("#my_cat").val();
                var courses     = $("#sub_cats").val();                
                if(courses == null){
                    $("#sub_cats").focus();
                    $("#sub_cats").siblings('.error').html('Please select a course to be mapped.');					
                }
                if(trainings == null){
                    $("#my_cat").focus();
                    $("#my_cat").siblings('.error').html('Please select a training to be mapped.');					
                }
                if(trainings == null || courses == null){
                    return false;
                }
                if(confirm('Are you sure to map the records?') == false){
                   return false;
                }
               
                //alert(trainings);alert(courses);
				//var final = '';
				//$('#my_cat:checked').each(function(){        
				//		var values = $(this).val();
                // 		final += values+',';
                //		alert(final);
				//});
                    
               $.ajax({
                    url : "$adminurl"+"Training/ajax_courses_mapping",
                    type: "post",  
                    data: { trainings:  trainings,courses:  courses }  ,
                    success: function (data) {
                         ajax_get_datatable_mapping();
						$('.res_res').html(data);
                        $(".response_modal").modal("toggle");						
                        //alert(data);
                        //alert("Mapping has been done successfully.");
						$('.select2-selection__rendered').html('');
						$("#my_cat").val('');
						$("#sub_cats").val('');
						var dates = "";
                        dataTable.columns(0).search(dates).draw();
                        //location.reload(true);
                    },
               });        
            }); 
        
            // delete mapping
            $(".delete_mapping").click(function(){
                var checkedNum = $('input[name="mapping_ids[]"]:checked').length;
                if (!checkedNum) {
                    alert('Please first select any mappning to be deleted.');
                    return false;
                }
                if(confirm('Are you sure to delete this entry?') == false){
                   return false;
               }
			   //var mapping_ids   = $("#mapping_ids").val();
			   //alert(mapping_ids); exit;
                var mapping_ids = [];
                $('.mapping_ids:checked').each(function(){      
					mapping_ids.push($(this).val());
					
                    //var values = $(this).val();
					//  final += values+',';
                    //alert(final);
                });
                    
               $.ajax({
                    url : "$adminurl"+"Training/ajax_delete_courses_training_mapping",
                    type: "post",  
                    data: { mapping_ids:  mapping_ids        },
                    success: function (data) {
						//$('.delete_modal').html(data);
						$(".delete_modal").modal("toggle");
						var dates = "";
						dataTable.columns(0).search(dates).draw();
                        //alert(data);
                        //alert("Mapping has been deleted successfully.");
                    }
               });        
            }); 
			
            
        
       } );				   
    </script>
    
        <script>
            $(".generatepdf").click(function(){ 
                var html    = "hello world";
                url         = "$apppath"+"application/third_party/mpdf/example1.php"
                //alert(html);  alert(url);   
                $.ajax({
                    url : url,
                    type: "post",  
                    data: { html:  html  },
                    success: function (data) {
                       alert('done');
                    },
                }); 
            });
        </script>
        
        
	<script>
		$(".all_empty").change(function() {
			$(this).siblings('.error').html('');	
		});
		$(".all_empty").keyup(function() {
			$(this).siblings('.error').html('');	
		});



function delete_mapping(){

    var checkedNum = $('input[name="mapping_ids[]"]:checked').length;
    if (!checkedNum) {
    alert('Please first select any mapping Id to be deleted.');
    return false;
    }
    if(confirm('Are you sure to delete?') == false){
    return false;
    }   

    var mapping_ids = [];
    $('input[name="mapping_ids[]"]:checked').each(function(){      
    mapping_ids.push($(this).val());
    });  

    $.ajax({
            url : "$adminurl"+"Training/ajax_delete_courses_training_mapping",
            type: "post",  
            data: { mapping_ids:  mapping_ids  },
            success: function (data) {
                $(".delete_modal").modal("toggle");
                ajax_get_datatable_mapping();
     
             },
    });    
}

function ajax_get_datatable_mapping() {
    $.ajax({
        url: "$adminurl" + "Training/ajax_course_mapping_list",
        type: "post",
        data: {},
        success: function(data) { 
            $('.put_table').html(data);
            var table = $('#example').DataTable({
                responsive: true,
                "columnDefs":[{                                
                        "targets":[3],
                         "orderable":false                        
                    }]

            });
            new $.fn.dataTable.FixedHeader(table);
        }
    });
}
        
     
   
      
    </script>   

        
        
   
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);

?>