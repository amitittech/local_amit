<?php 

    if(!isset($result['status']) OR empty($result['status'])){ ?>
<section class="panel">
    <header class="panel-heading">
       Sender ID(S)-Training Agencies Mapping Result
    </header>
    <div class="panel-body">
        <?php echo $result['message']; ?>
    </div>
</section>
   <?php die; } else{
    $result = $result['result'];
   }
?>

<section class="panel">
    
        <header class="panel-heading">
            Sender ID(S)-Training Agencies Mapping List
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span>
            
        
       
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                    <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                        <thead>
                            <tr>
                                 <th>S.No</th>
                                 <th>Training Agency</th>
                                 <th>Sender-ID</th>
                               
                                <th width="100" class="hello"><input name="delete" type="submit" id="delete" value="Delete" onClick="return delete_sender_mapping();" style="background:#F52126; color:#fff; border:none; font-size:12px; padding:4px 8px;">&nbsp;</th>

                            </tr>
                        </thead>
                        
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php $i=0; foreach($result as $row){ $i++; ?>
                            <tr class="gradeA odd">
                                <td class=""><?php echo $i; ?></td>
                                <td class=""><?php echo $row['traname']; ?></td>
                                <td class=""><?php echo $row['sendercode']; ?></td>
                               
                                 <td class=""><?php echo '<input value="' . $row['mapping_id'] . '" type="checkbox" name="mapping_ids[]" class="mapping_id">'; ?></td> 

                            </tr>
                            <?php }?>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </section>
