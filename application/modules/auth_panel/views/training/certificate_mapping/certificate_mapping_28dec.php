<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >

<?php //pre($result);die;  ?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Mapping For Training Agencies And Certificates
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">
                <div class="form-group" style="position:relative;">
                    <label for="exampleInputEmail1">Trainging Agencies<span style="color: red">*</span></label>                    
                    <select class="form-control js-example-basic-multiple all_empty" placeholder="mm/dd/yyyy" name="trarefids[]" id="my_cat" required="" multiple="multiple">
                        <?php foreach ($training_agencies as $each) {
                            ?>
                            <option value="<?php echo $each['trarefid']; ?>"><?php echo $each['traname']; ?></option>
                        <?php } ?>
                    </select>
					<a class="fa fa-chevron-down" style="position: absolute;top: 32px;right: 10px;"></a>
                    <span class="text-danger error"><?php echo form_error('trarefid'); ?></span> 
                </div>

                <div class="form-group" style="position:relative;">
                    <label for="exampleInputEmail1">Certificate Templates<span style="color: red">*</span></label>
                    <select class="form-control js-example-basic-multiple all_empty" name="certificates[]" id="sub_cats" required="" multiple="">
                        <?php
                        if (!empty($certificates)) {
                            foreach ($certificates as $each) {
                                ?>
                                <option value="<?php echo $each['ctmrefid']; ?>"> <?php echo $each['ctmtemplatename']; ?></option>
    <?php }
} ?>
                    </select>
					<a class="fa fa-chevron-down" style="position: absolute;top: 32px;right: 10px;"></a>
                    <span class="text-danger error"><?php echo form_error('certificates'); ?></span>
                </div>   

                <div  class="form-group">
                    <button name="mapping_btn" value="123" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>

<div id="myModal" class="modal fade response_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Mapping has been done as following </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered res_res"></table>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Mapping has been deleted successfully..</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 put_table hidden_section">
    
</div>

<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {
            $(".create_mapping").click(function(){ //alert('hello'); exit; 
                $(".error").html('');
                var trainings   = $("#my_cat").val();
                var certificates     = $("#sub_cats").val();
                
                if(certificates == null){
                    $("#sub_cats").focus();
                    $("#sub_cats").siblings('.error').html('Please select a certificate to be mapped.'); //return false;					
                }
				if(trainings == null){
                    $("#my_cat").focus();
                    $("#my_cat").siblings('.error').html('Please select a training to be mapped.'); //return false;					
                }
				if(trainings == null || certificates == null){
					return false;
				}
                if(confirm('Are you sure to map the records?') == false){
                   return false;
                }
                $.ajax({
                    url : "$adminurl"+"Training/ajax_map_certificates",
                    type: "post",  
                    data: { trainings:  trainings, certificates: certificates }  ,
                    success: function (data) {
						$('.res_res').html(data);
                        $(".response_modal").modal("toggle");					
                        $('.select2-selection__rendered').html('');
                        $("#my_cat").val('');
                        $("#sub_cats").val('');
                        ajax_get_datatable();
                    },
               });        
            }); 
            ajax_get_datatable();
        });	
         
    </script>
    <script>       
        function ajax_get_datatable(){        
            $.ajax({
                url : "$adminurl"+"Training/ajax_certificates_mapping_list", 
                type: "post", 
                data: {},
                success: function (data) { //alert(data);
                    $('.put_table').html(data);
                    //$('.hidden_section').show();
                    var table = $('#example').DataTable({
                        responsive: true,
                        "columnDefs":[{
                                "targets":[3],
                                "orderable":false
                        }],
                    });
                    new $.fn.dataTable.FixedHeader(table);
                }
            }); 
        }
    </script>
    <script>
            $(".all_empty").change(function() {
                    $(this).siblings('.error').html('');	
            });
            $(".all_empty").keyup(function() {
                    $(this).siblings('.error').html('');	
            });
    </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>