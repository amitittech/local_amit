<?php 
//pre($result); die;
if(!isset($result) OR empty($result)){ ?>
    <section class="panel">
        <header class="panel-heading">
            Mapping records
        </header>
        <div class="panel-body">
            No Record found
        </div>
    </section>
<?php die; } ?>

<section class="panel">    
    <header class="panel-heading">
        Mapping List
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Training Agency</th>
                            <th>Certificate Template</th>
                            <th width="100">
                                <button type="button" name="delete_btn" class="btn-xs bold btn btn-danger delete_mapping"  >
                                    DELETE
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=0; foreach($result as $row){ $i++; ?>
                        <tr class="gradeA odd">
                            <td class=""><?php echo $i; ?></td>
                            <td class=""><?php echo $row['traname']; ?></td>
                            <td class=""><?php echo $row['ctmtemplatename']; ?></td>
                            <td class=""><?php echo '<input value="' . $row['mapping_id'] . '" type="checkbox" name="mapping_ids[]" class="mapping_ids">'; ?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    $(".delete_mapping").click(function(){
        var checkedNum = $('input[name="mapping_ids[]"]:checked').length;
        if (!checkedNum) {
            alert('Please first select any mappning to be deleted.');
            return false;
        }
        if(confirm('Are you sure to delete?') == false){
           return false;
        }
        var mapping_ids = [];
            $('input[name="mapping_ids[]"]:checked').each(function(){      
            mapping_ids.push($(this).val());
        });

       $.ajax({
            url : "ajax_delete_certificate_mapping",
            type: "post",  
            data: { mapping_ids:  mapping_ids},
            success: function (data) {
                $(".delete_modal").modal("toggle");
                ajax_get_datatable();
            },
       });        
    }); 
</script>