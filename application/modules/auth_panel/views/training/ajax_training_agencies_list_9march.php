<?php 
 
    if(!isset($result['status']) OR empty($result['status'])){ ?>
<section class="panel">
    <header class="panel-heading">
        Training Agencies List
    </header>
    <div class="panel-body">
        <?php echo $result['message']; ?>
    </div>
</section>
   <?php die; } else{
    $result = $result['result'];
   }
?>

<section class="panel">
    
        <header class="panel-heading">
            Training Agencies
            <!-- <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span> -->
            <button style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary" data-toggle="modal" data-target="#modalForm">
        Add Training Agency
        </button>
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                    <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Training Agency</th>
                                <th>Logo</th>
                                <th>Splash</th>
                                <!-- <th>Update</th> -->
                             </tr>
                        </thead>
                        
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php $i=0; foreach($result as $row){ $i++; ?>
                            <tr class="gradeA odd">
                                <td class=""><?php echo $i; ?></td>
                                <td class=""><?php echo $row['traname']; ?></td>
                                <td>
                                    <?php  if(!empty($row['tralogo'])) {?>
                                    <img class="logo_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/logo/'.$row['tralogo']);?>" />
                                    <?php }else{  ?>
                                    <img class="logo_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/default-logo.png');?>" />
                                    <?php } ?>
                                 </td>
                                 <td>
                                    <?php  if(!empty($row['trasplash'])) {?>
                                    <img class="splash_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/splash/'.$row['trasplash']);?>" />
                                    <?php }else{  ?>
                                        <img class="logo_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/default-logo.png');?>" />
                                    <?php } ?>
                                 </td>
                               <!--  <td>
                                    <button class="btn btn-primary edit_btn" data-toggle="modal" data-target="#myModal" tralogo="<?php echo base_url('resources/tra/logo/'.$row['tralogo']);?>" trasplash="<?php echo base_url('resources/tra/splash/'.$row['trasplash']);?>" trarefid="<?php echo $row['trarefid']; ?>" traname="<?php echo $row['traname'];?>">Edit</button>
                                 </td> -->
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </section>
