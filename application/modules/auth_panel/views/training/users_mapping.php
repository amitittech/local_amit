<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >

<?php //pre($result);die;  ?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Users Mapping for trainings
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">
                <div class="form-group" style="position:relative;">
                    <label for="exampleInputEmail1">Trainging Agencies<span style="color: red">*</span></label>
                    <?php
                    if (set_value('trarefids') != '') {
                        $category1 = set_value('trarefids');
                    } else {
                        //$category1 = '';
                        //if(!empty($result['trarefid'])){$category1 = $result['trarefid'];}else{ //$category1='';}
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple all_empty" name="trarefids[]" id="my_cat" required="" multiple="multiple">
                        <?php foreach ($training_agencies as $each) {
                            ?>
                            <option value="<?php echo $each['trarefid']; ?>"><?php echo $each['traname']; ?></option>
                        <?php } ?>
                    </select>
					<a href="javascript:;" class="fa fa-chevron-down" style="position: absolute;top: 32px;right: 10px;"></a>
                    <span class="text-danger error"><?php echo form_error('trarefid'); ?></span>
                </div>
	
				 <div class="form-group" >
                        <label for="sel1">Users Mobiles <span style="color: red">*</span><small> (Multiple numbers can be enetered by comma(,) seperated )</small></label>
						
                        <input type="text" oninput="this.value=this.value.replace(/[^\+\d{10,15}(,\+\d{10,15})*$]/g,'');" class="form-control all_empty" value="<?php
						if (set_value('user_mobiles') != '') {
							echo set_value('user_mobiles');
						}
                        ?>" id="users_mobile" name="users_mobile" placeholder="Enter Users Mobile Number ..." required="">
						<span class="text-danger error"><?php echo form_error('users_mobile'); ?></span>
				</div> 
				
                <div class="form-group">
                    <label for="exampleInputEmail1">Users role<span style="color: red">*</span></label>
                    <?php
                    if (set_value('users_role') != '') {
                        $role = set_value('users_role');
                    }
                    ?>
                    <select class="form-control js-example-basic-multiple all_empty" name="users_role" id="users_role" required="">
                        <option value=""> Select Users Role </option>
						<option value="CL-L1 User"> Mentor</option>
						<option value="CL-Training Lead"> CL-Training Lead </option>
						<option value="CL-Training Manager"> CL-Training Manager</option>
						<option value="CL-Relationship Manager"> CL-Relationship Manager</option>
						
                        <?php /*
						$roles = [["users_role"=>"CL-L1 User"],["users_role"=>"CL-Training Lead"],["users_role"=>"CL-Training Manager"]];
                        if (!empty($roles)) {
                            foreach ($roles as $each) {
                                ?>
                                <option value="<?php echo $each['users_role']; ?>"> <?php echo $each['users_role']; ?></option>
						<?php } } */ ?>
                    </select>
					<span class="text-danger error"><?php echo form_error('users_role'); ?></span>
                </div>   

                <div  class="form-group">
                    <button name="mapping_btn" value="123" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>

<div class="col-sm-12 put_table hidden_section"></div>


<div id="myModal" class="modal fade response_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Mapping has been done as following </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered res_res"></table>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Mapping has been deleted successfully..</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {
            $(".create_mapping").click(function(){ 
			var any_error = "";
                $(".error").html('');
                var trainings = $("#my_cat").val();                
                var mobiles = $("#users_mobile").val();
                var role = $("#users_role").val();
				
                var regrExpr = /^\d{15}(?:,\d{15})*^/;
                if (!regrExpr.test($("#users_mobile").val())) {
                    //$("#users_mobile").focus();
                    //$("#users_mobile").siblings('.error').html('Please enter comma separated values.');
                    //return false;
                }
				if (!role) {
                    $("#users_role").focus();
                    $("#users_role").siblings('.error').html('Please select a role to be assign.');	
					any_error = 1;
                    //return false;
                }
				if (!mobiles) {
                    $("#users_mobile").focus();
                    $("#users_mobile").siblings('.error').html('Please enter any user mobile numbers.');
					any_error = 1;
                    //return false;
                }				
				if(trainings == null){
                    $("#my_cat").focus();
                    $("#my_cat").siblings('.error').html('Please select a training to be mapped.');	
					any_error = 1;
                    //return false;						
                }
				
				if(any_error){
					return false;
				}
                if(trainings && mobiles && role ){ 
                    if(confirm('Are you sure to map these records?') == false){
                        return false;
                    }	
                    $.ajax({
                        url : "$adminurl"+"Training/ajax_users_mapping",
                        type: "post",  
                        data: { trainings:trainings, users_mobiles:mobiles, user_role:role},
                        success: function (data) {
                            $('.res_res').html(data);
                            $(".response_modal").modal("toggle");

                            $('.select2-selection__rendered').html('');
                            $(".all_empty").val('');
                            ajax_get_datatable();
                        },
                    });
                }
            });             
            ajax_get_datatable();
       } );				   
    </script>
    <script>  
        function ajax_get_datatable(){        
            $.ajax({
                url : "$adminurl"+"Training/ajax_users_mapping_list", 
                type: "post", 
                data: {},
                success: function (data) { //alert(data);
                    $('.put_table').html(data);
                    //$('.hidden_section').show();
                    var table = $('#example').DataTable({
                        responsive: true,
                        //"pageLength": 25,
                        //"lengthMenu": [[25, 50, 100], [25, 50, 100]],
                        "columnDefs":[{
                                "targets":[6],
                                "orderable":false
                        }],
                    });
                    new $.fn.dataTable.FixedHeader(table);
                }
            }); 
        }
    </script>
    <script>
            $(".all_empty").change(function() {
                    $(this).siblings('.error').html('');	
            });
            $(".all_empty").keyup(function() {
                    $(this).siblings('.error').html('');	
            });
    </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>