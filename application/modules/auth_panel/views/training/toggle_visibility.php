<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            List of Trainings
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <form name="form2" action="<?php // echo AUTH_PANEL_URL. 'Training/delete_selected_courses_traing_mapping';   ?>" method="post">
                    <table  class="display table table-bordered table-striped" id="all-video-grid">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Training</th>
                                <th>Visibility</th>
                            </tr>
                        </thead>
                        <thead>
                            <!--
                            <tr>
                                <th align="center"></th>
                            </tr>-->
                        </thead>
                    </table>
                </form>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
        
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {
           var table = 'all-video-grid';
           var dataTable = jQuery("#"+table).DataTable( {
                "processing": true,
                "pageLength": 25,
                "lengthMenu": [[25, 50, 100], [25, 50, 100]],
                "serverSide": true,
				"columnDefs":[{
					"targets":[2],
					"orderable":false
				}],
                "order": [[ 0, "desc" ]],
                "ajax":{
                    url :"$adminurl"+"Training/ajax_visibility_list/", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                       jQuery("."+table+"-error").html("");
                       jQuery("#"+table+"_processing").css("display","none");
                    }
               }
           } );
           jQuery("#"+table+"_filter").css("display","none");
           $('.search-input-text').on( 'keyup click', function () {   // for text boxes
               var i =$(this).attr('data-column');  // getting column index
               var v =$(this).val();  // getting search input value
               dataTable.columns(i).search(v).draw();
           } );
            $('.search-input-select').on( 'change', function () {   // for select box
                var i =$(this).attr('data-column');
                var v =$(this).val();
                dataTable.columns(i).search(v).draw();
            } );
            // Re-draw the table when the a date range filter changes
            $('.date-range-filter').change(function() {
                if($('#min-date-video-list').val() !="" && $('#max-date-video-list').val() != "" ){
                    var dates = $('#min-date-video-list').val()+','+$('#max-date-video-list').val();
                    dataTable.columns(8).search(dates).draw();
                } 
                if($('#min-date-video-list').val() =="" || $('#max-date-video-list').val() == "" ){
                    var dates = "";
                    dataTable.columns(8).search(dates).draw();
                }  
            }); 
        
            // change visibility
			//$( ".change_button" ).on( "click", function() {
				//alert('yes');
			//});
			
            
			
			
       } );				   
    </script>
    <script>
        $(".change_button").click(function(){
            alert('yes');
            //abc();
        }); 
        function abc(e,id,visibility){
            //alert(id);alert(visibility);
            if(confirm('Are you sure to change its visibility?') == false){
                return false;
            }
            $.ajax({
                url : "$adminurl"+"Training/update_visibility",
                type: "post",  
                data: { trnrefid:id,isvisibleonclient:visibility},
                success: function (data) {	
                    //alert(data);
                    //alert("Visibility has been changed successfully.");
                    $(e).parent().effect( "shake", {times:5}, 1000 );
                    $(e).parent().html(data);
                    //location.reload(true);
                }
           });			   
        }
    </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>