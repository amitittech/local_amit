<link href="<?php echo AUTH_ASSETS; ?>css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>

<div class="bs-example">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span>Agency Courses</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                   <!--  <section class="content"> -->
                          <div class="row">
                            <div class="col-xs-12">
                              <div class="box">
                                <div class="box-body" >
                                  <table id="course_table" class="table table-bordered table-hover" >
                                      <thead>
                                      <tr>
                                        <th style="text-align: left;">Chapter</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      </tbody>
                                      <?php if($result['result']['0']['tra_course']){   ?>
                                      <?php foreach($result['result']['0']['tra_course'] as $row){ ?>
                                        <tr>
                                          
                                           <td style="text-align: left;" class=""><?php echo $row['chaptername']; ?></td>
                                        </tr>
                                      <?php } ?>
                                       <?php }else{ ?>
                                       <tr>
                                          
                                           <td style="text-align: center;" class="">No data available</td>
                                        </tr>
                                      <?php } ?>
                                     </tfoot>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                    <!-- </section> -->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span> Agency Certificate</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                    <!-- <section class="content"> -->
                          <div class="row">
                            <div class="col-xs-12">
                              <div class="box">
                                <div class="box-header">
                                </div>
                                <div class="box-body" >
                                  <table id="certificate_table" class="table table-bordered table-hover" >
                                    <thead>
                                    <tr>
                                      
                                      <th style="text-align: left;">Template</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php if($result['result']['0']['tra_certificate']){  ?>
                                    <?php foreach($result['result']['0']['tra_certificate'] as $row){ ?>
                                      <tr><td style="text-align: left;" class="">
									 
									  <a  id="commingsoon" onClick="showMsg()" href="#"><?php echo $row['template_name']; ?></a>
									  </td></tr>
                                    <?php } ?>
                                    <?php }else{ ?>
                                       <tr>
                                          
                                           <td style="text-align: center;" class="">No data available</td>
                                        </tr>
                                      <?php } ?>
                                   </tfoot>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                    <!-- </section> -->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> Agency Sender Ids</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse in">
                <div class="panel-body">
                        <!-- <section class="content"> -->
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="box">
                                    <div class="box-header">
                                    </div>
                                    <div class="box-body" >
                                      <table id="sender_table" class="table table-bordered table-hover" >
                                        <thead>
                                        <tr>
                                         <th style="text-align: left;">Sender Code</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                          <?php if($result['result']['0']['tra_sender']){  ?>
                                         <?php foreach($result['result']['0']['tra_sender'] as $row){ ?>
                                            <tr>
                                              <td style="text-align: left;" class=""><?php echo $row['sendercode']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php }else{ ?>
                                       <tr>
                                          
                                           <td style="text-align: center;" class="">No data available</td>
                                        </tr>
                                      <?php } ?>
                                       </tfoot>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                       <!--  </section> -->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-plus"></span> Agency Users</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body">
                      <!-- <section class="content"> -->
                            <div class="row">
                              <div class="col-xs-12">
                                <div class="box">
                                   </div>
                                  <div class="box-body">
                                    <table id="user_table" class="table table-bordered table-hover" >
                                      <thead>
                                      <tr>
                                        <th style="text-align: left;">Name</th>
                                        <th style="text-align: left;">Mobile</th>
                                        <th style="text-align: left;">Role</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                        <?php if($result['result']['0']['tra_user']){  ?>
                                       <?php foreach($result['result']['0']['tra_user'] as $row){  ?>
                                            <tr>
                                             
                                               <td style="text-align: left;" class=""><?php echo $row['name']; ?></td>
                                               <td style="text-align: left;" class=""><?php echo $row['usmmob']; ?></td>
                                               <td style="text-align: left;" class=""><?php echo $row['rlmrole']; ?></td>
                                            </tr>
                                      <?php } ?>
                                      <?php }else{ ?>
                                       <tr>
                                          
                                           <td colspan="3" style="text-align: center;" class="">No data available</td>
                                        </tr>
                                      <?php } ?>
                                     </tfoot>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                      <!-- </section> -->
                </div>
            </div>
        </div>
    </div>
    <p><strong>Note:</strong> Click on the linked heading text to expand or collapse accordion panels.</p>
</div>
<!--- comming soon modal --->
<div class="modal fade " id="commingsoon" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button " class="close close_comming_sson" >
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Template</h4>
            </div>
            <div class="modal-body">
                <p>Comming Soon..</p>
            </div>
         </div>
    </div>
</div>
<!--- save sucess modal ends  --->
<script type="text/javascript">

   function showMsg()
{
   alert('Certificate Comming Soon.....');
}

  </script>
                                                                                              

