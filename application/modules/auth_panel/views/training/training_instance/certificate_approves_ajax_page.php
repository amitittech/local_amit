<?php 
//pre($result); die;
if(!isset($result) OR empty($result)){ ?>
    <section class="panel">
        <header class="panel-heading">
            Mapping
        </header>
        <div class="panel-body">
            No Record found
        </div>
    </section>
<?php die; }else if($result && $result['status']==false){ ?>
    <section class="panel">
        <header class="panel-heading">
            Mapping
        </header>
        <div class="panel-body">
            <?php echo $result['status']; ?>
        </div>
    </section>
<?php } else{
    $result = $result['result'];
}
?>

<section class="panel">    
    <header class="panel-heading">
        Mapping List
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid" style="overflow-x:auto;">
                <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                    <thead>
                        <tr>
                            <th> S. No. &nbsp;&nbsp;&nbsp; </th>
                            <th>Training Agency</th>
                            <th>Training Instance</th>
                            <th>Batch Name</th>
                            <th>User Name</th>
                            <th>User Mobile</th>
                            <th>Assigned date</th>
                            <th width="100"><button type="button" name="delete_btn" class="btn-xs bold btn btn-danger delete_mapping"  >DELETE</button>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=0; foreach($result as $row){ $i++; ?>
                        <tr class="gradeA odd">
                            <td class="" style="width:100px;"><?php echo $i; ?></td>
                            <td class=""><?php echo $row['traname']; ?></td>
                            <td class=""><?php echo $row['trnname']; ?></td>
                            <td class=""><?php echo $row['batchname']; ?></td>
                            <td class=""><?php echo $row['usmfirstname'].(!empty($row['usmmiddlename'])?' '.$row['usmmiddlename']:"").(!empty($row['usmlastname'])?' '.$row['usmlastname']:""); ?></td>
                            <td class=""><?php echo $row['usmmob']; ?></td>
                            <td class=""><?php echo $row['createddate']; ?></td>
                            <td class=""><?php echo '<input value="' . $row['mapping_id'] . '" type="checkbox" name="mapping_ids[]" class="mapping_ids">'; ?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    $(".delete_mapping").click(function(){
        var checkedNum = $('input[name="mapping_ids[]"]:checked').length;
        if (!checkedNum) {
            alert('Please first select any mappning to be deleted.');
            return false;
        }
        if(confirm('Are you sure to delete this entry?') == false){
           return false;
        }
       
		
		var mapping_ids = [];
			app_table.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(i,v){
            mapping_ids.push($(v).val());
        });
        //alert(mapping_ids);
        $.ajax({
            url : "ajax_delete_certificate_approve",
            type: "post",  
            data: { mapping_ids:  mapping_ids},
                success: function (data) { //alert(data);
                $(".delete_modal").modal("toggle");
                ajax_get_datatable();
            }
       });

    }); 
</script>