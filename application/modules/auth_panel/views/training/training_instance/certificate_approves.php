<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Certificate Approver
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
	
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">

                <div class="form-group" >
                    <label for="sel1">Users Mobiles <span style="color: red">*</span><small> (Multiple numbers can be enetered by comma(,) seperated )</small></label>

                    <input type="text"  class="form-control all_empty" value="" id="users_mobile" name="users_mobile" placeholder="Enter Users Mobile Number ..." required="" onkeypress="return validate_phones(event)">
                    <span class="text-danger error"><?php echo form_error('users_mobile'); ?></span>
                </div> 
                <div class="form-group">
                    <label for="exampleInputEmail1">Trainging Agencies<span style="color: red">*</span></label>
                    <select class="form-control all_empty" name="trarefid" id="trarefid" required="">
                        <option value="">Select Training</option>
                        <?php
                        foreach ($training_agencies as $each) {
                            echo '<option value="' . $each['trarefid'] . '">' . $each['traname'] . '</option>';
                        }
                        ?>
                    </select>
                    <span class="text-danger error"><?php echo form_error('trarefid'); ?></span>
                </div>
                <div class="form-group training_instance_div">
                    <label for="exampleInputEmail1">Trainging Instances<span style="color: red">*</span></label>
                    <select class="form-control all_empty training_instance_options" name="trnrefid" id="trnrefid" required="">
                        <option value="">Select Training Instance</option>
                    </select>
                    <span class="text-danger error"><?php echo form_error('users_role'); ?></span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Users role<span style="color: red">*</span></label>
                    <?php
                    if (set_value('users_role') != '') {
                        $role = set_value('users_role');
                    }
                    ?>
                    <select class="form-control" name="users_role" required="" readonly>
                        <option value="10"> CL-Approver </option>
                    </select>
                    <span class="text-danger error"><?php echo form_error('users_role'); ?></span>
                </div>   

                <div  class="form-group">
                    <button name="mapping_btn" value="123" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>

<div class="col-sm-12 put_table hidden_section"></div>


<div id="myModal" class="modal fade response_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Mapping has been done as following </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered res_res"></table>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Mapping has been deleted successfully..</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {
            $(".create_mapping").click(function(){ 
                var any_error = "";
                $(".error").html('');                              
                var mobiles     = $("#users_mobile").val();
                var trarefid    = $("#trarefid").val();  
                var trnrefid    = $("#trnrefid").val();  
                var role        = $("#users_role").val();
				
                var regrExpr = /^\d{10,10}(,\d{10,10})*$/;
                if (!regrExpr.test($("#users_mobile").val())) {
                    $("#users_mobile").focus();
                    $("#users_mobile").siblings('.error').html('Please enter 10 digits phone numbers.');
                    return false;
                }
                if (!mobiles) {
                    $("#users_mobile").focus();
                    $("#users_mobile").siblings('.error').html('Please enter user mobile numbers.');
                    any_error = 1;;
                }
				if(!trarefid ){ 
                    $("#trarefid").focus();
                    $("#trarefid").siblings('.error').html('Please select a training.');	
                    any_error = 1;						
                }
                if(!trnrefid){
                    $("#trnrefid").focus();
                    $("#trnrefid").siblings('.error').html('Please select a training instance.');	
                    any_error = 1;						
                }
				
                if(any_error){
                    return false;
                }
                if(trarefid && mobiles && trnrefid ){ 
                    if(confirm('Are you sure to map these records?') == false){
                        return false;
                    }	
                    $.ajax({
                        url : "$adminurl"+"Training_instance/ajax_certificate_approves",
                        type: "post",  
                        data: { trarefid:trarefid,trnrefid:trnrefid, user_mobiles:mobiles, rlmrefid:role},
                        success: function (data) { //alert('success');
							$(".all_empty").val('');
							$(".training_instance_options").html('<option value="">Select Training Instance</option>');
                            $('.res_res').html(data);
                            $(".response_modal").modal("toggle");
                            $('.select2-selection__rendered').html('');                      														
                            ajax_get_datatable();
                        },
                    });
                }
            });             
            ajax_get_datatable();
       } );				   
    </script>
	<script>
        $(".all_empty").change(function() {
            $(this).siblings('.error').html('');	
        });
        $(".all_empty").keyup(function() {
            $(this).siblings('.error').html('');	
        });
    </script>
    <script>  
	var app_table = "";
        function ajax_get_datatable(){  
            $.ajax({
				url : "$adminurl"+"Training_instance/ajax_certificate_approves_list", 
                type: "post", 
                data: {},
                success: function (data) { //alert(data);
                    $('.put_table').html(data);
						app_table = $('#example').DataTable({
                        responsive: true,
                        //"scrollX": true,
						//.dataTables_scrollBody thead {
							//visibility: hidden;
						//},
						//drawCallback: function () { // this gets rid of duplicate headers
						  //$('.dataTables_scrollBody thead tr').css({ display: 'none' }); 
					  //},
                        //"pageLength": 10,
                        //"lengthMenu": [[25, 50, 100], [25, 50, 100]],
                        "columnDefs":[{
                            "targets":[7],
                            "orderable":false
                        }],
                    });
                    new $.fn.dataTable.FixedHeader(app_table);
                }
            }); 
        }
    </script>
    
    <script>
        //jQuery(document).ready(function() { 
        $("#trarefid").change(function(){ //alert('working');
            //$('.hidden_section').hide();
            //$('.error').html('');
            //$('.required_inputs_div').html('');
            var trarefid      = $('#trarefid').val(); //alert(name);
            if(trarefid == ""){
                $('.training_instance_options').html('<option value="">Select Training Instance</option>');
                exit;
            }
            $.ajax({
                url : "$adminurl"+"Training_instance/ajax_get_training_instance",
                type: "post",  
                data: { trntrarefid:  trarefid }  ,
                success: function (data) { //alert(data);        
                   $('.training_instance_options').html(data);
                }
            });        
        });
    </script>
	<script>
	function validate_phones(event) {
		var key = window.event ? event.keyCode : event.which; //alert(key);
		if (key == 8 || key == 39 || key == 44) {
			return true;
		}else if (key < 48 || key > 57  ) {
			return false;
		}
		else return true;
	};
	</script>
	 
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>