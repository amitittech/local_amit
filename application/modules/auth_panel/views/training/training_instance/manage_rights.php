<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading"> Manage View Rights
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="exampleInputEmail1">Training Managers<span style="color: red">*</span></label>
                    <select class="form-control all_empty" name="usmrefid" id="usmrefid" required="">
                        <option value="">Select Manager</option>
                        <?php
                        foreach ($managers as $each) {
                            echo '<option value="' . $each['usmrefid'] . '">' . $each['usmfirstname'].' '.$each['usmlastname'].' ('.$each['usm_mob'].')</option>';
                        }
                        ?>
                    </select>
                    <span class="text-danger error"><?php echo form_error('trarefid'); ?></span>
                </div>
				<div class="form-group">
                    <label for="exampleInputEmail1">Trainging Instances<span style="color: red">*</span></label>
                    <select class="form-control all_empty training_instance_options" name="trnrefid" id="trnrefid" required="">
                        <option value="">Select Training Instance</option>
                        
                    </select>
                    <span class="text-danger error"><?php echo form_error('trarefid'); ?></span>
                </div>
                <div  class="form-group">
                    <button name="mapping_btn" value="123" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>

<div class="col-sm-12 put_table hidden_section"></div>


<div id="myModal" class="modal fade response_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Manage Rights</h4>
            </div>
            <div class="modal-body">
                <p class="res_res">Rights has been assigned successfully.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">View Rights Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p >Mapping has been removed successfully.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
       jQuery(document).ready(function() {
            $(".create_mapping").click(function(){ 
                var any_error = "";
                $(".error").html('');
                var usmrefid    = $("#usmrefid").val();  
                var trnrefid    = $("#trnrefid").val();
				if(!usmrefid ){ 
                    $("#usmrefid").focus();
                    $("#usmrefid").siblings('.error').html('Please select a training Manager.');	
                    any_error = 1;						
                }
                if(!trnrefid){
                    $("#trnrefid").focus();
                    $("#trnrefid").siblings('.error').html('Please select a training instance.');	
                    any_error = 1;						
                }				
                if(any_error){
                    return false;
                }
                if(usmrefid && trnrefid ){ 
                    if(confirm('Are you sure to map these records?') == false){
                        return false;
                    }	
                    $.ajax({
                        url : "$adminurl"+"Training_instance/ajax_add_rights",
                        type: "post",  
                        data: { usmrefid:usmrefid,trnrefid:trnrefid},
                        success: function (data) { //alert('data');
							$('.training_instance_options').html('<option value="">Select Training Instance</option>');
							ajax_get_datatable();
                            $('.res_res').html(data);
                            $(".response_modal").modal("toggle");

                            $('.select2-selection__rendered').html('');
                            $(".all_empty").val('');
                            ajax_get_datatable();
                        },
                    });
                }
				
            });             
            
       } );				   
    </script>
    <script>  
		var table ="";
        function ajax_get_datatable(){ 
			var usmrefid      = $('#usmrefid').val();	
			if(usmrefid){
				$.ajax({
					url : "$adminurl"+"Training_instance/ajax_rights_list", 
					type: "post", 
					data: {tmrefid : usmrefid},
					success: function (data) { //alert(data);
						$('.put_table').html(data);
						 table = $('#example').DataTable({
							responsive: true,
							//"scrollX": true,
							"pageLength": 25,
							//"lengthMenu": [[25, 50, 100], [25, 50, 100]],
							"columnDefs":[{
									"targets":[4],
									"orderable":false
							}],
						});
						new $.fn.dataTable.FixedHeader(table);
					}
				}); 
			}
            
        }
    </script>
    <script>
        $(".all_empty").change(function() {
            $(this).siblings('.error').html('');	
        });
        $(".all_empty").keyup(function() {
            $(this).siblings('.error').html('');	
        });
    </script>
    <script>
        //jQuery(document).ready(function() { 
        $("#usmrefid").change(function(){ //alert('working');
            //$('.hidden_section').hide();
            //$('.error').html('');
            //$('.required_inputs_div').html('');
            var usmrefid      = $('#usmrefid').val();
            if(usmrefid == ""){
                $('.training_instance_options').html('<option value="">Select Training Instance</option>');
                exit;
            }
            $.ajax({
                url : "$adminurl"+"Training_instance/ajax_get_training_instance_for_manage_rights",
                type: "post",  
                data: { usmrefid:  usmrefid }  ,
                success: function (data) { //alert(data);        
                   $('.training_instance_options').html(data);
                }
            }); 
			ajax_get_datatable();
        });
    </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>