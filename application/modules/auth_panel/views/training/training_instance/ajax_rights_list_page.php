<?php 
//pre($result); die;
if(!isset($result) OR empty($result)){ ?>
    <section class="panel">
        <header class="panel-heading">
            Managers Rights
        </header>
        <div class="panel-body">
            No Record found
        </div>
    </section>
<?php die; }else if(isset($result['status']) && $result['status']==false){ ?>
    <section class="panel">
        <header class="panel-heading">
            Managers Rights
        </header>
        <div class="panel-body">
            <?php echo $result['status']; ?>
        </div>
    </section>
<?php } else{
    //$result = $result['result'];
}
?>

<section class="panel">    
    <header class="panel-heading">
        Managers Rights Mapping List
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Training Manager</th>
                            <th>Training Instance</th>
                            <th>Assigned date</th>
                            <th width="100"><button type="button" name="delete_btn" class="btn-xs bold btn btn-danger delete_mapping"  >DELETE</button>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=0; foreach($result as $row){ $i++; ?>
                        <tr class="gradeA odd">
                            <td class=""><?php echo $i; ?></td>
                            <td class=""><?php echo $row['usmfirstname'].' '.$row['usmlastname'].' ('.$row['usm_mob'].')'; ?></td>
                            <td class=""><?php echo $row['trnname']; ?></td>
                            <td class=""><?php echo $row['createddate']; ?></td>
                            <td class=""><?php echo '<input value="' . $row['clttitmrefid'] . '" type="checkbox" name="mapping_ids[]" class="mapping_ids">'; ?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    $(".delete_mapping").click(function(){
        var checkedNum = $('input[name="mapping_ids[]"]:checked').length;
        if (!checkedNum) {
            alert('Please first select any mappning to be deleted.');
            return false;
        }
        if(confirm('Are you sure to delete this entry?') == false){
           return false;
                        }
        var mapping_ids = [];
            $('input[name="mapping_ids[]"]:checked').each(function(){      
            mapping_ids.push($(this).val());
        });
        $.ajax({
            url : "ajax_delete_rights",
            type: "post",  
            data: { mapping_ids:  mapping_ids},
                success: function (data) { //alert(data);
                $(".delete_modal").modal("toggle");
                ajax_get_datatable();
            }
       });

    }); 
</script>