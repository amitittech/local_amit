
<?php 
// echo "<pre>";
// print_r($result);die;
 
    if(!isset($result['status']) OR empty($result['status'])){ ?>
<section class="panel">
    <header class="panel-heading">
        Training Agencies List
    </header>
    <div class="panel-body">
        <?php echo $result['message']; ?>
    </div>
</section>
   <?php die; } else{
    $result = $result['result'];
   }
?>

<section class="panel">
    
        <header class="panel-heading">
            Training Agencies
            <!-- <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span> -->
            <button style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary" data-toggle="modal" data-target="#modalForm">
        Add Training Agency
        </button>
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                    <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Training Agency</th>
                                <th>Logo</th>
                                <th>Splash</th>
                                <th>Created Date</th>
                                <th>Agency Info</th> 
                             </tr>
                        </thead>
                        
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php $i=0; foreach($result as $row){ $i++; ?>
                            <tr class="gradeA odd">
                                <td class=""><?php echo $i; ?></td>
                                <td class=""><?php echo $row['traname']; ?></td>
                                <td>
                                    <?php  if(!empty($row['tralogo'])) {?>
                                    <img class="logo_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/logo/'.$row['tralogo']);?>" />
                                    <?php }else{  ?>
                                    <img class="logo_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/default-logo.png');?>" />
                                    <?php } ?>
                                 </td>
                                 <td>
                                    <?php  if(!empty($row['trasplash'])) {?>
                                    <img class="splash_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/splash/'.$row['trasplash']);?>" />
                                    <?php }else{  ?>
                                        <img class="logo_img" height="50px" width="50px" src="<?php echo base_url('resources/tra/default-logo.png');?>" />
                                    <?php } ?>
                                 </td>
                                 <td>
                                    <?php 
                                    $date = strtotime($row['createddate']);
                                    echo date("j F Y", $date);?> 
                                 </td>
                                 <td>
                                    <button id="agency_info" class="btn btn-primary agency_info" data-id="<?php echo $row['trarefid']; ?>" data-toggle="modal" data-target="#infoModal">View Info</button>
                                   
                                 </td> 
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
   <?php $adminurl = AUTH_PANEL_URL; ?>
  $('.agency_info').on('click', function() {
       var id =  $(this).attr("data-id");
       $.ajax({
            //contentType: "application/x-www-form-urlencoded", 
            type:'POST',
            url: "http://www.server2.chalklit.in/index.php/auth_panel/Training/agency_info",
			data: {traid:id},
            
            success: function(data){
               $('.agencyinfo').html(data);
                
            },
            beforesend: function(data){
              $(".modal-body").html("");
                
            },
        });
 });
 
  </script>
  
     
