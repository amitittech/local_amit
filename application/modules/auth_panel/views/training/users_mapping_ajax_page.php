<?php 
//pre($result); die;
if(!isset($result) OR empty($result)){ ?>
    <section class="panel">
        <header class="panel-heading">
            Mapping
        </header>
        <div class="panel-body">
            No Record found
        </div>
    </section>
<?php die; } ?>

<section class="panel">    
    <header class="panel-heading">
        Mapping List
        
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                    <thead>
                        <tr>
                            <tr>
                                <th>S.No</th>
                                <th>Training Agency</th>
                                <th>User Name</th>
                                <th>User Mobile</th>
                                <th>Role</th>
                                <th>Assigned date</th>
                                <th width="100"><button type="button" name="delete_btn" class="btn-xs bold btn btn-danger delete_mapping"  >DELETE</button>&nbsp;</th>
                            </tr>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=0; foreach($result as $row){ $i++; 
						if($row['rlmrole'] == 'CL-L1 User'){ $row['rlmrole'] = 'Mentor'; }
						?>
                        <tr class="gradeA odd">
                            <td class=""><?php echo $i; ?></td>
                            <td class=""><?php echo $row['traname']; ?></td>
                            <td class=""><?php echo $row['usmfirstname'].(!empty($row['usmmiddlename'])?' '.$row['usmmiddlename']:"").(!empty($row['usmlastname'])?' '.$row['usmlastname']:""); ?></td>
                            <td class=""><?php echo $row['usmmob']; ?></td>
                            <td class=""><?php echo $row['rlmrole']; ?></td>
                            <td class=""><?php echo $row['revokedon']; ?></td>
                            <td class=""><?php echo '<input value="' . $row['mapping_id'] . '" type="checkbox" name="mapping_ids[]" class="mapping_ids">'; ?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    $(".delete_mapping").click(function(){
        var checkedNum = $('input[name="mapping_ids[]"]:checked').length;
        if (!checkedNum) {
            alert('Please first select any mappning to be deleted.');
            return false;
        }
        if(confirm('Are you sure to delete this entry?') == false){
        $('tbody tr td input[type="checkbox"]').each(function(){
        $(this).prop('checked', false);
        });
           return false;
         }
        var mapping_ids = [];

         user_mapping_table.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(i,v){
            mapping_ids.push($(v).val());
         })
         //    $('input[name="mapping_ids[]"]:checked').each(function(){      
         //    mapping_ids.push($(this).val());
         // });
        //alert(mapping_ids);
        $.ajax({
            url : "ajax_delete_users_mapping",
            type: "post",  
            data: { mapping_ids:  mapping_ids},
                success: function (data) {
                $(".delete_modal").modal("toggle");
                ajax_get_datatable();
            }
       });

    }); 
</script>