<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sender_Id extends MX_Controller {

    function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        $this->load->library('form_validation', 'uploads');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('custom');
    }


    public function index() {
        $view_data['page'] = 'sender_id';
        $data['page_data'] = $this->load->view('sender_id/sender_ids_list', $view_data, TRUE);
        echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
    }

    

    public function ajax_master_senderid_list() {

        $data['file_url'] = site_url() . '/data_model/Api/index';
        $data['opcode'] = 'sender_id_list';
        $data['login_id'] = $this->session->userdata('active_backend_user_id');
        $response = file_curl_contents($data);
        $this->load->view('sender_id/ajax_sender_list', array("result" => $response));
    }

    public function update_sender_id() {

        $input = $this->input->post();
        $file_url = site_url() . '/data_model/Api/index';
        $insert_data['opcode'] = 'edit_sender';
        $insert_data['login_id'] = $this->session->userdata('active_backend_user_id');
        $insert_data['sendercode'] = $input['sendercode'];
        $insert_data['senderrefid'] = $input['senderrefid'];
        $response = call_api("POST", $file_url, json_encode($insert_data));
        $return = json_decode($response);
        echo $return->status;
        die;
    }

    public function save_sender_id() {

        $input = $this->input->post();
        $file_url = site_url() . '/data_model/Api/index';
        $insert_data['opcode'] = 'add_sender';
        $insert_data['login_id'] = $this->session->userdata('active_backend_user_id');
        $insert_data['sendercode'] = $input['sendercode'];
        $response = call_api("POST", $file_url, json_encode($insert_data));
        $return = json_decode($response);
        echo $return->status;
        die;
    }

    public function ajax_delete_sender_id() {
        $input = $this->input->post();
        //pre($input); die;
        $file_url = site_url() . '/data_model/Api/index';
        $data['opcode'] = 'delete_sender';
        $data['login_id'] = $this->session->userdata('active_backend_user_id');
        $data['senderrefid'] = $input['senderrefids'];

        $response = call_api("POST", $file_url, json_encode($data));
        $response = json_decode($response);
        echo $response->status;
        die;
    }

}
