<?php

if(!isset($result['status']) OR empty($result['status'])){ ?>
    <section class="panel">
        <header class="panel-heading">
            Sender Ids Result
        </header>
        <div class="panel-body">
            <?php echo $result['message']; ?>
        </div>
    </section>
    <?php die; } else{
    $result = $result['result'];
}
?>

<section class="panel">

    <header class="panel-heading">
        Sender IDs List
        <!-- <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span> -->
        <button style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary" data-toggle="modal" data-target="#modalForm">
            Add Sender ID
        </button>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                    <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Sender Id</th>
                        <th>Action</th>

                       <!--  <th width="100" class="hello"><input name="delete" type="submit" id="delete" value="Delete" onClick="return show_confirm22();" style="background:#F52126; color:#fff; border:none; font-size:12px; padding:4px 8px;">&nbsp;</th> -->

                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $i=0; foreach($result as $row){ $i++; ?>
                        <tr class="gradeA odd">
                            <td class=""><?php echo $i; ?></td>
                            <td class=""><?php echo $row['sendercode']; ?></td>
                            <td>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" senderrefid="<?php echo $row['senderrefid']; ?>" sendercode="<?php echo $row['sendercode'];?>">Edit</button>
                            </td>
                            <!-- <?php if($row['ismapped']=='') { ?>
                                <td  class="">

                                    <?php echo '<input value="' . $row['senderrefid'] . '" type="checkbox" name="senderrefid[]" class="senderrefid">'; ?>
                                </td>
                            <?php }else{ ?>
                            <td class="">
                                <input type="checkbox" value="mapped" class="checkbox1" id="checkbox1" />

                                <?php } ?>
                            </td> -->



                        </tr>
                    <?php }?>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</section>
