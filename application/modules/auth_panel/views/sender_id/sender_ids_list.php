<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >
<style type="text/css">
    th ,td {text-align:center}
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .modal {    overflow-y: auto;}.modal-open {    overflow: auto;}.modal-open[style] {    padding-right: 0px !important;}*/
</style>
<!-- Modal -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Sender ID</h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" method= "POST" class="formdata" id="formdata" autocomplete="off">
                    <div class="form-group">
                        <label for="sendercode">Sender Id <span style="color: red">*</span><small style="color: red;">(Max 6 character are allowed.)</small></label>
                        <input type="text" autofocus="autofocus"  name="sendercode" maxlength="6" class="form-control all_empty" id="sendercode" placeholder="Enter sender code" required />
                        <p class="eMsg"></p>
                    </div>

                </form>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button name="mapping_btn" id="add_sender" value="submit" type="button" class="btn btn-info create_mapping">Submit</button>
            </div>
        </div>
    </div>
</div>
<!--- delete message modal  --->
<div id="cant_delete" class="modal fade cant_delete"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Sorry!</h4>
            </div>
            <div class="modal-body">
                <p>Sender ID is mapped ,you can't delete it</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--- delete message modal ends  --->
<!--- delete message modal  --->
<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Sender Id has been deleted successfully..</p>
            </div>

        </div>
    </div>
</div>
<!--- delete message modal ends  --->
<!--- save sucess message modal  --->
<div class="modal fade save_modal" id="save_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Save Sender ID</h4>
            </div>
            <div class="modal-body">
                <p>Sender Id has been saved successfully..</p>
            </div>
        </div>
    </div>
</div>
<!--- save sucess modal ends  --->
<!--- update sucess message modal  --->
<div class="modal fade update_sucess_modal" id="update_sucess_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Update Sender ID</h4>
            </div>
            <div class="modal-body">
                <p>Sender Id has been Update successfully..</p>
            </div>
        </div>
    </div>
</div>
<!--- save sucess modal ends  --->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabelupdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelupdate">Update Sender Id</h4>
            </div>

            <div class="modal-body">
                <p class="updatestatusMsg"></p>
                <form id="profileForm">
                    <input type="hidden"  name="senderrefid" id="senderrefidupdate" value="">
                    Sender Id <span style="color: red">*</span><small style="color: red;">(Max 6 character are allowed.)</small><input autofocus="autofocus" class="form-control" maxlength="6" name="sendercode" id="sendercodeupdate" value="" placeholder="sender code">
                    <p class="eeMsg"></p>
                </form>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary" id="edit_sender">Update</button>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 put_table">

</div>

<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>

<?php
$adminurl = AUTH_PANEL_URL;

$custum_js = <<<EOD

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >

    $('#myModal').on('show.bs.modal', function (e) {
  
       var opener=e.relatedTarget;//
       var sender_code =$(opener).attr('sendercode');
       var senderre_fid=$(opener).attr('senderrefid');
       $('#profileForm').find('[name="sendercode"]').val(sender_code);
       $('#profileForm').find('[name="senderrefid"]').val(senderre_fid);
    });

    //edit sender id
 $("#edit_sender").click(function(){ 

    var sender_code = $('#sendercodeupdate').val();
    var senderre_fid = $('#senderrefidupdate').val();

    if(sender_code.trim() == '' ){
       $('.eeMsg').html('<span style="color:red;">This field is required.</span>');
        $('#sendercodeupdate').focus();
        return false;
    }else{
        $.ajax({
            type:'POST',
            url : "$adminurl"+"Sender_id/update_sender_id",
            data: { sendercode:  sender_code ,senderrefid:senderre_fid },
            success:function(msg){
                if(msg == 'success'){
                    $(".update_sucess_modal").modal("toggle");
                        $("#myModal").modal("toggle");
                        ajax_get_datatable();
                }else if(msg == 'fail'){
                      $('.updatestatusMsg').html('<span style="color:red;">Sender Id already exists.</span>');

                }
                else{
                    $('.updatestatusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                }
            },
            
        });
    }
});

// function show_confirm22(){

//    var checkedNum = $('input[name="senderrefid[]"]:checked').length;
//     if (!checkedNum) {
//     alert('Please first select any Sender Id to be deleted.');
//     return false;
//     }
//     if(confirm('Are you sure to delete?') == false){
//     return false;
//     }   

//     var senderrefids = [];
//     $('input[name="senderrefid[]"]:checked').each(function(){      
//     senderrefids.push($(this).val());
//     });  
//     $.ajax({
//                 url : "$adminurl"+"Sender_id/ajax_delete_sender_id",
//                 type: "post", 
//                 data: { senderrefids:  senderrefids},
//                 success: function (data) {
//                     ajax_get_datatable();
//                     $(".delete_modal").modal("toggle");
//                     

//                 },

//             });    
//  }
    jQuery(document).ready(function() {

        ajax_get_datatable();

     });

function ajax_get_datatable() {
        $.ajax({
            url: "$adminurl" + "Sender_id/ajax_master_sender_id_list",
            type: "post",
            data: {},
            success: function(data) {
                $('.put_table').html(data);
                var table = $('#example').DataTable({
                    responsive: true,
                    "columnDefs":[{                                
                        "targets":[2],
                         "orderable":false                        
                    }]
                });
                
            },
            
        });
}

//add sender
 $("#add_sender").click(function(){ 
    var sendercode = $('#sendercode').val();
   
    if(sendercode.trim() == '' ){
       $('.eMsg').html('<span style="color:red;">This field is required.</span>');
        $('#sendercode').focus();
        return false;

    }else{
        $.ajax({
            type:'POST',
            url : "$adminurl"+"Sender_id/save_sender_id",
            data: { sendercode:  sendercode},
            success:function(msg){

                if(msg == 'success'){
                    $('#sendercode').val('');
                        $(".save_modal").modal("toggle");
                        $("#modalForm").modal("toggle");

                        ajax_get_datatable();
                }else if(msg == 'fail'){
                      $('.statusMsg').html('<span style="color:red;">Sender Id already exists.</span>');

                }else{
                    $('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                }
            },
          
        });
    }
});

</script>    
<script>
    $( function() {
      $( document ).on( "change", ":checkbox", function () {
        let valuex =   $(this).val();
        if( valuex == 'mapped'){
             $(".cant_delete").modal("toggle");
             jQuery(this).attr('checked', false);
        }
      });
    }); 

    $(function () {
      $('#sendercode').keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) {
              e.preventDefault();
          } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                  e.preventDefault();
              }
          }
        this.value = this.value.toUpperCase();
        if( this.value.length >= '6' ) {
           $('.eMsg').html('<span style="color:red;">Maximum characters are 6.</span>').show().delay(3000).fadeOut();
        }
       });
    });

$("#modalForm").on("hidden.bs.modal", function(){
    $(".eMsg").html("");
    $(".statusMsg").html("");
    $(this).find("input").val('').end();
});
$('#sendercodeupdate').keyup(function(){

    this.value = this.value.toUpperCase();
    if( this.value.length >= '6' ) {
           $('.eeMsg').html('<span style="color:red;">Maximum characters are 6.</span>').show().delay(3000).fadeOut();
    }
});

</script>  
<script>
        $(".all_empty").change(function() {
                $(this).siblings('.eMsg').html('');    
        });
       
    </script>                      
   
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>






