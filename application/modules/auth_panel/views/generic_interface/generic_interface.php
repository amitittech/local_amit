<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >

<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">Generic Interface
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data" class="formdata" id="formdata">
                <fieldset>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Query<span style="color: red">*</span></label>
                        <select class="form-control" placeholder="" name="query" id="my_cat" required="">
                            <option value=""> Select Query </option>
                            <?php foreach ($queries as $query) {
                                ?>
                                <option value="<?php echo $query['name']; ?>"> <?php echo $query['name']; ?>  </option>
                            <?php } ?>
                        </select>
                        <span class="text-danger error"><?php echo form_error('query'); ?></span> 
                    </div>
                </fieldset>        
                <div class="required_inputs_div">

                </div>

                <div  class="form-group">
                    <button name="submit_query" type="button" class="btn btn-info submit_query col-md-2" > Submit Query</button>
                </div>
            </form>
            <input type="hidden" class="selected_query" value="">
        </div>
    </section>
</div>

<div class="col-sm-12 put_table hidden_section">

</div>


<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>
<script>
    $(document).ready(function () {
        var table = $('#example').DataTable({
            responsive: true
        });

        new $.fn.dataTable.FixedHeader(table);
    });
</script>
<?php
$adminurl = AUTH_PANEL_URL;
//$adminurl = base_url('index.php');
$custum_js = <<<EOD
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script type="text/javascript" language="javascript" >
        jQuery(document).ready(function() { 
			
   
            $('.hidden_section').hide();
            $("#my_cat").change(function(){
                $('.hidden_section').hide();
                $('.error').html('');
                $('.required_inputs_div').html('');
                var name      = $('#my_cat').val(); //alert(name);
                if(name == ""){
                    exit;
                }
                $.ajax({
                    url : "$adminurl"+"Generic_interface/ajax_get_input_boxes",
                    type: "post",  
                    data: { name:  name }  ,
                    success: function (data) { //alert(data);
                        $('.required_inputs_div').html(data);
                    }
               });        
            });
        
            $(".submit_query").click(function(){
                $('.hidden_section').hide();
                $('.error').html('');
				var any_erorr 	= "";
                if ($('#my_cat').val() == "") {
					any_erorr = 1;
                    $('#my_cat').siblings('.error').html('Please select a query.');
                }
                var name        = $('#my_cat').val();
                var all_inputs  = '[';
                $(".inputs").each(function(i, field) { 
                    if ($(this).val() == "") {
                        //$(this).focus();
                        $(this).siblings('.error').html('This field is required.'); 
						any_erorr = 1;
                        //return false;exit;
                    }
                    if( i == 0){
                        all_inputs = all_inputs + '{"' + field.name + '":"' + field.value + '"}';
                    }else{
                        all_inputs = all_inputs + ',{ "' + field.name + '":"' + field.value + '"}';
                    }
                });
				if(any_erorr){
					return false; exit;
				}
                all_inputs = all_inputs + ']';
				
                $.ajax({
                    url : "$adminurl"+"Generic_interface/ajax_get_query_result", 
                    type: "post", 
                    data: { name:  name, values:  all_inputs },
                        beforeSend: function (){
                            add_spin();
                        },
                        success: function (data) { //alert(data);						
                        $('.put_table').html(data);
                        $('.hidden_section').show();
                        remove_spin('submit_query','Submit Query');
                        var table = $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                        { extend:'excelHtml5', text: 'Download Excel',exportOptions: {stripHtml: false}
                        },
                         ],
                        responsive: true,
                        "ordering": false,
                        });
                        new $.fn.dataTable.FixedHeader(table);
						
                    }
                });  
            });
        
            $(".download_csv").click(function(){
                alert('working');        
                var selected_query = $('.selected_query').val();
                alert(selected_query);
                $.ajax({
                    url :"$adminurl"+"Generic_interface/ajax_download_csv", 
                    type: "post",  
                    data: { query:  selected_query }  ,
                    success: function (data) { alert(data);}
                });
            });

       } );				   
    </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>

<script>
    function add_spin() {
        $('.submit_query').html("<i class='fa fa-spinner fa-spin submit_query_spin'></i> Processing Query");
        //$('.submit_query_spin').removeClass('hidden');
    }

    function remove_spin() {
        $('.submit_query').html("Submit Query");
        //$('.submit_query').prop('value',' Submit Query');
        //$('.submit_query_spin').addClass('hidden');
    }
</script>		
