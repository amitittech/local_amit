<?php
//pre(count($result));pre($query);pre($result); die;
if (!isset($result) OR empty($result)) {
    ?>
    <section class="panel">
        <header class="panel-heading">
            Query Result
        </header>
        <div class="panel-body">
            No Record found
        </div>
    </section>
    <?php die;
}
?>

<section class="panel">    
    <header class="panel-heading">
        Query Result
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
        <!--
        <a class="btn btn-success download_records button pull-right"  href="<?php echo $url; ?>">
            Download Excel
        </a>-->
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                <table id="example" class="table table-striped table-bordered nowrap display" style="width:100%">
                    <thead>
                        <tr role="row">
                            <?php foreach ($result[0] as $key => $each_hd) { ?>
                                <th class=""><?php echo $key; ?></th>
<?php } ?>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach ($result as $row) { ?>
                            <tr class="gradeA odd">
                                <?php foreach ($row as $each_hd) { ?>
                                    <td class=""><?php echo $each_hd; ?></td>
                            <?php } ?>
                            </tr>
<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
