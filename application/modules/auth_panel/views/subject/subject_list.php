<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >
<style type="text/css">
    th ,td {text-align:center}
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .modal {    overflow-y: auto;}.modal-open {    overflow: auto;}.modal-open[style] {    padding-right: 0px !important;}*/
</style>
<!-- Modal -->
<?php  $lang=array('hi'=>'Hindi','en'=>'English','or'=>'Odia') ; ?>
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Subject</h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" method= "POST" class="formdata" id="formdata" autocomplete="off">
                    <div class="form-group">
                        <label for="subname">Subject Name <span style="color: red">*</span></label>
                        <input type="text" autofocus="autofocus"  name="subname"  class="form-control all_empty" id="subname" placeholder="Enter Subject" autocomplete="off" required />
                        <p class="eMsg"></p>
                    </div>
                    <div class="form-group">
                         <label for="lngcode">Language <span style="color: red">*</span></label>
                        <select  id="lngcode" name="lngcode"  class="form-control all_empty" autocomplete="off"  >
                            <option selected="true" disabled>Choose Laguage</option>
                            <?php foreach ($lang as $key => $value) { ?>
                                <option value="<?php echo $key;  ?>"><?php echo $value; ?></option>
                               
                             <?php } ?>
                        </select> 
                        <p class="eMsg2"></p>
                    </div>

                </form>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button name="mapping_btn" id="add_sub" value="submit" type="button" class="btn btn-info create_mapping">Submit</button>
            </div>
        </div>
    </div>
</div>
<!--- delete message modal  --->
<div id="cant_delete" class="modal fade cant_delete"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: red;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Sorry!</h4>
            </div>
            <div class="modal-body">
                <p>Sender ID is mapped ,you can't delete it</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--- delete message modal ends  --->
<!--- delete message modal  --->
<div id="delete_modal" class="modal fade delete_modal"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title danger">Mapping Deleted</h4>
            </div>
            <div class="modal-body">
                <p>Sender Id has been deleted successfully..</p>
            </div>

        </div>
    </div>
</div>
<!--- delete message modal ends  --->
<!--- save sucess message modal  --->
<div class="modal fade save_modal" id="save_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Add Subject</h4>
            </div>
            <div class="modal-body">
                <p  class="SucessMsg">Subject has been saved successfully..</p>
            </div>
        </div>
    </div>
</div>
<!--- save sucess modal ends  --->
<!--- update sucess message modal  --->
<div class="modal fade update_sucess_modal" id="update_sucess_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabelmsg">Update Subject</h4>
            </div>
            <div class="modal-body">
                <p class="updtedmsg">Subject has been Update successfully..</p>
            </div>
        </div>
    </div>
</div>
<!--- save sucess modal ends  --->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabelupdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelupdate">Update Subject </h4>
            </div>

            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" method= "POST" class="formdata" id="formdata" autocomplete="off">
                    <div class="form-group">
                        <input type="hidden" name="subjectrefid" id="subjectrefid">
                        <label for="subname">Subject Name <span style="color: red">*</span></label>
                        <input type="text" autofocus="autofocus"  name="subname"  class="form-control" id="subname1" placeholder="Enter Subject" autocomplete="off" required />
                        <p class="ueMsg"></p>
                    </div>
                    <div class="form-group">
                         <label for="lngcode">Language <span style="color: red">*</span></label>
                        <select  id="lngcode1" name="lngcode"  class="form-control all_empty" autocomplete="off"  >
                            <option selected="true" disabled>Choose Laguage</option>
                            <?php foreach ($lang as $key => $value) { ?>
                                <option value="<?php echo $key;  ?>"><?php echo $value; ?></option>
                               
                             <?php } ?>
                        </select> 
                        <p class="ueMsg2"></p>
                    </div>

                </form>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary" id="edit_subject">Update</button>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 put_table">

    <section class="panel">

    <header class="panel-heading">
        Subject List
        <!-- <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span> -->
        <button style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary" data-toggle="modal" data-target="#modalForm">
            Add Subject
        </button>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
               


            </div>
        </div>
    </div>
</section>

</div>

<script src="<?php echo AUTH_ASSETS; ?>js/jquery3.3.1.js"></script>

<?php
$adminurl 	= AUTH_PANEL_URL;
$apiurljava = API_URL_JAVA;
$usmrefid = $currentuser;
$custum_js = <<<EOD

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>    
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
<script type="text/javascript" language="javascript" >
		
	var subjectListArr = [];
	jQuery(document).ready(function() {
		ajax_get_datatable();
	});
	var api_url_java = "$apiurljava";
	//alert(api_url_java);
	function ajax_get_datatable() {    
		var html = '<table id="example" class="table table-striped table-bordered nowrap display" style="width:100%"></table>';
		$("#dynamic-table_wrapper").empty().append(html);
		var usmrefid = "$usmrefid";
		var data = {
			opcode: 'clt-get-subjects',
			usmrefid: usmrefid,
			lngcode:''
		};
        var jsondata  = JSON.stringify(data);
        $.ajax({
            contentType	: "application/x-www-form-urlencoded", 
            dataType 	: "json",
            url			: api_url_java,
            type		: "post",
            data		: jsondata,
            success		: function(data) {
                console.log(data);
                subjectListArr = data.allsublist;
                var table = $('#example').DataTable({
                    responsive: true,
                    data:data.allsublist,
                    columns:[
                        { "data": "subjectrefid","title":"Subrefid"},
                        { "data": "subname","title":"Subject Name"},
                        { "data": "languagecode","title":"Language Code"},
                       
                        { "data": "","title":"Action", "render":function(data, type, row){
                            var html = '';
                            html += '<button class="btn btn-info btn-sm" value='+row.subjectrefid+' onclick="editSubject(this.value)">EDIT</button>&nbsp;&nbsp;';
                            html += '<button class="btn btn-danger btn-sm" value='+ row.subjectrefid +' onclick="deleteSubject(this.value)">DELETE</button>';
                            return html;
                            }
                        }
                    ],
                    "columnDefs":[{                                
                        "targets":[3],
                         "orderable":false                        
                    }]
                });
            },
        });
	}
	
	$("#add_sub").click(function(){ 
		setTimeout(function () { disableButton2(); }, 0);
		var subname = $('#subname').val();
		var lngcode = $('#lngcode').val();
		if(subname == '' ){
			$('.eMsg').html('<span style="color:red;">This field is required.</span>');
			$('#subname').focus();
			return false;
		}
		if (lngcode == null) {
			$('.eMsg2').html('<span style="color:red;">Please select a language.</span>');
			$('#lngcode').focus();
			return false;
		}
		var data_add = {
			opcode: 'clt-add-subjects',
			languagecode:lngcode,
			subname:subname,
			usmrefid:23
		};
		var jsondataadd  = JSON.stringify(data_add);

		$.ajax({
			contentType: "application/x-www-form-urlencoded", 
			dataType : "json",
			url: api_url_java,
			type: "post",
			data: jsondataadd,        
			success:function(data){
				if(data.status=='success'){
					$(".save_modal").modal("toggle");
					 $('.SucessMsg').html('<span style="color:green;">'+data.message+'</span>');
					$("#modalForm").modal("toggle");
				}
				if(data.status=='fail'){
					$(".save_modal").modal("toggle");
					 $('.SucessMsg').html('<span style="color:red;">'+data.message+'</span>');
					$("#modalForm").modal("toggle");
				}
				ajax_get_datatable();

			 }
		});
	});
         

	function deleteSubject(val){
        var result = confirm("Are you sure to delete this Subject?");
        if (result) {
		var subjectrefid = val;
		var data = {
			opcode: 'clt-delete-subjects',
			subjectrefid:subjectrefid,
			usmrefid:23
		};
		var jsondata  = JSON.stringify(data);
		$.ajax({
			contentType: "application/x-www-form-urlencoded", 
			dataType : "json",
			url: api_url_java,
			type: "post",
			data: jsondata,			
			success:function(data){
				if(data.status=='success'){
					$(".save_modal").modal("toggle");
					$('.SucessMsg').html('<span style="color:green;">'+data.message+'</span>');				   
				}
				if(data.status=='fail'){
					$(".save_modal").modal("toggle");
					$('.SucessMsg').html('<span style="color:red;">'+data.message+'</span>');
				}
				ajax_get_datatable();
			}
		});
      }  
	} 

	function editSubject(val){
		var data =   _.find(subjectListArr, function(item){ return item.subjectrefid == val;})
		subjectrefid = data.subjectrefid;
		subname = data.subname;
		lngcode = data.languagecode;
		subjectrefid = data.subjectrefid;
		$("#myModal #subname1").val( subname );
		$("#myModal #lngcode1").val( lngcode );
		$("#myModal #subjectrefid").val(subjectrefid);
		$('#myModal').modal('show');	  
	}

	$("#edit_subject").click(function(){ 
        setTimeout(function () { disableButton(); }, 0);
		var subname = $('#subname1').val();       
		var lngcode = $('#lngcode1').val();
            if(subname == '' ){
				$('.ueMsg').html('<span style="color:red;">This field is required.</span>');
                $('#subname').focus();
                return false;
            }
            if (lngcode == null) {
				$('.ueMsg2').html('<span style="color:red;">Please select a language.</span>');
                $('#lngcode').focus();
				return false;
            }
		var subjectrefid = $('#subjectrefid').val();
        var data = {
            opcode: 'clt-update-subjects',
            subjectrefid:subjectrefid,
            usmrefid:23,
            languagecode:lngcode,
            subname:subname
        };
        var jsondata  = JSON.stringify(data);
        $.ajax({
            contentType: "application/x-www-form-urlencoded", 
            dataType : "json",
            url: api_url_java,
            type: "post",
            data: jsondata,
            success:function(data){
				if(data.status=='success'){
					$("#myModal").modal("toggle");
					 $('.updtedmsg').html('<span style="color:green;">'+data.message+'</span>');
					$("#update_sucess_modal").modal("toggle");
				}
				if(data.status=='fail'){
					$("#myModal").modal("toggle");
					 $('.updtedmsg').html('<span style="color:red;">'+data.message+'</span>');
					$("#update_sucess_modal").modal("toggle");
				}
				 ajax_get_datatable();
			}
        });
        
	});
	
	function disableButton() {
		$("#edit_subject").prop('disabled', true);
	}
	
	function disableButton2() {
		$("#add_sub").prop('disabled', true);
	}
	
	$('#modalForm').on('hidden.bs.modal', function (e) {
		$("#add_sub").removeAttr('disabled');
		$(this)
			.find("input,textarea,select")
			.val('')
			.end()
			.find("input[type=checkbox], input[type=radio]")
		   .prop("checked", "")
		   .end();
	})
	$('#myModal').on('hidden.bs.modal', function (e) {
		$("#edit_subject").removeAttr('disabled'); 
		$('.ueMsg').html('');
		$('.ueMsg2').html('');
	})
</script>    
                     
   
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>








