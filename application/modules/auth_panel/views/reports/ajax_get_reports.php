<?php //echo "<pre>"; print_r($result['result']); ?>
<section class="panel">
  
    <header class="panel-heading">Assessment Data</header>

        <div class="panel-body">
            <div class="adv-table" id="down">
                <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">
                    
          <table id="example"  class="table table-striped table-bordered nowrap display" style="width:100%">
                        <thead>
                           
                            <tr>
                               <th class="limited" title="User Name">Name</th>
                               <th class="limited" title="User Mobile">Mobile</th>
                               <th class="limited" title="Total Marks">Marks</th>
                                <?php  $q=0;foreach ($ques as $key => $value) { ?>
                                <th class="limited" data-html="true"  title='<?php echo  $value['polquestion'];  ?>'><?php echo html_entity_decode($value['polquestion'],ENT_QUOTES);?></th>
                                <?php $q++;  }   ?> 
                                
                            </tr>
                             
                        </thead>
                      <tbody  role="alert" aria-live="polite" aria-relevant="all">

                         <?php foreach ($result['result']['user_answer'] as  $value) { ?>
                           <tr class="gradeA odd">
                        
                              <td class="limited" title="<?php echo $value['name'];?>"><?php echo $value['name'];   ?></td>
                              <td class="limited" title="<?php echo $value['mobile'];?>"><?php echo substr($value['mobile'], 3);?>
                                
                              </td>
                            <td>  <?php  $i=0;
                              foreach ($ques as $que => $qval) {
                                 
                                  foreach ($value['options'] as $key => $v) {

                                        if($key==$qval['polrefid']){
                                         $i = $i + $v['value'];
                                         //echo $i.'/'.$q;
                                        }
                                     }
                                  
                               }
                               echo $i.'/'.$q;
                                ?>
                              </td>
                                 <?php foreach ($ques as $key1 => $value1) {
                                if(isset($value['options'][$value1['polrefid']])){ ?>
                              <td><?php echo $value['options'][$value1['polrefid']]['value'];?></td>
                                 <?php  }else{?> 
                              <td><?php echo "-";?></td>
                                <?php } } ?>
                              

                        
                          </tr>
                        <?php }   ?> 

                      </tbody> 
           </table>

                </div>
            </div>
        </div>
 </section>
  
