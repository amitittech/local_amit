
<?php  //print_r($page); die;?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('auth_panel_assets/assets/select2/css/select2.min.css'); ?>"/>
<link href="<?php echo base_url('auth_panel_assets/') ?>css/sweetalert.css" rel="stylesheet" >
<style type="text/css">

#example {
  table-layout: fixed;
  width: 100% !important;
}
.buttons-excel {
  background-color: blue;
  color: white;
}
.buttons-csv {
  background-color: blue;
  color: white;
}
/*#example th{
  width: auto !important;
  white-space: normal;
  text-overflow: ellipsis;
  overflow: hidden;
  
}*/
.limited{
    white-space: nowrap;
    width: auto !important;                  
    overflow: hidden;
    text-overflow: ellipsis;
}


    .modal
    {
        position: fixed;
        z-index: 999;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
        -moz-opacity: 0.8;
    }
    .center
    {
        z-index: 1000;
        margin: 300px auto;
        padding: 10px;
        width: 80px;
       /* background-color: White;*/
        border-radius: 20px;
        filter: alpha(opacity=100);
        opacity: 1;
        -moz-opacity: 1;
    }
    .center img
    {
        height: 100px;
        width: 100px;
    }


</style>
<div class="modal" id="loader" style="display: none">
    <div class="center">
        <img alt="" src="<?php echo base_url('resources/ajax-loader.gif');?>" />
    </div>
</div>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">Take a Test Report
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!--<a href="javascript:;" class="fa fa-times"></a>-->
            </span>
        </header>
        <div class="panel-body">
            <form role="form" method= "POST" enctype="multipart/form-data" class="formdata" id="formdata">
                <fieldset>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Training Instance<span style="color: red">*</span></label>
                        <select  id="trnname" name="trnname"  class="form-control all_empty"  value="" >
                            <option selected="true" disabled>Choose Training Instance</option>
                            <?php foreach ($training as $key => $value) { ?>
                                <option value="<?php echo $value['trnrefid'];  ?>"><?php echo $value['trnname']; ?></option>
                               
                             <?php } ?>
                        </select> 
                        <span class="text-danger error"><?php echo form_error('chmname'); ?></span> 
                    </div>

                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Module having take a test<span style="color: red">*</span></label>
                        <select  id="module" name="rcgrefid"  class="form-control "   >
                           <option selected="true" disabled>Choose Module</option> 
                        </select> 
                        <span class="text-danger error"><?php echo form_error('module'); ?></span> 
                    </div>
                    
                </fieldset>
                
                

                <div  class="form-group">
                    <button name="mapping_btn" value="submit" type="button" class="btn btn-info create_mapping">Submit</button>
                </div>
            </form>
        </div>
    </section>
</div>
<div class="col-lg-12 put_table"></div>
<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
   
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    <script type="text/javascript" language="javascript" >
        jQuery(document).ready(function() {

            $(".create_mapping").click(function(){         
                var form = $('#formdata')[0];
                var data = new FormData(form);   
                $(".error").html('');
                var trnname             = $("#trnname").val();
                
				var any_error 			= "";				
				if(trnname == null || trnname == ""){
                    $("#trnname").focus();
                    $("#trnname").siblings('.error').html('Please select a training Instance.'); //return false;
					          any_error = 1;
        }
        var module =  $("#module").val();
            if(module == null || module == ""){
                    $("#module").focus();
                    $("#module").siblings('.error').html('No module for this Training assessment.'); return false;
                    any_error = 1;
        }
				 $(".create_mapping").prop("disabled", false);
                if(any_error){
					return false;
				}
                $.ajax({
                    url         : "$adminurl"+"Reports/getReports",
                    enctype     : 'multipart/form-data',
                    processData : false,
                     contentType : false,
                    cache       : false,
                    type        : "post",  
                    
                    data        : data,
                    success: function (data) {

                        $('.put_table').html(data);
                        var table = $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                        { extend:'excelHtml5',title: 'Take a Test', text: 'Download Excel'},
                        { extend: 'csvHtml5',title: 'Take a Test', text: 'Download Csv'}
                        ],  
                         
                        responsive: true,
                        "ordering": false,
                        });
                        $('#loader').hide();
                     },

                     beforeSend: function() {
                           $('#loader').show();
                     },
                 });   
            }); 
        
            
        });	


        $("#trnname").bind("change", function() {
            $.ajax({
                 type: "GET", 
                 url         : "$adminurl"+"Reports/ajax_get_modules",
                 data: "trnrefid="+$("#trnname").val(),
                 success: function(response) {

                     var obj = jQuery.parseJSON(response);
                      var length  = obj.length;

                        $("#module").empty();
                        for( var i = 0; i<length; i++){
                            var id = obj[i]['rcgrefid'];
                            var name = obj[i]['groupname'];
                            $("#module").append("<option  value='"+id+"'>"+name+"</option>");
                        }
                    $('#loader').hide();    
                    },
                    beforeSend: function() {
                           $('#loader').show();
                     },
                 });
    
       });

       $('th').tooltip({
    content: function() {
        return $(this).attr('title');
    }
   });
   $('td').tooltip({
    content: function() {
        return $(this).attr('title');
    }

    });
    </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>