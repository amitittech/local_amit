
<div class="col-lg-12">
    <section class="panel">
        <div class="panel-body">
            <form id="business_form" autocomplete="off" method="post" action="">
                <input type="hidden" name="id" value="<?php if(isset($result)){ echo $result['id']; }?>">
                <div class="login-wrap">
                    <center><span id="form_error_msg"></span></center>
                    <center><h4>Enter business details below</h4></center>
<!--                    <div class="form-group">
                        <input id="is_reclaim" type="checkbox" value=""> ReClaiming
                    </div>
                    <div class="form-group">
                        <label for="business_name">Business Name</label>
                        <input type="text" class="form-control dpd1" value="<?php if(isset($result)){ echo $result['business_name']; }?>" name = "business_name" id="business_name" placeholder="Enter Business name">
                    </div>-->
                    <div id="not_for_claim">
                    <div class="form-group">
                        <label for="owner_first_name">Business User First Name<span style="color: red">*</span></label><br>
                        <span class="error bold"></span>
                        <input type="text" class="form-control dpd1 alphabet trim_space" value="<?php if(isset($result)){ echo $result['owner_first_name']; }?>" name = "owner_first_name" id="owner_first_name" placeholder="Enter Business user first name">
                    </div>
                    <div class="form-group">
                        <label for="owner_last_name">Business User Last Name<span style="color: red">*</span></label><br>
                        <span class="error bold"></span>
                        <input type="text" class="form-control dpd1 alphabet trim_space" value="<?php if(isset($result)){ echo $result['owner_last_name']; }?>" name = "owner_last_name" id="owner_last_name" placeholder="Enter Business user last name">
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address<span style="color: red">*</span></label><br>
                        <span class="error bold"></span>
                        <input type="text" class="form-control trim_space" value="<?php if(isset($result)){ echo $result['email']; }?>" name="email" placeholder="Enter Email" id="email" value="<?php echo set_value('email'); ?>">
                    </div>
                    <div class="form-group">
                        <label for="mobile">Mobile<span style="color: red">*</span></label><br>
                        <span class="error bold"></span>
                        <input type="text" maxlength="16" class="form-control number trim_space" value="<?php if(isset($result)){ echo $result['mobile']; }?>" name="mobile" placeholder="Enter Mobile" id="mobile" value="<?php echo set_value('mobile'); ?>">
                    </div>
<!--                    <div class="form-group">
                        <label for="location">Location</label>
                        <input  type="text" class="form-control" value="<?php if(isset($result)){ echo $result['location']; }?>" name="location" placeholder="Enter Business Location" id="location" value="<?php echo set_value('location'); ?>">
                    </div>-->


<!--                    <br><hr><br>
                    <h4> Enter account details below</h4><br>-->
<!--                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" value="<?php if(isset($result)){ echo $result['username']; }?>" placeholder="Username" name="username" id="username">
                    </div>-->
                    <div class="form-group">
                        <label for="Password">Password<span style="color: red">*</span></label><br>
                        <span class="error bold"></span>
                        <input type="password" class="form-control trim_space" value="<?php if(isset($result)){ echo $result['password']; }?>" placeholder="Password" name="password" id="login_pwd">
                    </div>
                    <div class="form-group">
                        <label for="c_password">Confirm Password<span style="color: red">*</span></label><br>
                        <span class="error bold"></span>
                        <input type="password" class="form-control trim_space" value="<?php if(isset($result)){ echo $result['password']; }?>" placeholder="Re-type Password" name="c_password" id="c_password">
                    </div>


                    <button class="btn btn-primary btn-login" id="submit_business_form" type="button"><?php if(isset($result)){ echo "UPDATE USER"; } else { echo "CREATE USER";}?></button>

                </div>
            </form>


        </div>
    </section>
</div>



<?php
$adminurl = AUTH_PANEL_URL;
$auth_url = site_url('auth_panel');
$custum_js = <<<EOD
             
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
<script>
        $('#submit_business_form').click(function () {
                    $('.form-control').css({"border": "1px solid gray"});
                    $('.error').text("");
                    //alert(owner_first_name);
                    var exit_type=0;
                    if($('#owner_first_name').val()==""){
                        if(exit_type==0){
                            $('#owner_first_name').focus();
                        }
                        $('#owner_first_name').css({"border": "1px solid red"});
                        $('#owner_first_name').siblings('.error').text("Please Enter First Name");
                        exit_type=1;
                    }
                    if($('#owner_last_name').val()==""){
                        if(exit_type==0){
                            $('#owner_last_name').focus();
                        }
                        $('#owner_last_name').css({"border": "1px solid red"});
                        $('#owner_last_name').siblings('.error').text("Please Enter Last Name");
                        exit_type=1;
                    }
                    if($('#email').val()==""){
                        if(exit_type==0){
                            $('#email').focus();
                        }
                        $('#email').css({"border": "1px solid red"});
                        $('#email').siblings('.error').text("Please Enter Valid Email Id");
                        exit_type=1;
                    }
                    if($('#mobile').val()==""){
                        if(exit_type==0){
                            $('#mobile').focus();
                        }
                        $('#mobile').css({"border": "1px solid red"});
                        $('#mobile').siblings('.error').text("Please Enter Valid Mobile Number");
                        exit_type=1;
                    }
                    if($('#login_pwd').val()==""){
                        if(exit_type==0){
                            $('#login_pwd').focus();
                        }
                        $('#login_pwd').css({"border": "1px solid red"});
                        $('#login_pwd').siblings('.error').text("Please Enter Password With UpperCase, LowerCase, Special Character and Number");
                        exit_type=1;
                    }
                    if($('#c_password').val()==""){
                        if(exit_type==0){
                            $('#c_password').focus();
                        }
                        $('#c_password').css({"border": "1px solid red"});
                        $('#c_password').siblings('.error').text("Please Enter Confirm Password");
                        exit_type=1;
                    }
                    if(exit_type==1){
                        exit;
                    }
                    var psw = $('#login_pwd').val();
                    var c_psw = $('#c_password').val();
//                    if(psw=="" && ){
//                        $('#login_pwd').focus();
//                        $('#login_pwd').css({"border": "2px solid red"});
//                        $('#login_pwd').siblings('.error').text("Please Enter Password");
//                        exit;
//                    }
//                    if(c_psw==""){
//                        $('#c_password').focus();
//                        $('#c_password').css({"border": "2px solid red"});
//                        $('#c_password').siblings('.error').text("Please Enter Confirm Password");
//                        exit;
//                    }
                    if (psw != "") {
                        var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,13}$/;
                        var str = $('#login_pwd').val();
                        if (!re.test(str)) {
                            $('#login_pwd').focus();
                            $('#login_pwd').css({"border": "1px solid red"});
                            $('#login_pwd').siblings('.error').text("Password Should Contain Uppercase, LowerCase, Special character And Number");
                            exit;
                        }
                        if(psw!=c_psw){
                            $('#c_password').focus();
                            $('#c_password').css({"border": "1px solid red"});
                            $('#c_password').siblings('.error').text("Confirm Password Does Not Match");
                            exit;
                        }
                    }
            var form_inputs = $('#business_form').serialize();
            //alert(form_inputs);
            jQuery.ajax({
                url: '$auth_url/Admin/create_backend_user',
                method: 'POST',
                dataType: 'json',
                data: form_inputs,
                success: function (data) {
                    if (data.status == true) {
                        $('#form_error_msg').css('color', 'green');
                        $('#form_error_msg').html(data.message);
                        $('#submit_business_form input').val('');
                        window.location.href = '$auth_url/admin/business_user_list';
                    } else {
                        $('#' + data.field_id).focus();
                        $('#' + data.field_id).css({"border": "1px solid red"});
                        $('#' + data.field_id).siblings('.error').text(data.message);
                    }

                }
            });
        });
        
        $(".number").keypress(function (e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        //display error message
                        //$("#reg_agent_error_validation").html("Digits Only").show().fadeOut("slow");
                        return false;
                    }
                });
                $('.alphabet').keypress(function (e) {
                    var regex = new RegExp(/^[a-zA-Z\s]+$/);
                    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                    if (regex.test(str)) {
                        return true;
                    } else {
                        e.preventDefault();
                        return false;
                    }
                });
    </script>
        
 <script>
 $('#is_reclaim').change( function(){
    if($("#is_reclaim").is(':checked')){
       $('#not_for_claim').empty(); 
    } else {
        $('#not_for_claim').append('<div class="form-group"><label for="owner_first_name">Business User First Name</label><input type="text" class="form-control dpd1" value="" name = "owner_first_name" id="owner_first_name" placeholder="Enter Business name"></div><div class="form-group"><label for="owner_last_name">Business User Last Name</label><input type="text" class="form-control dpd1" value="" name = "owner_last_name" id="owner_last_name" placeholder="Enter Business name"></div>');
    }
 });    
 </script>       
        
        

EOD;

echo modules::run('auth_panel/template/add_custum_js', $custum_js);
?>







