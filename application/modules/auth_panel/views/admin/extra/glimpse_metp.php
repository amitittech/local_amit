<?php
$result = $result['result'];
?>
<div id="metp_dt_less_data_div" class="dataTables_wrapper form-inline metp_dt_less_data_div" role="grid">
    <table id="metp_dt_less_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Post No. & Title</th>
                <th>U</th>
                <th>TA</th>
                <th>C</th>
                <th>LC</th>
                <th>MC</th>
                <th>MSF-C</th>
                <th>TRA-C</th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                if ($i == 2) {
                    break;
                }
                $ta  = $row['ta']."|".$row['v']."|".$row['l']."|".$row['c']."|".$row['s'];
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['title']; ?></td>
                    <td class=""><?php echo $row['u']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['ta'].'</a>'; ?></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['mc']; ?></td>
                    <td class=""><?php echo $row['msf_c']; ?></td>
                    <td class=""><?php echo $row['tra_c']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="metp_dt_all_data_div" class="dataTables_wrapper form-inline metp_dt_all_data_div" role="grid">
    <table id="metp_dt_all_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Post No. & Title</th>
                <th>U</th>
                <th>TA</th>
                <th>C</th>
                <th>LC</th>
                <th>MC</th>
                <th>MSF-C</th>
                <th>TRA-C</th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                $ta  = $row['ta']."|".$row['v']."|".$row['l']."|".$row['c']."|".$row['s'];
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['title']; ?></td>
                    <td class=""><?php echo $row['u']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['ta'].'</a>'; ?></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['mc']; ?></td>
                    <td class=""><?php echo $row['msf_c']; ?></td>
                    <td class=""><?php echo $row['tra_c']; ?></td>
                    
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>

    $("#metp_dt_less_data").DataTable();
    $("#metp_dt_all_data").DataTable();

    $('#metp_dt_all_data_div').hide();


    $('#metp_see_more_btn').show();
    $('#metp_see_more_text').show();
    $('#metp_see_less_text').hide();
    
    
    
    
</script>