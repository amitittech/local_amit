<?php
$result = $result['result'];
?>
<div id="mewp_dt_less_data_div" class="dataTables_wrapper form-inline mewp_dt_less_data_div" role="grid">
    <table id="mewp_dt_less_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Title</th>
                <th>TA</th>
                <th>C</th>
                <th>LC</th>
                <th>MC</th>
                <th>MSF-C</th>
                <th>TRA-C</th>
                <th>U</th>
                <!-- <th>Update</th> -->
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) { //pre($row);die;
                if ($i == 2) {
                    break;
                }
                $ta  = $row['ta']."|".$row['v']."|".$row['l']."|".$row['c']."|".$row['s'];
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['title']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['ta'].'</a>'; ?></td></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['mc']; ?></td>
                    <td class=""><?php echo $row['msf_c']; ?></td>
                    <td class=""><?php echo $row['tra_c']; ?></td>
                    <td class=""><?php echo $row['u']; ?></td>

                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="mewp_dt_all_data_div" class="dataTables_wrapper form-inline mewp_dt_all_data_div" role="grid">
    <table id="mewp_dt_all_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Title</th>
                <th>TA</th>
                <th>C</th>
                <th>LC</th>
                <th>MC</th>
                <th>MSF-C</th>
                <th>TRA-C</th>
                <th>U</th>
                <!-- <th>Update</th> -->
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                $ta  = $row['ta']."|".$row['v']."|".$row['l']."|".$row['c']."|".$row['s'];
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['title']; ?></td>
                    <td class=""><?php echo '<a href="javascript:void(0)" class="ta_popup" value="'.$ta.'">'.$row['ta'].'</a>'; ?></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['mc']; ?></td>
                    <td class=""><?php echo $row['msf_c']; ?></td>
                    <td class=""><?php echo $row['tra_c']; ?></td>
                    <td class=""><?php echo $row['u']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>

    //$("datatable").DataTable();
    $("#mewp_dt_less_data").DataTable();
    $("#mewp_dt_all_data").DataTable();

    $('#mewp_dt_all_data_div').hide();


    $('#mewp_see_more_btn').show();
    $('#mewp_see_more_text').show();
    $('#mewp_see_less_text').hide();

    
    
    
</script>
