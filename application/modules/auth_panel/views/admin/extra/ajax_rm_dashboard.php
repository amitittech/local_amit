<?php
die;
//pre($result);
$tras = $result['result'];
$ti_ids = "";
?>

<div class="col-sm-12 appendtra">
    <?php foreach ($tras as $tra) { ?>
        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo $tra['traname']; ?></div>
            <div class="panel-body" id="trarefid_<?php echo $tra['trarefid']; ?>">
                <?php foreach ($tra['ti'] as $ti) { 
                    #if( ($ti['startdate'] >= $tdy && $ti['enddate'] >= $tdy) OR ($ti['regstartdate'] >= $tdy && $ti['regenddate'] >= $tdy) ){
                    $ti_ids[] = $ti['trnrefid']; ?>
                
                    <div class="col-sm-6">
                        <div class="panel panel-success"style="border: 1px solid">
                            <div class="panel-heading"><?php echo $ti['trnname']; ?></div>
                            <div class="panel-heading"><?php echo $ti['rcmname']; ?></div>
                            <div class="panel-body" style="height: 250px">                                 

                                <div class="html" id="trnrefid_<?php echo $ti['trnrefid']; ?>">

                                </div>

                            </div>
                        </div>
                    </div>
                <?php } //} ?>
            </div>
        </div>  
    <?php } 
    ?>
    <input type="text" id="ti_ids" value="<?php echo implode(',',$ti_ids); ?>">
</div>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBeA4V5mjOyGL9P4QEpGOdl58DXYWycRbc" async="" defer="defer"></script>
<script>
    google.charts.load("current", {packages: ["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ["Element", "Density", {role: "style"}],
            ["Registered", 8.94, "#b87333"],
            ["Expected", 19.30, "gold"]
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"},
            2]);

        var options = {
            title: "Training Registration",
            width: '100%',
            height: '100%',
            bar: {groupWidth: "95%"},
            legend: {position: "none"},
        };
        var chart = new google.visualization.BarChart(document.getElementById("trnrefid_36"));
        chart.draw(view, options);
    }
</script>