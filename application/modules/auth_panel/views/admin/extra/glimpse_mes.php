<?php
$result = $result['result'];
?>
<div id="mes_dt_less_data_div" class="dataTables_wrapper form-inline mes_dt_less_data_div" role="grid">
    <table id="mes_dt_less_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>TA</th>
                <th>C</th>
                <th>LC</th>
                <th>U</th>
                <th>Users in school</th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                if ($i == 2) {
                    break;
                }
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['name']; ?></td>
                    <td class=""><?php echo $row['ta']; ?></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['tu']; ?></td>
                    <td class=""><?php echo $row['uu']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="mes_dt_all_data_div" class="dataTables_wrapper form-inline mes_dt_all_data_div" role="grid">
    <table id="mes_dt_all_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>TA</th>
                <th>C</th>
                <th>LC</th>
                <th>U</th>
                <th>Users in school</th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['name']; ?></td>
                    <td class=""><?php echo $row['ta']; ?></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['tu']; ?></td>
                    <td class=""><?php echo $row['uu']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>

    $("#mes_dt_less_data").DataTable();
    $("#mes_dt_all_data").DataTable();

    $('#mes_dt_all_data_div').hide();


    $('#mes_see_more_btn').show();
    $('#mes_see_more_text').show();
    $('#mes_see_less_text').hide();
</script>