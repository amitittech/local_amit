<?php
$result = $result['result'];
?>
<div id="meu_dt_less_data_div" class="dataTables_wrapper form-inline meu_dt_less_data_div" role="grid">
    <table id="meu_dt_less_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>School</th>
                <th>Session Time</th>
                <th>TA (Wall + TI)</th>
                <th>C</th>
                <th>LC</th>
                <th>Training Completion %age</th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                if ($i == 2) {
                    break;
                }
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['name']; ?></td>
                    <td class=""><?php echo $row['school']; ?></td>
                    <td class=""><?php echo $row['st']; ?></td>
                    <td class=""><?php echo $row['ta']; ?></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['tcp']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="meu_dt_all_data_div" class="dataTables_wrapper form-inline meu_dt_all_data_div" role="grid">
    <table id="meu_dt_all_data" class="table table-striped table-bordered nowrap display" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>School</th>
                <th>Session Time</th>
                <th>TA (Wall + TI)</th>
                <th>C</th>
                <th>LC</th>
                <th>Training Completion %age</th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i = 0;
            foreach ($result as $row) {
                $i++;
                ?>
                <tr class="gradeA odd">
                    <td class=""><?php echo $i; ?></td>
                    <td class=""><?php echo $row['name']; ?></td>
                    <td class=""><?php echo $row['school']; ?></td>
                    <td class=""><?php echo $row['st']; ?></td>
                    <td class=""><?php echo $row['ta']; ?></td>
                    <td class=""><?php echo $row['c']; ?></td>
                    <td class=""><?php echo $row['lc']; ?></td>
                    <td class=""><?php echo $row['tcp']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>

    $("#meu_dt_less_data").DataTable();
    $("#meu_dt_all_data").DataTable();

    $('#meu_dt_all_data_div').hide();


    $('#meu_see_more_btn').show();
    $('#meu_see_more_text').show();
    $('#meu_see_less_text').hide();
    
    $(".ta_popup").click(function () {
        var ta = $(this).attr('value').split('|');
        var av = "<td>"+ta[0]+"</td><td>"+ta[1]+"</td><td>"+ta[2]+"</td><td>"+ta[3]+"</td><td>"+ta[4]+"</td>"
        $("#ta_values_tbody").html(av);
        $("#ta_values").modal("toggle");
    });
</script>