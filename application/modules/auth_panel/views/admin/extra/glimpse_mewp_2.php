<?php //pre($result);    ?>
<?php
$result = $result['result'];
//$ti_info = $result['ti_info'];
//pre($result['result']); die;   
?>

    <div class="panel-body" id="">
        <section class="panel">
            <header class="panel-heading">
                Most engaging wall post (By Activity)
                <button style="float: right;margin-right: 12px;margin-bottom: 10px" class="btn btn-primary see_more" >
                    <span id="see_more_text" class="col-md-3"> See more..</span>
                    <span id="see_less_text" class="col-md-3"> See less.. &nbsp;&nbsp;</span>
                </button>
            </header>
            <div class="panel-body">
                <div class="adv-table">
                    <div id="dynamic-table_wrapper1" class="dataTables_wrapper form-inline " role="grid">
                        <table id="datatable_1_1" class="table table-striped table-bordered nowrap display see_less_table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>TA</th>
                                    <th>C</th>
                                    <th>LC</th>
                                    <th>MC</th>
                                    <th>MSF-C</th>
                                    <th>TRA-C</th>
                                    <th>U</th>
                                    <!-- <th>Update</th> -->
                                </tr>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                <?php
                                $i = 0;
                                foreach ($result as $row) {
                                    if ($i == 2) {
                                        break;
                                    }
                                    $i++;
                                    ?>
                                    <tr class="gradeA odd">
                                        <td class=""><?php echo $i; ?></td>
                                        <td class=""><?php echo $row['title']; ?></td>
                                        <td class=""><?php echo $row['ta']; ?></td>
                                        <td class=""><?php echo $row['c']; ?></td>
                                        <td class=""><?php echo $row['lc']; ?></td>
                                        <td class=""><?php echo $row['mc']; ?></td>
                                        <td class=""><?php echo $row['msf_c']; ?></td>
                                        <td class=""><?php echo $row['tra_c']; ?></td>
                                        <td class=""><?php echo $row['u']; ?></td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 
                    <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline " role="grid">   
                        <table id="datatable_1_2" class="table table-striped table-bordered nowrap display see_less_table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>TA</th>
                                    <th>C</th>
                                    <th>LC</th>
                                    <th>MC</th>
                                    <th>MSF-C</th>
                                    <th>TRA-C</th>
                                    <th>U</th>
                                    <!-- <th>Update</th> -->
                                </tr>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                <?php
                                $i = 0;
                                foreach ($result as $row) {
                                    $i++;
                                    ?>
                                    <tr class="gradeA odd">
                                        <td class=""><?php echo $i; ?></td>
                                        <td class=""><?php echo $row['title']; ?></td>
                                        <td class=""><?php echo $row['ta']; ?></td>
                                        <td class=""><?php echo $row['c']; ?></td>
                                        <td class=""><?php echo $row['lc']; ?></td>
                                        <td class=""><?php echo $row['mc']; ?></td>
                                        <td class=""><?php echo $row['msf_c']; ?></td>
                                        <td class=""><?php echo $row['tra_c']; ?></td>
                                        <td class=""><?php echo $row['u']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
    </div>

<script>

    //$("datatable").DataTable();
    $("#datatable_1_1").DataTable();
    $("#datatable_1_2").DataTable();
    $("#datatable_2_1").DataTable();
    $("#datatable_2_2").DataTable();
    //$("#example").DataTable({});
    //$("#example1").DataTable({});
    $('#dynamic-table_wrapper').hide();
    $('#see_more_text').show();
    $('#see_less_text').hide();

    //alert('wdew');
    //$('#tra_parent_div').hide();
    //$('#glimpse_parent_div').show();

    $(".see_more").click(function () {
        $('#see_more_text').toggle();
        $('#see_less_text').toggle();
        $('#dynamic-table_wrapper').toggle();
        $('#dynamic-table_wrapper1').toggle();
        //alert('fvvdf'); 
    });

    $("#back_to_tra").click(function () {
        $('#tra_parent_div').show();
        $('#glimpse_parent_div').hide();
    });



</script>