<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wall_posting_users extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
		$this->load->model('Training_model');
		$this->load->model('Wall_model');
		$this->load->helper('custom');
    }
	
	public function get_wall_posting_users($input) {
        //$this->validate_sender_id_list();
        $result = $this->Wall_model->get_wall_posting_users($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
	/////////// Wall Posters  /////////////////////
	
	public function add_wall_poster($inputs) { 
        $this->validate_add_wall_poster($inputs);
		$role        = $this->Training_model->get_role(array("role"=>'CL-admin')); 
        if(empty($role)){
            return_data(false, 'Role could not match.', array());
        }
		$attribute        = $this->Training_model->get_attribute(array("attribute"=>'RIGHT_POST_EPC')); 
        if(empty($attribute)){
            return_data(false, 'attribute could not match.', array());
        }
		$inputs['rlmrefid'] = $role['rlmrefid'];
        $inputs['oearefid'] = $attribute['oearefid'];
        $result = $this->Wall_model->add_wall_poster($inputs);
        if ($result) {
            if ($result == true && !is_array($result)) {
                $result = array();
            }
            return array("status" => true, "message" => 'Wall user has been added successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
	
    public function validate_add_wall_poster($inputs) {        
        if (!isset($inputs['user_mobiles']) OR empty($inputs['user_mobiles']) OR is_array($inputs['user_mobiles'])== false) {
            return_data(false, 'user_mobiles is required in array format.', array());
        }
    }
	
	public function delete_wall_posters($input) {
        if (!isset($input['delete_ids']) OR empty($input['delete_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Wall_model->delete_wall_posters($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
}
