<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Custom extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
		$this->load->helper('custom');
    }
	
	//----------        ------------//
    public function get_kibana_custom_filters($input) { #pre($input);
		if(!isset($input['values']) OR empty($input['values'])){
			return array("status" => false, "message" => 'Please pass any value', "result" => []);
		}
		$converted[] = array("match_phrase"=>array("userid"=> "10"));
		$converted[] = array("match_phrase"=>array("userid"=> "20"));
		
		$my_array = array("query"=>array("bool"=>array("should"=>$converted,"minimum_should_match"=>1)));
		$string = '{}';
        return array("status" => true, "message" => 'Please use this format', "result" => $my_array);
    }
	
	public function user_retention($input) { #pre($input);
		if(!isset($input['set_of_users']) OR empty($input['set_of_users'])){
			return array("status" => false, "message" => 'Please pass set_of_users', "result" => []);
		}
		if(!isset($input['time_period']) OR empty($input['time_period'])){
			return array("status" => false, "message" => 'Please pass time_period', "result" => []);
		}else{
			$timeperiod = explode('to',$input['time_period']);			
		}
		if(!isset($input['clubbing']) OR empty($input['clubbing'])){
			return array("status" => false, "message" => 'Please pass clubbing', "result" => []);
		}else{
			if(!in_array($input['clubbing'],array('date','week','month','year'))){
				return array("status" => false, "message" => "Please pass clubbing one of 'date','week','month','year'", "result" => []);
			}
		}
		
		/*
		$query ="SELECT ".$input['clubbing']."(`activitydate`),"
						."(SELECT COUNT(`rcuusmrefid`) as 'total_users',`rcutrbrefid`"
							."FROM `currcu`"
							."JOIN `curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid`"
							."JOIN `clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid`"
							."WHERE `clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1' ) as ttl,"
						."COUNT(DISTINCT userid),round((COUNT(DISTINCT userid)/6)*100)`retension` "
				."FROM `esocial_log`.`activeusers` "
				."WHERE `userid` IN (25,75,388,5456,8915,8925) and activitydate BETWEEN '".$timeperiod[0]."' and '".$timeperiod[1]."' "
				."GROUP BY ".$input['clubbing']."(`activitydate`) "
				."ORDER by ".$input['clubbing']."(`activitydate`) DESC";
				
		$query ="SELECT dates.dated,"."COUNT(DISTINCT userid) users,"
					."(SELECT COUNT(`rcuusmrefid`)"
						." FROM `chalklit`.`currcu`"
						." JOIN `chalklit`.`curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid`"
						." JOIN `chalklit`.`clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid`"
						." WHERE `clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1'"
						.") as total_users,"					
					."round((COUNT(DISTINCT userid)/"
						."(	SELECT COUNT(`rcuusmrefid`) as 'total_users'"
							."FROM `chalklit`.`currcu`"
							."JOIN `chalklit`.`curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid`"
							."JOIN `chalklit`.`clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid`"
							."WHERE `clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1'))*100"
							.") as `retension`"
				." FROM `chalklit_analysis`.`dates`" 
				." JOIN `chalklit_analysis`.`activeusers` ON activeusers.activitydate=dates.dated " 
				." WHERE (`userid` IN (25,75,388,5456,8915,8925,0) OR userid IS null) and dates.dated BETWEEN '".$timeperiod[0]."' and '".$timeperiod[1]."'"				
				."GROUP BY ".$input['clubbing']."(`dated`) "
				."ORDER by ".$input['clubbing']."(`dated`) DESC";
				
		$query ="SELECT dates.dated,COUNT(DISTINCT userid) users,
					(SELECT COUNT(`rcuusmrefid`) 
						FROM `chalklit`.`currcu` 
						JOIN `chalklit`.`curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid` 
						JOIN `chalklit`.`clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid` 
						WHERE `clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1'
					) as total_users,
					round((COUNT(DISTINCT userid)/(	
							SELECT COUNT(`rcuusmrefid`) as 'total_users'
							FROM `chalklit`.`currcu`
							JOIN `chalklit`.`curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid`
							JOIN `chalklit`.`clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid`
							WHERE `clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1'))*100
					) as `retension` 
				FROM `chalklit_analysis`.`dates` 
				LEFT JOIN `chalklit_analysis`.`activeusers` ON activeusers.activitydate=dates.dated  
				WHERE 
				(`userid` IN (25,75,388,5456,8915,8925,0) OR userid IS null) and 
				dates.dated BETWEEN '".$timeperiod[0]."' and '".$timeperiod[1]."' 
				GROUP BY ".$input['clubbing']."(`dated`) 
				ORDER by ".$input['clubbing']."(`dated`) DESC";
		*/
		
		$query ="SELECT concat(year(`dated`),year(`month`),'|',".$input['clubbing']."(`dated`),|) as custom,date(dated),
					month(dated),
					year(dated),
					COUNT(DISTINCT userid) users,
					(SELECT COUNT(`rcuusmrefid`) 
						FROM `chalklit`.`currcu` 
						JOIN `chalklit`.`curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid` 
						JOIN `chalklit`.`clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid` 
						WHERE `clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1'
					) as total_users,
					round((COUNT(DISTINCT userid)/(	
							SELECT COUNT(`rcuusmrefid`) as 'total_users'
							FROM `chalklit`.`currcu`
							JOIN `chalklit`.`curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid`
							JOIN `chalklit`.`clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid`
							WHERE `clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1'))*100
					) as `retension` 
				FROM `chalklit_analysis`.`dates` 
				LEFT JOIN `chalklit_analysis`.`activeusers` ON date(activeusers.activitydate)=dates.dated
				LEFT JOIN `chalklit`.`currcu` ON `currcu`.rcuusmrefid=`activeusers`.userid
				LEFT JOIN `chalklit`.`curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid` 
				LEFT JOIN `chalklit`.`clt_trn` ON `clt_trn`.`trnrefid`=`curtrb`.`trbtrnrefid`
				WHERE (`clt_trn`.`trnname` = 'SCERT/Mensuration/2016/Dec/1' OR userid IS null) and 
							dates.dated BETWEEN '".$timeperiod[0]."' and '".$timeperiod[1]."' 
				GROUP BY custom 
				ORDER by ".$input['clubbing']."(`dated`) ASC;";
				
				
		#echo $query;	die;	
		$conn = mysqli_connect("13.127.36.81", "qadbuser_aws", "P@ssw@rd@12", "chalklit");
		
		/*
		$query 	= "SELECT dates.dated from `chalklit_analysis`.`dates`";
		$y = 2019;
		for($i=1; $i<5; $i++){
			$d=cal_days_in_month(CAL_GREGORIAN,$i,2019);
			for($j=1;$j<=$d;$j++){
				$date_day 	= ($j>9)? $j : '0'.$j;
				$date_mnth 	= '0'.$i;
				$date 		= '2019-'.$date_mnth.'-'.$date_day;
				$query 	= "INSERT INTO `chalklit_analysis`.`dates` SET `dated` = '".$date."'";
				$result = mysqli_query($conn, $query);
			}
		}
		*/
		$query = trim($query);
		$final = $query;
		$result = mysqli_query($conn, $query);
		if (mysqli_num_rows($result) > 0) {
			$final = [];
			while($row = mysqli_fetch_assoc($result)) {
				$final[] = $row;
				#pre($row); die;
		   		#echo "Total: " . $row["tt"]. "<br>";
			}
		} else {
			echo "0 results";
		}
		
		mysqli_close($conn);
		#die;
		#$my_array = array("query"=>array("bool"=>array("should"=>$converted,"minimum_should_match"=>1)));
		#$string = '{}';
        return array("status" => true, "message" => 'User retention displyed successfully', "result" => $final);
    }
}
