<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Story extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Reports_model');
        $this->load->helper('custom');
        $this->load->helper('url');
        $this->load->helper('directory');
    }
	

	public function story(){

		$view_data['page']      = 'story';
		
		$data['page_data']      = $this->load->view('story/story', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function get_story(){  

 $dir = "resources/stories/OEBPS";
 $i=0;
 if ($dh = opendir($dir)){
    while (($file = readdir($dh)) !== false){
    	$i;
    	$ext = pathinfo($file, PATHINFO_EXTENSION);
    	if($ext=='xhtml'){
         $story[] =   current(explode(".", $file));
    	 sort($story);
    	 $i++;
    	 }

    }
    closedir($dh);
  }

for($j=1;$j<=$i;$j++){

  	$data[] = base_url("/resources/stories/OEBPS/").$j.'.'.'xhtml';

  }
 return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>$data);

		 }

	   
	
	  
}
