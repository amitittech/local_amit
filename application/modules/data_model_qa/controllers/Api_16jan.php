<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Elasticsearch\ClientBuilder;

require APPPATH . 'third_party/elasticsearch/vendor/autoload.php';

class Api_16jan extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Training_instances_model');
        $this->load->model('Training_model');
        $this->load->model('Courses_model');
        $this->load->model('Wall_model');
        $this->load->model('User_model');
        $this->load->model('Reports_model');
        $this->load->model('Log_records_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }

    public function save_api_log($inputs) {
        $file_name = 'api_log';
        if (isset($inputs['opcode'])) {
            $file_name = $inputs['opcode'];
        }
        $log = "Time: " . date('Y-m-d, H') . PHP_EOL;
        $log = $log . "url: " . base_url("api_log") . PHP_EOL;
        $log = $log . "Request " . json_encode($inputs) . PHP_EOL;

        file_put_contents('logs/' . $file_name . '_' . date('d_m_Y') . '.txt', $log, FILE_APPEND);
    }

    public function index() {
        //die('api');    
        // to check the inputs
        $starting_time = milliseconds();
        $inputs = check_inputs();
        //pre($this->input->post()); die('working');
        //append request params in log file i.e. http://localhost/project_folder/chalklit/logs/api_log.txt  

        $this->save_api_log($inputs);

        //AUTH TOKEN HANDLING
        $active_user = $this->check_authentication($inputs); //login_id 

        $inputs['login_id'] = $active_user['login_id'];

        //OPCODE HANDLING
        $response = $this->opcode_handling($inputs);
        //die('feddddddsr');
        //processing time
        $response_time = milliseconds() - $starting_time;

        //response size
        $responsesize = 0;
        if (isset($response['result']) && !empty($response['result'])) {
            //$responsesize = mb_strlen($string, '8bit');//strlen(json_encode($response['result']));
            //$responsesize = mb_strlen(json_encode($response),'8bit');
            $responsesize = 2000;
        }

        //header values
        //$header_data  = $this->input->request_headers();
        //$header_data  = getallheaders();
        $header_data = $_SERVER;

        //insert logs records in log table for each activity
        $activity_log['server'] = $header_data["SERVER_NAME"];
        $activity_log['createddate'] = milliseconds();
        $activity_log['ipaddress'] = (isset($header_data['REMOTE_ADDR'])) ? $header_data['REMOTE_ADDR'] : '192.168.0.1'; //SERVER_ADDR
        $activity_log['useragent'] = (isset($header_data['HTTP_USER_AGENT'])) ? $header_data['HTTP_USER_AGENT'] : 'useragent';
        $activity_log['resstatus'] = (isset($response['status']) && !empty($response['status'])) ? API_RESPONSE_STATUS_TRUE : API_RESPONSE_STATUS_FALSE;
        $activity_log['resmessage'] = $response['message'];
        $activity_log['responsesize'] = $responsesize;
        $activity_log['processingtime'] = $response_time;
        $activity_log['opcode'] = $inputs['opcode'];
        $activity_log['appversion'] = (isset($inputs['appversion'])) ? $inputs['appversion'] : 'appversion';
        $request_params = $inputs;
        unset($request_params['authtoken']);
        $activity_log['request'] = json_encode($request_params);
        $activity_log['usmid'] = $active_user['login_id'];
        $this->activity_log($activity_log);
        //$this->activity_log($inputs,$response['status'],$response['message'],$responsesize,$response_time);
        //response back to the api requester
        if ($response['status'] == true) {
            return_data(API_RESPONSE_STATUS_TRUE, $response['message'], $response['result']);
        }

        return_data('fail', $response['message'], array());
    }

    //-------------**  CHECK AUTHTOKEN/SESSION FOR AUTHENTICATION PURPOSE  **-----------------------------//
    public function check_authentication($input) {
        //return array("login_id"=>11);
        if (isset($input['login_id'])) {
            //pre($this->session->userdata());die;
            return array("login_id" => $input['login_id']);
        } else {
            if (!isset($input['authtoken'])) {
                if (isset($input['opcode']) && $input['opcode'] == "cl_search") {
                    return_data('fail', ELASTICSEARCH_ERROR_MESSAGE, array());
                }
                return_data('fail', "You are not athorised to use this api.", array());
            } else {
                $this->db->where('accesstoken', $input['authtoken']);
                $exist = $this->db->get('esculm')->row_array();
                if ($exist) {
                    //pre($exist);
                    return array('login_id' => $exist['ulmusmrefid']);
                } else {
                    if (isset($input['opcode']) && $input['opcode'] == "cl_search") {
                        return_data('fail', ELASTICSEARCH_ERROR_MESSAGE, array());
                    }
                    return_data('fail', "You are not athorised to use this api.", array());
                }
            }
        }
        return_data('fail', "You are not athorised to use this api.", array());
    }

    //------------------**   REDIRECT MAIN API TO THE APPROPRIATE API   **-----------------------//
    private function request_n_header_params_handling($inputs) {
        if (!isset($inputs['opcode']) OR empty($inputs['opcode'])) {
            $error[] = "opcode key is required";
        }

        if (!isset($inputs['appversion']) OR empty($inputs['usmid'])) {
            $error[] = "appversion key is required";
        }
        if (isset($error)) {
            return_data(false, array_values($error)[0], array(), $error);
        }
        return true;
        /*
          post_check();
          $this->form_validation->set_rules('opcode', 'opcode', 'trim|required');
          $this->form_validation->run();
          $error = $this->form_validation->get_all_errors();
          if ($error) {
          return_data(false, array_values($error)[0], array(), $error);
          }
         */
    }

    //------**  CHECK OPCODE AND PASS THE PARAMS TO REQUIRED METHOD   ------------------//
    public function opcode_handling($inputs) {
        //$m = $inputs['opcode'].'($inputs)';	
        //$response = $this->$m;
        //return $response;
        //pre($inputs);
        if ($inputs['opcode'] == "testing") {
            $response = $this->testing($inputs);
        } else if ($inputs['opcode'] == "get_training_agencies") {
            $response = $this->get_training_agencies($inputs);
        } else if ($inputs['opcode'] == "get_training_courses") {
            $response = $this->get_training_courses($inputs);
        } else if ($inputs['opcode'] == "training_n_courses_mapping") {
            $response = $this->training_n_courses_mapping($inputs);
        } else if ($inputs['opcode'] == "training_n_courses_mapping_list") {
            $response = $this->training_n_courses_mapping_list($inputs);
        } else if ($inputs['opcode'] == "delete_courses_training_mapping") {
            $response = $this->delete_courses_training_mapping($inputs);
        } else if ($inputs['opcode'] == "cl_search") {
            $response = $this->cl_search($inputs);
        } else if ($inputs['opcode'] == "get_training_toggle_visibility") {
            $response = $this->get_training_toggle_visibility($inputs);
        } else if ($inputs['opcode'] == "update_training_toggle_visibility") {
            $response = $this->update_training_toggle_visibility($inputs);
        } else if ($inputs['opcode'] == "map_users_to_trainings") {
            $response = $this->map_users_to_trainings($inputs);
        } else if ($inputs['opcode'] == "list_of_mapping_users_to_trainings") {
            $response = $this->list_of_mapping_users_to_trainings($inputs);
        } else if ($inputs['opcode'] == "delete_users_mapping") {
            $response = $this->delete_users_mapping($inputs);
        } else if ($inputs['opcode'] == "sender_id_mapping_list") {
            $response = $this->sender_id_mapping_list($inputs);
        } else if ($inputs['opcode'] == "map_sender_id") {
            $response = $this->map_sender_id($inputs);
        } else if ($inputs['opcode'] == "delete_sender_id_mapping") {
            $response = $this->delete_sender_id_mapping($inputs);
        } else if ($inputs['opcode'] == "sender_id_list") {
            $response = $this->sender_id_list($inputs);
        } else if ($inputs['opcode'] == "add_sender_id") {
            $response = $this->add_sender_id($inputs);
        } else if ($inputs['opcode'] == "delete_sender_id") {
            $response = $this->delete_sender_id($inputs);
        } else if ($inputs['opcode'] == "edit_sender_id") {
            $response = $this->edit_sender_id($inputs);
        } else if ($inputs['opcode'] == "map_certificates_to_trainings") {
            $response = $this->map_certificates_to_trainings($inputs);
        } else if ($inputs['opcode'] == "list_of_mapping_certificates_to_trainings") {
            $response = $this->list_of_mapping_certificates_to_trainings($inputs);
        } else if ($inputs['opcode'] == "delete_certificates_mapping") {
            $response = $this->delete_certificates_mapping($inputs);
        } else if ($inputs['opcode'] == "certificates_list") {
            $response = $this->certificates_list($inputs);
        } else if ($inputs['opcode'] == "get_channel_list") {
            $response = $this->get_channel_list($inputs);
        } else if ($inputs['opcode'] == "master_channel_list") {
            $response = $this->master_channel_list($inputs);
        } else if ($inputs['opcode'] == "edit_channel") {
            $response = $this->edit_channel($inputs);
        } else if ($inputs['opcode'] == "get_wall_posting_users") {
            $response = $this->get_wall_posting_users($inputs);
        } else if ($inputs['opcode'] == "add_channel") {
            $response = $this->add_channel($inputs);
        } else if ($inputs['opcode'] == "delete_channels") {
            $response = $this->delete_channels($inputs);
        } else if ($inputs['opcode'] == "training_list") {
            $response = $this->training_list($inputs);
        } else if ($inputs['opcode'] == "add_training") {
            $response = $this->add_training($inputs);
        } else if ($inputs['opcode'] == "edit_training") {
            $response = $this->edit_training($inputs);
        } else if ($inputs['opcode'] == "get_training_instances") {
            $response = $this->get_training_instances($inputs);
        } else if ($inputs['opcode'] == "certificate_approves") {
            $response = $this->certificate_approves($inputs);
        } else if ($inputs['opcode'] == "certificate_approves_list") {
            $response = $this->certificate_approves_list($inputs);
        } else if ($inputs['opcode'] == "delete_certificate_approve") {
            $response = $this->delete_certificate_approve($inputs);
        } else if ($inputs['opcode'] == "add_wall_poster") {
            $response = $this->add_wall_poster($inputs);
        } else if ($inputs['opcode'] == "delete_wall_posters") {
            $response = $this->delete_wall_posters($inputs);
        }else if ($inputs['opcode'] == "users_list_by_role") {
            $response = $this->users_list_by_role($inputs);
        }else if ($inputs['opcode'] == "add_rights") {
            $response = $this->add_rights($inputs);
        }else if ($inputs['opcode'] == "list_of_mapping_rights") {
            $response = $this->list_of_mapping_rights($inputs);
        }else if ($inputs['opcode'] == "delete_instance_rights_mapping") {
            $response = $this->delete_instance_rights_mapping($inputs);
        }else if($inputs['opcode'] == "get_report"){
            $response = $this->get_report($inputs);
        }else if($inputs['opcode'] == "get_training_modules"){
            $response = $this->get_training_modules($inputs);
        }else {
            return_data(false, 'opcode was not matched', array());
        }
        return $response; 
    }

    public function get_training_modules($inputs){

        if(!isset($inputs['traningId']) OR empty($inputs['traningId'])){
            return array("status"=>false,"message"=>'Traning Id field is required.',"result"=>array());
        } 

       //$training = $this->Reports_model->checkTraning($inputs['traningId']);
        $Modules = $this->Reports_model->getTrainingInstanceModules($inputs['traningId']);
        
        if($Modules){

            return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>$Modules);
        }
          return array("status"=>false,"message"=>'No module found for this Training Instance.',"result"=>array());
    }

    public function get_report_old($inputs){

        if(!isset($inputs['traningId']) OR empty($inputs['traningId'])){
            return array("status"=>false,"message"=>'Traning Id field is required.',"result"=>array());
        } 
        if(!isset($inputs['moduleid']) OR empty($inputs['moduleid'])){
            return array("status"=>false,"message"=>'module Id field is required.',"result"=>array());
        }

       $check_training_instance = $this->Reports_model->checkTraning($inputs['traningId']);
       if($check_training_instance){

             $check_training_module = $this->Reports_model->checkTraningModule($inputs['moduleid'],$check_training_instance['trnrcmrefid']);

             //print_r($check_training_module);die;

            if($check_training_module){

             $user_submission =    $this->Reports_model->get_question_options($check_training_module['rcgrefid'],$check_training_instance['enddate']);
             $questions =   $this->Reports_model->get_questions($check_training_module['rcgrefid']);

             return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>array('user_answer'=>$user_submission,'ques'=>$questions));
            }else{

                return array("status"=>false,"message"=>'No module found on this Traning instance.',"result"=>array());
            }


       }else{

           return array("status"=>false,"message"=>'Invalid Traning instance.',"result"=>array());
       }

             echo "<pre>";print_r($check_training_instance);die;
    }
public function get_report($inputs){

        
    if(!isset($inputs['object_type']) OR empty($inputs['object_type'])){
        return array("status"=>false,"message"=>'object type  field is required.',"result"=>array());
    }
    if(!isset($inputs['object_entity_id']) OR empty($inputs['object_entity_id'])){
        return array("status"=>false,"message"=>'object type  field is required.',"result"=>array());
    }
    if(!isset($inputs['moduleid']) OR empty($inputs['moduleid'])){
       return array("status"=>false,"message"=>'module Id field is required.',"result"=>array());
    } 

    $questions =   $this->Reports_model->get_all_question_on_module($inputs['moduleid']);

    if($questions){
       

        if($inputs['object_type']=='TRN'){/*get take a test based on training or module */

           $training_instance = $this->Reports_model->checkTraning($inputs['object_entity_id']);


                if($training_instance) {

                    $trn_batch =   $this->Reports_model->get_take_a_test_on_training($training_instance['trnrefid'],$inputs['moduleid'],$training_instance['enddate'],$training_instance['startdate']);

                        if($trn_batch){

                            return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>array('user_answer'=>$trn_batch,'ques'=>$questions));
                        }else{
                            return array("status"=>false,"message"=>'This training has no users .',"result"=>array());
                        }
                }else{
                      return array("status"=>false,"message"=>'Invalid Training Instance.',"result"=>array());
                }

        }elseif($inputs['object_type']=='TRB'){/*get take a test for single batch */

                   $check_batch =   $this->Reports_model->check_training_batch($inputs['object_entity_id']); 

                if($check_batch){

                      $report_on_batch =   $this->Reports_model->get_take_a_test_on_batch($check_batch ['trbrefid'],$inputs['moduleid']);

                            if($report_on_batch){

                            return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>array('user_answer'=>$report_on_batch,'ques'=>$questions));
                            }else{

                                return array("status"=>false,"message"=>'This training has no users.',"result"=>array());
                            }
                }else{

                    return array("status"=>false,"message"=>'Invalid Batch.',"result"=>array());
                }
            
                

        }elseif($inputs['object_type']=='TSB'){ /*get take a test for super batch */

                $super_batch =  $this->Reports_model->check_super_batch($inputs['object_entity_id']);

                if($super_batch){
                        $report_on_super_batch= $this->Reports_model->get_take_a_test_on_super_batch($super_batch['tsbrefid'],$inputs['moduleid']);

                           if($report_on_super_batch){

                                  return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>array('user_answer'=>$report_on_super_batch,'ques'=>$questions));
                            }else{
                                return array("status"=>false,"message"=>'This training has no users.',"result"=>array());
                            }
                }else{
                     return array("status"=>false,"message"=>'Invalid Super Batch.',"result"=>array());
                }
        }
    }else{

        return array("status"=>false,"message"=>'No Post test available for this module.',"result"=>array());
     }

}
	public function users_list_by_role($input) {
        //$this->validate_get_training_instances();
        $result = $this->User_model->users_list_by_role($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
    //----------   GET LIST OF TRAINING Instances     ------------//
    public function get_training_instances($input) {
        //$this->validate_get_training_instances();
        $result = $this->Training_instances_model->get_training_instances($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    
    public function edit_channel($inputs) {

        if (!isset($inputs['chmname']) OR empty($inputs['chmname'])) {
            return array("status" => false, "message" => 'channel name is required.', "result" => array());
        }
        if (!isset($inputs['chmrefid']) OR empty($inputs['chmrefid'])) {
            return array("status" => false, "message" => 'chmrefid field is required.', "result" => array());
        }
        $result = $this->Wall_model->edit_channel($inputs);

        if ($result['msg'] == 'success') {
            $result = $this->Wall_model->get_channel_by_id($inputs['chmrefid']);
            return array("status" => true, "message" => 'Channel  Updated successfully.', "result" => $result);
        } elseif ($result['msg'] == 'exit') {
            return array("status" => false, "message" => "Channel already exists.", "result" => array());
        } elseif ($result['msg'] == 'exceed') {
            return array("status" => false, "message" => "File size must not exceed 10kb.", "result" => array());
        } elseif ($result['msg'] == 'invalid') {
            return array("status" => false, "message" => "Please select a valid image file (JPEG/JPG/PNG).", "result" => array());
        } else {
            return array("status" => false, "message" => 'Something went wrong.', "result" => array());
        }
    }

    //----------   ONLY FOR TESTING PURPOSE   ----------- //    
    public function testing() {
        //pre($_SERVER);        
        //pre(getallheaders()); die;
        $heaeds = $this->input->request_headers();
        pre($heaeds);
        die;
    }

    //----------GET LIST OF Documents by searching a keyword----------- //    
    public function cl_search($input) {
        //$client = ClientBuilder::create()->build();
        //pre($client);die;	
        //pre($input['searching_options']->cl_trainings);
        //pre($input);		die;
        $match_type = (isset($input['match_type']) && !empty($input['match_type'])) ? $input['match_type'] : 'match_phrase';
        //$searching_column = (isset($input['searching_column']) && !empty($input['searching_column'])) ? $input['searching_column'] : 'post_description';
        $pre_tag = (isset($input['pre_tag']) && !empty($input['pre_tag']) && isset($input['post_tag']) && !empty($input['post_tag'])) ? $input['pre_tag'] : '<em>';
        $post_tag = (isset($input['pre_tag']) && !empty($input['pre_tag']) && isset($input['post_tag']) && !empty($input['post_tag'])) ? $input['post_tag'] : '</em>';
        //$params['type']         = 'post';
        //$params['body']['from'] = (isset($input['offset']) && !empty($input['offset']))?$input['offset']:0;
        //$params['body']['size'] = (isset($input['size']) && !empty($input['size']))?$input['size']:10;
        //$params['body']['query']['bool']['must'][$match_type][$searching_column]    = $input['searching_keyword'];        
        $highlight = [
            'pre_tags' => [$pre_tag], // not required
            'post_tags' => [$post_tag], // not required
            'fields' => [
                "post_title" => new \stdClass(),
                "post_description" => new \stdClass()
            ],
            'require_field_match' => true
        ];

        /*
          $params['body']['query']['bool']['filter']  = [
          ["terms"=>["trbrefids"=>[15,27]]],
          ["terms"=>[$searching_column=>[$input['searching_keyword']]]]
          ];
          $params['body']['sort'] 					= [["popularity" => [ "order" => "desc"] ]  ];
          $params['body']['sort'] 					= ["popularity" => [ "order" => "desc"]];
         */

        #default response with no records found
        $no_search_found = "No posts found with searched text." . PHP_EOL . "Please note that this is a Beta version of Search feature, which currently allows searching for post text only. Make sure, you are searching for content inside posts to get the best results.";
        $final_response = [
                ["section" => "wall", "total" => 0, "message" => $no_search_found, "result" => array()],
                ["section" => "resources", "total" => 0, "message" => $no_search_found, "result" => array()],
                ["section" => "trainings", "total" => 0, "message" => $no_search_found, "result" => array()]
        ];


        #prepare default log params
        if (isset($input['appName']) && $input['appName'] == "EPMOB1.0") {
            $platform = "MOBILE";
        } else if (isset($input['appName']) && $input['appName'] == "IOS-CHALKLIT") {
            $platform = "MOBILE-IOS";
        } else {
            $platform = "WEB";
        }

        #wall search 
        if (isset($input['searching_options']['cl_wall']) && !empty($input['searching_options']['cl_wall'])) {
            $params = array();
            $params['body']['highlight'] = $highlight;
            //$params['body']['sort'] 					= ["post_id" => [ "order" => "asc"]];

            $params['index'] = 'cl_wall';
            $params['type'] = 'post';
            $params['body']['from'] = (isset($input['offset']) && !empty($input['offset'])) ? $input['offset'] : 0;
            $params['body']['size'] = (isset($input['size']) && !empty($input['size'])) ? $input['size'] : 3;
            //$params['body']['query']['bool']['must'][$match_type][$searching_column]    = $input['searching_keyword']; 

            $query = "SELECT GROUP_CONCAT( distinct `qutrefid`) as audiencesetid FROM `clt_aud_usm` WHERE `usmrefid`=" . $input['login_id'];
            $res = $this->db->query($query)->row_array();
            $audiencesetids = explode(',', $res['audiencesetid']);

            $params['body']['query']['bool']['filter']['terms']['audiencesetid'] = $audiencesetids;

            $params['body']['query']['bool']['must']['bool']['should'][]['match_phrase']['post_title'] = $input['searching_keyword'];
            $params['body']['query']['bool']['must']['bool']['should'][]['match_phrase']['post_description'] = $input['searching_keyword'];
            //$params['body']['query']['bool']['must']['bool']['must'][]['match']['audiencesetid'] = $audiencesetids; 
            /*
              {
              "query": {
              "bool": {
              "must": {
              "bool" : {
              "should": [
              { "match": { "title": "Elasticsearch" }},
              { "match": { "title": "Solr" }}
              ],
              "must": { "match": { "authors": "clinton gormely" }}
              }
              },
              "must_not": { "match": {"authors": "radu gheorge" }}
              }
              }
              } */

            $client = ClientBuilder::create()->build();
            try {
                $response = $client->search($params);
            } catch (Exception $e) {
                return array("status" => false, "message" => ELASTICSEARCH_ERROR_MESSAGE, "result" => array());
                //return array("status"=>false,"message"=>"Exception : ".$e->getMessage(),"result"=>array());
            }

            //$response 			= $client->search($params);
            if (isset($response['hits']['total']) && !empty($response['hits']['total'])) {
                $wall_posts = $response['hits']['hits'];
                $final_response[0]['total'] = $response['hits']['total'];
                $final_response[0]['message'] = "Searching records displayed successfully.";
                $final_response[0]['result'] = $response['hits']['hits'];
                //$final_response[] = array("section"=>"wall","total"=>$response['hits']['total'],"result"=>$response['hits']['hits']);
            }
            if (isset($input['offset']) && $input['offset'] == 0) {
                $search_log['usmrefid'] = $input['login_id'];
                $search_log['searchphrase'] = $input['searching_keyword'];
                $search_log['searchsection'] = 'cl_wall';
                $search_log['searchresultcount'] = (isset($response['hits']['total']) && !empty($response['hits']['total'])) ? $response['hits']['total'] : 0;
                $search_log['platform'] = $platform;
                $search_log['searchedon'] = date('Y-m-d H:i:s');
                $this->create_logs_for_search_api($search_log);
            }
        }
        if (isset($input['searching_options']['cl_resources']) && !empty($input['searching_options']['cl_resources'])) {
            $params = array();
            $params['body']['highlight'] = $highlight;
            //$params['body']['sort'] 					= ["post_id" => [ "order" => "asc"]];
            $params['index'] = 'cl_resources';
            $params['type'] = 'post';
            $params['body']['from'] = (isset($input['offset']) && !empty($input['offset'])) ? $input['offset'] : 0;
            $params['body']['size'] = (isset($input['size']) && !empty($input['size'])) ? $input['size'] : 10;
            //$params['body']['query']['bool']['must'][$match_type][$searching_column]    = $input['searching_keyword'];   
            $params['body']['query']['bool']['must']['bool']['should'][]['match_phrase']['post_title'] = $input['searching_keyword'];
            $params['body']['query']['bool']['must']['bool']['should'][]['match_phrase']['post_description'] = $input['searching_keyword'];

            $client = ClientBuilder::create()->build();
            //$response 			= $client->search($params);
            try {
                $response = $client->search($params);
            } catch (Exception $e) {
                return array("status" => false, "message" => ELASTICSEARCH_ERROR_MESSAGE, "result" => array());
                //return array("status"=>false,"message"=>"Exception : ".$e->getMessage(),"result"=>array());
            }
            if (isset($response['hits']['total']) && !empty($response['hits']['total'])) {
                $resources_posts = $response['hits']['hits'];
                $final_response[1]['total'] = $response['hits']['total'];
                $final_response[1]['message'] = "Searching records displayed successfully.";
                $final_response[1]['result'] = $response['hits']['hits'];
                //$final_response[] = array("section"=>"resources","total"=>$response['hits']['total'],"result"=>$response['hits']['hits']);
            }

            if (isset($input['offset']) && $input['offset'] == 0) {
                $search_log['usmrefid'] = $input['login_id'];
                $search_log['platform'] = "android"; //$input['login_id'];        
                $search_log['searchphrase'] = $input['searching_keyword'];
                $search_log['searchsection'] = 'cl_resources';
                $search_log['searchresultcount'] = (isset($response['hits']['total']) && !empty($response['hits']['total'])) ? $response['hits']['total'] : 0;
                $search_log['platform'] = $platform;
                $search_log['searchedon'] = date('Y-m-d H:i:s');
                $this->create_logs_for_search_api($search_log);
            }
        }
        if (isset($input['searching_options']['cl_trainings']) && !empty($input['searching_options']['cl_trainings'])) {
            $params = array();
            $params['body']['highlight'] = $highlight;
            //$params['body']['sort'] 					= ["post_id" => [ "order" => "asc"]];
            $params['index'] = 'cl_trainings';
            $params['type'] = 'post';
            //$params['body']['query']['bool']['must'][$match_type][$searching_column]    = $input['searching_keyword'];   
            $params['body']['query']['bool']['must']['bool']['should'][]['match_phrase']['post_title'] = $input['searching_keyword'];
            $params['body']['query']['bool']['must']['bool']['should'][]['match_phrase']['post_description'] = $input['searching_keyword'];


            //unset($params['body']['from'],$params['body']['size']);            

            $params['body']['from'] = 0;
            $params['body']['size'] = 10000;
            $client = ClientBuilder::create()->build();
            //$response 					= $client->search($params);
            try {
                $response = $client->search($params);
            } catch (Exception $e) {
                return array("status" => false, "message" => ELASTICSEARCH_ERROR_MESSAGE, "result" => array());
                //return array("status"=>false,"message"=>"Exception : ".$e->getMessage(),"result"=>array());
            }
            //pre($params);            pre($response);
            $hits = [];
            if (isset($response['hits']['total']) && !empty($response['hits']['total'])) {
                $query = "SELECT GROUP_CONCAT(distinct `rcutrbrefid`) as batchids FROM `currcu` WHERE `isactive`=1 and `rcuusmrefid`=" . $input['login_id'];
                $res = $this->db->query($query)->row_array();
                //pre($input['login_id'] = 388);//pre($res);
                $batchids = explode(',', $res['batchids']);

                foreach ($response['hits']['hits'] as $hit) {
                    $intersect = array_intersect($batchids, $hit['_source']['trbrefids']);
                    if (!empty($intersect)) {
                        $int_values = array_map('intval', array_values(array_intersect($batchids, $hit['_source']['trbrefids'])));
                        $hit['_source']['trbrefids_users'] = implode('|', $batchids);
                        $hit['_source']['trbrefids_docs'] = implode('|', $hit['_source']['trbrefids']);
                        $hit['_source']['trbrefids_intersact'] = implode('|', $int_values);
                        $hit['_source']['trbrefids'] = $int_values;
                        $hit['_source']['login_id'] = $input['login_id'];
                        $hits[] = $hit;
                    }
                }

                $from = (isset($input['offset']) && !empty($input['offset'])) ? $input['offset'] : 0;
                $size = (isset($input['size']) && !empty($input['size'])) ? $input['size'] : 3;
                $trainings_posts = array_slice($hits, $from, $size);
                $final_response[2]['total'] = count($hits); //$response['hits']['total'];
                $final_response[2]['message'] = "Searching records displayed successfully.";
                //$final_response[2]['result']  = $response['hits']['hits'];    
                $final_response[2]['result'] = $trainings_posts; //$response['hits']['hits'];    
                //$final_response[]             = array("section"=>"trainings","total"=>$response['hits']['total'],"result"=>$response['hits']['hits']);
            }
            if (isset($input['offset']) && $input['offset'] == 0) {
                $search_log['usmrefid'] = $input['login_id'];
                $search_log['platform'] = "android"; //$input['login_id'];        
                $search_log['searchphrase'] = $input['searching_keyword'];
                $search_log['searchsection'] = 'cl_trainings';
                $search_log['searchresultcount'] = (isset($response['hits']['total']) && !empty($response['hits']['total'])) ? $response['hits']['total'] : 0;
                $search_log['platform'] = $platform;
                $search_log['searchedon'] = date('Y-m-d H:i:s');
                $this->create_logs_for_search_api($search_log);
            }
            unset($params['body']['query']['bool']['filter']);
        }
        if (empty($wall_posts) && empty($resources_posts) && empty($trainings_posts)) {
            return array("status" => false, "message" => $no_search_found, "result" => array());
        }
        return array("status" => true, "message" => 'Search result displayed successfully.', "result" => $final_response);
    }

    //----------GET LIST OF TRAINING AGENCIES----------- //    
    public function get_training_agencies($input) {
        //$this->validate_get_training_agencies();
        $result = $this->Training_model->get_training_agencies($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    //----------   GET LIST OF TRAINING COURSES     ------------//
    public function get_training_courses($input) {
        //$this->validate_get_training_courses();
        $result = $this->Courses_model->get_training_courses($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
        
    //----------   MAP COURSES TO TRAINING AGENCIES    ----------- //
    public function training_n_courses_mapping($inputs) {  //echo json_encode($inputs);  	die; 
        $this->validate_training_n_courses_mapping($inputs);
        $result = $this->Courses_model->training_n_courses_mapping($inputs);
        //$last_query =  $this->db->last_query();//die;
        if ($result) {
            if ($result == true && !is_array($result)) {
                $result = array();
            }
            //pre($result);
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_training_n_courses_mapping($inputs) {
        if (!isset($inputs['trainings']) OR empty($inputs['trainings'])) {
            return_data(false, 'trainings is required.', array());
        }
        if (!isset($inputs['courses']) OR empty($inputs['courses'])) {
            return_data(false, 'courses is required.', array());
        }
    }

    //----------   GET LIST OF MAPPEING BETWEEN COURSES AND TRAINING     ----------- //
    public function training_n_courses_mapping_list($input) {//user_id
        //$this->validate_training_n_courses_mapping_list();
        $result = $this->Courses_model->training_n_courses_mapping_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping list displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function delete_courses_training_mapping($input) {
        //pre($input); die;
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Courses_model->delete_courses_training_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    //---------Change/Toggle TRAINING VISIBILITY------------------------//

    public function update_training_toggle_visibility($inputs) {
        $this->validate_update_training_toggle_visibility($inputs);
        $result = $this->Training_model->update_training_toggle_visibility($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Visibility has been updated successfully!', "result" => array());
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_update_training_toggle_visibility($inputs) {
        if (!isset($inputs['trnrefid']) OR empty($inputs['trnrefid'])) {
            return_data(false, 'trnrefid is required.', array());
        }
        if (!isset($inputs['isvisibleonclient']) OR ( empty($inputs['isvisibleonclient']) AND $inputs['isvisibleonclient'] != 0 )) {
            return_data(false, 'isvisibleonclient is required.', array());
        }
    }

    //---------Get LIST OF TRAINING VISIBILITY------------------------//
    public function get_training_toggle_visibility($input) { //user_id
        //$this->validate_get_training_toggle_visibility();
        $result = $this->Training_model->get_training_toggle_visibility($input);
        if ($result) {
            return array("status" => true, "message" => 'List displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    //----------   MAP USERS TO TRAINING AGENCIES    ----------- //
    public function map_users_to_trainings($inputs) { //pre($inputs);die;        
        $this->validate_map_users_to_trainings($inputs);
        $result = $this->Training_model->map_users_to_trainings($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_map_users_to_trainings($inputs) {
        if (!isset($inputs['trainings']) OR empty($inputs['trainings'])) {
            return_data(false, 'trainings is required in array format.', array());
        }
        if (!isset($inputs['user_mobiles']) OR empty($inputs['user_mobiles'])) {
            return_data(false, 'user_mobiles is required in array format.', array());
        }
        if (!isset($inputs['user_role']) OR empty($inputs['user_role'])) {
            return_data(false, 'user_role is required.', array());
        }
    }

    //list_of_mapping_users_to_trainings
    public function list_of_mapping_users_to_trainings($input) {//user_id
        //$this->validate_list_of_mapping_users_to_trainings();
        $result = $this->Training_model->list_of_mapping_users_to_trainings($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping list displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function validate_list_of_mapping_users_to_trainings($inputs) {
        
    }

    public function delete_users_mapping($input) {
        //pre($input); die;
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_model->delete_users_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    ////////////////////////SENDER ID MAPPING /////////////////////

    public function sender_id_mapping_list($input) {//user_id
        //$this->validate_sender_id_mapping_list();
        $result = $this->Training_model->sender_id_mapping_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping list displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function map_sender_id($inputs) { //pre($inputs);die;        
        $this->validate_map_sender_id($inputs);
        $result = $this->Training_model->map_sender_id($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_map_sender_id($inputs) {
        if (!isset($inputs['trainings']) OR empty($inputs['trainings'])) {
            return_data(false, 'trainings is required in array format.', array());
        }
        if (!isset($inputs['sender_ids']) OR empty($inputs['sender_ids'])) {
            return_data(false, 'sender_ids is required.', array());
        }
    }

    public function delete_sender_id_mapping($input) {
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_model->delete_sender_id_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function sender_id_list($input) {

        $result = $this->Training_model->sender_id_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    //////////////////////// CERTIFICATES TEMPLATES MAPPING /////////////////////

    public function list_of_mapping_certificates_to_trainings($input) {//user_id
        //$this->validate_certificates_mapping_list();
        $result = $this->Training_model->list_of_mapping_certificates_to_trainings($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping list displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function map_certificates_to_trainings($inputs) { //pre($inputs);die;        
        $this->validate_map_certificates_to_trainings($inputs);
        $result = $this->Training_model->map_certificates_to_trainings($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_map_certificates_to_trainings($inputs) {
        if (!isset($inputs['trainings']) OR empty($inputs['trainings'])) {
            return_data(false, 'trainings is required in array format.', array());
        }
        if (!isset($inputs['certificates']) OR empty($inputs['certificates'])) {
            return_data(false, 'certificates is required in array format.', array());
        }
    }

    public function delete_certificates_mapping($input) {
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_model->delete_certificates_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function certificates_list($input) {
        //$this->validate_sender_id_list();
        $result = $this->Training_model->certificates_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function get_channel_list($input) {
        //$this->validate_sender_id_list();
        $result = $this->Wall_model->get_channel_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function master_channel_list($input) {//pre($input);
        //$this->validate_sender_id_list();
        $result = $this->Wall_model->master_channel_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function get_wall_posting_users($input) {
        //$this->validate_sender_id_list();
        $result = $this->Wall_model->get_wall_posting_users($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function add_channel($input) {
        //pre($input); pre($_FILES);die;
        $this->validate_add_channel($input);
        $result = $this->Wall_model->add_channel($input); //pre($result);
        if ($result) {
            return array("status" => true, "message" => "Channel creation has been done as following", "result" => $result);
            //return_data(true, 'Success.', $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_add_channel($inputs) { //pre($inputs);
        if (!isset($inputs['chmname']) OR empty($inputs['chmname'])) {
            return_data(false, 'chmname is required in array format.', array());
        }
        if (!isset($inputs['chmdesc']) OR empty($inputs['chmdesc'])) {
            return_data(false, 'chmdesc is required in array format.', array());
        }
        if (!isset($inputs['chmusmrefids']) OR empty($inputs['chmusmrefids'])) {
            return_data(false, 'chmusmrefids is required in array format.', array());
        }
    }

    public function delete_channels($input) {
        //$this->validate_delete_courses_training_mapping();
        if (!isset($input['channel_ids']) OR empty($input['channel_ids'])) {
            return array("status" => false, "message" => 'channel_ids is required.', "result" => array());
        }
        $result = $this->Wall_model->delete_channels($input);
        if ($result) {
            return array("status" => true, "message" => 'Selected Channels deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    //------- LOG for api---------------------------------------------------//    
    public function activity_log($log_data) {
        $result = $this->Log_records_model->activity_log($log_data);
        return true;
    }

    //------- Logs for search api ---------------------------------------------------//    
    public function create_logs_for_search_api($log_data) {
        $result = $this->Log_records_model->create_logs_for_search_api($log_data);
        //$result                     = $this->create_elastic_logs_for_search_api($log_data);
        return true;
    }

    public function training_list($input) {
        $result = $this->Training_model->training_list($input);
        if ($result) {
            return array("status" => true, "message" => 'Data displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function create_elastic_logs_index_for_search_api($input = "") {
        $client = ClientBuilder::create()->build();
        $exist = $client->indices()->exists(array("index" => "cl_search"));
        if ($exist) {
            $response = $client->indices()->delete(array("index" => "cl_search"));
        }
        $exist = $client->indices()->exists(array("index" => "cl_search"));
        if (!$exist) {
            $mapping_json = '{
				"index_name"		: "cl_search",
				"type_name"			: "clt_his_search",
				"number_of_shards"	: "5",
				"number_of_replicas": "1",
				"fields" 			: [
					{"field_name":"searchrefid","field_type":"integer"},
					{"field_name":"usmrefid","field_type":"integer"},
					{"field_name":"platform","field_type":"text"},
					{"field_name":"searchphrase","field_type":"text"},
					{"field_name":"searchsection","field_type":"text"},
					{"field_name":"searchresultcount","field_type":"integer"},
					{"field_name":"searchedon","field_type":"date"}
				]
			}';
            $input = json_decode($mapping_json, true);
            foreach ($input['fields'] as $each) {
                $properties[$each['field_name']] = ["type" => $each['field_type']];
            }
            $params = [
                'index' => $input['index_name'],
                'body' => [
                    'settings' => [
                        'number_of_shards' => $input['number_of_shards'],
                        'number_of_replicas' => $input['number_of_replicas']
                    ],
                    'mappings' => [
                        $input['type_name'] => [
                            '_source' => [
                                'enabled' => true
                            ],
                            'properties' => $properties
                        ]
                    ]
                ]
            ];
        }
    }

    public function create_elastic_logs_for_search_api($input) {
        $client = ClientBuilder::create()->build();
        $body = array(
            "searchrefid" => (int) time() . rand(0001, 9999),
            "usmrefid" => (int) $input['usmrefid'],
            "platform" => $input['platform'],
            "searchphrase" => $input['searchphrase'],
            "searchsection" => $input['searchsection'],
            "searchresultcount" => $input['searchresultcount'],
            "searchedon" => $input['searchedon']
        );
        $params['index'] = 'cl_search';
        $params['type'] = 'clt_his_search';
        //$params['id'] 	= //$body['searchrefid'];
        $params['body'] = $body;
        $response = $client->index($params);
        //pre($params);
        return true;
    }

    public function add_training($inputs) {
        if (empty($_FILES['trasplash']['name'])) {
            return_data(false, 'trasplash is required in array format.', array());
        }
        if (empty($_FILES['tralogo']['name'])) {
            return array("status" => false, "message" => 'tralogo is required in array format.', "result" => array());
        }
        if (!isset($inputs['traname']) OR empty($inputs['traname'])) {
            return array("status" => false, "message" => 'traname is required.', "result" => array());
        }

        $result = $this->Training_model->get_training($inputs);
        if ($result) {
            return_data('fail', 'Training already exist.', array());
        }
        $result = $this->Training_model->add_training($inputs);
        if ($result['status'] == 'success') {
            $result = $this->Training_model->get_training($inputs);
            return array("status" => true, "message" => 'Training created successfully.', "result" => $result);
        } elseif ($result['status'] == 'fail') {
            return array("status" => false, "message" => $result['message'], "result" => array());
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function edit_training($inputs) {
        if (!isset($inputs['trarefid']) OR empty($inputs['trarefid'])) {
            return array("status" => false, "message" => 'trarefid is required.', "result" => array());
        }
        if (!isset($inputs['traname']) OR empty($inputs['traname'])) {
            return array("status" => false, "message" => 'training field is required.', "result" => array());
        }

        $exit = $this->Training_model->get_by_trarefid($inputs);

        if (!empty($exit)) {
            $check_duplicate = $this->Training_model->get_training($inputs);
            if ($check_duplicate) {
                return array("status" => false, "message" => 'Training already exists.', "result" => array());
            }
            $result = $this->Training_model->edit_training($inputs);

            if ($result['status'] == 'success') {
                return array("status" => true, "message" => 'Training created successfully.', "result" => $result['result']);
            } elseif ($result['status'] == 'fail') {
                return array("status" => false, "message" => $result['message'], "result" => array());
            }
        } else {
            return array("status" => false, "message" => 'trarefid not matched.', "result" => array());
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function add_sender_id($input) {
        if (!isset($input['sendercode']) OR empty($input['sendercode'])) {
            return array("status" => false, "message" => 'sendercode is required.', "result" => array());
        }
        $result = $this->Training_model->add_sender_id($input);
        if ($result['message'] == "false") {
            return array("status" => false, "message" => 'Sender Id already exists', "result" => array());
        } elseif ($result['message'] == "true") {
            return array("status" => true, "message" => 'Data Saved successfully.', "result" => array('sendercode' => $result['sendercode']));
        } else {
            return array("status" => false, "message" => 'Something went wrong', "result" => array());
        }
    }

    public function edit_sender_id($input) {

        if (!isset($input['senderrefid']) OR empty($input['senderrefid'])) {
            return array("status" => false, "message" => 'senderrefid is required.', "result" => array());
        }
        if (!isset($input['sendercode']) OR empty($input['sendercode'])) {
            return array("status" => false, "message" => 'sendercode is required.', "result" => array());
        }
        $result = $this->Training_model->edit_sender_id($input);
        if ($result['message'] == 'true') {
            $data = $this->Training_model->get_sender_id($input);
            return array("status" => true, "message" => 'Data Updated successfully.', "result" => $data);
        } elseif ($result['message'] == 'false') {
            return array("status" => false, "message" => 'Sender code already exists.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function delete_sender_id($input) {

        if (!isset($input['senderrefid']) OR empty($input['senderrefid'])) {
            return array("status" => false, "message" => 'senderrefid is required.', "result" => array());
        }

        $result = $this->Training_model->delete_sender_id($input);
        if ($result) {
            return array("status" => true, "message" => 'Data Removed successfully.', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    
    //----------   certificate approves   ----------- //
    public function certificate_approves($inputs) { 
        $this->validate_certificate_approves($inputs);
        $result = $this->Training_instances_model->certificate_approves($inputs);
        if ($result) {
            if ($result == true && !is_array($result)) {
                $result = array();
            }
            return array("status" => true, "message" => 'Mapping has been done successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
    
    public function validate_certificate_approves($inputs) {        
        if (!isset($inputs['trarefid']) OR empty($inputs['trarefid'])) {
            return_data(false, 'trarefid is required.', array());
        }
        if (!isset($inputs['trnrefid']) OR empty($inputs['trnrefid'])) {
            return_data(false, 'trnrefid is required.', array());
        }
        if (!isset($inputs['user_mobiles']) OR empty($inputs['user_mobiles']) OR is_array($inputs['user_mobiles'])== false) {
            return_data(false, 'user_mobiles is required in array format.', array());
        }
    }
    
    public function certificate_approves_list($inputs) { 
        //$this->validate_certificate_approves($inputs);
        $result = $this->Training_instances_model->certificate_approves_list($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Records has been displyed successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
    
    public function delete_certificate_approve($input) { 
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_instances_model->delete_certificate_approve($input);
        if ($result) {
            return array("status" => true, "message" => 'Data Removed successfully.', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
    
	/////////// Wall Posters  /////////////////////
	
	public function add_wall_poster($inputs) { 
        $this->validate_add_wall_poster($inputs);
		$role        = $this->Training_model->get_role(array("role"=>'CL-admin')); 
        if(empty($role)){
            return_data(false, 'Role could not match.', array());
        }
		$attribute        = $this->Training_model->get_attribute(array("attribute"=>'RIGHT_POST_EPC')); 
        if(empty($attribute)){
            return_data(false, 'attribute could not match.', array());
        }
		$inputs['rlmrefid'] = $role['rlmrefid'];
        $inputs['oearefid'] = $attribute['oearefid'];
        $result = $this->Wall_model->add_wall_poster($inputs);
        if ($result) {
            if ($result == true && !is_array($result)) {
                $result = array();
            }
            return array("status" => true, "message" => 'Wall user has been added successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
	
    public function validate_add_wall_poster($inputs) {        
        if (!isset($inputs['user_mobiles']) OR empty($inputs['user_mobiles']) OR is_array($inputs['user_mobiles'])== false) {
            return_data(false, 'user_mobiles is required in array format.', array());
        }
		
    }
	
	public function delete_wall_posters($input) {
        if (!isset($input['delete_ids']) OR empty($input['delete_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Wall_model->delete_wall_posters($input);
        if ($result) {
            return array("status" => true, "message" => 'Mapping deleted successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
	
	public function list_of_mapping_rights($inputs) { //pre($inputs);die;
		//return_data(false, 'usmrefid is required.', array());
		//$this->validate_list_of_mapping_rights($inputs);
        $result = $this->Training_instances_model->list_of_mapping_rights($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Records has been displyed successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
	
	public function add_rights($inputs) { 
        $this->validate_add_rights($inputs);
		$result = $this->Training_instances_model->add_rights($inputs);
        if ($result) {
            if ($result == 1) {
				return array("status" => true, "message" => 'Rights has been added successfully!', "result" => array());
            }elseif ($result == 2) {
				return array("status" => true, "message" => 'Rights has been added successfully!', "result" => array());
			}else{
				return array("status" => false, "message" => 'Rights already exist!', "result" => array());
			}
            
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
	
	public function validate_add_rights($inputs) {        
        if (!isset($inputs['usmrefid']) OR empty($inputs['usmrefid'])) {
            return_data(false, 'usmrefid is required.', array());
        }
		if (!isset($inputs['trnrefid']) OR empty($inputs['trnrefid'])) {
            return_data(false, 'trnrefid is required.', array());
        }
		
    }
	
	public function delete_instance_rights_mapping($input) {
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_instances_model->delete_instance_rights_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Rights removed successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
}
