<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Training_visibility extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Training_instances_model');
        $this->load->model('Training_model');
        $this->load->model('Courses_model');
        $this->load->model('Wall_model');
        $this->load->model('User_model');
        $this->load->model('Reports_model');
        $this->load->model('Log_records_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }
	
	//---------Change/Toggle TRAINING VISIBILITY------------------------//

    public function update_training_toggle_visibility($inputs) {
        $this->validate_update_training_toggle_visibility($inputs);
        $result = $this->Training_model->update_training_toggle_visibility($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Visibility has been updated successfully!', "result" => array());
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }

    public function validate_update_training_toggle_visibility($inputs) {
        if (!isset($inputs['trnrefid']) OR empty($inputs['trnrefid'])) {
            return_data(false, 'trnrefid is required.', array());
        }
        if (!isset($inputs['isvisibleonclient']) OR ( empty($inputs['isvisibleonclient']) AND $inputs['isvisibleonclient'] != 0 )) {
            return_data(false, 'isvisibleonclient is required.', array());
        }
    }

    //---------Get LIST OF TRAINING VISIBILITY------------------------//
    public function get_training_toggle_visibility($input) { //user_id
        //$this->validate_get_training_toggle_visibility();
        $result = $this->Training_model->get_training_toggle_visibility($input);
        if ($result) {
            return array("status" => true, "message" => 'List displyed successfully.', "result" => $result);
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }

    public function clt_get_comments_by_object($inputs){
       
        if(!isset($inputs['objtype']) OR empty($inputs['objtype'])){
            return array("status"=>false,"message"=>'objtype  field is required.',"result"=>array());
        }
        if(!isset($inputs['objid']) OR empty($inputs['objid'])){
            return array("status"=>false,"message"=>'objid  field is required.',"result"=>array());
        }
        if(!isset($inputs['fromdate']) OR empty($inputs['fromdate'])){
            return array("status"=>false,"message"=>'fromdate  field is required.',"result"=>array());
        }
        if(!isset($inputs['todate']) OR empty($inputs['todate'])){
            return array("status"=>false,"message"=>'todate  field is required.',"result"=>array());
        }
        $objtype  = $inputs['objtype'];
        $objid    = $inputs['objid'];
        $fromdate = $inputs['fromdate'];
        $todate   = $inputs['todate'];
       
        $this->db->db_debug = false;
        $query = $this->db->query("CALL ChalkLit_Get_Comments_By_Object('$objtype','$objid','$fromdate','$todate')");

        if(!@$this->db->query($query)){

          return array("status"=>false,"message"=>'Server error.',"result"=>array());  
        }
        $comments =  $query->result_array();
        $i=0;$result_array=[];
        foreach ($comments as $key => $value) {

            $result_array[$i]['user_name']         = $value['User Name'];
            $result_array[$i]['mobile']            = intval($value['Mobile']);
            $result_array[$i]['training_instance'] = $value['Training Instance'];
            $result_array[$i]['super_batch_name']  = $value['SB Name'];
            $result_array[$i]['batch_name']        = $value['Batch Name'];
            $result_array[$i]['module_number']     = intval($value['Module Number']);
            $result_array[$i]['post_number']       = intval($value['Post Number']);
            $result_array[$i]['comment_date']      = $value['Date of Comment'];
            $result_array[$i]['comment']           = $value['Comment'];
            $result_array[$i]['url']               = $value['URL'];
            $result_array[$i]['category']          = $value['Category'];
            $result_array[$i]['is_committed']      = $value['Is Committed'];
            $i++;
        }
        
        return array("status" => true, "message" => 'Comments Fetched successfully.', "result" => $result_array);
    }

    public function clt_get_data_tra($inputs){

       $tralist= $this->Training_model->training_list();
     
   

       $i=0;
       foreach ($tralist as $key => $tra) {

        $course                          =  $this->Training_model->getTraCourses($tra['trarefid']);
        $certificate                     =  $this->Training_model->getTraCertificateMapping($tra['trarefid']);
        $sender                          =  $this->Training_model->getTraSenderMapping($tra['trarefid']);
        $user                            =  $this->Training_model->getTraUserMapping($tra['trarefid']);
        $numer_of_ti                     =  $this->Training_model->getTrnInstance($tra['trarefid']);
        $numerof_teacher_trained         =  $this->Training_model->getTrnTotalTeacherTrained($tra['trarefid']);

       
        $data[$i]['name']                = $tra['traname'];
        $data[$i]['tralogo']             = TARGET_PATH_TO_DISPLAY_TRA_LOGO.$tra['tralogo'];
        $data[$i]['trasplash']           = TARGET_PATH_TO_DISPLAY_TRA_SPLASH.$tra['trasplash'];
        $data[$i]['tra_course']          = $course;
        $data[$i]['tra_certificate']     = $certificate;
        $data[$i]['tra_sender']          = $sender;
        $data[$i]['tra_user']            = $user;
        $data[$i]['number_of_ti']        = intval($numer_of_ti['ti']);
        $data[$i]['teacher_trained']     = intval($numerof_teacher_trained['trainee']);

          $i++;
       }
       return array("status" => true, "message" => 'Training agencies data fetched sucessfully.', "result" => array('tra'=>$data) );
    }

	
}
