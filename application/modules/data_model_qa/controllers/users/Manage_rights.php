<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_rights extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Training_instances_model');
        $this->load->model('Training_model');
        $this->load->model('Courses_model');
        $this->load->model('Wall_model');
        $this->load->model('User_model');
        $this->load->model('Reports_model');
        $this->load->model('Log_records_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }
	
	
	public function list_of_mapping_rights($inputs) { //pre($inputs);die;
		//return_data(false, 'usmrefid is required.', array());
		//$this->validate_list_of_mapping_rights($inputs);
        $result = $this->Training_instances_model->list_of_mapping_rights($inputs);
        if ($result) {
            return array("status" => true, "message" => 'Records has been displyed successfully!', "result" => $result);
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
	
	public function add_rights($inputs) { 
        $this->validate_add_rights($inputs);
		$result = $this->Training_instances_model->add_rights($inputs);
        if ($result) {
            if ($result == 1) {
				return array("status" => true, "message" => 'Rights has been added successfully!', "result" => array());
            }elseif ($result == 2) {
				return array("status" => true, "message" => 'Rights has been added successfully!', "result" => array());
			}else{
				return array("status" => false, "message" => 'Rights already exist!', "result" => array());
			}
            
        }
        return array("status" => false, "message" => 'Something went wrong.', "result" => array());
    }
	
	public function validate_add_rights($inputs) {        
        if (!isset($inputs['usmrefid']) OR empty($inputs['usmrefid'])) {
            return_data(false, 'usmrefid is required.', array());
        }
		if (!isset($inputs['trnrefid']) OR empty($inputs['trnrefid'])) {
            return_data(false, 'trnrefid is required.', array());
        }
		
    }
	
	public function delete_instance_rights_mapping($input) {
        if (!isset($input['mapping_ids']) OR empty($input['mapping_ids'])) {
            return array("status" => false, "message" => 'mapping_ids is required.', "result" => array());
        }
        $result = $this->Training_instances_model->delete_instance_rights_mapping($input);
        if ($result) {
            return array("status" => true, "message" => 'Rights removed successfully', "result" => array());
        }
        return array("status" => false, "message" => 'No record found', "result" => array());
    }
	
}
