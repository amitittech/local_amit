<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Elasticsearch\ClientBuilder;

require APPPATH . 'third_party/elasticsearch/vendor/autoload.php';

class Testing extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        //$this->load->model('Training_model');
        $this->load->helper('custom');
        //$this->load->helper('Services');
    }
	
	public function my_testing() {
		$inputs 		= check_inputs();
		return_data(false,"custom testing message",$inputs);
	}

    public function get_settings() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        $client = ClientBuilder::create()->build();
        $response = $client->indices()->getSettings($input); //$client->index($input);
        echo json_encode($response);
    }

    public function get_mapping() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            //return_data("fail", "Please pass index.", array());
        }
        $client = ClientBuilder::create()->build();
        $response = $client->indices()->getMapping(array("index"=>$input['index'])); //$client->index($input);
        echo json_encode($response);
    }

    public function testing() {
        $arr = [
            "appversion" => "1.2",
            "opcode" => "elasticsearch",
            "auth_token" => "22f9814d-yfyfy",
            "searching_keyword" => "hello",
            "searching_options" => array("cl_wall" => 1, "cl_resources" => 0, "cl_trainings" => 1)
        ];
        pre($arr);
        $wall = [["col1" => "val1", "col2" => "val2"], ["col1" => "val1", "col2" => "val2"]];
        $resources = [["col1" => "val1", "col2" => "val2"], ["col1" => "val1", "col2" => "val2"]];
        $trainings = [["col1" => "val1", "col2" => "val2"], ["col1" => "val1", "col2" => "val2"]];

        $post['wall'] = $wall;
        $post['trainings'] = $trainings;
        $post['resources'] = $resources;

        $response['status'] = "success";
        $response['message'] = "Data displyed successfully.";
        $response['Result'] = $post;
        pre($response);
        echo json_encode($response);
    }

    public function sink_wall() {
        $tt = time();
        $this->db->select('chprefid,chptopic,chpcontent,chmname,chmlogo,chpqutrefid');
        $this->db->where('escchp.isdeleted', 0);
        //$this->db->where('escchm.isdeleted',0);
        $this->db->join('escchm', 'escchm.chmrefid=escchp.chpchmrefid');
        $result 	= $this->db->get('escchp')->result_array();
        $i 			= 1;
        foreach ($result as $each) {
            $i 						= $i + 1;
            $body = array(
                "post_id" 			=> (int)$each['chprefid'],
                "post_title" 		=> $each['chptopic'],
                "post_description" 	=> $each['chpcontent'],
                "channel_name" 		=> $each['chmname'],
                "channel_logo" 		=> (!empty($each['chmlogo'])) ? "http://www.qa.chalklit.in/chalklitone/resources/newhome/images/" . $each['chmlogo'] : "",
                "audiencesetid" 	=> (int)$each['chpqutrefid']
            );
            $params['index'] 	= 'cl_wall';
            $params['type'] 	= 'post';
            $params['id'] 		= $each['chprefid'];
            $params['body'] 	= $body;
            
            $client = ClientBuilder::create()->build();
            $response = $client->index($params);
            //pre($response);die;
        }
        $response = array("time_taken_in_seconds" => time() - $tt, "total records" => $i);
        echo json_encode($response);
        //{"time_taken_in_seconds":8,"total records":982}
    }

    public function sink_resources() {
        /*
          case
          when type = 'video' then concat('http:',thumbnailurl)
          else ifnull(thumbnailurl,'')
          end `postlogo`
         */
        $tt = time();
        $this->db->select("rchrefid,title,description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,isfortraining,rcgrefid,groupname,case
  when type = 'video' then concat('http:',thumbnailurl)
  else ifnull(thumbnailurl,'')    
end postlogo");
        $this->db->where('currcg.isdeleted', 0);
        $this->db->where('currch.isdeleted', 0);
        $this->db->where('currcm.isfortraining', 0);
        $this->db->where('currcm.isforreference', 0);
        $this->db->where('currcm.islive', 1);
        $this->db->join('currcg', 'currcg.rcgrcmrefid=currcm.rcmrefid');
        $this->db->join('currch', 'currch.rchrcgrefid=currcg.rcgrefid');
        $result = $this->db->get('currcm')->result_array();
        //pre($result);die;
        $i = 1;
        foreach ($result as $each) {
            $i 					= $i + 1;
            $params['index'] 	= 'cl_resources';
            $params['type'] 	= 'post';
            $params['id'] 		= $each['rchrefid'];
            $params['body'] 	= [
                "post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
                "post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
                "post_description" 	=> (isset($each['description']) && !empty($each['description'])) ? $each['description'] : "",
                "chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
                "chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
                "class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
                "subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
                "language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
                "module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
                "module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
                "chapter_logo" 		=> "",
                "module_logo" 		=> "",
                "post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : ""
            ];
            $client 	= ClientBuilder::create()->build();
            $response 	= $client->index($params);
        }
        $response 		= array("time_taken_in_seconds" => time() - $tt, "total records" => $i);
        echo json_encode($response);
        //{"time_taken_in_seconds":28,"total records":3818}
    }

    public function sink_trainings() {
        $tt = time();
        $this->db->select("rchrefid,title,description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,rcgrefid,groupname,case
  when type = 'video' then concat('http:',thumbnailurl) else ifnull(thumbnailurl,'') end postlogo");
        $this->db->where('currcg.isdeleted', 0);
        $this->db->where('currch.isdeleted', 0);
        $this->db->where('currcm.isfortraining', 1);
        $this->db->where('`currcm`.`rcmrefid` IN (select distinct rcurcmrefid from currcu A join curtrb B on (A.rcutrbrefid = B.trbrefid) where A.isactive = 1 and B.isdeleted = 0)',NULL,FALSE);
        $this->db->join('currcg', 'currcg.rcgrcmrefid=currcm.rcmrefid');
        $this->db->join('currch', 'currch.rchrcgrefid=currcg.rcgrefid');
        $result = $this->db->get('currcm')->result_array();
        //echo $this->db->last_query()."<br>"; 
        $i = 1;
        //pre($result);//die;
        foreach ($result as $each) {
            //$q = "SELECT distinct rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $each['rcmrefid'] . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";
            $q = "SELECT rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $each['rcmrefid'] . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";            
            
            $res = $this->db->query($q)->row_array();
            // $this->db->last_query()."<br>";  pre($res);             die;
            //continue;
            if (!$res OR empty($res['trbrefids'])) {
                continue;
            }
            //$res['trbrefids'] = array("a"=>10);
            $trb = (object)array_map('intval', explode(',', $res['trbrefids']));
            
            //$trb = (object)$trb;
            $i = $i + 1;
            $params['index'] 	= 'cl_trainings';
            $params['type'] 	= 'post';
            $params['id'] 		= $each['rchrefid'];
            $params['body'] 	= [
                "post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
                "post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
                "post_description" 	=> (isset($each['description']) && !empty($each['description'])) ? $each['description'] : "",
                "chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
                "chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
                "class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
                "subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
                "language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
                "module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
                "module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
                "trbrefids" 		=> $trb,
                "chapter_logo" 		=> "",
                "module_logo" 		=> "",
                "post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : ""
            ];
            //pre($params);
            $client = ClientBuilder::create()->build();
            $response = $client->index($params);
            //echo json_encode($response); die;
        }
        //echo json_encode($response);
        $response = array("time_taken_in_seconds" => time() - $tt, "total records" => $i);
        echo json_encode($response);
        //{"time_taken_in_seconds":8,"total records":883}
    }

    public function bulk() {
        foreach ($result as $each) {
            $params['body'][] = [
                'index' => [
                    '_index' => 'cl_resourses',
                    '_type' => 'post',
                ]
            ];
            $params['body'][] = [
                'my_field' => 'my_value',
                'second_field' => 'some more values'
            ];
        }
        $client = ClientBuilder::create()->build();
        $response = $client->bulk($params);
        echo json_encode($response);
    }

    public function add_document() {
        $params = $this->validate_add_document();

        $client = ClientBuilder::create()->build();
        $response = $client->index($params);
        echo json_encode($response);
    }

    public function validate_add_document() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        if (!isset($input['type']) OR empty($input['type'])) {
            return_data("type", "Please pass type.", array());
        }
        if (!isset($input['body']) OR empty($input['body'])) {
            return_data("fail", "Please pass body parameters.", array());
        }
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        $params['body'] = $input['body'];
        if (isset($input['id']) && !empty($input['id'])) {
            $params['id'] = $input['id'];
        }
        return $params;
    }

    public function get_document_by_id() {
        $params = $this->validate_get_document_by_id();

        $client = ClientBuilder::create()->build();
        $response = $client->get($params);

        echo json_encode($response);
    }

    public function validate_get_document_by_id() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        if (!isset($input['type']) OR empty($input['type'])) {
            return_data("type", "Please pass type.", array());
        }
        if (!isset($input['id']) OR empty($input['id'])) {
            return_data("fail", "Please pass id.", array());
        }
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        $params['id'] = $input['id'];
        return $params;
    }

    public function delete_document_by_id() {
        $params = $this->validate_delete_document_by_id();

        $client = ClientBuilder::create()->build();
        $response = $client->delete($params);

        echo json_encode($response);
    }

    public function validate_delete_document_by_id() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        if (!isset($input['type']) OR empty($input['type'])) {
            return_data("type", "Please pass type.", array());
        }
        if (!isset($input['id']) OR empty($input['id'])) {
            return_data("fail", "Please pass id.", array());
        }
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        $params['id'] = $input['id'];
        return $params;
    }

    public function search_documents_by_a_column() {
		//$dataa = file_get_contents("http://localhost:9200/cl_post_comments/_search");
		//pre(json_decode($dataa,true));
		//die;
        $params = $this->validate_search_documents_by_a_column();
        //$params['body']['from'] = 0;
        //$params['body']['size'] = 3;
        //$params['body']['query']['match_phrase']['post_description'] = $params['searching_val'];
        //$params['body']['query']['bool']['terms']['audiencesetid'] = $params['audiencesetids'];
        //$params['body']['query']['term'][$params['searching_col']] = $params['searching_val'];
        //$params['body']['query']['bool']['must']['match_phrase'][$params['searching_col']] = $params['searching_val'];
        /*
        if(isset($params['audiencesetids'])){
            foreach($params['audiencesetids'] as $audiencesetid){
                $params['body']['query']['terms']['audiencesetid'] = $params['audiencesetids'];
            }
        }*/
        //pre($params); //die;
        //$params['body']['highlight']['fields']['content'] = '{}';
		$params['body']['query']['match'][$params['searching_col']] = $params['searching_val'];
        unset($params['searching_col'], $params['searching_val'],$params['audiencesetids']);
        $client = ClientBuilder::create()->build();
        $response = $client->search($params);
        //pre($response);
        echo json_encode($response);
    }

    public function validate_search_documents_by_a_column() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        //pre($input); die;
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        if (!isset($input['type']) OR empty($input['type'])) {
            return_data("fail", "Please pass type.", array());
        }
        if (!isset($input['searching_col']) OR empty($input['searching_col'])) {
            return_data("fail", "Please pass searching_col.", array());
        }
        if (!isset($input['searching_val']) OR empty($input['searching_val'])) {
            return_data("fail", "Please pass searching_val.", array());
        }
        return $input;
    }

    public function search_documents_with_and_operator() {
        $params = $this->validate_search_documents_with_and_operator();

        $client = ClientBuilder::create()->build();
        $response = $client->search($params);

        echo json_encode($response);
    }

    public function validate_search_documents_with_and_operator() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        if (!isset($input['type']) OR empty($input['type'])) {
            return_data("fail", "Please pass type.", array());
        }
        if (!isset($input['multi_search']) OR empty($input['multi_search'])) {
            return_data("fail", "Please pass multi_search.", array());
        }
        if (!is_array($input['multi_search'])) {
            return_data("fail", "Please pass multi_search as an array.", array());
        }
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        foreach ($input['multi_search'] as $each) {
            $params['body']['query']['bool']['must'][]['match'][$each['searching_col']] = $each['searching_val'];
        }
        return $params;
    }

    public function create_index_with_mappings($input = "") {
		if(!empty($input)){
		}else{
			$json 	= file_get_contents('php://input');
			$input 	= json_decode($json, true);
		}
        
        foreach($input['fields'] as $each){
            $properties[$each['field_name']] = ["type"=>$each['field_type']]; 
        }
        $params = [
                    'index' => $input['index_name'],
                    'body' => [
                        'settings' => [
                            'number_of_shards' => $input['number_of_shards'],
                            'number_of_replicas' => $input['number_of_replicas']
                        ],
                        'mappings' => [
                            $input['type_name'] => [
                                '_source' => [
                                    'enabled' => true
                                ],
                                'properties' =>$properties
                                /*'properties' => [
                                    'post_id' => ['type' => 'long'],
                                    'post_title' => ['type' => 'keyword'],
                                    'post_description' => ['type' => 'text'],
                                    'channel_name' => ['type' => 'keyword'],
                                    'channel_name' => ['type' => 'keyword'],
                                    'audiencesetid' => ['type' => 'integer']
                                ]*/
                            ]
                        ]
                    ]
                ];
        //pre($params);die;
                        
        $client = ClientBuilder::create()->build();
        $response = $client->indices()->create($params);
        echo json_encode($response);
        
        /*
        pre($input); die;
        $params['index'] = $input['index'];
        if(isset($input['settings']) && !empty($input['settings'])){
            $params['body']['settings']['number_of_shards']     = $input['settings']['number_of_shards'];
            $params['body']['settings']['number_of_replicas']     = $input['settings']['number_of_replicas'];
        }
        if(isset($input['mappings']) && !empty($input['mappings'])){
            if(isset($input['mappings']['properties']) && !empty($input['mappings']['properties'])){
                foreach ($input['mappings']['properties'] as $each){
                    //$properties = array("name"=>,"type"=>);
                }
            }
            $params['body']['mappings'][$input['mappings']['type']]['_source'] ['enabled']    = $input['mappings']['source_enabled'];
            $params['body']['mappings']['properties']     = $input['settings']['number_of_replicas'];
        }*/
    }

    public function create_index() {
        $params = $this->validate_create_index();
        $client = ClientBuilder::create()->build();
        $response = $client->indices()->create($params);
        echo json_encode($response);
    }

    public function validate_create_index() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        $params['index'] = $input['index'];
        return $params;
    }

    public function delete_index() {
        $params = $this->validate_delete_index();
        $client = ClientBuilder::create()->build();
        $response = $client->indices()->delete($params);
        echo json_encode($response);
    }

    public function validate_delete_index() {
        $json = file_get_contents('php://input');
        $input = json_decode($json, true);
        if (!isset($input['index']) OR empty($input['index'])) {
            return_data("fail", "Please pass index.", array());
        }
        $params['index'] = $input['index'];
        return $params;
    }

    // ELASTICSEARCH METHODS ///////////////////////////////////////////////////////////

    function elastic_active_records($input, $action) {
        $client = ClientBuilder::create()->build();
        return $response = $client->index($input);
    }

    function elastic_add_document($input) {
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        $params['body'] = $input['body'];
        if (isset($input['id'])) {
            $params['id'] = $input['id'];
        }
        $client = ClientBuilder::create()->build();
        return $response = $client->index($input);
    }

    function elastic_get_document_by_id($input) {
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        $params['id'] = $input['id'];
        $client = ClientBuilder::create()->build();
        return $response = $client->get($params);
    }

    function elastic_delete_document_by_id($input) {
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        $params['id'] = $input['id'];
        $client = ClientBuilder::create()->build();
        return $response = $client->delete($params);
    }

    function elastic_search_documents_by_a_column($input) {
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        $params['body']['from'] = 0;
        $params['body']['size'] = 2;
        $params['body']['query']['match'][$input['searching_col']] = $input['searching_val'];

        $client = ClientBuilder::create()->build();
        return $response = $client->search($params);
    }

    function elastic_search_documents_with_and_operator($input) { //pre($input); die;	
        $params['index'] = $input['index'];
        $params['type'] = $input['type'];
        foreach ($input['multi_search'] as $each) {
            $params['body']['query']['bool']['must'][]['match'][$each['searching_col']] = $each['searching_val'];
        }
        $client = ClientBuilder::create()->build();
        return $response = $client->search($params);
    }

    function elastic_delete_index($input) {
        //$params['index'] 	= $input['index'];
        $client = ClientBuilder::create()->build();
        return $response = $client->indices()->delete($input);
    }
	
	// BULK INSERTION
	/*
		$params = ['body' => []];

		for ($i = 1; $i <= 1234567; $i++) {
			$params['body'][] = [
				'index' => [
					'_index' => 'my_index',
					'_type' => 'my_type',
					'_id' => $i
				]
			];

			$params['body'][] = [
				'my_field' => 'my_value',
				'second_field' => 'some more values'
			];

			// Every 1000 documents stop and send the bulk request
			if ($i % 1000 == 0) {
				$responses = $client->bulk($params);
				// erase the old bulk request
				$params = ['body' => []];
				// unset the bulk response when you are done to save memory
				unset($responses);
			}
		}

		// Send the last batch if it exists
		if (!empty($params['body'])) {
			$responses = $client->bulk($params);
		}
	*/
	
	public function get_elasticsearch_logs(){
		$input = $this->input->post();
		$params = [
			'index' => 'cl_search',
			'type'  => 'clt_his_search'
		];
		$client 					= ClientBuilder::create()->build();
		$cl_trainings 				= $client->search($params);
		$final_response['searching_phrases']['count'] = $cl_trainings['hits']['total'];
		$final_response['searching_phrases']['result'] = $cl_trainings['hits']['hits'];
		$query  = '{
			"bool": {
				"must": {
					"bool" : {
						"must": { "match": { "searchsection": "cl_wall" }} 
					}
				}
			}
		}';		
		$params['body']['query'] 	= json_decode($query,true); //pre($params);
		$client 					= ClientBuilder::create()->build();
		$cl_trainings 				= $client->search($params);
		
		$final_response['searching_phrases']['wall_count'] = $cl_trainings['hits']['total'];
		$final_response['searching_phrases']['wall_result'] = $cl_trainings['hits']['hits'];
		//echo json_encode($final_response);
		$query  = '{
			"bool": {
				"must": {
					"bool" : {
						"must": { "match": { "searchsection": "cl_trainings" }} 
					}
				}
			}
		}';		
		$params['body']['query'] 	= json_decode($query,true); //pre($params);
		$client 					= ClientBuilder::create()->build();
		$cl_trainings 				= $client->search($params);
		
		$final_response['searching_phrases']['trainings_count'] = $cl_trainings['hits']['total'];
		$final_response['searching_phrases']['trainings_result'] = $cl_trainings['hits']['hits'];
		//echo json_encode($final_response);
		$query  = '{
			"bool": {
				"must": {
					"bool" : {
						"must": { "match": { "searchsection": "cl_resources" }} 
					}
				}
			}
		}';		
		$params['body']['query'] 	= json_decode($query,true); //pre($params);
		$client 					= ClientBuilder::create()->build();
		$cl_trainings 				= $client->search($params);
		
		$final_response['searching_phrases']['resources_count'] = $cl_trainings['hits']['total'];
		$final_response['searching_phrases']['resources_result'] = $cl_trainings['hits']['hits'];
		echo json_encode($final_response);
		
		
		//$final_response
		/*
		$query  = '{
			"bool": {
				"must": {
					"bool" : { 
						"should": [
							{ "match": { "title": "Elasticsearch" }},
							{ "match": { "title": "Solr" }} 
						],
						"must": { "match": { "authors": "clinton gormely" }} 
					}
				},
				"must_not": { "match": {"authors": "radu gheorge" }}
			}
		}';
		*/
		
	}
	
	public function add_bulk_records(){ 
		$client = ClientBuilder::create()->build();
		$params = ['body' => []];
		for ($i = 11; $i <= 20; $i++) {
			$params['body'][] = [
				'index' => [
					'_index'	=> 'cl_search',
					'_type' 	=> 'clt_his_search',
					'_id' => $i
				]
			];
			$params['body'][] = [
				"searchrefid" 		=> (int)time(),
				"usmrefid" 			=> (int)'usmrefid'.time(),
				"platform" 			=> 'platform'.time(),
				"searchphrase" 		=> 'searchphrase'.time(),
				"searchsection" 	=> 'searchsection'.time(),
				"searchresultcount" => time(),		
				"searchedon" 		=> date('Y-m-d H:i:s')
			];
			// Every 1000 documents stop and send the bulk request
			if ($i % 1000 == 0) {
				$responses = $client->bulk($params);
				// erase the old bulk request
				$params = ['body' => []];
				// unset the bulk response when you are done to save memory
				unset($responses);
			}
		}

		// Send the last batch if it exists
		if (!empty($params['body'])) {
			$responses = $client->bulk($params);
		}
		
		pre($responses); 
		/*
		$body = array(
			"searchrefid" 		=> (int)time(),
			"usmrefid" 			=> (int)$input['usmrefid'],
			"platform" 			=> $input['platform'],
			"searchphrase" 		=> $input['searchphrase'],
			"searchsection" 	=> $input['searchsection'],
			"searchresultcount" => $input['searchresultcount'],			
			"searchedon" 		=> $input['searchedon']
		);		
		$params = [
			'index' => 'cl_search',
			'type' 	=> 'clt_his_search'
		];
		$client = ClientBuilder::create()->build();
		$response = $client->search($params);
		echo json_encode($response);
		*/
	}
	
	/*
	public function create_index_with_mappings($input) {
        foreach($input['fields'] as $each){
            $properties[$each['field_name']] = ["type"=>$each['field_type']]; 
        }
        $params = [
			'index' => $input['index_name'],
			'body' => [
				'settings' => [
					'number_of_shards' => $input['number_of_shards'],
					'number_of_replicas' => $input['number_of_replicas']
				],
				'mappings' => [
					$input['type_name'] => [
						'_source' => [
							'enabled' => true
						],
						'properties' =>$properties
					]
				]
			]
		];                        
        $client 	= ClientBuilder::create()->build();
        $response 	= $client->indices()->create($params);
        //echo json_encode($response);
    }*/
	
	public function synchronize_comments() {
		$start_time = time();
		$n 	= $t = 0;		
		/*
		//for($k=0;$k<2;$k++){ $kn = 1000*$k;			
			
			$this->db->select("rcccontent as comments,rccparentrccrefid as child_of,rccrefid,currch.rchrefid,currcm.rcmrefid,currcg.rcgrefid,currcc.modifieddate as comment_date, title,description,chaptername,clmname,sbmname,rcmlngcode,rcgrefid,groupname");
			$this->db->where('currcc.isdeleted', 0);
			$this->db->where('currcc.isactive', 1);
			$this->db->where('currcg.isdeleted', 0);
			$this->db->where('currch.isdeleted', 0);
			$this->db->where('currcm.isfortraining', 1);
			$this->db->where('`currcm`.`rcmrefid` IN (select distinct rcurcmrefid from currcu A join curtrb B on (A.rcutrbrefid = B.trbrefid) where A.isactive = 1 and B.isdeleted = 0)',NULL,FALSE);
			$this->db->join('currch', 'currch.rchrefid=currcc.rccrchrefid');
			$this->db->join('currcg', 'currcg.rcgrefid=currch.rchrcgrefid');
			$this->db->join('currcm', 'currcm.rcmrefid=currcg.rcgrcmrefid');
			$this->db->limit(10000,30000);
			$comments = $this->db->get('currcc')->result_array(); 
			if(!$comments){
				//continue;
			}//pre($comments);
			//echo count($comments); die;		
			foreach($comments as $each){ $n++; $t++;
			/*
				$this->db->select("title,description,chaptername,clmname,sbmname,rcmlngcode,rcgrefid,groupname");
				$this->db->where('currcc.rccrefid', $comment['rccrefid']);
				$this->db->where('currcg.isdeleted', 0);
				$this->db->where('currch.isdeleted', 0);
				$this->db->where('currcm.isfortraining', 1);
				$this->db->where('`currcm`.`rcmrefid` IN (select distinct rcurcmrefid from currcu A join curtrb B on (A.rcutrbrefid = B.trbrefid) where A.isactive = 1 and B.isdeleted = 0)',NULL,FALSE);
				$this->db->join('currch', 'currch.rchrefid=currcc.rccrchrefid');
				$this->db->join('currcg', 'currcg.rcgrefid=currch.rchrcgrefid');
				$this->db->join('currcm', 'currcm.rcmrefid=currcg.rcgrcmrefid');
				$each = $this->db->get('currcc')->row_array();
				//$each = array_merge($comment,$post);
				
				$each['id'] 			= '3_'.$each['rccrefid'];
				$each['post_type'] 	= 'trainings';
				//$q = "SELECT distinct rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $each['rcmrefid'] . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";
				$q  				= "SELECT rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $each['rcmrefid'] . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";
				$res 				= $this->db->query($q)->row_array();
				if (!$res OR empty($res['trbrefids'])) {
					continue;
				}
				$each['trbrefids']				= (object)array_map('intval', explode(',', $res['trbrefids']));
				//$result3[] = $comment;
				//pre($comment); die;
				
				//$commnets 		= (isset($each->commnets) && !empty($each->commnets)) ? $each->commnets : "";
				//$description 	= (isset($each->description) && !empty($each->description)) ? $each->description : "";
				//unset($each->description,$each->commnets);
				//$each 			= (array)$each;			
				$body = [
					"post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
					"post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
					"post_description" 	=> (isset($each['description']) && !empty($each['description'])) ? $each['description'] : "",
					"chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
					"chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
					
					"class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
					"subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
					"language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
					"module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
					"module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
					"trbrefids" 		=> (isset($each['trbrefids']) && !empty($each['trbrefids'])) ? $each['trbrefids'] : "",
					"chapter_logo" 		=> "",
					"module_logo" 		=> "",
					"post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : "",
					
					"channel_name"		=> "",
					"channel_logo"		=> "",
					"audiencesetid"		=> ""
				];
				
				$body['child_of'] 		= $each['child_of'];			
				$body['post_type'] 		= $each['post_type'];
				$body['comments'] 		= $each['comments'];
				//pre($body); die;
				$params['body'][] = [
					'index' => [
						'_index'	=> 'cl_post_comments',
						'_type' 	=> 'post_comments',
						'_id' 		=> $each['id']
					]
				];
				$params['body'][] 		= $body;
				
				
				// Every 1000 documents stop and send the bulk request
				if ($n % 1000 == 0) {
					$client 			= ClientBuilder::create()->build();
					$responses = $client->bulk($params);
					// erase the old bulk request
					$params = [];				
					//pre($responses);
					// unset the bulk response when you are done to save memory
					//unset($responses);
				} 
			}
			if (!empty($params['body'])) {
				$client 			= ClientBuilder::create()->build();
				$responses = $client->bulk($params);
				$params = [];	
			}
		//}	
		$response['Training'] = array("time_taken_in_seconds" => time() - $start_time, "total records" => $t);
		//echo json_encode($response);
		
		/* */
		
		$n = 0;
		$start_time = time();
		//$result1 = $result2 = $result3 = [];
		// get wall post comments
        $this->db->select("chprefid,chptopic,chpcontent as description,chmname,chmlogo,chpqutrefid,'wall' as post_type,cpccontent as comments,cpcparentcpcrefid as child_of,esccpc.modifieddate as comment_date,concat('1_',esccpc.cpcrefid) as id");
        $this->db->where('esccpc.isdeleted', 0);
        $this->db->where('esccpc.isactive', 1);
        $this->db->where('escchp.isdeleted', 0);
        //$this->db->where('escchm.isdeleted',0);
        $this->db->join('escchp', 'escchp.chprefid=esccpc.cpcchprefid');
        $this->db->join('escchm', 'escchm.chmrefid=escchp.chpchmrefid');
        $result1 	= $this->db->get('esccpc')->result();		
		foreach ($result1 as $each) { $n++; //echo $i;
			$commnets 		= (isset($each->commnets) && !empty($each->commnets)) ? $each->commnets : "";
			$description 	= (isset($each->description) && !empty($each->description)) ? $each->description : "";
			unset($each->description,$each->commnets);
			$each = (array)$each;
			$body = [
				"post_id" 		=> (int)$each['chprefid'],
				"post_title" 	=> $each['chptopic'],
				"post_description" => $description,
				"channel_name" 	=> $each['chmname'],
				"channel_logo" 	=>(!empty($each['chmlogo']))?"http://www.qa.chalklit.in/chalklitone/resources/newhome/images/".$each['chmlogo']:"",
				"audiencesetid" => (int)$each['chpqutrefid'],
				"class"			=> "",
				"subject"		=> "",
				"language"		=> "",
				"module_id"		=> "",
				"module_title"	=> "",
				"trbrefids"		=> "",
				"chapter_logo"	=> "",
				"module_logo"	=> "",
				"post_logo"		=> "",
				"trbrefids"		=> []				
			];		
			$body['comments'] 	= $commnets;
			$body['post_type'] 	= $each['post_type'];
			$body['child_of'] 	= $each['child_of'];			
			$params['body'][] = [
				'index' => [
					'_index'	=> 'cl_post_comments',
					'_type' 	=> 'post_comments',
					'_id' 		=> $each['id']
				]
			];
			$params['body'][] = $body;
        }		
		$client 	= ClientBuilder::create()->build();
		$responses 	= $client->bulk($params);
		$params 	= [];
		$response['wall'] = array("time_taken_in_seconds" => time() - $start_time, "total records" => $n);
		//echo json_encode($response);
		//pre($response);
		//echo "<br> RESOURCES <br>";
		
		
		
		//get resource post comments
		$n =0;
		$start_time = time();
		$this->db->select("rchrefid,title,description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,isfortraining,rcgrefid,groupname,case
  when type = 'video' then concat('http:',thumbnailurl)
  else ifnull(thumbnailurl,'')    
end postlogo,'resources' as post_type,currcc.modifieddate as comment_date,rcccontent as comments,rccparentrccrefid as child_of,concat('2_',currcc.rccrefid) as id");
		$this->db->where('currcc.isdeleted', 0);
        $this->db->where('currcc.isactive', 1);
        $this->db->where('currcg.isdeleted', 0);
        $this->db->where('currch.isdeleted', 0);
        $this->db->where('currcm.isfortraining', 0);
        $this->db->where('currcm.isforreference', 0);
        $this->db->where('currcm.islive', 1);
        //$this->db->join('currcg', 'currcg.rcgrcmrefid=currcm.rcmrefid');
        //$this->db->join('currch', 'currch.rchrcgrefid=currcg.rcgrefid');
		//currcm
		$this->db->join('currch', 'currch.rchrefid=currcc.rccrchrefid');
		$this->db->join('currcg', 'currcg.rcgrefid=currch.rchrcgrefid');
        $this->db->join('currcm', 'currcm.rcmrefid=currcg.rcgrcmrefid');
        $result2 = $this->db->get('currcc')->result();
		
		foreach ($result2 as $each) { $n++; //echo $i;
			$commnets 		= (isset($each->commnets) && !empty($each->commnets)) ? $each->commnets : "";
			$description 	= (isset($each->description) && !empty($each->description)) ? $each->description : "";
			unset($each->description,$each->commnets);
			$each = (array)$each;
			$body = [
				"post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
				"post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
				"post_description" 	=> $description,
				"chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
				"chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
				"class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
				"subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
				"language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
				"module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
				"module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
				"chapter_logo" 		=> "",
				"module_logo" 		=> "",
				"post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : "",					
				"channel_name"		=> "",
				"channel_logo"		=> "",
				"audiencesetid"		=> "",
				"trbrefids"			=> []
			];
			$body['child_of'] = $each['child_of'];
			$body['comments'] = $commnets;
			$body['post_type'] = $each['post_type'];
			//pre($body); die;
			$params['body'][] = [
				'index' => [
					'_index'	=> 'cl_post_comments',
					'_type' 	=> 'post_comments',
					'_id' 		=> $each['id']
				]
			];
			$params['body'][] = $body;
			if ($n % 500 == 0) {
				$client 			= ClientBuilder::create()->build();
				$responses = $client->bulk($params);
				$params = [];	
			} 
        }
		if (!empty($params['body'])) {
			$responses = $client->bulk($params);
		}
		$response['resources'] = array("time_taken_in_seconds" => time() - $start_time, "total records" => $n);
		/* */
		echo json_encode($response);
		//pre($response);
		$params 	= [];
		
		
		
		/*
		//pre($comments); die;
		
		
		// get training post comments
		//$this->db->select("rchrefid,title,description as description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,rcgrefid,groupname, currcc.modifieddate as comment_date,'rcccontent' as comments, rccparentrccrefid as child_of,rccrefid");
		//$this->db->select("rchrefid,title,description as description,rcmrefid,chaptername,clmname,sbmname,rcmlngcode,rcgrefid,currcc.modifieddate as comment_date,rcccontent as comments,rccparentrccrefid as child_of,rccrefid,'groupname' as groupname");
		//$this->db->select("description as description,groupname as groupname,rchrefid,title,rcmrefid,chaptername,clmname,sbmname, rcmlngcode,rcgrefid");
		$this->db->select("");
		$this->db->where('currcc.isdeleted', 0);
        $this->db->where('currcc.isactive', 1);
        $this->db->where('currcg.isdeleted', 0);
        $this->db->where('currch.isdeleted', 0);
        $this->db->where('currcm.isfortraining', 1);
        $this->db->where('`currcm`.`rcmrefid` IN (select distinct rcurcmrefid from currcu A join curtrb B on (A.rcutrbrefid = B.trbrefid) where A.isactive = 1 and B.isdeleted = 0)',NULL,FALSE);
        //$this->db->join('currcc', 'currcc.rccrchrefid=currch.rchrefid');
		$this->db->join('currch', 'currch.rchrefid=currcc.rccrchrefid');
		$this->db->join('currcg', 'currcg.rcgrefid=currch.rchrcgrefid');
        $this->db->join('currcm', 'currcm.rcmrefid=currcg.rcgrcmrefid');
        //$this->db->join('currcm', 'currcm.rcmrefid=currch.rchrcmrefid');
        //$result3 = $this->db->get('currch')->result();
		$result3 = $this->db->get('currcc')->result();
		//echo count($result3);
		pre($result3); die;
		
		$r3c = count($result3);
        for($i=0;$i<$r3c;$i++) {
			//$each = (array)$each;
			//if(empty($each['thumbnailurl']) OR $each['thumbnailurl'] == null){
				//$each['thumbnailurl'] = "";
			//}else if($each['type'] == 'video'){
				//$each['thumbnailurl'] = 'http:'.$each['thumbnailurl'];
			//}
			$each['id'] = '3_'.$each['rccrefid'];
            $result3[$i]->post_type = 'trainings';
            //$q = "SELECT distinct rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $each['rcmrefid'] . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";
            $q  				= "SELECT rcurcmrefid,GROUP_concat(distinct rcutrbrefid) as trbrefids from currcu where rcurcmrefid =" . $result3[$i]->rcmrefid . " and isactive = 1 group by rcurcmrefid order by rcurcmrefid";
            $res 				= $this->db->query($q)->row_array();
            if (!$res OR empty($res['trbrefids'])) {
                continue;
            }
            $result3[$i]->trbrefids 				= (object)array_map('intval', explode(',', $res['trbrefids']));
			//$result3[] = $each;
		}
		*/
		/*
		$n = 0;
		foreach ($result3 as $each) { $n++; //echo $i;
			$commnets 		= (isset($each->commnets) && !empty($each->commnets)) ? $each->commnets : "";
			$description 	= (isset($each->description) && !empty($each->description)) ? $each->description : "";
			unset($each->description,$each->commnets);
			$each 			= (array)$each;			
			$body = [
				"post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
				"post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
				"post_description" 	=> $description,
				"chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
				"chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
				
				"class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
				"subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
				"language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
				"module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
				"module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
				"trbrefids" 		=> (isset($each['trbrefids']) && !empty($each['trbrefids'])) ? $each['trbrefids'] : "",
				"chapter_logo" 		=> "",
				"module_logo" 		=> "",
				"post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : "",
				
				"channel_name"		=> "",
				"channel_logo"		=> "",
				"audiencesetid"		=> ""
			];
			
			$body['child_of'] 		= $each['child_of'];			
			$body['post_type'] 		= $each['post_type'];
			$body['comments'] 		= $commnets;
			//pre($body); die;
			$params['body'][] = [
				'index' => [
					'_index'	=> 'cl_post_comments',
					'_type' 	=> 'post_comments',
					'_id' 		=> $each['id']
				]
			];
			$params['body'][] = $body;
			
			// Every 1000 documents stop and send the bulk request
			if ($n % 500 == 0) {
				$client 			= ClientBuilder::create()->build();
				$responses = $client->bulk($params);
				// erase the old bulk request
				$params = [];				
				//pre($responses);
				// unset the bulk response when you are done to save memory
				//unset($responses);
			}            
            //$client 			= ClientBuilder::create()->build();
            //$response 			= $client->index($params);
        }
		if (!empty($params['body'])) {
			$responses = $client->bulk($params);
		}
		$response = array("time_taken_in_seconds" => time() - $start_time, "total records" => $n);
		echo json_encode($response);
		
		echo "case 5 <br>";
		$result = array_merge($result1,$result2,$result3);
		echo "case 6 <br>";
		//pre($result); die;
		//pre($result2); pre($result1);pre($result3); 
		//die;
		
		//return $result;
		$i = 0;
		echo count($result);
		echo " wall ".count($result1);
		echo " resources ".count($result2);
		echo " trainings ".count($result3);
		
        foreach ($result as $each) { $i++; //echo $i;
			$commnets 		= (isset($each->commnets) && !empty($each->commnets)) ? $each->commnets : "";
			$description 	= (isset($each->description) && !empty($each->description)) ? $each->description : "";
			unset($each->description,$each->commnets);
			$each = (array)$each;
			if($each['post_type'] == 'wall'){ //die('case1');
				
				$body = [
					"post_id" 		=> (int)$each['chprefid'],
					"post_title" 	=> $each['chptopic'],
					"post_description" => $description,
					"channel_name" 	=> $each['chmname'],
					"channel_logo" 	=>(!empty($each['chmlogo']))?"http://www.qa.chalklit.in/chalklitone/resources/newhome/images/".$each['chmlogo']:"",
					"audiencesetid" => (int)$each['chpqutrefid'],
					"class"			=> "",
					"subject"		=> "",
					"language"		=> "",
					"module_id"		=> "",
					"module_title"	=> "",
					"trbrefids"		=> "",
					"chapter_logo"	=> "",
					"module_logo"	=> "",
					"post_logo"		=> "",
					"trbrefids"		=> []
					
				];
			}else if($each['post_type'] == 'trainings'){
				$comments = 
				$body = [
					"post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
					"post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
					"post_description" 	=> $description,
					"chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
					"chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
					
					"class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
					"subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
					"language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
					"module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
					"module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
					"trbrefids" 		=> (isset($each['trbrefids']) && !empty($each['trbrefids'])) ? $each['trbrefids'] : "",
					"chapter_logo" 		=> "",
					"module_logo" 		=> "",
					"post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : "",
					
					"channel_name"		=> "",
					"channel_logo"		=> "",
					"audiencesetid"		=> ""
				];
			}else if($each['post_type'] == 'resources'){ 
			$each = (array)$each;
				$body = [
					"post_id" 			=> (int)(isset($each['rchrefid']) && !empty($each['rchrefid'])) ? $each['rchrefid'] : "",
					"post_title" 		=> (isset($each['title']) && !empty($each['title'])) ? $each['title'] : "",
					"post_description" 	=> $description,
					"chapter_id" 		=> (int)(isset($each['rcmrefid']) && !empty($each['rcmrefid'])) ? $each['rcmrefid'] : "",
					"chapter_name" 		=> (isset($each['chaptername']) && !empty($each['chaptername'])) ? $each['chaptername'] : "",
					"class" 			=> (isset($each['clmname']) && !empty($each['clmname'])) ? $each['clmname'] : "",
					"subject" 			=> (isset($each['sbmname']) && !empty($each['sbmname'])) ? $each['sbmname'] : "",
					"language" 			=> (isset($each['rcmlngcode']) && !empty($each['rcmlngcode'])) ? $each['rcmlngcode'] : "",
					"module_id" 		=> (int)(isset($each['rcgrefid']) && !empty($each['rcgrefid'])) ? $each['rcgrefid'] : "",
					"module_title" 		=> (isset($each['groupname']) && !empty($each['groupname'])) ? $each['groupname'] : "",
					"chapter_logo" 		=> "",
					"module_logo" 		=> "",
					"post_logo" 		=> (isset($each['postlogo']) && !empty($each['postlogo'])) ? $each['postlogo'] : "",					
					"channel_name"		=> "",
					"channel_logo"		=> "",
					"audiencesetid"		=> "",
					"trbrefids"			=> []
				];
			}else{
				continue;
				pre($params); die;
			}		
			$body['child_of'] = $each['child_of'];
			$body['comments'] = $commnets;
			$body['post_type'] = $each['post_type'];
			//pre($body); die;
			$params['body'][] = [
				'index' => [
					'_index'	=> 'cl_post_comments',
					'_type' 	=> 'post_comments',
					'_id' 		=> $each['id']
				]
			];
			$params['body'][] = $body;
			
			// Every 1000 documents stop and send the bulk request
			if ($i % 500 == 0) {
				$client 			= ClientBuilder::create()->build();
				$responses = $client->bulk($params);
				// erase the old bulk request
				$params = [];
				//pre($responses);
				// unset the bulk response when you are done to save memory
				//unset($responses);
			}
            
            //$client 			= ClientBuilder::create()->build();
            //$response 			= $client->index($params);
        }
		if (!empty($params['body'])) {
			$responses = $client->bulk($params);
		}
		//pre($responses);
		
        $response = array("time_taken_in_seconds" => time() - $start_time, "total records" => $i);
		pre($response);
		//return $response;
        //echo json_encode($response);
        //{"time_taken_in_seconds":8,"total records":982}
		*/
    }
	
	function create_post_comments_indexes() {
		//create comments
		$client = ClientBuilder::create()->build();
		$exist 		= $client->indices()->exists(array("index"=>"cl_post_comments"));
		if($exist){
			$response 	= $client->indices()->delete(array("index"=>"cl_post_comments"));
		} 
		
		
		$comments 		= $this->get_post_comments_mapping_structure();//pre($comments);die;
		$response 		= $this->create_index_with_mappings($comments);	
		pre($response);
    }
	
	function get_post_comments_mapping_structure() {
		$mapping_json ='{
			"index_name"		: "cl_post_comments",
			"type_name"			: "post_comments",
			"number_of_shards"	: "5",
			"number_of_replicas": "1",
			"fields" : [
				{"field_name":"post_id","field_type":"integer"},
				{"field_name":"post_title","field_type":"text"},
				{"field_name":"post_description","field_type":"text"},
				{"field_name":"channel_name","field_type":"text"},
				{"field_name":"channel_logo","field_type":"keyword"},
				{"field_name":"audiencesetid","field_type":"integer"},
			
				{"field_name":"chapter_id","field_type":"integer"},
				{"field_name":"chapter_name","field_type":"text"},
				{"field_name":"class","field_type":"text"},
				{"field_name":"subject","field_type":"text"},
				{"field_name":"language","field_type":"text"},
				{"field_name":"module_id","field_type":"integer"},
				{"field_name":"module_title","field_type":"text"},
				{"field_name":"trbrefids","field_type":"object"},
				{"field_name":"chapter_logo","field_type":"keyword"},
				{"field_name":"module_logo","field_type":"keyword"},
				{"field_name":"post_logo","field_type":"keyword"},
				
				{"field_name":"post_type","field_type":"keyword"},
				{"field_name":"child_of","field_type":"integer"},
				{"field_name":"comments","field_type":"text"}
			]
		}';
		return json_decode($mapping_json,true);
	}
		
		
	public function get_post_comments_documents() {
		$data = file_get_contents("http://localhost:9200/cl_post_comments/_search");
		pre(json_decode($data,true));
		die;
    }
	
}
