<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }    

    public function getTraining($traname) { 

        $this->db->where('trnrefid',$traname);
         $result = $this->db->get('clt_trn')->row();
         return $result;
    }

    public function checkTraningModule($rcgrefid,$trnrcmrefid){

        $this->db->where('rcgrefid',$rcgrefid);
        $this->db->where('rcgrcmrefid',$trnrcmrefid);
        $this->db->where('isactive',TRUE);  
        $this->db->where('isdeleted',FALSE);
        $result = $this->db->get('currcg')->row_array();
        return $result;
    }

    public function checkTraning($trnrefid) { 

      $this->db->select('trnrcmrefid,enddate,startdate,trnrefid');
        $this->db->where('trnrefid',$trnrefid);
        $this->db->where('isactive',TRUE);
        $this->db->where('isdeleted',FALSE);
        $result = $this->db->get('clt_trn')->row_array();
        return $result;
    }

    public function getAllModules($trnrcmrefid){

        $this->db->select('rcgrefid');
        $this->db->where('currcg.rcgrcmrefid',$trnrcmrefid);
        $modules = $this->db->get('currcg')->result_array();
        return $modules;
    }

    public function getTrainingInstanceModules($trnrefid){

        $data=  $this->checkTraning($trnrefid);
        $this->db->select('currcg.rcgrefid,currcg.groupname,currcg.rchname');
        $this->db->where('currcg.rcgrcmrefid',$data['trnrcmrefid']);
        $this->db->where('escpor.isattached','1');
        $this->db->join('currcg','currcg.rcgrefid=escpor.porobjectid','left');

        $this->db->group_by('rcgrefid');
        $modules = $this->db->get('escpor')->result_array();
        
        return $modules;
    }
    public function check_test_available($rcgrefid){

        $this->db->select('porrefid,porpolrefid');
        $this->db->where('escpor.porobjectid',$rcgrefid);
        $test = $this->db->get('escpor')->result_array();
        return $test;
    }

    public function getallActiveTrainings(){
        $this->db->where('isactive',TRUE);
        $this->db->where('isdeleted',FALSE);
        $this->db->select('trnrefid,trnname,trnrcmrefid');
        $result = $this->db->get('clt_trn')->result_array();
         return $result;

    }

   public function check_super_batch($id){

        $this->db->where('tsbrefid',$id);
        $result = $this->db->get('clt_tsb')->row_array();
        return $result;
    }

   public function get_all_training_batches($id){

        $this->db->where('tsbrefid',$id);
        $this->db->select('trbrefid');
        $result = $this->db->get('clt_tsb_trb')->result_array();
        return $result;

  }


    public function get_question_options($obj_id,$training_end_date){
       $this->db->select("escpol.polrefid,escpol.polquestion");
        $this->db->where('escpor.porobjectid',$obj_id);
        $this->db->join('escpor','escpor.porpolrefid=escpol.polrefid');
        $questions = $this->db->get('escpol')->result_array();

      $this->db->select("escusm.usmrefid,escusm.usmfirstname,escusm.usmlastname,escusm.usmmob,escpol.polrefid,escpol.polquestion,escpom.pomoption,escpom.iscorrect");
        $this->db->join('escusm','escusm.usmrefid=escprm.prmusmrefid');
        $this->db->join('escpol','escpol.polrefid=escprm.prmpolrefid');
        $this->db->join('escpor','escpor.porpolrefid=escprm.prmpolrefid');
        $this->db->join('escpom','escpom.pomrefid=escprm.prmpomrefid');
        //$this->db->limit(100,10);
        $this->db->where('escpor.porobjectid',$obj_id);
        $this->db->where('escprm.createddate <=',$training_end_date);
        $options = $this->db->get('escprm')->result_array();
        if($options){
          foreach($options as $option){
              $new_array[$option['usmrefid']]['name']   = $option['usmfirstname'];
              $new_array[$option['usmrefid']]['mobile'] = $option['usmmob'];
              $new_array[$option['usmrefid']]['options'][$option['polrefid']]['value'] = $option['iscorrect'];
          }
          $options = $new_array;
        }
        return $options;

         }

  public function get_take_a_test_on_training($id,$moduleid,$training_end_date,$training_start_date){

      $data=array();
     
       $sql = "  SELECT `escusm`.`usmrefid`, `escusm`.`usmfirstname`,
        `escusm`.`usmlastname`, `escusm`.`usmmob`
        ,`escprm`.`prmpomrefid`,`escprm`.`createddate`,
        `escpor`.`porpolrefid`, `escpol`.`polquestion`,
        `escpom`.pomoption, `escpom`.`iscorrect`,`escpor`.`porpolweight`,`escpor`.`porpolindex`
        FROM `currcu`

        JOIN `escusm` ON `escusm`.`usmrefid`=`currcu`.`rcuusmrefid`
        JOIN `curtrb` ON `curtrb`.`trbrefid`=`currcu`.`rcutrbrefid`
        JOIN `escpor`
       
        JOIN escpol on (porpolrefid = polrefid)
        
         
        LEFT OUTER JOIN `escprm` ON (`escprm`.`prmusmrefid`=`currcu`.`rcuusmrefid` and `escprm`.`prmpolrefid` = `escpor`.`porpolrefid`
        )
         left outer JOIN `escpom` ON `escpom`.`pomrefid`=`escprm`.`prmpomrefid`
       
        WHERE `curtrb`.`trbtrnrefid` = $id AND (`escprm`.`createddate` <= '$training_end_date' OR `escprm`.`createddate` IS NULL) 
        AND `currcu`.`isactive` = '1' AND `escpor`.`porobjectid`= $moduleid And `escpor`.`isattached` = '1' ORDER BY `escpor`.`porpolindex` ASC "  ;

        $query = $this->db->query($sql);
        $all_users =  $query->result_array();
        //echo $this->db->last_query();die;
        if($all_users){

          foreach($all_users as $option){

            $new_array[$option['usmrefid']]['name']   = $option['usmfirstname'].' '.$option['usmlastname'];
            $new_array[$option['usmrefid']]['mobile'] = $option['usmmob'];
            $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['value'] = $option['iscorrect'];
            $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['weight'] = $option['porpolweight'];
            $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['user_choice'] = $option['pomoption'];
            $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['index'] = $option['porpolindex'];
          }
            $all_users = $new_array;
        }     
      // echo count($all_users); die;  
       return $all_users;
    
}

 public function check_training_batch($id){

      $this->db->where('trbrefid',$id);
     $result = $this->db->get('curtrb')->row_array();
      return $result;
  }

  public function get_take_a_test_on_batch($id,$moduleid){

    $sql = "SELECT `escusm`.`usmrefid`, `escusm`.`usmfirstname`,`escusm`.`usmlastname`, `escusm`.`usmmob` ,`escprm`.`prmpomrefid`,`escpor`.`porpolrefid`, `escpol`.`polquestion`,`escpom`.`pomoption`, `escpom`.`iscorrect`,`escpor`.`porpolweight`,`escpor`.`porpolindex`
    FROM `currcu`
    JOIN `escusm` ON `escusm`.`usmrefid`=`currcu`.`rcuusmrefid`
    JOIN `escpor`
    join escpol on (porpolrefid = polrefid)
    LEFT OUTER JOIN `escprm` ON (`escprm`.`prmusmrefid`=`currcu`.`rcuusmrefid` and `escprm`.`prmpolrefid` = `escpor`.`porpolrefid`)
    left outer JOIN `escpom` ON `escpom`.`pomrefid`=`escprm`.`prmpomrefid`
    WHERE `currcu`.`rcutrbrefid` = $id
    AND `currcu`.`isactive` = '1' AND `escpor`.`porobjectid`= $moduleid And `escpor`.`isattached` = 1  ORDER BY `escpor`.`porpolindex` ASC ";
    $query = $this->db->query($sql);

    $all_users =  $query->result_array();

   if($all_users){
      foreach($all_users as $option){

        $new_array[$option['usmrefid']]['name']   = $option['usmfirstname'].' '.$option['usmlastname'];
        $new_array[$option['usmrefid']]['mobile'] = $option['usmmob'];
        $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['value'] = $option['iscorrect'];
        $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['weight'] = $option['porpolweight'];
        $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['user_choice'] = $option['pomoption'];
        $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['index'] = $option['porpolindex'];
      }
            $all_users = $new_array;
        }     
    return  $all_users;
  }



   public function get_all_question_on_module($id){

      $this->db->select("escpol.polrefid,escpol.polquestion,escpor.porpolweight,escpor.porpolindex");
      $this->db->where('escpor.porobjectid',$id);
      $this->db->where('escpor.isattached','1');
      $this->db->join('escpol','escpol.polrefid=escpor.porpolrefid','left');
      $ques = $this->db->get('escpor')->result_array();
      return $ques;
   }


   public function get_take_a_test_on_super_batch($super_batch_id,$moduleid){

    $sql = "SELECT `escusm`.`usmrefid`, `escusm`.`usmfirstname`,`escusm`.`usmlastname`, `escusm`.`usmmob` ,`escprm`.`prmpomrefid`,`escpor`.`porpolrefid`, `escpol`.`polquestion`,`escpom`.`pomoption`, `escpom`.`iscorrect`,`escpol`.`polrefid`,`escpor`.`porpolweight`,`escpor`.`porpolindex`
    FROM `currcu`
    JOIN `escusm` ON `escusm`.`usmrefid`=`currcu`.`rcuusmrefid`
    JOIN `clt_tsb_trb` ON `clt_tsb_trb`.`trbrefid`=`currcu`.`rcutrbrefid`
    JOIN `escpor`
    join escpol on (porpolrefid = polrefid)
    LEFT OUTER JOIN `escprm` ON (`escprm`.`prmusmrefid`=`currcu`.`rcuusmrefid` and `escprm`.`prmpolrefid` = `escpor`.`porpolrefid`)
    left outer JOIN `escpom` ON `escpom`.`pomrefid`=`escprm`.`prmpomrefid`
    WHERE `escpor`.`porobjectid`= $moduleid
    AND `currcu`.`isactive` = '1' AND `escpor`.`porobjectid`= $moduleid And `escpor`.`isattached` = '1' And `clt_tsb_trb`.`tsbrefid` = $super_batch_id And `clt_tsb_trb`.`isactive` = '1'  ORDER BY `escpor`.`porpolindex` ASC " ;

    $query = $this->db->query($sql);

    $all_users =  $query->result_array();
    

    $new_array = array();
        if($all_users){
           
            foreach($all_users as $option){
               
                $new_array[$option['usmrefid']]['name']   = $option['usmfirstname'].' '.$option['usmlastname'];
                $new_array[$option['usmrefid']]['mobile'] = $option['usmmob'];
                $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['value'] = $option['iscorrect'];
                $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['weight'] = $option['porpolweight'];
                $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['user_choice'] = $option['pomoption'];
                $new_array[$option['usmrefid']]['options'][$option['porpolrefid']]['index'] = $option['porpolindex'];
                 
            }
            $all_users = $new_array;
        }     
       
        return  $all_users;
        
   }


   

}
