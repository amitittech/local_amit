<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }    

    public function getTraining($traname) { 

        $this->db->where('trnrefid',$traname);
         $result = $this->db->get('clt_trn')->row();
         return $result;
    }

    public function checkTraningModule($rcgrefid,$trnrcmrefid){

    	
        $this->db->where('rcgrefid',$rcgrefid);
        $this->db->where('rcgrcmrefid',$trnrcmrefid);
        // $this->db->where('isactive',TRUE);
        // $this->db->where('isdeleted',FALSE);
        $result = $this->db->get('currcg')->row_array();
        return $result;
    }
    public function checkTraning($trnrefid) { 

    	$this->db->select('trnrcmrefid,enddate');
        $this->db->where('trnrefid',$trnrefid);
        $this->db->where('isactive',TRUE);
        $this->db->where('isdeleted',FALSE);
        $result = $this->db->get('clt_trn')->row_array();
        return $result;
    }

    public function getAllModules($trnrcmrefid){

        $this->db->select('rcgrefid');
        $this->db->where('currcg.rcgrcmrefid',$trnrcmrefid);
        $modules = $this->db->get('currcg')->result_array();
        return $modules;
    }

    public function getTrainingInstanceModules($trnrcmrefid){

      $this->db->select('rcgrefid,groupname,rchname');
        $this->db->where('currcg.rcgrcmrefid',$trnrcmrefid);
        $modules = $this->db->get('currcg')->result_array();
        return $modules;

    }

    public function get_question_options($obj_id,$training_end_date){

       $this->db->select("escpol.polrefid,escpol.polquestion");
        $this->db->where('escpor.porobjectid',$obj_id);
        $this->db->where('escpor.isattached',TRUE);
        $this->db->join('escpor','escpor.porpolrefid=escpol.polrefid');
        $questions = $this->db->get('escpol')->result_array();

      $this->db->select("escusm.usmrefid,escusm.usmfirstname,escusm.usmlastname,escusm.usmmob,escpol.polrefid,escpol.polquestion,escpom.pomoption,escpom.iscorrect");
        $this->db->join('escusm','escusm.usmrefid=escprm.prmusmrefid');
        $this->db->join('escpol','escpol.polrefid=escprm.prmpolrefid');
        $this->db->join('escpor','escpor.porpolrefid=escprm.prmpolrefid');
        $this->db->join('escpom','escpom.pomrefid=escprm.prmpomrefid');
        //$this->db->limit(100,10);
        $this->db->where('escprm.createddate <=',$training_end_date);
         $this->db->where('escpor.isattached',TRUE);
        $this->db->where('escpor.porobjectid',$obj_id);
        $options = $this->db->get('escprm')->result_array();
        if($options){
          foreach($options as $option){
              $new_array[$option['usmrefid']]['name']   = $option['usmfirstname'];
              $new_array[$option['usmrefid']]['mobile'] = $option['usmmob'];
              $new_array[$option['usmrefid']]['options'][$option['polrefid']]['value'] = $option['iscorrect'];
          }
          $options = $new_array;
        }
        return $options;

    }

    public function get_questions($obj_id){
        $this->db->select("escpol.polrefid,escpol.polquestion");
        $this->db->where('escpor.porobjectid',$obj_id);
        $this->db->where('escpor.isattached',TRUE);
        $this->db->join('escpor','escpor.porpolrefid=escpol.polrefid');
        $questions = $this->db->get('escpol')->result_array();
        return $questions;
    }


    
    
}
