<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Training_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function add_sender_id($data) {

        $this->db->where('sendercode', trim($data['sendercode']));
        $this->db->where('isactive', true);
        $this->db->where('isdeleted', FALSE);
        $check_if_exist = $this->db->get('clt_mat_sms_sender')->row();
        if ($check_if_exist) {
            return $response[] = array("message" => "false");
        }

        $input['sendercode'] = strtoupper($data['sendercode']);
        $input['createdby'] = $data['login_id'];
        $input['isactive'] = TRUE;
        $input['createddate'] = date('Y-m-d H:i:s');

        $result = $this->db->insert('clt_mat_sms_sender', $input);
        return $response[] = array("sendercode" => $data['sendercode'], "message" => "true");
    }

    public function edit_sender_id($data) {

        $this->db->where('sendercode', trim($data['sendercode']));
        $this->db->where('isactive', true);
        $this->db->where('isdeleted', FALSE);
        $this->db->where('senderrefid !=', $data['senderrefid']);
        $check_if_exist = $this->db->get('clt_mat_sms_sender')->row();
        if ($check_if_exist) {
            return $response[] = array("message" => "false", "result" => array());
        }
        $this->db->where('senderrefid', $data['senderrefid']); //which row want to upgrade  
        $result = $this->db->update('clt_mat_sms_sender', array('sendercode' => strtoupper($data['sendercode']), 'isactive' => TRUE, 'isdeleted' => FALSE));
        return $response[] = array("message" => "true", "result" => $result);
    }

    public function delete_sender_id($data) {

        if (is_array($data['senderrefid'])) {
            foreach ($data['senderrefid'] as $key => $value) {
                $this->db->where('senderid', $value);
                $checkmapping = $this->db->get('clt_tra_sms_sender')->result_array();
                $count = count($checkmapping);
                if ($count == '0') {
                    $this->db->where('senderrefid', $value);
                    $this->db->update('clt_mat_sms_sender', array('isdeleted' => TRUE, 'isactive' => FALSE));
                }
            }
            return true;
        }
    }

    public function training_list() {

        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->order_by('trarefid', 'desc');
        $result = $this->db->get('curtra')->result_array();
        return $result;
    }

    public function get_training($data) {

        if (isset($data['traname']) && !empty($data['traname'])) {
            $this->db->where('traname', $data['traname']);
        }
        $this->db->where('trarefid !=', $data['trarefid']);
        $result = $this->db->get('curtra')->row();

        return $result;
    }

    public function get_training_agencies($data) {
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->order_by('trarefid', 'desc');
        $result = $this->db->get('curtra')->result_array();
        return $result;
    }

    public function get_training_agency($data) {
        if (isset($data['trarefid']) && !empty($data['trarefid'])) {
            $this->db->where('trarefid', $data['trarefid']);
        }
        $result = $this->db->get('curtra')->row_array();
        return $result;
    }

    public function get_sender_id($data) {
        if (isset($data['senderrefid']) && !empty($data['senderrefid'])) {
            $this->db->where('senderrefid', $data['senderrefid']);
        }
        $result = $this->db->get('clt_mat_sms_sender')->row_array();
        return $result;
    }

    public function get_certificates($data) {
        $this->db->order_by('ctmrefid', 'desc');
        $result = $this->db->get('cltcertificatetemplate')->result_array();
        return $result;
    }

    public function get_certificate($data) {
        if (isset($data['ctmrefid']) && !empty($data['ctmrefid'])) {
            $this->db->where('ctmrefid', $data['ctmrefid']);
        }
        $result = $this->db->get('cltcertificatetemplate')->row_array();
        return $result;
    }

    public function get_training_toggle_visibility($data) {
        $select = "trnrefid,trnname,regstartdate,isvisibleonclient";
        $this->db->select($select);
        $this->db->where('isactive', 1);
        $this->db->where('isdeleted', 0);
        $this->db->where('regstartdate >', date('Y-m-d H:i:s'));
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->order_by('regstartdate', 'desc');
        $result = $this->db->get('clt_trn')->result_array();
        if ($result) {
            $select = "trnrefid";
            $this->db->select($select);
            $this->db->where('isactive', 1);
            $this->db->where('isdeleted', 0);
            //$this->db->where('regstartdate >', date('Y-m-d H:i:s'));
            $this->db->order_by('regstartdate', 'desc');
            $ttl = $this->db->get('clt_trn')->result_array();
            $result[0]['total_records'] = count($ttl);
        }
        return $result;
    }

    public function update_training_toggle_visibility($data) { // echo "here";  
        $this->db->where('trnrefid', $data['trnrefid']);
        $exist = $this->db->get('clt_trn')->row();
        if ($exist) {
            $this->db->where('trnrefid', $data['trnrefid']);
            $this->db->update('clt_trn', array('isvisibleonclient' => $data['isvisibleonclient'], 'modifiedby' => $data['login_id'], 'modifieddate' => date('Y-m-d H:i:s')));
            return true;
        }
        return false;
    }

    public function map_users_to_trainings($data) {
        $response = array();
        $inserting['assignedby'] = $data['login_id'];
        $inserting['assignedon'] = date('Y-m-d H:i:s');
        if (isset($data['trainings']) && !empty($data['trainings']) && isset($data['user_mobiles']) && !empty($data['user_mobiles']) && isset($data['user_role']) && !empty($data['user_role'])) {
            //SELECT * FROM `escrlm` WHERE rlmrole = 'CL-L1 User/CL-Training Lead/CL-Training Manager'
            $i = 0;
            $training_ids = $data['trainings']; //explode(',',$data['trainings']);
            $get_last_char = substr($data['user_mobiles'], -1); // returns last character
            if ($get_last_char == ',') {
                $data['user_mobiles'] = rtrim($data['user_mobiles'], ',');
            }
            $user_mobiles = explode(',', $data['user_mobiles']); //die('hello');

            foreach ($training_ids as $trarefid) {
                $training = $this->get_training_agency(array("trarefid" => $trarefid));
                $inserting['trarefid'] = $trarefid;
                foreach ($user_mobiles as $user_mobile) {
                    $i++;
                    if (empty($training)) {
                        $response [] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => "n/a", "mobile" => $user_mobile, "role" => $data['user_role'], "status" => "fail", "message" => "Training not exist");
                        continue;
                    }
                    //SELECT * FROM `esculm` WHERE `ulmusername` = '7836948474'
                    $this->db->where('ulmusername', $user_mobile);
                    $user = $this->db->get('esculm')->row_array(); //echo $this->db->last_query()."<br>";  pre($user);
                    if (empty($user)) {
                        $response [] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "mobile" => $user_mobile, "role" => $data['user_role'], "status" => "fail", "message" => "Mobile number is not in use");
                        continue;
                    }
                    $this->db->select("rlmrefid");
                    $this->db->where('rlmrole', $data['user_role']);
                    $this->db->where('isdeleted', 0);
                    $this->db->where('isactive', 1);
                    $role = $this->db->get('escrlm')->row_array();
                    if (empty($role)) {
                        $response [] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "mobile" => $user_mobile, "role" => $data['user_role'], "status" => "fail", "message" => "Role is not in use");
                        continue;
                    }
					
					$this->db->where('urmusmrefid', $user['ulmusmrefid']);
                    $this->db->where('urmrlmrefid', $role['rlmrefid']);
                    $already = $this->db->get('escurm')->row_array();
                    if (empty($already)) {
                        $inserting = array();
                        $inserting['urmusmrefid'] = $user['ulmusmrefid'];
                        $inserting['urmrlmrefid'] = $role['rlmrefid'];
                        $inserting['createddate'] = date('Y-m-d H:i:s');
                        $inserting['isdeleted'] = 0;
                        $inserting['isactive'] = 1;
                        $this->db->insert('escurm', $inserting);
                        //$response [] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "mobile" => $user_mobile, "role" => $data['user_role'], "status" => "success", "message" => "Mapping done");
                    }if(!empty($already) && $already['isdeleted']==1){
						$updating['modifieddate']	= date('Y-m-d H:i:s');
						$updating['isdeleted'] 		= 0;
						$updating['isactive'] 		= 1;
						$this->db->where('urmrefid', $already['urmrefid']);
						$this->db->update('escurm', $updating);
					}

                    $this->db->where('trarefid', $trarefid);
                    $this->db->where('tmrefid', $user['ulmusmrefid']);
                    $this->db->where('rlmrefid', $role['rlmrefid']);
                    $exist = $this->db->get('clt_tra_tm')->row_array();
                    if ($exist) {
                        if ($exist['isdeleted'] == 0) {
                            $response[] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "mobile" => $user_mobile, "role" => $data['user_role'], "status" => "fail", "message" => "Mapping already exist");
                            continue;
                        } else {
							$updating = [];
                            $updating['revokedon']	= date('Y-m-d H:i:s');
							$updating['revokedby'] 	= $data['login_id'];
                            $updating['isdeleted'] 	= 0;
                            $updating['isactive'] 	= 1;
                            $this->db->where('tratmrefid', $exist['tratmrefid']);
                            $this->db->update('clt_tra_tm', $updating);
                        }
                    } else {
						$inserting = array();
                        $inserting['trarefid'] = $trarefid;
                        $inserting['tmrefid'] = $user['ulmusmrefid'];
                        $inserting['rlmrefid'] = $role['rlmrefid'];
                        $inserting['isdeleted'] = 0;
                        $inserting['isactive'] = 1;
                        $inserting['assignedby'] = $data['login_id'];
                        $inserting['assignedon'] = date('Y-m-d H:i:s');
						$inserting['revokedon']	 = date('Y-m-d H:i:s');
						$inserting['revokedby']  = $data['login_id'];
                        $this->db->insert('clt_tra_tm', $inserting);
                        //$response [] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "mobile" => //$user_mobile, "role" => $data['user_role'], "status" => "success", "message" => "Mapping done");
                        //continue;
                    }                    
                    // $response [] = array("s.no"=>$i,"training"=>$trarefid,"mobile"=>$user_mobile,"role"=>$data['user_role'],"status"=>"success","message"=>"");
                    $response[] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "mobile" => $user_mobile, "role" => $data['user_role'], "status" => "success", "message" => "Mapping done");
                }
            }
            //pre($response);
            return $response;
        }
        //pre($response);
        return $response;
    }

    public function list_of_mapping_users_to_trainings($data) {
        $this->db->select('traname,usmfirstname,usmmiddlename,usmlastname,usmmob,rlmrole,clt_tra_tm.tratmrefid as mapping_id,clt_tra_tm.assignedon,clt_tra_tm.assignedby,clt_tra_tm.revokedby,clt_tra_tm.revokedon');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->where('clt_tra_tm.isdeleted', 0);
        $this->db->where('clt_tra_tm.isactive', 1);
        $this->db->join('curtra', 'curtra.trarefid=clt_tra_tm.trarefid');
        $this->db->join('escusm', 'escusm.usmrefid=clt_tra_tm.tmrefid');
        $this->db->join('escrlm', 'escrlm.rlmrefid=clt_tra_tm.rlmrefid');
        $this->db->order_by('clt_tra_tm.revokedon', 'desc');
        $result = $this->db->get('clt_tra_tm')->result_array();
        //echo $this->db->last_query();die;
        return $result;
    }

    public function delete_users_mapping($data) {
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach ($data['mapping_ids'] as $m_id) {
            $this->db->where('tratmrefid', $m_id);
            $exist = $this->db->get('clt_tra_tm')->row();
            if ($exist) {
                $this->db->where('tratmrefid', $m_id);
                $this->db->update('clt_tra_tm', array('isdeleted' => 1, 'isactive' => 0, 'revokedon' => date('Y-m-d H:i:s'), 'revokedby' => $data['login_id']));
            }
        }
        // echo $this->db->last_query();
        return true;
    }

        
    ///////////////////// CERTIFICATE APPROVES ////////////////////////////////////////
    
    public function get_training_instance($data){
        $this->db->where('trbtrnrefid', $data['trnrefid']);
        $batches = $this->db->get('clt_trn')->result_array();
        return $batches;
    }
    
    
    
    public function get_role($data){
		if(isset($data['role']) && !empty($data['role'])){
			$this->db->where('rlmrole', $data['role']);
		}
        $role = $this->db->get('escrlm')->row_array();
        return $role;
    }
	
	public function get_attribute($data){
		if(isset($data['attribute']) && !empty($data['attribute'])){
			$this->db->where('oeaattributename', $data['attribute']);
		}        
        $role = $this->db->get('escoea')->row_array();
        return $role;
    }
    /*
    public function certificate_approves_list($data){
        $this->db->select('curtrm.trmrefid,curtra.traname,clt_trn.trnname,curtrb.batchname,escusm.usmfirstname,escusm.usmlastname,escusm.usmmob,curtrm.createddate,curtrm.trmrefid as mapping_id');
        $this->db->join('escusm','escusm.usmrefid=curtrm.trmusmrefid');
        $this->db->join('curtrb','curtrb.trbrefid=curtrm.trmtrbrefid');
        $this->db->join('clt_trn','clt_trn.trnrefid=curtrb.trbtrnrefid');
        $this->db->join('curtra','curtra.trarefid=clt_trn.trntrarefid');
        $this->db->where('curtrm.isdeleted', 0);
        $this->db->where('curtrm.isactive', 1);
        $this->db->order_by('curtrm.trmrefid','desc');
        $result = $this->db->get('curtrm')->result_array(); //pre($result);
        return $result;
    }
    
    public function certificate_approves($data) {  //pre($data); //die;
        
        $role        = $this->get_role(array("role"=>$data['role'])); 
        if(empty($role)){
            return_data(false, 'Role could not match.', array());
        }        
        $trn        = $this->get_training_instances(array("trnrefid"=>$data['trnrefid'])); //pre($trn); die;
        if(empty($trn)){
            return_data(false, 'Training instace not found.', array());
        }
        $response   = array();
        $i          = 0;
        $batches = $this->get_training_instance_batches($data);
        if(empty($batches)){
            return_data(false, 'No batches available for this training instance.', array());
        }
        if($batches){ //pre($batches);
            foreach($batches as $batch){
                foreach ($data['user_mobiles'] as $user_mobile) { 
                    $i++;
                    if (empty($batch)) {
                        $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => "n/a", "mobile" => $user_mobile, "status" => "fail", "message" => "Batch not exist");
                        continue;
                    }
                    
                    $this->db->where('ulmusername', $user_mobile);
                    $user = $this->db->get('esculm')->row_array(); //echo $this->db->last_query()."<br>";  pre($user);
                    if (empty($user)) {
                        $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => $batch['batchname'], "mobile" => $user_mobile, "status" => "fail", "message" => "Invalid mobile number");
                        continue;
                    }
                    
                    $this->db->where('urmusmrefid', $user['ulmusmrefid']);
                    $this->db->where('urmrlmrefid', $role['rlmrefid']);
                    $already = $this->db->get('escurm')->row_array();
                    if (empty($already)) {
                        $inserting                  = array();
                        $inserting['urmusmrefid']   = $user['ulmusmrefid'];
                        $inserting['urmrlmrefid']   = $role['rlmrefid'];
                        $inserting['createddate']   = date('Y-m-d H:i:s');
                        $inserting['isdeleted']     = 0;
                        $inserting['isactive']      = 1;
                        $this->db->insert('escurm', $inserting);
                    }else if($already['isdeleted'] == 1){
                        $this->db->where("urmrefid",$already['urmrefid']);
                        $this->db->update('escurm', array("isdeleted"=>0,"isactive"=>1,"modifieddate"=>date('Y-m-d H:i:s')));
                    }
                    
                    
                    //$this->db->select("rlmrefid");
                    $this->db->where('trmtrbrefid', $batch['trbrefid']);
                    $this->db->where('trmusmrefid', $user['ulmusmrefid']);
                    $exist = $this->db->get('curtrm')->row_array();
                    if($exist){ 
                        if($exist['isdeleted']==0){
                            $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => $batch['batchname'], "mobile" => $user_mobile, "status" => "fail", "message" => "Record already exist");
                            continue;
                        }else{ 
                            $updating['isdeleted']      = 0;
                            $updating['isactive']       = 1;
                            $updating['modifiedby']     = $data['login_id'];
                            $updating['modifieddate']   = date('Y-m-d H:i:s');
                            $this->db->where('trmrefid', $exist['trmrefid']);
                            $this->db->update('curtrm', $updating);
                        }
                    }else{
						$inserting 					= array();
                        $inserting['trmtrbrefid']   = $batch['trbrefid'];
                        $inserting['trmusmrefid']   = $user['ulmusmrefid'];
                        $inserting['isdeleted']     = 0;
                        $inserting['isactive']       = 1;
                        $inserting['createdby']     = $data['login_id'];
                        $inserting['createddate']   = date('Y-m-d H:i:s');
                        $this->db->insert('curtrm', $inserting);
                    }
                    $response [] = array("s_no" => $i, "training_instance" => $trn[0]['trnname'], "batch" => $batch['batchname'], "mobile" => $user_mobile, "status" => "success", "message" => "Done Successfully");
                    
                }
            }
        }
        return $response;
    }
    */
    public function delete_certificate_approve($data) {//pre($data);
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach ($data['mapping_ids'] as $m_id) {//echo $m_id;
            $this->db->where('trmrefid', $m_id);
            $exist = $this->db->get('curtrm')->row();
            if ($exist) {
                $this->db->where('trmrefid', $m_id);
                $this->db->update('curtrm', array('isdeleted' => 1, 'isactive' => 0, 'modifieddate' => date('Y-m-d H:i:s'), 'modifiedby' => $data['login_id']));
            }
        }
        // echo $this->db->last_query();
        return true;
    }
    
////////////////////////  SENDER ID MAPPING   ///////////////////////////////

    public function sender_id_mapping_list($data) {
        $this->db->select('trasenderrefid as mapping_id,sendercode,traname');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->where('clt_tra_sms_sender.isdeleted', 0);
        $this->db->where('clt_tra_sms_sender.isactive', 1);
        $this->db->join('clt_mat_sms_sender', 'clt_mat_sms_sender.senderrefid=clt_tra_sms_sender.senderid');
        $this->db->join('curtra', 'curtra.trarefid=clt_tra_sms_sender.trarefid');
        $this->db->order_by('trasenderrefid', 'desc');
        $result = $this->db->get('clt_tra_sms_sender')->result_array();
        //echo $this->db->last_query();die;
        return $result;
    }

    public function map_sender_id($data) {
        $i = 0;
        $inserting['assignedby'] = $data['login_id']; //$data['assignedby'];        
        if (isset($data['trainings']) && !empty($data['trainings']) && isset($data['sender_ids']) && !empty($data['sender_ids'])) {
            foreach ($data['trainings'] as $trarefid) {
                $inserting['trarefid'] = $trarefid;
                $training = $this->get_training_agency(array("trarefid" => $trarefid));
                foreach ($data['sender_ids'] as $sender_id) {
                    $i++;
                    $sender_id_data = $this->get_sender_id(array("senderrefid" => $sender_id));
                    if (empty($training) && empty($sender_id_data)) {
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => "n/a", "sender_id_id" => $sender_id, "sender_id_name" => "n/a", "status" => "fail", "message" => "Training and SENDER-ID not exist");
                        continue;
                    } else if (empty($sender_id_data)) {
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "sender_id_id" => $sender_id, "sender_id_name" => "n/a", "status" => "fail", "message" => "SENDER-ID not exist");
                        continue;
                    } else if (empty($training)) {
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => "n/a", "sender_id_id" => $sender_id, "sender_id_name" => $sender_id_data['sendercode'], "status" => "fail", "message" => "Training not exist");
                        continue;
                    }

                    $this->db->where('trarefid', $trarefid);
                    $this->db->where('senderid', $sender_id);
                    $exist = $this->db->get('clt_tra_sms_sender')->row_array(); //echo $this->db->last_query()."-";
                    if ($exist) {
                        if ($exist['isdeleted'] == 0) {
                            $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "sender_id_id" => $sender_id, "sender_id_name" => $sender_id_data['sendercode'], "status" => "fail", "message" => "Mapping already exist");
                            continue;
                        } else {
                            $updating['revokedon'] = date('Y-m-d H:i:s');
                            $updating['isdeleted'] = 0;
                            $updating['isactive'] = 1;
                            $this->db->where('trasenderrefid', $exist['trasenderrefid']);
                            $this->db->update('clt_tra_sms_sender', $updating);
                            $response[] = array("s.no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "sender_id_id" => $sender_id, "sender_id_name" => $sender_id_data['sendercode'], "status" => "success", "message" => "Mapping done");
                            continue;
                        }
                    } else {
                        $inserting['assignedon'] = date('Y-m-d H:i:s');
                        $inserting['senderid'] = $sender_id;
                        $inserting['isdeleted'] = 0;
                        $inserting['isactive'] = 1;
                        $this->db->insert('clt_tra_sms_sender', $inserting);
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "sender_id_id" => $sender_id, "sender_id_name" => $sender_id_data['sendercode'], "status" => "success", "message" => "Mapping done");
                        continue;
                    }
                    //echo $this->db->last_query()."<br>";					
                }
            }
        }//die('9working');
        return $response;
    }

    public function delete_sender_id_mapping($data) {
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach ($data['mapping_ids'] as $m_id) { //echo 1;
            $this->db->where('trasenderrefid', $m_id);
            $exist = $this->db->get('clt_tra_sms_sender')->row();
            if ($exist) {
                $this->db->where('trasenderrefid', $m_id);
                $this->db->update('clt_tra_sms_sender', array('isdeleted' => 1, 'isactive' => 0, 'revokedon' => date('Y-m-d H:i:s'), 'revokedby' => $data['login_id']));
            }
        }
        // echo $this->db->last_query();
        return true;
    }

    public function sender_id_list($data) {

        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->where('isactive', '1');
        $this->db->order_by('senderrefid', 'desc');
        $result = $this->db->get('clt_mat_sms_sender')->result_array();

        foreach ($result as $key => $value) {

            $this->db->where('senderid', $value['senderrefid']);
            $checkmapping = $this->db->get('clt_tra_sms_sender')->result_array();
            if (!empty($checkmapping)) {
                $result[$key]['ismapped'] = true;
            } else {
                $result[$key]['ismapped'] = false;
            }
        }
        return $result;
    }

    ////////////////////////  CERTIFICATE TEMPLATES MAPPING   ///////////////////////////////

    public function list_of_mapping_certificates_to_trainings($data) {
        $this->db->select('tracertrefid as mapping_id,certtempid,ctmtemplatename,curtra.trarefid,traname,clt_tra_cert_temp.isdeleted,clt_tra_cert_temp.isactive');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->where('clt_tra_cert_temp.isdeleted', 0);
        $this->db->where('clt_tra_cert_temp.isactive', 1);
        $this->db->join('cltcertificatetemplate', 'cltcertificatetemplate.ctmrefid=clt_tra_cert_temp.certtempid');
        $this->db->join('curtra', 'curtra.trarefid=clt_tra_cert_temp.trarefid');

        $this->db->order_by('tracertrefid', 'desc');
        $result = $this->db->get('clt_tra_cert_temp')->result_array();
        //echo $this->db->last_query();die;
        return $result;
    }

    public function map_certificates_to_trainings($data) {
        $i = 0;
        $response = array();
        $inserting['assignedby'] = $data['login_id']; //$data['assignedby'];        
        if (isset($data['trainings']) && !empty($data['trainings']) && isset($data['certificates']) && !empty($data['certificates'])) {
            foreach ($data['trainings'] as $trarefid) {
                $inserting['trarefid'] = $trarefid;
                $training = $this->get_training_agency(array("trarefid" => $trarefid));
                foreach ($data['certificates'] as $ctmrefid) {
                    $i++;
                    $certificate = $this->get_certificate(array("ctmrefid" => $ctmrefid));
                    if (empty($training) && empty($certificate)) {
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => "n/a", "certificate_id" => $ctmrefid, "certificate_name" => "n/a", "status" => "fail", "message" => "Training and Certificate not exist");
                        continue;
                    } else if (empty($certificate)) {
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "certificate_id" => $ctmrefid, "certificate_name" => "n/a", "status" => "fail", "message" => "Certificate not exist");
                        continue;
                    } else if (empty($training)) {
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => "n/a", "certificate_id" => $ctmrefid, "certificate_name" => $certificate['ctmtemplatename'], "status" => "fail", "message" => "Training not exist");
                        continue;
                    }

                    $this->db->where('trarefid', $trarefid);
                    $this->db->where('certtempid', $ctmrefid);
                    $exist = $this->db->get('clt_tra_cert_temp')->row_array(); //echo $this->db->last_query()."-";
                    if ($exist) {
                        if ($exist['isdeleted'] == 0) {
                            $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "certificate_id" => $ctmrefid, "certificate_name" => $certificate['ctmtemplatename'], "status" => "fail", "message" => "Mapping already exist");
                            continue;
                        } else {
                            $updating['revokedon'] = date('Y-m-d H:i:s');
                            $updating['revokedby'] = $data['login_id'];
                            $updating['isdeleted'] = 0;
                            $updating['isactive'] = 1;
                            $this->db->where('tracertrefid', $exist['tracertrefid']);
                            $this->db->update('clt_tra_cert_temp', $updating);
                            $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "certificate_id" => $ctmrefid, "certificate_name" => $certificate['ctmtemplatename'], "status" => "success", "message" => "Mapping done");
                            continue;
                        }
                    } else {
                        $inserting['assignedon'] = date('Y-m-d H:i:s');
                        $inserting['certtempid'] = $ctmrefid;
                        $inserting['isdeleted'] = 0;
                        $inserting['isactive'] = 1;
                        $this->db->insert('clt_tra_cert_temp', $inserting);
                        $response[] = array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'], "certificate_id" => $ctmrefid, "certificate_name" => $certificate['ctmtemplatename'], "status" => "success", "message" => "Mapping done");
                        continue;
                    }
                    //echo $this->db->last_query()."<br>";	
                }
            }//die('9working');
            //pre($response);die('12');
            return $response;
        }
    }

    public function delete_certificates_mapping($data) { //die('working');
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach ($data['mapping_ids'] as $m_id) { //echo 1;
            $this->db->where('tracertrefid', $m_id);
            $exist = $this->db->get('clt_tra_cert_temp')->row();
            if ($exist) {
                $this->db->where('tracertrefid', $m_id);
                $this->db->update('clt_tra_cert_temp', array('isdeleted' => 1, 'isactive' => 0, 'revokedon' => date('Y-m-d H:i:s'), 'revokedby' => $data['login_id']));
            }
        }
        // echo $this->db->last_query();
        return true;
    }

    public function certificates_list($data) { //die('jhgj');
        $this->db->select('ctmrefid,ctmtemplatename');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->order_by('ctmrefid', 'desc');
        $result = $this->db->get('cltcertificatetemplate')->result_array();
        return $result;
    }

    public function get_channel_list($data) { //die('jhgj');
        $this->db->select('chmrefid,chmname');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->where('isactive', 1);
        $this->db->where('isdeleted', 0);
        $this->db->order_by('chmrefid', 'desc');
        $result = $this->db->get('escchm')->result_array();
        return $result;
    }

    public function get_wall_posting_users($data) { //die('jhgj');
        $this->db->select('escusm.usmrefid,escusm.usmfirstname,escusm.usmmiddlename,escusm.usmlastname');
        $this->db->join('escusm', 'escusm.usmrefid=escuep.uepusmrefid');
        $this->db->join('escoea', 'escoea.oearefid=escuep.uepoearefid');
        $this->db->where('escoea.oeaattributename', 'RIGHT_POST_EPC');
        $result = $this->db->get('escuep')->result_array();
        return $result;
    }

    public function channel_mapping_list($data) { //die('jhgj');
        $this->db->select('chmrefid,chmname,usmfirstname,usmmiddlename,usmlastname,usmmob,usmemailid');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->where('isactive', 1);
        $this->db->where('isdeleted', 0);
        $this->db->join('escusm', 'escusm.usmrefid=escchm.chmusmrefid');
        $this->db->order_by('chmrefid', 'desc');
        $result = $this->db->get('escchm')->result_array();
        return $result;
    }

    public function add_training($input) {


        if (!empty($input['traname']) || !empty($_FILES['trasplash']['name']) || !empty($_FILES['tralogo']['name'])) {


            $uploadedFile = '';
            if (!empty($_FILES["trasplash"]["type"])) {

                if($_FILES["trasplash"]["size"] > 50000){
                    return  $response[] = array("result" => array(),"message"=>"Splash :File size must not exceed 10kb.","status"=>"fail");
                }

                list($width, $height) = getimagesize($_FILES['trasplash']['tmp_name']);
                if ($height != '640' || $width != '512') {
                    return $response[] = array("result" => array(), "message" => "Splash :File resolution must be 512x640.", "status" => "fail");
                }

                // $fileName = rand(100000, 999999) . $_FILES['trasplash']['name'];
                $random_three_digit = rand(100,999);
                $fileName=date('YmdHis').$random_three_digit;
                $valid_extensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $_FILES["trasplash"]["name"]);
                $file_extension = end($temporary);
                if ((($_FILES["trasplash"]["type"] == "image/png") || ($_FILES["trasplash"]["type"] == "image/jpg") || ($_FILES["trasplash"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)) {
                    $sourcePath = $_FILES['trasplash']['tmp_name'];
                    $path = TARGET_PATH_TO_UPLOAD_TRA_SPLASH;
                    move_uploaded_file($sourcePath, $path . $fileName);
                    $data['trasplash'] = $fileName;
                } else {

                    return $response[] = array("result" => array(), "message" => "Splash :Please select a valid image file (JPEG/JPG/PNG)", "status" => "fail");
                }
            }

            if (!empty($_FILES["tralogo"]["type"])) {

                list($width, $height) = getimagesize($_FILES['tralogo']['tmp_name']);
                if ($height != '100' || $width != '100') {
                    return $response[] = array("result" => array(), "message" => "Logo:resolution must be 100x100.", "status" => "fail");
                }
                if ($_FILES["tralogo"]["size"] > 10000) {
                    return $response[] = array("result" => array(), "message" => "Logo :File size must not exceed 10kb.", "status" => "fail");
                }
                // $fileName = rand(100000, 999999) . $_FILES['tralogo']['name'];
                 $random_three_digit = rand(100,999);
                $fileName=date('YmdHis').$random_three_digit;
                $valid_extensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $_FILES["tralogo"]["name"]);
                $file_extension = end($temporary);
                if ((($_FILES["tralogo"]["type"] == "image/png") || ($_FILES["tralogo"]["type"] == "image/jpg") || ($_FILES["tralogo"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)) {
                    $sourcePath = $_FILES['tralogo']['tmp_name'];
                    $path = TARGET_PATH_TO_UPLOAD_TRA_LOGO;
                    move_uploaded_file($sourcePath, $path . $fileName);
                    $data['tralogo'] = $fileName;
                } else {

                    return $response[] = array("result" => array(), "message" => "Logo :Please select a valid image file (JPEG/JPG/PNG)", "status" => "fail");
                }
            }
        }

        $data['traname'] = $input['traname'];
        $data['createdby'] = $input['login_id'];
        $data['isactive'] = TRUE;
        $data['isdeleted'] = 0;
        $data['createddate'] = date('Y-m-d H:i:s');
        $result = $this->db->insert('curtra', $data);
        return $response[] = array("result" => $result, "message" => "true", "status" => "success");
    }

    public function get_by_trarefid($data) {

        if (isset($data['trarefid']) && !empty($data['trarefid'])) {
            $this->db->where('trarefid', $data['trarefid']);
        }
        $result = $this->db->get('curtra')->row();
        return $result;
    }

    public function edit_training($data) {
        $this->db->where('trarefid', $data['trarefid']);
        $exist = $this->db->get('curtra')->row();
        if ($exist) {

            if (!empty($_FILES['trasplash']['name']) || !empty($_FILES['tralogo']['name'])) {


                $uploadedFile = '';
                if (!empty($_FILES["trasplash"]["type"])) {

                    // if($_FILES["trasplash"]["size"] > 10000){
                    //     return  $response[] = array("result" => array(),"message"=>"Splash :File size must not exceed 10kb.","status"=>"fail");
                    // }
                    list($width, $height) = getimagesize($_FILES['trasplash']['tmp_name']);
                    if ($height != '100' || $width != '400') {
                        return $response[] = array("result" => array(), "message" => "Splash :File resolution must be 100x400.", "status" => "fail");
                    }

                    $fileName = rand(100000, 999999) . $_FILES['trasplash']['name'];
                    $valid_extensions = array("jpeg", "jpg", "png");
                    $temporary = explode(".", $_FILES["trasplash"]["name"]);
                    $file_extension = end($temporary);
                    if ((($_FILES["trasplash"]["type"] == "image/png") || ($_FILES["trasplash"]["type"] == "image/jpg") || ($_FILES["trasplash"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)) {
                        $sourcePath = $_FILES['trasplash']['tmp_name'];
                        $path = TARGET_PATH_TO_UPLOAD_TRA_SPLASH;
                        move_uploaded_file($sourcePath, $path . $fileName);
                        $data['trasplash'] = $fileName;
                    } else {

                        return $response[] = array("result" => array(), "message" => "Splash :Please select a valid image file (JPEG/JPG/PNG)", "status" => "fail");
                    }
                }
            }
            if (!empty($_FILES['tralogo']['name'])) {
                if (!empty($_FILES["tralogo"]["type"])) {


                    list($width, $height) = getimagesize($_FILES['tralogo']['tmp_name']);
                    if ($height != '100' || $width != '100') {
                        return $response[] = array("result" => array(), "message" => "Logo:resolution must be 100x100.", "status" => "fail");
                    }

                    if ($_FILES["tralogo"]["size"] > 10000) {
                        return $response[] = array("result" => array(), "message" => "Logo :File size must not exceed 10kb.", "status" => "fail");
                    }
                    $fileName = rand(100000, 999999) . $_FILES['tralogo']['name'];
                    $valid_extensions = array("jpeg", "jpg", "png");
                    $temporary = explode(".", $_FILES["tralogo"]["name"]);
                    $file_extension = end($temporary);
                    if ((($_FILES["tralogo"]["type"] == "image/png") || ($_FILES["tralogo"]["type"] == "image/jpg") || ($_FILES["tralogo"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)) {
                        $sourcePath = $_FILES['tralogo']['tmp_name'];
                        $path = TARGET_PATH_TO_UPLOAD_TRA_LOGO;
                        move_uploaded_file($sourcePath, $path . $fileName);
                        $data['tralogo'] = $fileName;
                    } else {

                        return $response[] = array("result" => array(), "message" => "Logo :Please select a valid image file (JPEG/JPG/PNG)", "status" => "fail");
                    }
                }
            }


            if (isset($data['tralogo'])) {
                $update['tralogo'] = $data['tralogo'];
            }
            if (isset($data['trasplash'])) {
                $update['trasplash'] = $data['trasplash'];
            }
            if (isset($data['traname'])) {
                $update['traname'] = $data['traname'];
            }
            $update['modifieddate'] = date('Y-m-d H:i:s');

            $this->db->where('trarefid', $data['trarefid']);
            $update = $this->db->update('curtra', $update);
            if ($update) {
                $result = $this->get_by_trarefid($data['trarefid']);
                return $response[] = array("result" => $result, "message" => "true", "status" => "success");
            }
        }
        return false;
    }

    public function getTraCourses($traid){

    $select = "currcm.rcmname,currcm.chaptername,currcm.rcmversionlabel";
    $this->db->select($select);
    $this->db->where('clt_tra_rcm.trarefid',$traid);
   // $this->db->where('curtra.trarefid',$traid);
    $this->db->where('clt_tra_rcm.isactive',1);
    $this->db->where('clt_tra_rcm.isdeleted',0);                
    $this->db->where('curtra.isdeleted',0);  
    $this->db->join('curtra','curtra.trarefid=clt_tra_rcm.trarefid');
    $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid'); 
    $this->db->order_by('clt_tra_rcm.trarcmrefid','desc');        
    $result = $this->db->get('clt_tra_rcm')->result_array();
    
    return $result;  
}


public function getTraCertificateMapping($traid){

    $this->db->select('tracertrefid as certificate_id,ctmtemplatename as template_name');

    $this->db->where('clt_tra_cert_temp.trarefid', $traid);
    $this->db->where('clt_tra_cert_temp.isdeleted', 0);
    $this->db->where('clt_tra_cert_temp.isactive', 1);
    $this->db->join('cltcertificatetemplate', 'cltcertificatetemplate.ctmrefid=clt_tra_cert_temp.certtempid');
    $this->db->join('curtra', 'curtra.trarefid=clt_tra_cert_temp.trarefid');

    $this->db->order_by('tracertrefid', 'desc');
    $result = $this->db->get('clt_tra_cert_temp')->result_array();
    return $result;
}

public function getTraSenderMapping($traid){

    $this->db->select('trasenderrefid as mapping_id,sendercode');
    if (isset($data['length']) && isset($data['start'])) {
        $this->db->limit($data['length'], $data['start']);
    }
    $this->db->where('clt_tra_sms_sender.trarefid', $traid);
    $this->db->where('clt_tra_sms_sender.isdeleted', 0);
    $this->db->where('clt_tra_sms_sender.isactive', 1);
    $this->db->join('clt_mat_sms_sender', 'clt_mat_sms_sender.senderrefid=clt_tra_sms_sender.senderid');
    $this->db->join('curtra', 'curtra.trarefid=clt_tra_sms_sender.trarefid');
    $this->db->order_by('trasenderrefid', 'desc');
    $result = $this->db->get('clt_tra_sms_sender')->result_array();
    return $result;
}

public function getTraUserMapping($traid){

    $this->db->select("CONCAT((usmfirstname),(' '),(usmmiddlename),(' '),(usmlastname)) as name ,usmmob,rlmrole");
    $this->db->where('clt_tra_tm.trarefid', $traid);
    $this->db->where('clt_tra_tm.isdeleted', 0);
    $this->db->where('clt_tra_tm.isactive', 1);
    $this->db->join('curtra', 'curtra.trarefid=clt_tra_tm.trarefid');
    $this->db->join('escusm', 'escusm.usmrefid=clt_tra_tm.tmrefid');
    $this->db->join('escrlm', 'escrlm.rlmrefid=clt_tra_tm.rlmrefid');
    $this->db->order_by('clt_tra_tm.revokedon', 'desc');
    $result = $this->db->get('clt_tra_tm')->result_array();
    return $result;
}


public function getTrnInstance($traid){

    $this->db->select('curtra.trarefid, curtra.traname, COUNT(clt_trn.trnrefid) AS ti');
    $this->db->where('clt_trn.isvisibleonclient', 1);
    $this->db->where('clt_trn.isactive', 1);
    $this->db->where('curtra.trarefid', $traid);
    $this->db->join('clt_trn', 'clt_trn.trntrarefid=curtra.trarefid');
    
    $this->db->order_by('ti', 'desc');
    $result = $this->db->get('curtra')->row_array();
    return $result;
}

public function getTrnTotalTeacherTrained($traid){

    $this->db->select('curtra.traname, COUNT(currcu.rcuusmrefid) AS trainee');
    $this->db->where('clt_trn.isvisibleonclient', 1);
    $this->db->where('clt_trn.isactive', 1);
    $this->db->where('curtra.trarefid', $traid);
    $this->db->join('curtrb', 'curtrb.trbrefid=currcu.rcutrbrefid');
    $this->db->join('clt_trn', 'clt_trn.trnrefid=curtrb.trbtrnrefid');
    $this->db->join('curtra', 'curtra.trarefid=clt_trn.trntrarefid');
    $this->db->order_by('trainee', 'desc');
    $result = $this->db->get('currcu')->row_array();
    return $result;
}

 public function deleteUser($inputs){

        foreach($inputs['users'] as  $userid){

           $data['isactive'] = '0';
           $data['deactivedate'] = date('Y-m-d H:i:s');
           $data['extrainfo'] = 'Deleted from panel';
           $this->db->where('rcurefid',$userid);
           $update = $this->db->update('currcu',$data);
        }
        return true;
}
public function updateUser($inputs){

        foreach($inputs['users'] as  $userid){

           $data['rcurole'] = trim($inputs['rcurole']);
           $data['extrainfo'] = 'Updated from panel';
           $this->db->where('rcurefid',$userid);
           $update = $this->db->update('currcu',$data);
        }
        return true;
}

public function getUserByAuthToken($access_token){

    $result = $this->db->where('accesstoken',$access_token);
    $result = $this->db->get('esculm')->row_array();
    return $result['ulmusmrefid'];
}
public function getAllTraAssignToUser($userid){

    $this->db->select('trarefid');
    $result = $this->db->where('tmrefid',$userid);
    $result = $this->db->get('clt_tra_tm')->result_array();
    return $result;
}

}
