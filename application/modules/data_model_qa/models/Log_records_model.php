<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_records_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function activity_log($input) {
        //pre($input);
        $input['createddate'] = date('Y-m-d H:i:s');
        $this->db->db_select('esocial_log');
        $result = $this->db->insert('json_activity_log',$input);
        //echo $this->db->last_query();
        $this->db->db_select('chalklit');
        return $result;
    }
	
	public function create_logs_for_search_api($input) {
		$input['searchedon'] 	= date('Y-m-d H:i:s');
        $result 				= $this->db->insert('clt_his_search',$input);
        return true;
    }  
	
	
	
	
			
}
