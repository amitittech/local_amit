<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        $this->load->model('Reports_model');
        $this->load->helper('custom');
    }
	
	public function get_report($inputs){        
		if(!isset($inputs['object_type']) OR empty($inputs['object_type'])){
			return array("status"=>false,"message"=>'object type  field is required.',"result"=>array());
		}
		if(!isset($inputs['object_entity_id']) OR empty($inputs['object_entity_id'])){
			return array("status"=>false,"message"=>'object type  field is required.',"result"=>array());
		}
		if(!isset($inputs['moduleid']) OR empty($inputs['moduleid'])){
		   return array("status"=>false,"message"=>'module Id field is required.',"result"=>array());
		} 
		$questions =   $this->Reports_model->get_all_question_on_module($inputs['moduleid']);
		if($questions){
			if($inputs['object_type']=='TRN'){/*get take a test based on training or module */
				$training_instance = $this->Reports_model->checkTraning($inputs['object_entity_id']);
				if($training_instance) {
					$trn_batch =   $this->Reports_model->get_take_a_test_on_training($training_instance['trnrefid'],$inputs['moduleid'],$training_instance['enddate'],$training_instance['startdate']);
					if($trn_batch){
						$result[0] = new \stdClass();
						$result[1] = new \stdClass();
						$result[0]->user_answer = $trn_batch;
						$result[1]->ques = $questions;
						return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=> $result);
					}else{
						return array("status"=>false,"message"=>'This training has no users .',"result"=>array());
					}
				}else{
					 return array("status"=>false,"message"=>'Invalid Training Instance.',"result"=>array());
				}
			}elseif($inputs['object_type']=='TRB'){/*get take a test for single batch */
				$check_batch =   $this->Reports_model->check_training_batch($inputs['object_entity_id']);
				if($check_batch){
					$report_on_batch =   $this->Reports_model->get_take_a_test_on_batch($check_batch ['trbrefid'],$inputs['moduleid']);
					if($report_on_batch){
						$result[0] = new \stdClass();
						$result[1] = new \stdClass();
						$result[0]->user_answer = $report_on_batch;
						$result[1]->ques = $questions;
						return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=> $result);
						// return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>array('user_answer'=>$report_on_batch,'ques'=>$questions));
					}else{
						return array("status"=>false,"message"=>'This training has no users.',"result"=>array());
					}
				}else{
					return array("status"=>false,"message"=>'Invalid Batch.',"result"=>array());
				}
			}elseif($inputs['object_type']=='TSB'){ /*get take a test for super batch */
				$super_batch =  $this->Reports_model->check_super_batch($inputs['object_entity_id']);
				if($super_batch){
					$report_on_super_batch= $this->Reports_model->get_take_a_test_on_super_batch($super_batch['tsbrefid'],$inputs['moduleid']);
					if($report_on_super_batch){
						$result[0] = new \stdClass();
						$result[1] = new \stdClass();
						$result[0]->user_answer = $report_on_super_batch;
						$result[1]->ques = $questions;
						return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=> $result);
						// return array("status"=>true,"message"=>'Data fetched sucessfully.',"result"=>array('user_answer'=>$report_on_super_batch,'ques'=>$questions));
					}else{
						return array("status"=>false,"message"=>'This training has no users.',"result"=>array());
					}
				}else{
					return array("status"=>false,"message"=>'Invalid Super Batch.',"result"=>array());
				}
			}
		}else{
			return array("status"=>false,"message"=>'No Post test available for this module.',"result"=>array());
		}
	}
		
}
