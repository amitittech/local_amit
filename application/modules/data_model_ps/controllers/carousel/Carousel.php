<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Carousel extends MX_Controller {
   
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            //echo json_encode(array('status'=>'fail', 'message'=>'use only post method', 'result'=>array()));die;
            //die('use only post method');
        }
        parent::__construct();
        // $this->load->model('Reports_model');
        // $this->load->helper('custom');
       

        // $this->load->helper('aul');
        // modules::run('auth_panel/auth_panel_ini/auth_ini');
        // $this->load->library('session');
        // $this->load->helper(array('form', 'url'));
  }


public function trn_count($input){

    $SLAVE_DB = $this->load->database('chalklit_slave_db', TRUE);
   
$SLAVE_DB->from('escchm');

$query = $SLAVE_DB->get();

    //$query="SELECT COUNT(*) as training_count FROM `clt_trn` WHERE `isvisibleonclient`=1;";
    //$this->$SLAVE_DB->db_debug = false;

    // if(!@$SLAVE_DB->query($query)){

    //     $error = $SLAVE_DB->error();
    //     return_data('false', $error['message'], array());
        
    // }else{
         $training_count =   $SLAVE_DB->query($query)->row();
    //     return_data(true, 'Total training fetched sucessfully', $training_count->training_count);

    //  }
  }

  public function trn_total_teachers($input){

  $query = "select count(distinct rcuusmrefid) teacher from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn on(trbtrnrefid = trnrefid) where isvisibleonclient = 1;";
    $this->db->db_debug = false;

    if(!@$this->db->query($query)){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $teacher_count =   $this->db->query($query)->row();

        return_data(true, 'Total Teachers fetched sucessfully', $teacher_count->teacher);

     }

  }
  public function trn_total_topics_covered($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $topic_covered_count =   $this->db->query($input['query'])->row();
        return_data(true, 'Total Topics fetched sucessfully', $topic_covered_count->topics);

     }

  }

  public function trn_state_wise_count($input){

    $query="select count(*)`totaltrn`,statename from clt_trn A join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 group by A.trnstaterefid order by statename;";
    $this->db->db_debug = false;

    if(!@$this->db->query($query)){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($query)->result_array();
         $i=0;
        foreach ($data as $key => $value) {
          $region_data[$i]['state'] = $value['statename'];
          $region_data[$i]['trn'] = intval($value['totaltrn']) ;
          $i++;
          
        }
       return_data(true, 'State wise training fetched sucessfully', $region_data);

     }

  }
  public function state_wise_training_last_28_days($input){

    $query="select count(*)`totaltrn`,statename from clt_trn A join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 and (A.regstartdate >= date_add(date(now()), Interval -300 day) or A.regenddate >= date_add(date(now()), Interval -300 day) or A.startdate >= date_add(date(now()), Interval -300 day) or A.enddate >= date_add(date(now()), Interval -300 day)) group by A.trnstaterefid order by statename;";
    $this->db->db_debug = false;

    if(!@$this->db->query($query)){
       $error = $this->db->error();
       return_data('false', $error['message'], array());
     }else{
        $data =   $this->db->query($query)->result_array();
        if($data){

             $i=0;
             foreach ($data as $key => $value) {
                $region_data[$i]['state'] = $value['statename'];
                $region_data[$i]['trn'] = intval($value['totaltrn']) ;
              $i++;
              
            }
        }
       return_data(true, 'State wise training fetched last 28 days sucessfully', $region_data);

     }

  }

  public function statewise_teacher_trained($input){

    $query ="select statename ,count(distinct rcuusmrefid) teacher from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn A on(trbtrnrefid = trnrefid) join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 group by A.trnstaterefid order by statename;";
    $this->db->db_debug = false;
     if(!@$this->db->query($query)){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($query)->result_array();
          $i=0;
        foreach($data as $key => $value) {
          $resul_array[$i]['state'] = $value['statename'];
          $resul_array[$i]['teacher'] = intval($value['teacher']);
          $i++;
        }
    return_data(true, 'state wise teacher trained fetched sucessfully', $resul_array);
    }
  }
   public function statewise_teacher_trained_last_28_days($input){

      $query = "select statename , count(distinct rcuusmrefid) as teacher from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn A on(trbtrnrefid = trnrefid) join clt_mat_state B on(A.trnstaterefid = B.staterefid) where isvisibleonclient = 1 and (A.regstartdate >= date_add(date(now()), Interval -500 day) or A.regenddate >= date_add(date(now()), Interval -500 day) or A.startdate >= date_add(date(now()), Interval -500 day) or A.enddate >= date_add(date(now()), Interval -500 day)) group by A.trnstaterefid order by statename;";

      $this->db->db_debug = false;
       if(!@$this->db->query($query)){

          $error = $this->db->error();
          return_data('false', $error['message'], array());
          
      }else{
          $data =   $this->db->query($query)->result_array();
            $i=0;
          foreach($data as $key => $value) {
            $resul_array[$i]['state'] = $value['statename'];
            $resul_array[$i]['teacher'] = intval($value['teacher']);
            $i++;
          }
      return_data(true, 'state wise teacher trained fetched sucessfully', $resul_array);
      }
  }



  public function wall_post_count($input){

     if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $wall_post_count =   $this->db->query($input['query'])->row();
        return_data(true, 'Total wall post fetched sucessfully', $wall_post_count->total_post);

     }

  }


  public function content_chapter_count($input){
    
     if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $chapter_count =   $this->db->query($input['query'])->row();
        return_data(true, 'Total chapter count fetched sucessfully', $chapter_count->total_chapter);

     }

  }
  
  public function trn_state_school_covered($input){

    $query = "select statename ,count(distinct usmschoolname) AS school from currcu join curtrb on(rcutrbrefid = trbrefid) join clt_trn A on(trbtrnrefid = trnrefid) join clt_mat_state B on(A.trnstaterefid = B.staterefid) join escusm on(rcuusmrefid = usmrefid) where isvisibleonclient = 1 and ifnull(usmschoolname,'') <> '' group by A.trnstaterefid order by statename;";

    $this->db->db_debug = false;

    if(!@$this->db->query($query)){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($query)->result_array();
        
        $i=0;
        foreach ($data as $key => $value) {
          $region_data[$i]['state'] = $value['statename'];
          $region_data[$i]['school'] = intval($value['school']);
          $i++;
        }
       // echo "<pre>";    print_r($region_data);die;
    return_data(true, 'Post by source fetched sucessfully', $region_data);

     }
}

  public function get_posts_by_source($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($input['query'])->result_array();
        $i=0;
        foreach ($data as $key => $value) {
          $region_data[$i]['source'] = $value['source'];
          $region_data[$i]['total_post'] = intval($value['total_post']);
          $i++;
        }
       // echo "<pre>";    print_r($region_data);die;
    return_data(true, 'Post by source fetched sucessfully', $region_data);

     }
}
public function get_posts_by_type($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($input['query'])->result_array();
        $i=0;
        foreach ($data as $key => $value) {
           $i=0;
          foreach( $value as $k => $v) {
            $post_type[$i]['type'] = $k;
            $post_type[$i]['count'] = intval($v);
            $i++;
          }
        }
      return_data(true, 'Post Type  fetched sucessfully', $post_type);
    }
}
public function trn_state_wise_teaher($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($input['query'])->result_array();

        $i=0;
        foreach($data as $key => $value) {
          $resul_array[$i]['state'] = $value['statename'];
          $resul_array[$i]['teacher'] = intval($value['teacher']);
          $i++;
        }
        //print_r($data);die;
    return_data(true, 'state wise teachers fetched sucessfully', $resul_array);
    }
}
public function trn_state_wise_school_covered($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($input['query'])->result_array();

        $i=0;
        foreach ($data as $key => $value) {
          $resul_array[$i]['state'] = $value['statename'];
          $resul_array[$i]['school'] = intval($value['Total_School']);
          $i++;
        }
        
    return_data(true, 'state wise school count fetched sucessfully', $resul_array);
    }
}
public function activity_distribution_today($input){

    if (empty($input['query'])) {
          return_data(false, 'query is required .', array());
    }
    $this->db->db_debug = false;

    if(!@$this->db->query($input['query'])){

        $error = $this->db->error();
        return_data('false', $error['message'], array());
        
    }else{
        $data =   $this->db->query($input['query'])->result_array();

        $i=0;
        foreach ($data as $key => $value) {
          $resul_array[$i]['activity'] = $value['Description'];
          $resul_array[$i]['count'] = intval($value['totalcount']);
          $i++;
        }
        
    return_data(true, 'Today activity distribution count fetched sucessfully', $resul_array);
    }
}



















  public function executeQuery($query,$type){
// $db['default']['db_debug'] = FALSE;
    if (empty($query)) {
          return_data(false, 'query is required .', array());
    }
    if (empty($type)) {
          return_data(false, 'type is required .', array());
    }

    if($type=='map'){

   
            $query_result = $query;
            $this->db->db_debug = false;

          if(!@$this->db->query($query_result))
          {
              $error = $this->db->error();
              return_data(FALSE, $error['message'], array());
              
          }else{

            $result_data =   $this->db->query($query)->result_array();
            // echo "<pre>";print_r($result_data);die;


            
          }
    
        
        
     }


  }  

	public function training_genderPiechartData($input){
    //print_r($input);die;

  $data =   $this->executeQuery($input['query'],$input['type']);
  echo $data;die;

    echo "<pre>";print_r($input['query']);  die;

    	$query = $this->db->query('SELECT COUNT(*) as totalusers,`usmgender` FROM escusm WHERE `usmgender`= "Male" OR `usmgender`= "female" GROUP BY `usmgender`');
$output=array();
     	foreach ($query->result_array() as $row) {
				$output[] = array(
				 $row["usmgender"],
				 floatval($row["totalusers"])
				);
       }


      // echo "<pre>";print_r($output);die;
      echo json_encode($output);
   }
       
}
