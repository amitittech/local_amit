<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wall_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }    

    public function get_channel_list($data) { //die('jhgj');
        $this->db->select('chmrefid,chmname');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        $this->db->where('isactive',1);
        $this->db->where('isdeleted',0);
        $this->db->order_by('chmrefid', 'desc');
        $result = $this->db->get('escchm')->result_array();
        return $result;
    }
	
	public function master_channel_list($data) { //return array("status" => false, "message" => 'case1', "result" => array());
        $this->db->select('chmrefid,chmusmrefid,chmname,chmlogo,usmfirstname,usmmiddlename,usmlastname,usmmob,usmemailid,escchm.isdeleted,escchm.isactive');
        if (isset($data['length']) && isset($data['start'])) {
            $this->db->limit($data['length'], $data['start']);
        }
        if (isset($data['chmusmrefid']) && !empty($data['chmusmrefid'])) {
            $this->db->where('chmusmrefid', $data['chmusmrefid']);
        }
        if (isset($data['chmname']) && !empty($data['chmname'])) {
            $this->db->where('chmname', $data['chmname']);
        }
        if (!isset($data['all']) OR empty($data['all'])) {
            $this->db->where('escchm.isactive',1);
            $this->db->where('escchm.isdeleted',0);
        }        
        $this->db->join('escusm','escusm.usmrefid=escchm.chmusmrefid');
        $this->db->order_by('chmrefid', 'desc');
        $result = $this->db->get('escchm')->result_array();
        return $result;
    }
	
	public function add_channel($data) { //pre($data);
		$i =0;
		if(isset($data['logo_image']) && !empty($data['logo_image'])) {
			$my_data['chmlogo'] = $data['logo_image'];
		}else if (isset($_FILES['image']) && !empty($_FILES['image'])) {
			$filename          = rand(100000,999999).$_FILES['image']['name'];
			move_uploaded_file($_FILES['image']["tmp_name"],TARGET_PATH_TO_UPLOAD_CHANNEL_LOGO.$filename);
			$my_data['chmlogo'] = $filename;
		}
        foreach($data['chmusmrefids'] as $chmusmrefid){ $i++;
			$this->db->select('usmrefid,usmfirstname,usmlastname');
			$this->db->where('usmrefid', $chmusmrefid);
			$user = $this->db->get('escusm')->row_array();
			if(empty($user)){
				$response[] 	= array("s_no" => $i, "chmname" =>$data['chmname'],"usmrefid" =>$chmusmrefid,"usmname" =>"","status" => "fail", "message" => "User not exist");
				continue;
			}
            $exist = $this->master_channel_list(array("chmname" =>$data['chmname'],"chmusmrefid" =>$chmusmrefid,"all"=>1));
            if($exist){
                if($exist[0]['isdeleted']==0){
                    //$response[] 	= array("status" => "fail", "message" => "Channel already exist");
					$response[] 	= array("s_no" => $i, "chmname" =>$data['chmname'],"usmrefid" =>$chmusmrefid,"usmname" => $user['usmfirstname'].' '.$user['usmlastname'], "status" => "fail", "message" => "Channel already exist");
					continue;
                }else{                    
                    $my_data['isdeleted']     = 0;
                    $my_data['isactive']      = 1;
                    $this->db->where('chmrefid',$chmusmrefid);
                    $this->db->update('escchm', $my_data);
					$response[] 	= array("s_no" => $i, "chmname" =>$data['chmname'],"usmrefid" =>$chmusmrefid,"usmname" => $user['usmfirstname'].' '.$user['usmlastname'], "status" => "success", "message" => "Channel created successfully");
					continue;
                    //$response[] 			= array("status" => "success", "message" => "Channel created successfully");
                }            
            }else{
                $my_data['createdate']    = date('Y-m-d H:i:s');
                $my_data['chmusmrefid']   = $chmusmrefid;
                $my_data['chmname']       = $data['chmname'];
                $my_data['chmdesc']       = $data['chmdesc'];
                $my_data['isdeleted']     = 0;
                $my_data['isactive']      = 1;
                $this->db->insert('escchm', $my_data);
				$response[] 	= array("s_no" => $i, "chmname" =>$data['chmname'],"usmrefid" =>$chmusmrefid,"usmname" => $user['usmfirstname'].' '.$user['usmlastname'], "status" => "success", "message" => "Channel created successfully");
				continue;
                //$response[] = array("status" => "success", "message" => "Channel created successfully");
            }
        }        		
        return $response;
    }
	
    public function edit_channel($input){
		$check_duplicate =  $this->get_channel_by_name($input);
		if($check_duplicate=="TRUE"){
			return $response[] = array("status"=>"fail", "msg"=>"exit");
		}
		if(!empty($_FILES["chmlogo"]["type"])){
			if($_FILES["chmlogo"]["size"] > 10000){
				return  $response[] = array("status" => "fail","msg"=>"exceed");
			}
			$fileName = rand(100000,999999).$_FILES['chmlogo']['name'];
			$valid_extensions = array("jpeg", "jpg", "png");
			$temporary = explode(".", $_FILES["chmlogo"]["name"]);
			$file_extension = end($temporary);

			if((($_FILES["chmlogo"]["type"] == "image/png") || ($_FILES["chmlogo"]["type"] == "image/jpg") || ($_FILES["chmlogo"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
				$sourcePath = $_FILES['chmlogo']['tmp_name'];
				move_uploaded_file($sourcePath,TARGET_PATH_TO_UPLOAD_CHANNEL_LOGO.$fileName);
				$data['chmlogo'] = $fileName;

			}else{
				return  $response[] = array("status" => "fail","msg"=>"invalid");
			}
		} 
		if(isset($input['chmname']) && !empty($input['chmname'])){
			$data['chmname'] = $input['chmname'];
		}

		$data['modifieddate']   = date('Y-m-d H:i:s');
		$this->db->where('chmrefid',$input['chmrefid']);
		$result = $this->db->update('escchm',$data);
		if($result){
			return  $response[] = array("status" => "true","msg"=>"success"); 
		}
    }
	
    public function get_channel_by_name($input){
        $this->db->where('chmname', $input['chmname']);        
        $this->db->where('isactive', TRUE);
        $this->db->where('isdeleted', FALSE);
        $result = $this->db->get('escchm')->row();
        if($result){
            return  "TRUE";die;
        }  
        return  "FALSE";die;
    }
    public function get_channel_by_id($id){
        if (isset($id) && !empty($id)) {
            $this->db->where('chmrefid', $id);
           return  $result = $this->db->get('escchm')->row();
        }
    }
    
    
    public function get_wall_posting_users($data) { //die('jhgj');
        $this->db->select('escusm.usmrefid,escusm.usmfirstname,escusm.usmmiddlename,escusm.usmlastname');
        $this->db->join('escusm','escusm.usmrefid=escuep.uepusmrefid');
        $this->db->join('escoea','escoea.oearefid=escuep.uepoearefid');
        $this->db->where('escoea.oeaattributename','RIGHT_POST_EPC');
        $result = $this->db->get('escuep')->result_array();
		//echo $this->db->last_query();
        return $result;
    }
    
    
    
    public function delete_channels($data) { //die('working');
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach ($data['channel_ids'] as $cid) { //echo 1;
            $this->db->where('chmrefid', $cid);
            $exist = $this->db->get('escchm')->row();
            if ($exist) {
                $this->db->where('chmrefid', $cid);
                $this->db->update('escchm', array('isdeleted' => 1, 'isactive' => 0, 'modifieddate' => date('Y-m-d H:i:s')));
            }
        }
        // echo $this->db->last_query();
        return true;
    }
}
