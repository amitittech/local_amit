<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function users_list_by_role($data) { //pre($data);
        //$this->db->where('urmusmrefid', $user['ulmusmrefid']);
		$this->db->select("usmrefid,usmfirstname,usmlastname,usmmob,RIGHT(usmmob,10) as usm_mob");
		$this->db->join('escusm','escusm.usmrefid=escurm.urmusmrefid');
		//$this->db->join('escusm','escusm.usmrefid=escurm.urmusmrefid');
		$this->db->join('escrlm','escrlm.rlmrefid=escurm.urmrlmrefid');
		if(isset($data['role_name']) && !empty($data['role_name'])){
			$this->db->where('rlmrole', $data['role_name']);
		}
		//$this->db->group_by('escurm.urm');
		$users = $this->db->get('escurm')->result_array();
        return $users;
    }
}
