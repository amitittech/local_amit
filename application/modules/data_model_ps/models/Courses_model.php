<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Courses_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }  
    
    public function get_training_courses($data) {  
        if(isset($data['length']) && isset($data['start'])){
            $this->db->limit($data['length'],$data['start']);
        }
        $this->db->where('isfortraining',1);
        $this->db->order_by('rcmrefid','desc');
        $result = $this->db->get('currcm')->result_array();
        return $result;
    }
	
	public function get_course($data) {  
        if(isset($data['rcmrefid']) && !empty($data['rcmrefid'])){
            $this->db->where('rcmrefid',$data['rcmrefid']);
        }
        $result = $this->db->get('currcm')->row_array();
        return $result;
    }
	
	public function get_training_agency($data) {
        if(isset($data['trarefid']) && !empty($data['trarefid'])){
            $this->db->where('trarefid',$data['trarefid']);
        }
        $result = $this->db->get('curtra')->row_array();
        return $result;
    }
    
    public function training_n_courses_mapping($data) { $i=1;
        $inserting['assignedby'] = $data['login_id'];//$data['assignedby'];        
        if(isset($data['trainings']) && !empty($data['trainings']) && isset($data['courses']) && !empty($data['courses'])){ 
            //$trarefids = explode(',',$data['trarefids']);
            //$rcmrefids = explode(',',$data['rcmrefids']);
            foreach( $data['trainings'] as $trarefid){
                $inserting['trarefid'] = $trarefid;
				$training               = $this->get_training_agency(array("trarefid"=>$trarefid));
                foreach($data['courses'] as $rcmrefid){ //echo $i."<br>";
					$course           = $this->get_course(array("rcmrefid"=>$rcmrefid));
					if(empty($training) && empty($course)){
                        $response[] 	= array("s_no" => $i, "training_id" => $trarefid, "training_name" => "n/a","course_id" => $rcmrefid, "course_name" => "n/a",  "status" => "fail", "message" => "Training and course not exist");
                        continue;
                    }else if(empty($course)){
                        $response[] 	= array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'],"course_id" => $rcmrefid, "course_name" => "n/a",  "status" => "fail", "message" => "Course not exist");
                        continue;
                    }else if(empty($training)){
						$response[] 	= array("s_no" => $i, "training_id" => $trarefid, "training_name" => "n/a","course_id" => $rcmrefid, "course_name" => $course['rcmname'],  "status" => "fail", "message" => "Training not exist");
                        continue;
					}
					
					
                    $this->db->where('trarefid',$trarefid);
                    $this->db->where('rcmrefid',$rcmrefid);
                    $exist = $this->db->get('clt_tra_rcm')->row_array(); //echo $this->db->last_query()."-";
                    if($exist){
						if($exist['isdeleted'] == 0){
							$response[] 	= array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'],"course_id" => $rcmrefid, "course_name" => $course['rcmname'],  "status" => "fail", "message" => "Mapping already exist");
							continue;
						}else{
							//$updating['revokedon']= date('Y-m-d H:i:s');
							$updating['isdeleted']=0;
							$updating['isactive'] = 1;
							$this->db->where('trarcmrefid',$exist['trarcmrefid']);
							$this->db->update('clt_tra_rcm',$updating);
							$response[] 	= array("s_no" => $i, "training_id" => $trarefid, "training_name" => $training['traname'],"course_id" => $rcmrefid, "course_name" => $course['rcmname'],  "status" => "success", "message" => "Mapping done");
							continue;
						}
                    }else{
                        $inserting['assignedon']= date('Y-m-d H:i:s');
                        $inserting['rcmrefid'] = $rcmrefid;
						$inserting['isdeleted']=0;
						$inserting['isactive'] = 1;
                        $this->db->insert('clt_tra_rcm',$inserting);
						$response[] 	= array("s_no" => $i, "training_id" => $trarefid, "training_name" =>$training['traname'],"course_id" => $rcmrefid, "course_name" => $course['rcmname'],  "status" => "success", "message" => "Mapping done");
                        continue;
                    }         
					//echo $this->db->last_query()."<br>";					
                }
            }
        }
		//pre($response);
		return $response;
    }
    
    
    public function training_n_courses_mapping_list($data) {
        $select = "clt_tra_rcm.trarcmrefid as mapping_id,clt_tra_rcm.trarefid,clt_tra_rcm.rcmrefid,curtra.traname,currcm.rcmname,currcm.chaptername,currcm.rcmversionlabel";
        $this->db->select($select);
        if(isset($data['length']) && isset($data['start'])){
            $this->db->limit($data['length'],$data['start']);
        }
        $this->db->where('clt_tra_rcm.isactive',1);
        $this->db->where('clt_tra_rcm.isdeleted',0);                
        $this->db->where('curtra.isdeleted',0);  
        $this->db->join('curtra','curtra.trarefid=clt_tra_rcm.trarefid');
        $this->db->join('currcm','currcm.rcmrefid=clt_tra_rcm.rcmrefid'); 
        $this->db->order_by('clt_tra_rcm.trarcmrefid','desc');        
        $result = $this->db->get('clt_tra_rcm')->result_array();
        //echo $this->db->last_query();
        return $result;        
    }
    
    public function delete_courses_training_mapping($data) { // echo "here";        
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach($data['mapping_ids'] as $m_id){
            $this->db->where('trarcmrefid',$m_id);
            $exist = $this->db->get('clt_tra_rcm')->row();
            if($exist){
                $this->db->where('trarcmrefid',$m_id);
                $this->db->update('clt_tra_rcm',array('isdeleted'=>1,'isactive'=>0));
            }
        }
       // echo $this->db->last_query();
        return true;
    }
    
    

}
