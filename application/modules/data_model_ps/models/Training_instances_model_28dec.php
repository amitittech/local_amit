<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Training_instances_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

	public function get_training_instances4($data) { //pre($data);die;
        if (isset($data['trnrefid']) && !empty($data['trnrefid'])) {
            $this->db->where('clt_trn.trnrefid',$data['trnrefid']);
        }
        if (isset($data['trntrarefid']) && !empty($data['trntrarefid'])) {
            $this->db->where('clt_trn.trntrarefid',$data['trntrarefid']);
        }
        $this->db->where('clt_trn.isdeleted', 0);
        $this->db->where('clt_trn.isactive', 1);
        $trn = $this->db->get('clt_trn')->result_array();
        return $trn;
    }
	
	public function get_training_instances($data) {
		if(isset($data['tm_not_mapped']) && !empty($data['tm_not_mapped'])){
						$this->db->select("GROUP_CONCAT(clttirefid) as tirefids");
						$this->db->where('clt_ti_tm.isdeleted', 0);
						$this->db->where('clt_ti_tm.clttmrefid', $data['tm_not_mapped']);
			$already = 	$this->db->get('clt_ti_tm')->row_array();
			
			$this->db->WHERE_NOT_IN('clt_trn.trnrefid',$already['tirefids']);
		}	
		
        if (isset($data['trnrefid']) && !empty($data['trnrefid'])) {
            $this->db->where('clt_trn.trnrefid',$data['trnrefid']);
        }
        if (isset($data['trntrarefid']) && !empty($data['trntrarefid'])) {
            $this->db->where('clt_trn.trntrarefid',$data['trntrarefid']);
        }
		if (isset($data['not_created_by']) && !empty($data['not_created_by'])) {
            $this->db->where('clt_trn.createdby !=',$data['not_created_by']);
        }		
		if (isset($data['for_tra_of_this_tm']) && !empty($data['for_tra_of_this_tm'])) {
            $this->db->where('clt_tra_tm.tmrefid',$data['for_tra_of_this_tm']);
        }		
        $this->db->where('clt_trn.isdeleted', 0);
        $this->db->where('clt_trn.isactive', 1);
		$this->db->join('curtra','curtra.trarefid=clt_trn.trntrarefid');
		$this->db->join('clt_tra_tm','clt_tra_tm.trarefid=curtra.trarefid');
		//$this->db->group_by('clt_trn.trnrefid');
        $trn = $this->db->get('clt_trn')->result_array();
		echo count($trn);	echo $this->db->last_query();
        return $trn;
    }
	
	public function add_rights($data){
		$this->db->where('clttirefid', $data['trnrefid']);
		$this->db->where('clttmrefid', $data['usmrefid']);
		$already = $this->db->get('clt_ti_tm')->row_array();
		if (empty($already)) {
			$inserting                  = array();
			$inserting['clttirefid']   = $data['trnrefid'];
			$inserting['clttmrefid']   = $data['usmrefid'];
			$inserting['createddate']   = date('Y-m-d H:i:s');
			$inserting['createdby']     = $data['login_id'];
			$inserting['isdeleted']     = 0;
			$inserting['isactive']      = 1;
			$this->db->insert('clt_ti_tm', $inserting);
			return 1;
		}else if($already['isdeleted'] == 1){
			$this->db->where("clttitmrefid",$already['clttitmrefid']);
			$this->db->update('clt_ti_tm', array("isdeleted"=>0,"isactive"=>1,"modifieddate"=>$data['login_id'],"modifieddate"=>date('Y-m-d H:i:s')));
			return 2;
		}else{
			return 3;
		}
		return false;
        
    }
	
	public function list_of_mapping_rights($data) { //pre($data);die;
		$this->db->select('clttitmrefid,usmrefid,usmfirstname,usmlastname,usmmob,RIGHT(usmmob,10) as usm_mob,trnname,clt_ti_tm.createddate');
        $this->db->where('clt_ti_tm.isdeleted', 0);
        $this->db->where('clt_ti_tm.isactive', 1);
		$this->db->join('escusm','escusm.usmrefid=clt_ti_tm.clttmrefid');
        $this->db->join('clt_trn','clt_trn.trnrefid=clt_ti_tm.clttirefid');
		$this->db->order_by('clttitmrefid','desc');
        $trn = $this->db->get('clt_ti_tm')->result_array();
        return $trn;
    }
	
	public function delete_instance_rights_mapping($data) {//pre($data);
        //$m_ids = explode(',',$data['mapping_ids']);
        foreach ($data['mapping_ids'] as $m_id) {//echo $m_id;
            $this->db->where('clttitmrefid', $m_id);
            $exist = $this->db->get('clt_ti_tm')->row();
            if ($exist) {
                $this->db->where('clttitmrefid', $m_id);
                $this->db->update('clt_ti_tm', array('isdeleted' =>1, 'isactive' =>0, 'modifieddate' => date('Y-m-d H:i:s'), 'modifiedby' => $data['login_id']));
            }
        }
        // echo $this->db->last_query();
        return true;
    }
	
	
}
