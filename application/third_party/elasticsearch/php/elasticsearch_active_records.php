 <?php

    use Elasticsearch\ClientBuilder;
	require '../vendor/autoload.php';
	function common(){		
		return $client = ClientBuilder::create()->build();
	}
	
	function set_index($input){
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		$params['body'] 	= $input['body'];
		if(isset($input['id'])){
		$params['id'] 		= $input['id'];
		}		
		$client = common();
		return $response = $client->index($params);
	}
	
	function get_record($input){		
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		$params['id'] 		= $input['id'];
		$client = common();
		return $response = $client->get($params);
	}
	
	function search_record($input){		
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		if(isset($input['searching_col']) && isset($input['searching_val'])){			
			$params['body']['query']['match'][$input['searching_col']] = $input['searching_val'];
		}				
		$client = common();
		return $response = $client->search($params);
	}
	
	function search_record_with_and_operator($input){	//pre($input); die;	
		$params['index'] 	= $input['index'];
		$params['type'] 	= $input['type'];
		foreach($input['multi_search'] as $each){
			$params['body']['query']['bool']['must'][]['match'][$each['searching_col']] = $each['searching_val'];
		}		
		$client = common();
		//return pre($params); //die;	
		return $response = $client->search($params);
	}
	
	
	
	
	