<?php

require_once __DIR__ . '/vendor/autoload.php';
header('Content-Type: application/pdf'); 
header('Content-Description: inline; filename.pdf'); 

$mpdf = new \Mpdf\Mpdf();
$html = '<h1>My PDF!</h1>
		<table style="border: 2px solid black; width: 100%">
  <tr>
    <th>Firstname</th>
    <th>Lastname</th> 
    <th>Age</th>
  </tr>
  <tr>
    <td>Jill</td>
    <td>Smith</td>
    <td>50</td>
  </tr>
  <tr>
    <td>Eve</td>
    <td>Jackson</td>
    <td>94</td>
  </tr>
  <tr>
    <td>John</td>
    <td>Doe</td>
    <td>80</td>
  </tr>
</table>';
$mpdf->WriteHTML($html);
$mpdf->Output('file_name.pdf','D');
