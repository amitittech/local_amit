<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('pre')) {

    function pre($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }

}

function sorting_with_multi_array($a, $b) {
    return strcmp($a["discount"], $b["discount"]);
}

if (!function_exists('is_json')) {

    function is_json($string, $return_data = false) {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

}


if (!function_exists('send_sms')) {

    function send_sms($data) { //mobile and message. 
        //echo "working"; die; 
        return true;
        /*
          $mobile = $data['mobile'];
          $message = $data['message'];
          require_once(APPPATH . 'third_party/twilio-php/Services/Twilio.php');
          $account_sid = 'ACa7cc64d4263494e79217bf0b65427092';
          $auth_token = '262b0bf82523d785055239f9b7c5d252';
          $client = new Services_Twilio($account_sid, $auth_token); //echo  $mobile;
          $message = $client->account->messages->create(array(
          'To' => $mobile,
          'From' => "+18583338842",
          'Body' => $message));
          if ($message->sid != '') {
          return true;
          } else {
          return false;
          }
         */
    }

}

if (!function_exists('check_inputs')) {

    function check_inputs() {
        $json = file_get_contents('php://input');
        if (is_string($json) && is_array(json_decode($json, true))) { 
            return ((array) json_decode($json,true));
        } else if ($_REQUEST) {
            return $_REQUEST;
        }else if ($_POST) {
            return $_POST;
        } else {
            echo json_encode(array("status" => 'fail', "message" => "Invalid Input", "result" => array()));
            die;
        }
    }

}

if (!function_exists('create_log_file')) {

    function create_log_file($data) { //mobile and message.  
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . base_url($data['url']) . PHP_EOL;
        $log = $log . "Request " . json_encode($data['request']) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data['response'])) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/' . $data['url'] . '.txt', $log, FILE_APPEND);
    }

}

if (!function_exists('return_data')) {

    function return_data($status = false, $mesage = "", $result = array()) { //mobile and message.         
        echo json_encode(array("status" => $status, "message" => $mesage, "result" => $result));
        die;
    }

}


if (!function_exists('file_curl_contents')) {

    function file_curl_contents($data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $data['file_url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        unset($data['file_url']);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($document));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if(is_json($server_output)){
            $server_output               = json_decode($server_output, true);
        } 
        return $server_output;
    }

}

if (!function_exists('call_api')) {

    function call_api($method, $url, $data) { 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if(is_json($server_output)){
            $server_output       = json_decode($server_output, true);
        }        
        return $server_output;
    }

}


if (!function_exists('return_data')) {

    function return_data($status = false, $message = "", $data = array(), $error = array()) {
        // if(!$data){
        // 	$data = array("status"=>0);
        // }
        //pre($data);
        echo json_encode(array('status' => $status, 'message' => $message, 'result' => $data, 'error' => $error));
        die;
    }

}


if (!function_exists('post_check')) {

    function post_check() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            echo json_encode(array('status' => false, 'message' => "Invalid input parameter.Please use post method.", 'data' => array(), 'error' => array()));
            die;
        }
    }

}

if (!function_exists('milliseconds')) {

    function milliseconds() {
        $mt = explode(' ', microtime());
        return ((int) $mt[1]) * 1000 + ((int) round($mt[0] * 1000));
    }

}

